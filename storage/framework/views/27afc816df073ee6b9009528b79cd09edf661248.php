 <?php echo $__env->make('LaravelGoogleChart::includes.package_loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div id="chart_<?php echo $id; ?>"></div>
<script type="text/javascript">
    google.charts.setOnLoadCallback(function() {
    var data = google.visualization.arrayToDataTable(<?php echo json_encode($data); ?>);
    var options = <?php echo json_encode($options); ?>;
    var material = new google.visualization.PieChart(document.getElementById('chart_<?php echo $id; ?>'));
    material.draw(data, options);
    });
</script> 