<!-- Content Wrapper. Contains page content -->


<?php $__env->startSection('content'); ?>  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            <?php echo e($pageTitle); ?>

        </h1>
        <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                        <?php
                                if($id!=0){ ?>

                    <?php echo Html::decode( Html::link(route('admin.category.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn  btn-primary'])); ?>

                                <?php 
                            }
                         ?>
 <?php if($id==0): ?>
 <?php $id=''; ?>
                            <?php echo Html::decode(Html::link(route('admin.category.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])); ?> <?php else: ?>

<?php echo Html::decode(Html::link(route('admin.category.create',['id'=>$id]),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])); ?> 
                              <?php endif; ?>

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="40%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('name', trans('admin.NAME')));?></th>
                                    <?php if($id==0): ?><th width="10%"><?php echo e(trans('admin.SUBCATEGORY')); ?></th><?php endif; ?>
                                    
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('created_at', trans('admin.CREATED_AT')));?></th>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('updated_at', trans('admin.UPDATED_AT')));?></th>
                                    <th  width="15%" align="center"><?php echo e(trans('admin.ACTION')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$categorylist->isEmpty()): ?>
                                <?php foreach($categorylist as $category): ?>
                                <tr>
                                    <td><?php echo e(ucfirst($category->name)); ?></td>
                                    <?php if($id==0): ?> <td><?php echo Html::link(route('admin.category.index', ['id' =>$category->id]),$category->children->count(),['class'=>'']); ?> </td><?php endif; ?>
                                    
                                    <td><?php echo e(date_val($category->created_at,DATE_FORMATE )); ?></td>
                                    <td><?php echo e(date_val($category->updated_at,DATE_FORMATE )); ?></td>
                                    <td align="center">
                                        <?php if($category->status == 1): ?>
                                        <?php echo Html::decode(Html::link(route('admin.category.status_change',['id' => $category->id,'status'=>$category->status,'parent_id'=>$id]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])); ?>

                                        <?php else: ?>
                                        <?php echo Html::decode(Html::link(route('admin.category.status_change',['id' => $category->id,'status'=>$category->status,'parent_id'=>$id]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])); ?>

                                        <?php endif; ?>
                                        <?php echo Html::decode(Html::link(route('admin.category.edit',['id'=>$category->id,'parent_id'=>$id]),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])); ?>


                                    </td>
                                    <?php endforeach; ?>
                                    <?php else: ?>

                                <tr><td colspan="4"><div class="data_not_found"> Data Not Found </div></td></tr>


                                <?php endif; ?>

                            </tbody>
                        </table>
                        <?php echo $categorylist->appends(Input::all('page'))->render(); ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>
<!-- /.content-wrapper -->

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>