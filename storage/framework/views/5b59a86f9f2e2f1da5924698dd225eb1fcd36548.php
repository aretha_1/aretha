<!-- Content Wrapper. Contains page content -->


<?php $__env->startSection('content'); ?>  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            <?php echo e($pageTitle); ?>

        </h1>
        <?php echo $__env->make('includes.admin.breadcrumb', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                              <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                   
                         <li class="<?php echo e(($status=='all')?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>
                         'all'])); ?>" >All</a></li> 
                          <li class="<?php echo e(($status==Config::get('global.order_status.success'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.success')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.success'))); ?></a></li> 
                          <li class="<?php echo e(($status==Config::get('global.order_status.inprocess'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.inprocess')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.inprocess'))); ?></a></li> 
                           <li class="<?php echo e(($status==Config::get('global.order_status.shipped'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.shipped')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.shipped'))); ?></a></li>    
                          <li class="<?php echo e(($status==Config::get('global.order_status.delivered'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.delivered')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.delivered'))); ?></a></li>
                                <li class="<?php echo e(($status==Config::get('global.order_status.pending'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.pending')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.pending'))); ?></a></li>   
                        <li class="<?php echo e(($status==Config::get('global.order_status.cancel'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.cancel')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.cancel'))); ?></a></li>   
                        <li class="<?php echo e(($status==Config::get('global.order_status.failed'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.failed')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.failed'))); ?></a></li>  
                         <li class="<?php echo e(($status==Config::get('global.order_status.reject'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.reject')])); ?>" ><?php echo e(ucfirst(Config::get('global.order_status.reject'))); ?></a></li>




                    </ul>
                    </div>
                    <div class="row search-from">
                         <?php echo Form::model($search, ['route' => ['admin.orders.index', $status], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'get',  'files' => true  , 'id' => 'edit-settings']); ?>

                                <div class="col-md-12">
                                    <div class="col-md-4 form-group ">
                                      
                                         <?php echo Form::label(trans('admin.ORDER_ID'),null,['class'=>'col-sm-4 control-label']); ?>

                                        <div class="col-sm-8">
                                    
                                              <?php echo Form::text('order_id',Input::old('order_id'),['class'=>'form-control','placeholder'=>trans('admin.ORDER_ID')]); ?>

                                        </div>
                                    </div>
                                         <div class="col-md-4 form-group ">
                                           <?php echo Form::label(trans('admin.USER'),null,['class'=>'col-sm-4 control-label']); ?>

                                        <div class="col-sm-8">
                                           

                                            <?php echo Form::text('first_name',Input::old('first_name'),['class'=>'form-control','placeholder'=>trans('admin.FIRST_NAME')]); ?>

                                        </div>
                                    </div>      <div class="col-md-4 form-group ">
                                    <?php echo Form::label(trans('admin.PAYMENT_BY'),null,['class'=>'col-sm-4 control-label']); ?>

                                        <div class="col-sm-8">
                                             <?php
                                    $payment_by =   array('' => trans('admin.PLEASE_SELECT')) + array_map('ucfirst', Config::get('global.payment_by'));
                                    ?>
                                        <?php echo Form::select('payment_by', $payment_by, null, ['class' => 'form-control select2']); ?>

                                        </div>
                                    </div>

                                </div>
                                      <div class="col-md-12">
                                    <div class="col-md-4 form-group ">
                                      
                                         <?php echo Form::label(trans('admin.PAYMENT_STATUS'),null,['class'=>'col-sm-4 control-label']); ?>

                                        <div class="col-sm-8">
                                    
                                                             <?php
                                    $payment_status =   array('' => trans('admin.PLEASE_SELECT')) + array_map('ucfirst', Config::get('global.payment_status'));
                                    ?>
                                        <?php echo Form::select('payment_status', $payment_status, null, ['class' => 'form-control select2']); ?>

                                        </div>
                                    </div>
                                         <div class="col-md-4 form-group ">
                                           <?php echo Form::label(trans('admin.ORDER_FROM'),null,['class'=>'col-sm-4 control-label']); ?>

                                        <div class="col-sm-8">
                                           

                                            <?php echo Form::text('from',Input::old('from'),['id'=>'start_date','class'=>'form-control','placeholder'=>trans('admin.ORDER_FROM')]); ?>

                                        </div>
                                    </div>      <div class="col-md-4 form-group ">
                                    <?php echo Form::label(trans('admin.TO'),null,['class'=>'col-sm-4 control-label']); ?>

                                        <div class="col-sm-8">
                                              <?php echo Form::text('to',Input::old('to'),['id'=>'end_date','class'=>'form-control','placeholder'=>trans('admin.ORDER_TO')]); ?>

                                        </div>
                                        </div>
                                    </div>

                                </div>


                                            <!-- nav-tabs-custom -->
                    <div class="col-md-12">

                        <?php echo Html::link(route('admin.orders.index', $status), trans('admin.CANCEL'), ['id' => 'linkid','class' => 'btn btn-default pull-right']); ?>

                        

                        <?php echo Form::submit(trans('admin.SUBMIT'),['class' => 'btn btn-info pull-right']); ?>

                    </div>

            <?php echo Form::close(); ?>

                            </div>
                              <?php if($status==Config::get('global.order_status.inprocess') ): ?>
                                     <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                   
                         
                          <li class="<?php echo e(($shipped_by==Config::get('global.shipped_by.delhivery'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.inprocess'),'shipped_by'=>Config::get('global.shipped_by.delhivery')])); ?>" ><?php echo e(ucfirst(Config::get('global.shipped_by.delhivery'))); ?></a></li> 
               
                          <li class="<?php echo e(($shipped_by==Config::get('global.shipped_by.postal'))?'active':''); ?>"><a href="<?php echo e(route('admin.orders.index',['status'=>Config::get('global.order_status.inprocess'),'shipped_by'=>Config::get('global.shipped_by.postal')])); ?>" ><?php echo e(ucfirst(Config::get('global.shipped_by.postal'))); ?></a></li>
           




                    </ul>
                    </div>




                      <?php endif; ?>

                       <?php if($status==Config::get('global.order_status.inprocess') &&  $shipped_by==Config::get('global.shipped_by.delhivery')): ?>

                      <div class="pull-right">
                             <?php echo Form::button("<i class='fa  fa-download'></i> ".trans('admin.DOWNLOAD_MANIFEST'),['class' => 'btn btn-info pull-right download_manifest']); ?>

                        </div>


                      <?php endif; ?>
                      <?php echo Form::model($search, ['route' => ['admin.orders.download_manifest'], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post',  'files' => true  , 'id' => 'download_manifest']); ?>

                        <table id="example2" class="table table-bordered table-striped example2">
                            <thead>
                                <tr>
                                 <?php if($status==Config::get('global.order_status.inprocess') &&  $shipped_by==Config::get('global.shipped_by.delhivery')): ?>
                                    <th width="10%">  <?php echo Form::checkbox('selectAll',0,null, array('class' => 'minimal select_all ')); ?></th>
                                            <?php endif; ?>
                                    <th width="10%"><?php echo e(trans('admin.ORDER_ID')); ?></th>
                                    <th width="15%"><?php echo e(trans('admin.USER')); ?></th>
                                    <th width="10%"><?php echo e(trans('admin.PAYMENT_BY')); ?></th>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('payble_amount', trans('admin.PAYBLE_AMOUNT')));?></th>
                                    <?php if($status=='all'): ?>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('order_status', trans('admin.ORDER_STATUS')));?></th>
                                       <?php endif; ?>
                                        <?php if($status==Config::get('global.order_status.shipped')): ?>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('shipped_by', trans('admin.SHIPPED_BY')));?></th>
                                       <?php endif; ?>
                                    <th width="10%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('payment_status', trans('admin.PAYMENT_STATUS')));?></th>

                                    <th width="7%"><?php echo \Kyslik\ColumnSortable\Sortable::link(array ('created_at', trans('admin.CREATED_AT')));?></th>
                                    <th  width="15%" align="center"><?php echo e(trans('admin.ACTION')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!$orders_list->isEmpty()): ?>
                                <?php foreach($orders_list as $orders): ?>
                                <tr>
                                    <?php if($status==Config::get('global.order_status.inprocess') &&  $shipped_by==Config::get('global.shipped_by.delhivery')): ?>
                                    <td>  <?php echo Form::checkbox('order_ids[]', $orders->id,null, array('class' => 'minimal check_box_list')); ?></td>
                                                   <?php endif; ?>
                                    <td> <?php echo e($orders->order_id); ?> </td>
                                    <td> <?php echo e(ucwords($orders->user->first_name.' '.$orders->user->last_name)); ?> </td>
                                    <td> <?php echo e(ucfirst($orders->payment_by)); ?> </td>
                                    <td> <?php echo e(display_price($orders->payble_amount)); ?> </td>
                                     <?php if($status=='all'): ?>
                                      <td> <?php echo e(ucfirst($orders->order_status)); ?> </td>
                                      <?php endif; ?>   

                                      <?php if($status==Config::get('global.order_status.shipped')): ?>
                                      <td> <?php echo e(ucfirst($orders->shipped_by)); ?> </td>
                                      <?php endif; ?>
                                    <td> <?php echo e(ucfirst($orders->payment_status)); ?> </td>
                                    <td> <?php echo e(date_val($orders->created_at,DATE_FORMATE )); ?> </td>
                                    
                                    <td align="center">
                                        <?php if(($orders->order_status == Config::get('global.order_status.inprocess') || $orders->order_status == Config::get('global.order_status.shipped')) &&  $orders->shipped_by == Config::get('global.shipped_by.delhivery') ): ?>

                                        <?php echo Html::decode(Html::link(route('admin.orders.package_slip_print',['id' => $orders->id]),"<i class='fa  fa-print'></i>",['target'=>'_blank', 'class'=>'btn btn-default ','data-toggle'=>'tooltip','title'=>trans('admin.PRINT_PACKAGE_SLIP')])); ?>

                                        <?php echo Html::decode(Html::link(route('admin.orders.package_slip_download',['id' => $orders->id]),"<i class='fa  fa-file-pdf-o'></i>",['class'=>'btn btn-warning ','data-toggle'=>'tooltip','title'=>trans('admin.DOWNLOAD_PACKAGE_SLIP')])); ?>


                                           <?php endif; ?>
                                          <?php if($orders->order_status == Config::get('global.order_status.success')): ?>
                                        <?php echo Html::decode(Html::link(route('admin.orders.process',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.MANIFESTED'), "data-alert"=>trans('admin.MANIFESTED_ALERT')])); ?>

                               
                                        <?php echo Html::decode(Html::link(route('admin.orders.reject',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger reject_link','data-toggle'=>'tooltip','title'=>trans('admin.REJECT'), "data-alert"=>trans('admin.REJECT_ALERT')])); ?>

                                        <?php endif; ?>

                                        <?php echo Html::decode(Html::link(route('admin.orders.show',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-eye'></i>",['class'=>'btn btn-default ','data-toggle'=>'tooltip','title'=>trans('admin.VIEW')])); ?>



                                          <?php if($orders->order_status == Config::get('global.order_status.success')): ?>
                                        <?php echo Html::decode(Html::link(route('admin.orders.inprocess_by_postal',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-truck'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.PROCESS_BY_POSTAL'), "data-alert"=>trans('admin.SHIPPED_ALERT')])); ?>


                                               <?php endif; ?>

                                          <?php if($orders->order_status == Config::get('global.order_status.inprocess')): ?>


                                          <?php if($orders->shipped_by == Config::get('global.shipped_by.delhivery')): ?>
                                        <?php echo Html::decode(Html::link(route('admin.orders.shipped',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-truck'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.SHIPPED'), "data-alert"=>trans('admin.SHIPPED_ALERT')])); ?>

                                         <?php endif; ?>  
                                           <?php if($orders->shipped_by == Config::get('global.shipped_by.postal')): ?>

                                                   <?php echo Html::decode(Html::link(route('admin.orders.shipped_by_postal',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-truck'></i>",['class'=>'btn btn-success shipped_by_postal','data-toggle'=>'tooltip','title'=>trans('admin.SHIPPED_POSTAL')])); ?>

                                              <?php endif; ?> 


                                               <?php endif; ?>          


                                           <?php if($orders->order_status == Config::get('global.order_status.shipped')): ?>
                                                 <?php echo Html::decode(Html::link(route('admin.orders.delivered',['id' => $orders->id,'status'=>$status]),"<i class='fa   fa-check-circle'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.DELIVERED'), "data-alert"=>trans('admin.DELIVERED_ALERT')])); ?>


                                           <?php endif; ?>
                                         <?php if($orders->order_status == Config::get('global.order_status.delivered')): ?>
                                                 <?php echo Html::decode(Html::link(route('admin.orders.invoice',['id' => $orders->id,'status'=>$status]),"<i class='fa   fa-files-o'></i>",['class'=>'btn btn-success ','data-toggle'=>'tooltip','title'=>trans('admin.INVOICE')])); ?>


                                           <?php endif; ?>
                                             

                                        
                                    </td>
                                    <?php endforeach; ?>
                                    <?php else: ?>

                                <tr><td colspan="7"><div class="data_not_found"> Data Not Found </div></td></tr>


                                <?php endif; ?>

                            </tbody>
                        </table>
                        <?php echo Form::close(); ?>

                        <?php echo $orders_list->appends(Input::all('page'))->render(); ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<div id="myModal" class="modal fade">
<div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Shipped vai Postal or Courier </h4>
              </div>
              <div class="modal-body">
                
                <div class="row">
 <?php echo Form::open(['id'=>'saveForm']); ?>  
                    <div class="col-md-12">
                     <div class="row">

                        <div class="col-md-12 form-group ">
                                <?php echo Form::label(trans('admin.AWB_NUMBER'),null,['class'=>'required_label']); ?>

                                <?php echo Form::text('awb_number',null,['class'=>'form-control','placeholder'=>trans('admin.AWB_NUMBER')]); ?>

                                <div class="error awb_number"></div>
                            </div><!-- /.form-group -->

                        
                                   
                      </div><!-- /.row -->   
                                    <div class="row">                                
                             <div class="col-md-12 form-group ">
                                <?php echo Form::label(trans('admin.POST_BY'),null,['class'=>'required_label']); ?>

                                <?php echo Form::text('post_by',null,['class'=>'form-control','placeholder'=>trans('admin.POST_BY')]); ?>

                                <div class="error post_by"></div>
                            </div><!-- /.form-group -->
                                            
                         </div><!-- /.row -->                               
                         </div><!-- /.row -->   
                             <?php echo Form::close(); ?>                            

                </div><!-- /.row -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary submitbtn">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
          var startDate = new Date();
        $("#start_date").datepicker({
            
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#end_date').datepicker('setStartDate', startDate);
        });
        ;


        $("#end_date").datepicker({
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#start_date').datepicker('setEndDate', FromEndDate);
        });
        
       $(document).on('click', '.shipped_by_postal', function(event){
 event.preventDefault()
    $this=  $(this);

        $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

            return false;
}); 
    
       $(document).on('click', '.download_manifest', function(event){ 
            if($('.check_box_list').filter(':checked').length ==0){

              bootbox.alert("Please select atleast one order");
            }else{
              $('#download_manifest').submit();

            }


        });
       $('.select_all').on('ifChecked', function(event){
       
            $('.check_box_list').iCheck('check');
             $('.check_box_list').iCheck('update');
         });  

         $('.select_all').on('ifUnchecked', function(event){
          $('.check_box_list').iCheck('uncheck');
           $('.check_box_list').iCheck('update');
         });


    



        $(document).on('click', '.submitbtn', function(event){
          event.preventDefault()
    
            $.ajax({
      url:$('.shipped_by_postal').attr('href'),
      method:'POST',
      data:$("#saveForm").serialize(),
      dataType:'json',
      success:function(data) {
        
        if(data.status_code==1){
           
          location.reload();

        }else{
          
            if( typeof data.errors.awb_number !='undefined' && data.errors.awb_number !=''){
                $('.awb_number').html(data.errors.awb_number['0']);
              
            }else{
                $('.awb_number').html('');

            }


            if( typeof data.errors.post_by !='undefined' && data.errors.post_by !=''){
                $('.post_by').html(data.errors.post_by['0']);
              
            }else{
                $('.post_by').html('');

            }
        }
      }
   });
    });

    });
    </script>
<?php $__env->stopSection(); ?>
<!-- /.content-wrapper -->

<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>