<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    


     <?php 
        $rote_name =  Request::route()->getName();
     ?>

    
       <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
   <?php foreach($admin_menus as $admin_menu): ?>
                      
        <?php if(empty($admin_menu['child_list'])): ?>


            <li class=" treeview  <?php echo e(($rote_name==$admin_menu['route'])?'active':''); ?>">
        <a href="<?php echo e(($admin_menu['route']!='')?route($admin_menu['route']):'#'); ?>">
          <i class="fa <?php echo e($admin_menu['icon']); ?>"></i> <span><?php echo e($admin_menu['name']); ?></span>
        </a>
      </li>
<?php else: ?>
  <?php 
        $active_class = '';
         foreach ($admin_menu['child_list'] as $child_list){
            if($rote_name==$child_list['route']){
                $active_class = 'active';
                break;
            }

         }

  ?>
  

      <li class="treeview <?php echo e($active_class); ?>">
        <a href="#">
          <i class="fa <?php echo e($admin_menu['icon']); ?>"></i>
         <span><?php echo e($admin_menu['name']); ?></span>
          <i class="fa fa-angle-left pull-right"></i>
              <ul class="treeview-menu">
                  
                   <?php foreach($admin_menu['child_list'] as $child_list): ?>
          
  <li class=" <?php echo e(($rote_name==$child_list['route'])?'active':''); ?> "><a href="<?php echo e(($child_list['route']!='')?route($child_list['route']):'#'); ?>"">
            <i class="fa <?php echo e($child_list['icon']); ?>"></i><?php echo e($child_list['name']); ?> </a></li>
                    <?php endforeach; ?> 


    
                
            </ul>
        </a>
      </li>                                
          
<?php endif; ?>

      <?php endforeach; ?> 
     </ul>
  </section>
  <!-- /.sidebar -->
</aside>
