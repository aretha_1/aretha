/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';
	
	var result	=	CONFIG_ALLOWED_IMAGE_TYPE.split(',').join('|'); 

	var regex 	= 	new RegExp(result);
	
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: WEBSITE_ADMIN_URL+'upload-product-images/'+id,
		async : true,
		acceptFileTypes: regex,
        maxFileSize: UPLOAD_FILE_SIZE_IMAGE, // 5 MB  
        limitConcurrentUploads: 1,
        maxNumberOfFiles: MAX_FILE_COUNT_UPLOAD,
		limitMultiFileUploads: MAX_FILE_COUNT_UPLOAD,
		autoUpload:true
    }).bind('fileuploadadd', function (e) {

        $("#submitBusiness").show();
    })
	.bind('fileuploadstart', function (e) {
		//blockUI();
		
	})
    .bind('fileuploadstop', function (e) {
		//unblockUI(); 
	});

	
    //Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
	
  
});
