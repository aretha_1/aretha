webpackJsonp([0],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var platform_browser_dynamic_1 = __webpack_require__(1);
	var core_1 = __webpack_require__(3);
	var environment_1 = __webpack_require__(23);
	var app_module_1 = __webpack_require__(24);
	if (environment_1.environment.production) {
	    core_1.enableProdMode();
	}
	var platform = platform_browser_dynamic_1.platformBrowserDynamic();
	platform.bootstrapModule(app_module_1.AppModule);


/***/ },

/***/ 23:
/***/ function(module, exports) {

	"use strict";
	exports.environment = {
	    production: false
	};


/***/ },

/***/ 24:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var platform_browser_1 = __webpack_require__(21);
	var forms_1 = __webpack_require__(27);
	var http_1 = __webpack_require__(31);
	var app_component_1 = __webpack_require__(32);
	var app_routing_module_1 = __webpack_require__(360);
	var auth_guard_1 = __webpack_require__(414);
	var authentication_service_1 = __webpack_require__(369);
	var user_service_1 = __webpack_require__(378);
	var product_service_1 = __webpack_require__(373);
	var pager_service_1 = __webpack_require__(374);
	var cart_service_1 = __webpack_require__(370);
	var header_component_1 = __webpack_require__(417);
	var footer_component_1 = __webpack_require__(420);
	var home_component_1 = __webpack_require__(362);
	var page_component_1 = __webpack_require__(365);
	var faq_component_1 = __webpack_require__(366);
	var login_component_1 = __webpack_require__(368);
	var product_component_1 = __webpack_require__(372);
	var search_component_1 = __webpack_require__(415);
	var productdetail_component_1 = __webpack_require__(377);
	var notfound_component_1 = __webpack_require__(380);
	var contactus_component_1 = __webpack_require__(382);
	var resetpassword_component_1 = __webpack_require__(384);
	var myaccount_component_1 = __webpack_require__(386);
	var myprofile_component_1 = __webpack_require__(388);
	var myaddress_component_1 = __webpack_require__(390);
	var myorder_component_1 = __webpack_require__(392);
	var mywallet_component_1 = __webpack_require__(394);
	var changepassword_component_1 = __webpack_require__(396);
	var wishlist_component_1 = __webpack_require__(398);
	var cart_component_1 = __webpack_require__(400);
	var checkout_component_1 = __webpack_require__(402);
	var checkoutaddress_component_1 = __webpack_require__(404);
	var ordersummary_component_1 = __webpack_require__(406);
	var orderdetail_component_1 = __webpack_require__(408);
	var payment_component_1 = __webpack_require__(410);
	var tahnk_you_component_1 = __webpack_require__(412);
	var ng2_social_share_1 = __webpack_require__(422);
	var pipe_1 = __webpack_require__(428);
	var equal_validator_directive_1 = __webpack_require__(429);
	var myoption_1 = __webpack_require__(431);
	var AppModule = (function () {
	    function AppModule() {
	    }
	    return AppModule;
	}());
	AppModule = __decorate([
	    core_1.NgModule({
	        imports: [platform_browser_1.BrowserModule, app_routing_module_1.AppRoutingModule, forms_1.FormsModule, http_1.HttpModule],
	        declarations: [
	            app_component_1.AppComponent,
	            header_component_1.HeaderComponent,
	            footer_component_1.FooterComponent,
	            home_component_1.HomeComponent,
	            page_component_1.PageComponent,
	            pipe_1.ValuesPipe,
	            pipe_1.KeysPipe,
	            pipe_1.FlattenPipe,
	            notfound_component_1.NotFoundComponent,
	            faq_component_1.FaqComponent,
	            login_component_1.LoginComponent,
	            product_component_1.ProductComponent,
	            contactus_component_1.ContactUsComponent,
	            resetpassword_component_1.ResetPasswordComponent,
	            productdetail_component_1.ProductDetailComponent,
	            pipe_1.CapitalizePipe,
	            pipe_1.UcupperPipe,
	            myaccount_component_1.MyAccountComponent,
	            myaddress_component_1.MyAddressComponent,
	            myprofile_component_1.MyProfileComponent,
	            myorder_component_1.MyOrderComponent,
	            mywallet_component_1.MyWalletComponent,
	            changepassword_component_1.ChangePasswordComponent,
	            cart_component_1.CartComponent,
	            wishlist_component_1.WishlistComponent,
	            checkout_component_1.CheckoutComponent,
	            checkoutaddress_component_1.CheckoutAddressComponent,
	            ordersummary_component_1.OrderSummaryComponent,
	            payment_component_1.PaymentComponent,
	            ng2_social_share_1.CeiboShare,
	            tahnk_you_component_1.TahnkYouComponent,
	            orderdetail_component_1.OrderDetailComponent,
	            search_component_1.SearchComponent,
	            pipe_1.SanitizeHtmlPipe, pipe_1.SafeUrlPipe,
	            equal_validator_directive_1.EqualValidator
	        ],
	        providers: [
	            auth_guard_1.AuthGuard,
	            authentication_service_1.AuthenticationService,
	            user_service_1.UserService,
	            product_service_1.ProductService,
	            cart_service_1.CartService,
	            pager_service_1.PagerService, { provide: http_1.RequestOptions, useClass: myoption_1.MyOptions }
	        ],
	        bootstrap: [app_component_1.AppComponent]
	    }),
	    __metadata("design:paramtypes", [])
	], AppModule);
	exports.AppModule = AppModule;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 25:
/***/ function(module, exports) {

	function __decorate(decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	
	if (typeof module !== 'undefined' && module.exports) {
	    exports = module.exports = __decorate;
	}
	
	exports.__decorate = __decorate;

/***/ },

/***/ 26:
/***/ function(module, exports) {

	function __metadata(k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	
	if (typeof module !== 'undefined' && module.exports) {
	    exports = module.exports = __metadata;
	}
	
	exports.__metadata = __metadata;

/***/ },

/***/ 32:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var page_service_1 = __webpack_require__(33);
	var AppComponent = (function () {
	    function AppComponent(pageService) {
	        this.pageService = pageService;
	        this.settings = [];
	    }
	    AppComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.pageService.getSettings().subscribe(function (settings) {
	            _this.settings = settings;
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    AppComponent.prototype.ngAfterViewInit = function () {
	        jQuery(document).ready(function () {
	            jQuery('.button-collapse').sideNav();
	            jQuery('.modal').modal();
	            setTimeout(function () {
	                jQuery('.dropdown-button').dropdown({
	                    inDuration: 300,
	                    outDuration: 225,
	                    constrain_width: false,
	                    hover: true,
	                    gutter: 0,
	                    belowOrigin: false,
	                    alignment: 'left'
	                });
	            }, 2000);
	            jQuery(".mobile_search").click(function () {
	                jQuery("#search").toggle(200);
	            });
	            jQuery("button").click(function () {
	                jQuery("input").toggle(200);
	            });
	            jQuery("#nav-mobile .child-menu ul li a").click(function () {
	                jQuery('.button-collapse').sideNav('hide');
	            });
	        });
	    };
	    return AppComponent;
	}());
	AppComponent = __decorate([
	    core_1.Component({
	        selector: 'my-app',
	        template: "\n        <my-header></my-header>\n        <router-outlet></router-outlet>\n        <my-footer></my-footer>\n    "
	    }),
	    __metadata("design:paramtypes", [page_service_1.PageService])
	], AppComponent);
	exports.AppComponent = AppComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 33:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var http_1 = __webpack_require__(31);
	var Rx_1 = __webpack_require__(34);
	__webpack_require__(227);
	__webpack_require__(150);
	var PageService = (function () {
	    function PageService(http) {
	        this.http = http;
	        this.apiUrl = 'api/get-all-cms';
	        this.pageUrl = 'api/get-cms-page-detail/';
	        this.menuUrl = 'api/get-all-category-for-menu';
	        this.settingsUrl = 'api/get-all-settings';
	        this.sliderUrl = 'api/get-slider';
	        this.blockUrl = 'api/get-all-block';
	        this.faqUrl = 'api/get-faq';
	        this.check_token_url = 'api/check-socail-login/';
	    }
	    PageService.prototype.getPages = function () {
	        var headers = new http_1.Headers();
	        headers.append('Content-Type', 'application/json');
	        headers.append('X-Requested-With', 'XMLHttpRequest');
	        return this.http.get(this.apiUrl, { headers: headers })
	            .map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getPageDetail = function (slug) {
	        return this.http.get("" + this.pageUrl + slug)
	            .map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getMenus = function () {
	        return this.http.get(this.menuUrl).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getSettings = function () {
	        return this.http.get(this.settingsUrl).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getSliders = function () {
	        return this.http.get(this.sliderUrl).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getBlocks = function () {
	        return this.http.get(this.blockUrl).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getFaq = function () {
	        return this.http.get(this.faqUrl).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.contactUs = function (modelData) {
	        return this.http.post('api/contact-us', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    PageService.prototype.checkSocailLogin = function (token) {
	        return this.http.get("" + this.check_token_url + token)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    PageService.prototype.getOrder = function (uId, page) {
	        var url = 'api/get-order/' + uId + '?page=' + page;
	        return this.http.get(url).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getOrderDetail = function (uId, order_id) {
	        var url = 'api/get-order-detail/' + uId + '/' + order_id;
	        return this.http.get(url).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.cancelOrder = function (uId, order_id, modelData) {
	        var url = 'api/order-cancel/' + order_id + '/' + uId;
	        return this.http.post(url, { data: modelData }).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.getWallet = function (uId, page) {
	        var url = 'api/get-wallet/' + uId + '?page=' + page;
	        return this.http.get(url).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.checkMobileNumberVerified = function (user_id) {
	        return this.http.get('api/check-address-number/' + user_id).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.resendOtpCodNumber = function (user_id) {
	        return this.http.get('api/resend-otp-cod-mobile-number/' + user_id).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.mobileNumberVerified = function (data, user_id) {
	        return this.http.post('api/cod-mobile-verify/' + user_id, { data: data }).map(function (res) { return res.json(); })
	            .catch(this.handleServerError);
	    };
	    PageService.prototype.handleServerError = function (error) {
	        return Rx_1.Observable.throw(error || 'Server error');
	    };
	    PageService.prototype.newslatterSubscription = function (modelData) {
	        return this.http.post('api/newslatter', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    return PageService;
	}());
	PageService = __decorate([
	    core_1.Injectable(),
	    __metadata("design:paramtypes", [http_1.Http])
	], PageService);
	exports.PageService = PageService;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 360:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var http_1 = __webpack_require__(31);
	var router_1 = __webpack_require__(361);
	var home_component_1 = __webpack_require__(362);
	var page_component_1 = __webpack_require__(365);
	var faq_component_1 = __webpack_require__(366);
	var login_component_1 = __webpack_require__(368);
	var product_component_1 = __webpack_require__(372);
	var productdetail_component_1 = __webpack_require__(377);
	var notfound_component_1 = __webpack_require__(380);
	var contactus_component_1 = __webpack_require__(382);
	var resetpassword_component_1 = __webpack_require__(384);
	var myaccount_component_1 = __webpack_require__(386);
	var myprofile_component_1 = __webpack_require__(388);
	var myaddress_component_1 = __webpack_require__(390);
	var myorder_component_1 = __webpack_require__(392);
	var mywallet_component_1 = __webpack_require__(394);
	var changepassword_component_1 = __webpack_require__(396);
	var wishlist_component_1 = __webpack_require__(398);
	var cart_component_1 = __webpack_require__(400);
	var checkout_component_1 = __webpack_require__(402);
	var checkoutaddress_component_1 = __webpack_require__(404);
	var ordersummary_component_1 = __webpack_require__(406);
	var orderdetail_component_1 = __webpack_require__(408);
	var payment_component_1 = __webpack_require__(410);
	var tahnk_you_component_1 = __webpack_require__(412);
	var auth_guard_1 = __webpack_require__(414);
	var search_component_1 = __webpack_require__(415);
	var page_service_1 = __webpack_require__(33);
	var appRoutes = [
	    { path: '', component: home_component_1.HomeComponent },
	    { path: 'page/:slug', component: page_component_1.PageComponent },
	    { path: 'login', component: login_component_1.LoginComponent },
	    { path: 'faq', component: faq_component_1.FaqComponent },
	    { path: 'thanks-you', component: tahnk_you_component_1.TahnkYouComponent },
	    { path: 'category/:slug', component: product_component_1.ProductComponent },
	    { path: 'product/:slug', component: productdetail_component_1.ProductDetailComponent },
	    { path: 'contact-us', component: contactus_component_1.ContactUsComponent },
	    { path: 'search/:search_text', component: search_component_1.SearchComponent },
	    { path: 'wishlist', component: wishlist_component_1.WishlistComponent, canActivate: [auth_guard_1.AuthGuard] },
	    { path: 'reset-password/:token', component: resetpassword_component_1.ResetPasswordComponent },
	    { path: 'cart', component: cart_component_1.CartComponent, canActivate: [auth_guard_1.AuthGuard] },
	    { path: 'myaccount', component: myaccount_component_1.MyAccountComponent, canActivate: [auth_guard_1.AuthGuard],
	        children: [
	            { path: '', redirectTo: 'profile', pathMatch: 'full' },
	            { path: 'profile', component: myprofile_component_1.MyProfileComponent },
	            { path: 'address', component: myaddress_component_1.MyAddressComponent },
	            { path: 'order', component: myorder_component_1.MyOrderComponent },
	            { path: 'order-detail/:order_id', component: orderdetail_component_1.OrderDetailComponent },
	            { path: 'wallet', component: mywallet_component_1.MyWalletComponent },
	            { path: 'change-password', component: changepassword_component_1.ChangePasswordComponent },
	        ]
	    },
	    { path: 'checkout', component: checkout_component_1.CheckoutComponent, canActivate: [auth_guard_1.AuthGuard],
	        children: [
	            { path: '', redirectTo: 'address', pathMatch: 'full' },
	            { path: 'address', component: checkoutaddress_component_1.CheckoutAddressComponent },
	            { path: 'summary', component: ordersummary_component_1.OrderSummaryComponent },
	            { path: 'payment', component: payment_component_1.PaymentComponent },
	        ]
	    },
	    { path: '**', component: notfound_component_1.NotFoundComponent }
	];
	var AppRoutingModule = (function () {
	    function AppRoutingModule() {
	    }
	    return AppRoutingModule;
	}());
	AppRoutingModule = __decorate([
	    core_1.NgModule({
	        imports: [
	            router_1.RouterModule.forRoot(appRoutes), http_1.HttpModule, http_1.JsonpModule
	        ],
	        providers: [
	            page_service_1.PageService
	        ],
	        exports: [
	            router_1.RouterModule
	        ]
	    }),
	    __metadata("design:paramtypes", [])
	], AppRoutingModule);
	exports.AppRoutingModule = AppRoutingModule;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 362:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var page_service_1 = __webpack_require__(33);
	var global_1 = __webpack_require__(363);
	var HomeComponent = (function () {
	    function HomeComponent(pageService) {
	        this.pageService = pageService;
	        this.sliders = [];
	        this.blocks = [];
	        this.block_1 = [];
	        this.block_2 = [];
	        this.block_3 = [];
	        this.block_4 = [];
	        this.block_5 = [];
	        this.block_6 = [];
	        this.block_7 = [];
	        this.block_8 = [];
	        this.newslatter = {};
	    }
	    HomeComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        jQuery(document).ready(function () {
	            jQuery('.slider').slider({ full_width: true });
	        });
	        jQuery(window).load(function () {
	            jQuery('.flexslider').flexslider({
	                animation: "slide",
	                start: function (slider) {
	                    jQuery('body').removeClass('loading');
	                }
	            });
	        });
	        this.pageService.getBlocks().subscribe(function (block) {
	            _this.blocks = block;
	            _this.block_1 = _this.blocks['position-1'];
	            _this.block_2 = _this.blocks['position-2'];
	            _this.block_3 = _this.blocks['position-3'];
	            _this.block_4 = _this.blocks['position-4'];
	            _this.block_5 = _this.blocks['position-5'];
	            _this.block_6 = _this.blocks['position-6'];
	            _this.block_7 = _this.blocks['position-7'];
	            _this.block_8 = _this.blocks['position-8'];
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    HomeComponent.prototype.newslattersaved = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.pageService.newslatterSubscription(this.newslatter)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.newslatter = {};
	                jQuery('#newsletter')[0].reset();
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    return HomeComponent;
	}());
	HomeComponent = __decorate([
	    core_1.Component({
	        selector: 'my-home',
	        template: __webpack_require__(364),
	    }),
	    __metadata("design:paramtypes", [page_service_1.PageService])
	], HomeComponent);
	exports.HomeComponent = HomeComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 363:
/***/ function(module, exports) {

	"use strict";
	function toast(massage, type) {
	    var type_class = '';
	    if (type == 'success') {
	        type_class = "tost-success";
	    }
	    if (type == 'error') {
	        type_class = "tost-error";
	    }
	    Materialize.toast(massage, 5000, type_class);
	}
	exports.toast = toast;
	function blockUI() {
	    jQuery(document).ready(function () {
	        jQuery.blockUI({
	            fadeIn: 0,
	            fadeOut: 0,
	            css: {
	                padding: 0,
	                margin: 0,
	                left: '34%',
	                textAlign: 'center',
	                color: '#000',
	                border: 'none',
	                backgroundColor: 'none',
	                cursor: 'wait',
	            },
	            overlayCSS: {
	                backgroundColor: '#8E9093',
	                opacity: .50
	            },
	            message: '<div class="preloader-wrapper small active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left">        <div class="circle"></div> </div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
	            baseZ: 999999999
	        });
	    });
	}
	exports.blockUI = blockUI;
	function unblockUI() {
	    jQuery(document).ready(function () {
	        jQuery.unblockUI();
	    });
	}
	exports.unblockUI = unblockUI;
	function modalBlockUI() {
	    jQuery(document).ready(function () {
	        jQuery('div.div-loader').block({
	            message: '<div class="preloader-wrapper small active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left">        <div class="circle"></div> </div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
	            css: { border: 'none', backgroundColor: '#ccc' },
	            overlayCSS: {
	                backgroundColor: '#ccc',
	                opacity: 0.99
	            },
	            baseZ: 999999999
	        });
	    });
	}
	exports.modalBlockUI = modalBlockUI;
	function modalUnBlockUI() {
	    jQuery(document).ready(function () {
	        jQuery('div.div-loader').unblock();
	    });
	}
	exports.modalUnBlockUI = modalUnBlockUI;


/***/ },

/***/ 364:
/***/ function(module, exports) {

	module.exports = "<!--<section class=\"main-slider\">\n    <div class=\"slider\">\n        <ul class=\"slides\">\n            <li *ngFor=\"let slider of sliders\">\n                <img class=\"img-full\" [src]=\"slider['image']\">  random image \n                <div class=\"caption left-align\">\n                    <h3>{{slider['title']}}</h3>\n                    <span class=\"line\"></span>\n                    <h5 class=\"light grey-text text-lighten-3\">{{slider['description']}}</h5>\n                    <a class=\"waves-effect fabivo-btn-2 btn\" [routerLink]=\"['/']\">Shop now</a>\n                </div>\n            </li>\n\n        </ul>\n    </div>\n\n\n</section>\n<section class=\"category\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12 m7 pad-rg-5\">\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_1.slug]\">\n                        <img class=\"img-full\" src=\"{{block_1.image}}\"/>\n                    </a>\n                    <div class=\"category-bx-on\">\n                        <h3>{{block_1.title}}</h3>\n                        <h4> {{block_1.description}}</h4>\n                        <a [routerLink]=\"['./category',block_1.slug]\" class=\"waves-effect fabivo-btn-2 btn\">Shop now</a> \n                    </div>\n                </div>\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_2.slug]\">\n                        <img class=\"img-full\" src=\"{{block_2.image}}\"/> \n                    </a>\n                    <div class=\"category-bx-on on-2\">\n                        <h3>{{block_2.title}}</h3>\n                        <h4> {{block_2.description}}</h4>\n                        <a [routerLink]=\"['./category',block_2.slug]\" class=\"waves-effect fabivo-btn-2 btn\">Shop now</a> \n                    </div>\n                </div> \n            </div>\n            <div class=\"col s12 m5\">\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_3.slug]\">\n                        <img class=\"img-full\" src=\"{{block_3.image}}\"/>\n                    </a>\n                    <div class=\"category-bx-on on-3\">\n                        <h3>{{block_3.title}}</h3>\n                        <h4>{{block_3.description}}</h4>\n                        <a [routerLink]=\"['./category',block_3.slug]\" class=\"waves-effect fabivo-btn-2 btn\">Shop now</a> \n                    </div>\n                </div>\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_4.slug]\">\n                        <img class=\"img-full\" src=\"{{block_4.image}}\"/>\n                    </a>\n                    <div class=\"category-bx-on\">\n                        <h3>{{block_4.title}}</h3>\n                        <h4>{{block_4.description}}</h4>\n                        <a [routerLink]=\"['./category',block_4.slug]\" class=\"waves-effect fabivo-btn-2 btn\">Shop now</a> \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n<section class=\"advertisement\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m12 s12\">\n                <div class=\"advertisement-bx\">\n                    <img class=\"responsive-img\" src=\"public/img/banner.png\"/>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n<section class=\"category\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m6 s12 pad-rg-8\">\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_5.slug]\">\n                        <img class=\"img-full\" src=\"{{block_5.image}}\"/>\n                    </a>  \n                    <div class=\"category-bx-on on-4\">\n                        <a [routerLink]=\"['./category',block_5.slug]\" class=\"waves-effect fabivo-btn-2 btn\">Shop now</a> \n                        <h3>{{block_5.title}}</h3>\n                    </div>\n                </div>\n\n            </div>\n            <div class=\"col m6 s12 pad-lf-8\">\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_6.slug]\">\n                        <img class=\"img-full\" src=\"{{block_6.image}}\"/>\n                    </a>\n                    <div class=\"category-bx-on on-4\">\n                        <a [routerLink]=\"['./category',block_6.slug]\" class=\"waves-effect fabivo-btn-2 btn\">Shop now</a> \n                        <h3>{{block_6.title}}</h3>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section> \n<section class=\"sign-section\">\n    <div class=\"overlay\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div col m12>\n                    <div class=\"sign-contant\">\n                        <h1>For All Collection</h1>\n                        <h2>SAVE 70% OFF SALE</h2>\n                        <h4>Be the first to hear about new styles and offers and see how you’ve helped.</h4>\n                    </div>\n                    <div class=\"sign-bx\">\n                    <form  name=\"newsletter\" id=\"newsletter\" (ngSubmit)=\"newsletter.form.valid && newslattersaved()\"  #newsletter=\"ngForm\" novalidate>\n                        <div class=\"input-field\">          \n                            <input id=\"search\" placeholder=\"Your Email Address\" required=\"\" type=\"search\" name='email_news' [(ngModel)]=\"newslatter.email_news\" #email_news=\"ngModel\" required  pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\">\n                            <i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>\n                            <button class=\"waves-effect fabivo-btn btn\" type=\"submit\">Sign Up</button>\n                        </div>\n                         <div *ngIf=\"newsletter.submitted && email_news.errors && email_news.errors.required\" class=\"help-block err-msg\">The email field is required.</div>\n                            <div *ngIf=\"newsletter.submitted && email_news.errors && email_news.errors.pattern\" class=\"help-block err-msg\"> Email is invalid.</div>\n                           </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n<section class=\"category\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m6 s12 pad-rg-8\">\n                <div class=\"category-bx store-bx-img\">  \n                    <img class=\"img-full\" src=\"public/img/store-banner.png\"/>\n                    <div class=\"store-bx\">\n                        <h4>Download Our Mobile App</h4>\n                        <a class=\"aap-store waves-effect waves-light\" href=\"#\"></a>\n                        <a class=\"play-store waves-effect waves-light\" href=\"#\"></a>               \n                     </div>\n                </div>\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_7.slug]\">\n                        <img class=\"img-full\" src=\"{{block_7.image}}\"/>\n                    </a>\n                </div>\n            </div>\n            <div class=\"col m6 s12 pad-lf-8\">\n                <div class=\"category-bx\">\n                    <a [routerLink]=\"['./category',block_8.slug]\">\n                        <img class=\"img-full\" src=\"{{block_8.image}}\"/>\n                    </a> \n                    <div class=\"category-bx-on on-5\">\n                        <h3>{{block_8.title}}</h3>\n                        <h4>{{block_8.description}}</h4>\n                        <a [routerLink]=\"['./category',block_8.slug]\" class=\"waves-effect fabivo-btn-2 btn\">Shop now</a> \n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section> -->\n\n<section class=\"main-slider\">\n   <div class=\"demo-cont\"> \n  <!-- slider start -->\n  <div class=\"fnc-slider example-slider\">\n    <div class=\"fnc-slider__slides\"> \n      <!-- slide start -->\n      <div class=\"fnc-slide m--active-slide\">\n        <div class=\"fnc-slide__inner\">\n          <div align=\"center\" class=\"embed-responsive embed-responsive-16by9\">\n            <video autoplay loop class=\"embed-responsive-item\">\n              <source src=\"public/videos/Aritha_altiplano_10sec_boucle.mp4\" type=\"video/mp4\">\n            </video>\n          </div>\n        </div>\n      </div>\n      <!-- slide end --> \n      <!-- slide start -->\n      <div class=\"fnc-slide\">\n        <div class=\"fnc-slide__inner\">\n          <div align=\"center\" class=\"embed-responsive embed-responsive-16by9\">\n            <video autoplay loop class=\"embed-responsive-item\">\n              <source src=\"public/videos/Sunlight_Journey_HO_Slider.mp4\" type=\"video/mp4\">\n            </video>\n          </div>\n        </div>\n      </div>\n      <!-- slide end --> \n      <!-- slide start -->\n      <div class=\"fnc-slide\">\n        <div class=\"fnc-slide__inner\">\n          <div align=\"center\" class=\"embed-responsive embed-responsive-16by9\">\n            <video autoplay loop class=\"embed-responsive-item\">\n              <source src=\"public/videos/Possesssion2017_Actress_10sec_v2_1280_720-2.mp4\" type=\"video/mp4\">\n            </video>\n          </div>\n        </div>\n      </div>\n      <!-- slide end --> \n      <!-- slide start -->\n      <div class=\"fnc-slide\">\n        <div class=\"fnc-slide__inner\">\n          <div align=\"center\" class=\"embed-responsive embed-responsive-16by9\">\n            <video autoplay loop class=\"embed-responsive-item\">\n              <source src=\"public/videos/Sunlight_Journey_HO_Slider.mp4\" type=\"video/mp4\">\n            </video>\n          </div>\n        </div>\n      </div>\n      <!-- slide end --> \n      <!-- slide start -->\n      <div class=\"fnc-slide\">\n        <div class=\"fnc-slide__inner\">\n          <div align=\"center\" class=\"embed-responsive embed-responsive-16by9\">\n            <video autoplay loop class=\"embed-responsive-item\">\n              <source src=\"public/videos/Possesssion2017_Actress_10sec_v2_1280_720-2.mp4\" type=\"video/mp4\">\n            </video>\n          </div>\n        </div>\n      </div>\n      <!-- slide end --> \n    </div>\n    <nav class=\"fnc-nav\">\n      <div class=\"fnc-nav__bgs\">\n        <div class=\"fnc-nav__bg m--active-nav-bg\"></div>\n        <div class=\"fnc-nav__bg\"></div>\n        <div class=\"fnc-nav__bg\"></div>\n        <div class=\"fnc-nav__bg\"></div>\n        <div class=\"fnc-nav__bg\"></div> \n      </div>\n      <div class=\"fnc-nav__controls\">\n        <button class=\"fnc-nav__control\">\n        <h3>Sunlight Journey</h3>\n        <p>The scenic beauty of the Amalfi coast</p>\n        <span class=\"fnc-nav__control-progress\"></span> </button>\n        <button class=\"fnc-nav__control\">\n        <h3>Altiplano watches</h3>\n        <p>60 years of ultimate elegance</p>\n        <span class=\"fnc-nav__control-progress\"></span> </button>\n        <button class=\"fnc-nav__control\">\n        <h3>Possession</h3>\n        <p>Turn and the world is yours</p>\n        <span class=\"fnc-nav__control-progress\"></span> </button>\n        <button class=\"fnc-nav__control\">\n        <h3>Piaget Magazine</h3>\n        <p>Creating moment of Radiance</p>\n        <span class=\"fnc-nav__control-progress\"></span> </button>\n        <button class=\"fnc-nav__control\">\n        <h3>Altiplano watches</h3>\n        <p>60 years of ultimate elegance</p>\n        <span class=\"fnc-nav__control-progress\"></span> </button>\n      </div>\n    </nav>\n  </div>\n  <!-- slider end --> \n</div>\n</section>\n\n<section class=\"category\">\n  <div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col s12 m4\">\n            <div class=\"category-bx\">\n                <a [routerLink]=\"['./category',block_1.slug]\"> \n                    <img class=\"img-full\" src=\"{{block_1.image}}\" alt=\"\"/> \n                </a>\n                <div class=\"category-bx-on\">\n                    <h3>{{block_1.title}}</h3>\n                    <h4>{{block_1.title_2}}</h4>\n                    <h5>{{block_1.title_3}}</h5>\n                    <p [innerHTML]=\"block_1.description\"></p>\n                    <a [routerLink]=\"['./category',block_1.slug]\" class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now!</a> \n                </div>\n            </div>\n        </div>\n        \n        <div class=\"col s12 m4\">\n            <div class=\"mid category-bx\"> \n                <a [routerLink]=\"['./category',block_2.slug]\"> \n                    <img class=\"img-full\" src=\"{{block_2.image}}\" alt=\"\"/> \n                </a>\n                <div class=\"category-bx-on on-2\">\n                    <h3>{{block_2.title}}</h3>\n                    <h4>{{block_2.title_2}}</h4>\n                    <a [routerLink]=\"['./category',block_2.slug]\" class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a> </div>\n            </div>\n            <div class=\"mid category-bx\">\n                <a [routerLink]=\"['./category',block_3.slug]\"> \n                    <img class=\"img-full\" src=\"{{block_3.image}}\" alt=\"\"/> \n                </a>\n                <div class=\"category-bx-on on-3\">\n                    <h3>{{block_3.title}}</h3>\n                    <h4>{{block_3.title_2}}</h4>\n                    <a [routerLink]=\"['./category',block_3.slug]\" class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a> </div>\n            </div>\n        </div>\n        \n        <div class=\"col s12 m4\">\n            <div class=\"category-bx\"> \n                <a [routerLink]=\"['./category',block_4.slug]\"> \n                    <img class=\"img-full\" src=\"{{block_4.image}}\" alt=\"\"/> \n                </a>\n                <div class=\"category-bx-on on-4\">\n                    <h3>{{block_4.title}}</h3>\n                    <h4>{{block_4.title_2}}</h4>\n                    <h5>{{block_4.title_3}}</h5>\n                </div>\n                <div class=\"on-4-shop\">\n                    <a [routerLink]=\"['./category',block_4.slug]\" class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>\n                </div>\n            </div>\n        </div>\n    </div>\n  </div>\n</section>\n\n<section class=\"product-slider\">\n  <div class=\"product-bg\">\n    <div id=\"main\" role=\"main\">\n      <div class=\"flexslider\">\n        <ul class=\"slides\">\n          <li> <img src=\"public/img/center_bg.png\" alt=\"\" />\n            <div class=\"product-module-content\">\n              <div class=\"row\">\n                <div class=\"col s12 m6\">\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col s12 m6\">\n                  <div class=\"slider-content\">\n                    <div class=\"Collec-text\">\n                      <h2>Swanky Collections</h2>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus accusamus\n                        debitis architecto, vel facere quis nisi repudiandae. Consectetur adipisicing.</p>\n                      <a class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a> </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </li>\n          <li> <img src=\"public/img/center_bg.png\" alt=\"\" />\n            <div class=\"product-module-content\">\n              <div class=\"row\">\n                <div class=\"col s12 m6\">\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col s12 m6\">\n                  <div class=\"slider-content\">\n                    <div class=\"Collec-text\">\n                      <h2>Swanky Collections</h2>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus accusamus\n                        debitis architecto, vel facere quis nisi repudiandae. Consectetur adipisicing.</p>\n                      <a class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a> </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </li>\n          <li> <img src=\"public/img/center_bg.png\" alt=\"\" />\n            <div class=\"product-module-content\">\n              <div class=\"row\">\n                <div class=\"col s12 m6\">\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col s12 m6\">\n                  <div class=\"slider-content\">\n                    <div class=\"Collec-text\">\n                      <h2>Swanky Collections</h2>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus accusamus\n                        debitis architecto, vel facere quis nisi repudiandae. Consectetur adipisicing.</p>\n                      <a class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a> </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </li>\n          <li> <img src=\"public/img/center_bg.png\" alt=\"\" />\n            <div class=\"product-module-content\">\n              <div class=\"row\">\n                <div class=\"col s12 m6\">\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"product-list-item\">\n                    <div class=\"product-item\"> <a href=\"\"> <img src=\"public/img/bracelets_large-200x200.jpg\" alt=\"\"/> </a> </div>\n                    <div class=\"product-item-content\">\n                      <h3>Bracelets</h3>\n                      <div class=\"price\">$75.00</div>\n                    </div>\n                    <div class=\"item-product\">\n                      <div class=\"hover-field\">\n                        <div class=\"midle\">\n                          <div class=\"rotate-45\">\n                            <div class=\"btn-share\">\n                              <div class=\"quick-view\"> <a href=\"\" class=\"animat-icon\" title=\"view\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"share\"><i class=\"fa fa-share-alt\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                            <div class=\"clearfix\"></div>\n                            <div class=\"btn-share\">\n                              <div class=\"heart\"> <a href=\"\" class=\"animat-icon\" title=\"Compare\"> <i class=\"fa fa-heart\" aria-hidden=\"true\"></i> </a> </div>\n                            </div>\n                            <div class=\"btn-share\">\n                              <div class=\"add-to\"><a href=\"#\" class=\"animat-icon\" title=\"Compare\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i></a></div>\n                            </div>\n                          </div>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col s12 m6\">\n                  <div class=\"slider-content\">\n                    <div class=\"Collec-text\">\n                      <h2>Swanky Collections</h2>\n                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus accusamus\n                        debitis architecto, vel facere quis nisi repudiandae. Consectetur adipisicing.</p>\n                      <a class=\"waves-effect fabivo-btn-2 btn shop-btn\">Shop now <i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a> </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </li>\n        </ul>\n      </div>\n    </div>\n  </div>\n</section>\n\n<section class=\"newsletter\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col m12 s12\">\n        <div class=\"news_letter\">\n          <div class=\"col m6 bord\">\n            <p>SIGN UP FOR NEWSLETTER</p>\n          </div>\n          <div class=\"col m6\">\n            <div class=\"sign-bx\">\n              <form id=\"newsletter\" name=\"newsletter\"  (ngSubmit)=\"newsletter.form.valid && newslattersaved()\"  #newsletter=\"ngForm\" novalidate class=\"ng-pristine ng-invalid ng-touched\">\n                <div class=\"input-field\">\n                  <input id=\"search\" name=\"email_news\" pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\" [(ngModel)]=\"newslatter.email_news\" #email_news=\"ngModel\" placeholder=\"Your Email Address\" required class=\"ng-pristine ng-invalid ng-touched\" type=\"search\">\n                  <i aria-hidden=\"true\" class=\"fa fa-envelope\"></i> \n                </div>\n                <button type=\"submit\" class=\"waves-effect fabivo-btn btn news-btn\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></button>\n                <div *ngIf=\"newsletter.submitted && email_news.errors && email_news.errors.required\" class=\"help-block err-msg\">The email field is required.</div>\n                <div *ngIf=\"newsletter.submitted && email_news.errors && email_news.errors.pattern\" class=\"help-block err-msg\"> Email is invalid.</div>\n              </form>\n            </div>\n          </div>\n          <div class=\"ring\"><img src=\"public/img/ringbg.png\" alt=\"ring_image\"></div>\n          <div class=\"clearfix\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</section>"

/***/ },

/***/ 365:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var page_service_1 = __webpack_require__(33);
	var platform_browser_1 = __webpack_require__(21);
	var PageComponent = (function () {
	    function PageComponent(pageService, route, titleService) {
	        this.pageService = pageService;
	        this.route = route;
	        this.titleService = titleService;
	        this.page = [];
	    }
	    PageComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.route.params.subscribe(function (params) {
	            var slug = params['slug'];
	            _this.pageService.getPageDetail(slug).subscribe(function (pageDetail) {
	                jQuery("html, body").animate({ scrollTop: 10 }, "slow");
	                _this.page = pageDetail['data']['0'];
	                _this.titleService.setTitle(_this.page['title']);
	                jQuery('meta[name="title"]').attr('content', _this.page['meta_title']);
	                jQuery('meta[name="description"]').attr('content', _this.page['meta_description']);
	                jQuery('meta[name="keywords"]').attr('content', _this.page['meta_keywords']);
	            }, function (err) {
	                console.log(err);
	            });
	        });
	    };
	    return PageComponent;
	}());
	PageComponent = __decorate([
	    core_1.Component({
	        templateUrl: 'public/templates/page/page.html',
	    }),
	    __metadata("design:paramtypes", [page_service_1.PageService,
	        router_1.ActivatedRoute,
	        platform_browser_1.Title])
	], PageComponent);
	exports.PageComponent = PageComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 366:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var page_service_1 = __webpack_require__(33);
	var platform_browser_1 = __webpack_require__(21);
	var FaqComponent = (function () {
	    function FaqComponent(pageService, route, titleService) {
	        this.pageService = pageService;
	        this.route = route;
	        this.titleService = titleService;
	        this.faqs = [];
	    }
	    FaqComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.setTitle("Faq");
	        this.route.params.subscribe(function (params) {
	            _this.pageService.getFaq().subscribe(function (pageDetail) {
	                _this.faqs = pageDetail['data'];
	                setTimeout(function () {
	                    jQuery('.tabs').tabs();
	                    jQuery('.collapsible').collapsible();
	                }, 2000);
	            }, function (err) {
	                console.log(err);
	            });
	        });
	        if (jQuery(window).width() < 640) {
	            jQuery(".left_active").show();
	            jQuery(".right_active").hide();
	            jQuery(".back_link").click(function () {
	                jQuery(".left_active").show();
	                jQuery(".right_active").hide();
	            });
	        }
	        else {
	            jQuery(".left_active").show();
	            jQuery(".right_active").show();
	            jQuery(".back_link").hide();
	        }
	        jQuery(window).resize(function () {
	            if (jQuery(window).width() < 640) {
	                jQuery(".left_active").show();
	                jQuery(".right_active").hide();
	                jQuery(".back_link").click(function () {
	                    jQuery(".left_active").show();
	                    jQuery(".right_active").hide();
	                });
	            }
	            else {
	                jQuery(".left_active").show();
	                jQuery(".right_active").show();
	                jQuery(".back_link").hide();
	            }
	        });
	    };
	    FaqComponent.prototype.tabClick = function () {
	        if (jQuery(window).width() < 640) {
	            jQuery(".left_active").hide();
	            jQuery(".right_active").show();
	            jQuery(".back_link").show();
	        }
	    };
	    FaqComponent.prototype.setTitle = function (newTitle) {
	        this.titleService.setTitle(newTitle);
	    };
	    return FaqComponent;
	}());
	FaqComponent = __decorate([
	    core_1.Component({
	        selector: 'faq',
	        template: __webpack_require__(367)
	    }),
	    __metadata("design:paramtypes", [page_service_1.PageService,
	        router_1.ActivatedRoute,
	        platform_browser_1.Title])
	], FaqComponent);
	exports.FaqComponent = FaqComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 367:
/***/ function(module, exports) {

	module.exports = "     \n<section class=\"faq-section\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div   class=\"col m4 s12 left_active \">\n                <div class=\"faq-category\">\n                    <ul class=\"tabs\">\n\n                        <li class=\"tab col m12\" *ngFor=\"let faq of faqs; let firstItem = first; let i = index\"><a  [ngClass]=\"{ active: firstItem }\" (click)=\"tabClick()\"  href=\"#{{faq['id']}}\"><span>{{faq['name']}}{{ (i==1)?active:''}}</span> <i class=\"fa fa-hand-o-right\" aria-hidden=\"true\"></i>\n                            </a></li>\n\n\n                        <div class=\"clearfix\"></div>\n                    </ul>\n                </div>\n\n            </div>\n            <div  class=\"col m8 s12 right_active\">\n                <div class=\"row\">\n\n                        <div class=\"col m12 s12 hide-on-large-only\">\n                                <a class=\"back_link waves-effect fabivo-btn btn\"><i class=\"fa fa-arrow-left\"></i> Back</a>\n                        </div>\n                         <div [hidden]=\"!firstItem\"*ngFor=\"let faqcat of faqs; let firstItem = first; let i = index\" id=\"{{faqcat['id']}}\" class=\"col m12 s12\">\n                        <div class=\"question-bx\">\n                            <ul class=\"collapsible\" data-collapsible=\"accordion\" *ngIf=\"faqcat.faqs.length > 0\">\n                                <li *ngFor=\"let faqs of faqcat.faqs;\" >\n                                    <div class=\"collapsible-header\"><i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                                        <span>Q</span>{{faqs.question}}\n                                    </div>\n                                    <div class=\"collapsible-body\" [innerHTML]=\"faqs.answer\" ></div>\n                                </li>\n\n\n                            </ul>\n                        </div>\n                         </div>\n\n\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n"

/***/ },

/***/ 368:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var common_1 = __webpack_require__(22);
	var authentication_service_1 = __webpack_require__(369);
	var cart_service_1 = __webpack_require__(370);
	var global_1 = __webpack_require__(363);
	var LoginComponent = (function () {
	    function LoginComponent(router, authenticationService, location, cartService) {
	        this.router = router;
	        this.authenticationService = authenticationService;
	        this.location = location;
	        this.cartService = cartService;
	        this.model = {};
	        this.loading = false;
	        this.error = '';
	        this.success = '';
	        this.authData = [];
	        this.cartItem = [];
	    }
	    LoginComponent.prototype.ngOnInit = function () {
	        global_1.blockUI();
	        if (this.authenticationService.isLoggedIn()) {
	            this.location.back();
	            global_1.unblockUI();
	            global_1.toast('You are already logged in.', 'error');
	        }
	        else {
	            global_1.unblockUI();
	        }
	    };
	    LoginComponent.prototype.login = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.authenticationService.login(this.model.email, this.model.password)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.model = '';
	                _this.error = '';
	                _this.authData = JSON.parse(localStorage.getItem('loginUser'));
	                jQuery('#signInForm')[0].reset();
	                jQuery('#signInForm').find('label').removeClass('active');
	                _this.getCartItem();
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.router.navigate(['/myaccount', 'profile']);
	                global_1.toast(_this.success, 'success');
	                setTimeout(function () {
	                    jQuery('.dropdown-button').dropdown({
	                        inDuration: 300,
	                        outDuration: 225,
	                        constrain_width: false,
	                        hover: true,
	                        gutter: 0,
	                        belowOrigin: false,
	                        alignment: 'left'
	                    });
	                    jQuery("#nav-mobile .child-menu ul li a").click(function () {
	                        jQuery('.button-collapse').sideNav('hide');
	                    });
	                }, 2000);
	            }
	            else {
	                _this.error = result['message'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    LoginComponent.prototype.getCartItem = function () {
	        var _this = this;
	        if (this.authData && this.authData.id) {
	            this.cartService.getCart(this.authData.id).subscribe(function (cartData) {
	                _this.cartService.cart = cartData['data'];
	            }, function (err) {
	                console.log(err);
	            });
	        }
	    };
	    return LoginComponent;
	}());
	LoginComponent = __decorate([
	    core_1.Component({
	        selector: 'login',
	        template: __webpack_require__(371)
	    }),
	    __metadata("design:paramtypes", [router_1.Router,
	        authentication_service_1.AuthenticationService,
	        common_1.Location,
	        cart_service_1.CartService])
	], LoginComponent);
	exports.LoginComponent = LoginComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 369:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var http_1 = __webpack_require__(31);
	__webpack_require__(227);
	var AuthenticationService = (function () {
	    function AuthenticationService(http) {
	        this.http = http;
	        this.authData = [];
	        this.loggedIn = false;
	        this.authData = JSON.parse(localStorage.getItem('loginUser'));
	    }
	    AuthenticationService.prototype.login = function (email, password) {
	        var _this = this;
	        return this.http.post('api/login', { email: email, password: password })
	            .map(function (response) {
	            var auth_data = JSON.stringify(response.json().user_data);
	            if (auth_data) {
	                localStorage.setItem('loginUser', auth_data);
	                localStorage.setItem('loginUser_id', auth_data['id']);
	                localStorage.setItem('loginUser_token', auth_data['token']);
	                _this.setAuthData();
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    AuthenticationService.prototype.logout = function () {
	        localStorage.removeItem('loginUser');
	        localStorage.removeItem('loginUser');
	        localStorage.removeItem('loginUser');
	    };
	    AuthenticationService.prototype.isLoggedIn = function () {
	        if (JSON.parse(localStorage.getItem('loginUser'))) {
	            return JSON.parse(localStorage.getItem('loginUser'));
	        }
	    };
	    AuthenticationService.prototype.socialLogin = function (user_data) {
	        var auth_data = JSON.stringify(user_data);
	        if (auth_data) {
	            this.authData = auth_data;
	            localStorage.setItem('loginUser', auth_data);
	            this.setAuthData();
	            return true;
	        }
	        else {
	            return false;
	        }
	    };
	    AuthenticationService.prototype.signup = function (modelData) {
	        return this.http.post('api/sign-up', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    AuthenticationService.prototype.mobileNumberVerified = function (modelData) {
	        return this.http.post('api/mobile-verify', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    AuthenticationService.prototype.mobileNumberSaved = function (modelData) {
	        return this.http.post('api/mobile-number-update', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    AuthenticationService.prototype.socialLoginSaveData = function (modelData, login_type) {
	        return this.http.post('api/social-signup/' + login_type, { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    AuthenticationService.prototype.forgotPassword = function (modelData) {
	        return this.http.post('api/forgot-password', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    AuthenticationService.prototype.resendOtpPassword = function (user_id) {
	        return this.http.get('api/resend-otp-password/' + user_id)
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    AuthenticationService.prototype.setAuthData = function () {
	        this.authData = JSON.parse(localStorage.getItem('loginUser'));
	    };
	    AuthenticationService.prototype.setMobileNumberONAuth = function (mobile_number) {
	        this.authData = JSON.parse(localStorage.getItem('loginUser'));
	        this.authData.phone = mobile_number;
	        localStorage.setItem('loginUser', JSON.stringify(this.authData));
	        this.setAuthData();
	    };
	    AuthenticationService.prototype.getUserName = function () {
	        return this.authData['first_name'] + " " + this.authData['last_name'];
	    };
	    return AuthenticationService;
	}());
	AuthenticationService = __decorate([
	    core_1.Injectable(),
	    __metadata("design:paramtypes", [http_1.Http])
	], AuthenticationService);
	exports.AuthenticationService = AuthenticationService;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 370:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var http_1 = __webpack_require__(31);
	var Rx_1 = __webpack_require__(34);
	__webpack_require__(227);
	__webpack_require__(150);
	var global_1 = __webpack_require__(363);
	var CartService = (function () {
	    function CartService(http) {
	        var _this = this;
	        this.http = http;
	        this.cart = [];
	        this.authDatas = [];
	        this.wishlist = [];
	        this.wishlistArray = [];
	        this.orderData = [];
	        this.add_wishlist = [];
	        this.getCartUrl = 'api/get-cart/';
	        this.getCartItemUrl = 'api/get-cart-item/';
	        this.getWishlistUrl = 'api/get-wishlist/';
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        if (this.authDatas) {
	            this.getCart(this.authDatas.id).subscribe(function (cartData) {
	                _this.cart = cartData['data'];
	            }, function (err) {
	                console.log(err);
	            });
	            this.getToWishlist(this.authDatas.id);
	        }
	    }
	    CartService.prototype.getCart = function (id) {
	        return this.http.get(this.getCartUrl + id)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    CartService.prototype.addCart = function (add_cart) {
	        var _this = this;
	        return this.http.post('api/add-cart', { data: add_cart })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                _this.cart.push(add_cart);
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    CartService.prototype.updateCart = function (edit_cart) {
	        return this.http.post('api/update-cart', { data: edit_cart })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    CartService.prototype.removeCart = function (remove_product) {
	        return this.http.post('api/remove-cart', { data: remove_product })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    CartService.prototype.checkCode = function (id, coupan_code) {
	        return this.http.get('api/check-coupon-code/' + id + '/' + coupan_code)
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    CartService.prototype.getcartItem = function (id, coupan_code) {
	        return this.http.get(this.getCartItemUrl + id + '/' + coupan_code)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    CartService.prototype.getWishlist = function (id) {
	        return this.http.get(this.getWishlistUrl + id)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    CartService.prototype.addWishlist = function (add_wishlist) {
	        var _this = this;
	        console.log(add_wishlist);
	        return this.http.post('api/add-wishlist', { data: add_wishlist })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                _this.wishlist.push(add_wishlist);
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    CartService.prototype.removeWishlist = function (remove_product) {
	        return this.http.post('api/remove-wishlist', { data: remove_product })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    CartService.prototype.addToWishlist = function (product_id) {
	        var _this = this;
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        if (this.authDatas) {
	            global_1.blockUI();
	            var userData = this.authDatas;
	            var add_wishlist = {};
	            add_wishlist['product_id'] = product_id;
	            add_wishlist['user_id'] = userData['id'];
	            add_wishlist['userToken'] = userData['token'];
	            this.addWishlist(add_wishlist).subscribe(function (result) {
	                global_1.unblockUI();
	                if (result['status_code'] === 1) {
	                    global_1.toast(result['message'], 'success');
	                    _this.wishlist = result['data'];
	                    _this.wishlistArray = result['dataArray'];
	                }
	                else {
	                    global_1.toast(result['message'], 'error');
	                }
	            });
	        }
	        else {
	            jQuery('#modal1').modal('open');
	        }
	    };
	    CartService.prototype.removeToWishlist = function (product_id) {
	        var _this = this;
	        var userData = this.authDatas;
	        var remove_product = {};
	        remove_product['product_id'] = product_id;
	        remove_product['user_id'] = userData['id'];
	        global_1.blockUI();
	        this.removeWishlist(remove_product).subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.getToWishlist(userData['id']);
	                _this.wishlist = result['data'];
	                _this.wishlistArray = result['dataArray'];
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    CartService.prototype.getToWishlist = function (user_id) {
	        var _this = this;
	        this.getWishlist(user_id).subscribe(function (wishData) {
	            _this.wishlist = wishData['data'];
	            _this.wishlistArray = wishData['dataArray'];
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    CartService.prototype.payment = function (data) {
	        return this.http.post('api/payment', { data: data })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    CartService.prototype.checkwalletBalance = function (id, data) {
	        return this.http.post('api/wallet-balance/' + id, { data: data })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    return CartService;
	}());
	CartService = __decorate([
	    core_1.Injectable(),
	    __metadata("design:paramtypes", [http_1.Http])
	], CartService);
	exports.CartService = CartService;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 371:
/***/ function(module, exports) {

	module.exports = "<div class=\"container\">\n    <div class=\"login-bx login-page\"> \n    <div class=\"row\">\n      <div class=\"col s12\">\n      <form class=\"login-form\" id=\"signInForm\" name=\"form\" (ngSubmit)=\"f.form.valid && login()\" #f=\"ngForm\" novalidate>\n        <div class=\"row\">\n          <div class=\"input-field col s12 center\">\n               <div class=\"contant\">\n            <h4 class=\"center login-form-text\">Login</h4>\n               </div>\n          </div>\n        </div>\n        <div class=\"row\" [ngClass]=\"{ 'has-error': f.submitted && !email.valid }\">\n          <div class=\"input-field col s12\">\n            <i class=\"fa fa-user\"></i>\n            <input id=\"email\" type=\"text\" name=\"email\" [(ngModel)]=\"model.email\" #email=\"ngModel\" required >\n            <label for=\"email\" >Email</label>\n            <div *ngIf=\"f.submitted && !email.valid\" class=\"help-block err-msg\">Email is required</div>\n          </div>\n        </div>\n        <div class=\"row\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\n          <div class=\"input-field col s12\">\n            <i class=\" fa fa-lock\"></i>\n            <input id=\"password\" type=\"password\" name=\"password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required>\n            <label for=\"password\" class=\"\">Password</label>\n            <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block err-msg\">Password is required</div>\n          </div>\n        </div>\n        <div class=\"row\">          \n          <div class=\"input-field col s12 m12 l12  login-text\">\n              <div class=\"row\">\n                  <div class=\"col m6\">\n                       <input id=\"remember-me\" type=\"checkbox\">\n              <label class=\"mr-lf\" for=\"remember-me\">Remember me</label>\n                  </div>\n                  <div class=\"col m6 pull-right\">\n                    <p class=\"margin right-align medium-small\">\n                        <a href=\"#modal3\" class=\"modal-close\">Forgot password ?</a>\n                    </p>\n                  </div>\n              </div>\n          </div>\n        </div>\n        <div *ngIf=\"error\" class=\"alert alert-danger err-msg\">{{error}}</div>\n        <div class=\"row center-align\">\n          <span class=\"input-field login-btn\">\n            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">Sign in</button>\n          </span>\n            \n        \n        </div>\n        \n\n      </form>\n    </div>\n    </div>\n    </div>\n</div>"

/***/ },

/***/ 372:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var platform_browser_1 = __webpack_require__(21);
	var router_1 = __webpack_require__(361);
	var product_service_1 = __webpack_require__(373);
	var pager_service_1 = __webpack_require__(374);
	var global_1 = __webpack_require__(363);
	var common_1 = __webpack_require__(22);
	var cart_service_1 = __webpack_require__(370);
	var _ = __webpack_require__(375);
	var ProductComponent = (function () {
	    function ProductComponent(productService, route, router, pagerService, location, titleService, cartService) {
	        this.productService = productService;
	        this.route = route;
	        this.router = router;
	        this.pagerService = pagerService;
	        this.location = location;
	        this.titleService = titleService;
	        this.cartService = cartService;
	        this.products = [];
	        this.filters = [];
	        this.categories = [];
	        this.page = 1;
	        this.search = [];
	        this.pager = {};
	        this.sortBy = 'popular';
	        this.attributes = [];
	        this.valuses = [];
	    }
	    ProductComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.route.params.subscribe(function (params) {
	            _this.slug = params['slug'];
	            _this.router.routerState.root.queryParams.subscribe(function (params) {
	                if (_this.location.path().indexOf('category/') > -1) {
	                    if (params['page']) {
	                        _this.page = parseInt(params['page']);
	                    }
	                    if (params['sortBy']) {
	                        _this.sortBy = params['sortBy'];
	                    }
	                    if (params['filter']) {
	                        var filter = params['filter'];
	                        var filter_data = filter.split(";");
	                        for (var _i = 0, filter_data_1 = filter_data; _i < filter_data_1.length; _i++) {
	                            var attribute_data = filter_data_1[_i];
	                            var split_array = attribute_data.split("--");
	                            _this.search.push({ attribute: split_array[0], data: split_array[1] });
	                            _this.attributes.push(split_array[0]);
	                            var split_value = split_array[1].split(",");
	                            for (var _a = 0, split_value_1 = split_value; _a < split_value_1.length; _a++) {
	                                var value = split_value_1[_a];
	                                _this.valuses.push(value);
	                            }
	                        }
	                    }
	                    else {
	                        _this.search = [];
	                        _this.attributes = [];
	                        _this.valuses = [];
	                    }
	                    _this.getList();
	                }
	            });
	            _this.productService.getAttribute(_this.slug).subscribe(function (attributedata) {
	                _this.filters = attributedata['filter'];
	                _this.categories = attributedata['category'];
	                _this.titleService.setTitle(attributedata['category']['name']);
	                jQuery('meta[name="title"]').attr('content', attributedata['category']['meta_title']);
	                jQuery('meta[name="description"]').attr('content', attributedata['category']['meta_description']);
	                jQuery('meta[name="keywords"]').attr('content', attributedata['category']['meta_keywords']);
	                setTimeout(function () {
	                    jQuery('.collapsible').collapsible();
	                }, 2000);
	            }, function (err) {
	                console.log(err);
	            });
	        });
	    };
	    ProductComponent.prototype.setsortBy = function (sort_by) {
	        this.sortBy = sort_by;
	        this.page = 1;
	        jQuery('#modalSort').modal('close');
	        this.getList();
	    };
	    ProductComponent.prototype.setfilter = function (attribute, value) {
	        var attribute_data = this.search.filter(function (search) { return search.attribute === attribute; }).pop();
	        if (attribute_data) {
	            var data = attribute_data.data;
	            var spitdata = data.split(",");
	            var index = _.indexOf(spitdata, value);
	            if (index == -1) {
	                spitdata.push(value);
	                this.valuses.push(value);
	            }
	            else {
	                spitdata = _.without(spitdata, value);
	                this.valuses = _.without(this.valuses, value);
	            }
	            if (spitdata.length > 0) {
	                data = spitdata.join();
	                Object.assign(attribute_data, { data: data });
	            }
	            else {
	                this.search = this.search.filter(function (search) { return search.attribute !== attribute; });
	                this.attributes = _.without(this.attributes, attribute);
	            }
	        }
	        else {
	            this.search.push({ attribute: attribute, data: value });
	            this.attributes.push(attribute);
	            this.valuses.push(value);
	        }
	        this.page = 1;
	        jQuery('#modalFilter').modal('close');
	        this.getList();
	    };
	    ProductComponent.prototype.clearFilter = function () {
	        this.search = [];
	        this.attributes = [];
	        this.valuses = [];
	        this.getList();
	    };
	    ProductComponent.prototype.setPage = function (page) {
	        if (page < 1 || page > this.pager.totalPages) {
	            return;
	        }
	        this.page = page;
	        this.getList();
	    };
	    ProductComponent.prototype.getList = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.productService.getProduct(this.slug, this.page, this.sortBy, this.search).subscribe(function (productsdata) {
	            _this.products = productsdata['products'];
	            _this.total = productsdata['products']['total'];
	            _this.per_page = productsdata['products']['per_page'];
	            ;
	            _this.pager = _this.pagerService.getPager(_this.total, _this.page, _this.per_page);
	            _this.changeUrl();
	            setTimeout(function () {
	                jQuery(document).ready(function () {
	                    jQuery('.collapsible').collapsible();
	                    jQuery('.modal').modal();
	                });
	            }, 1000);
	        }, function (err) {
	            console.log(err);
	        });
	        global_1.unblockUI();
	    };
	    ProductComponent.prototype.changeUrl = function () {
	        var url = 'category/' + this.slug + '?page=' + this.page + '&sortBy=' + this.sortBy;
	        if (this.search) {
	            var filter = '';
	            var i = 0;
	            for (var _i = 0, _a = this.search; _i < _a.length; _i++) {
	                var entry = _a[_i];
	                if (i > 0) {
	                    filter = filter + ';';
	                }
	                filter = filter + entry.attribute + "--" + entry.data;
	                i = i + 1;
	            }
	            if (filter) {
	                url = url + "&filter=" + filter;
	            }
	        }
	        var count = 0;
	        var query_string = '';
	        this.location.go(url);
	    };
	    return ProductComponent;
	}());
	ProductComponent = __decorate([
	    core_1.Component({
	        selector: 'product-listing',
	        template: __webpack_require__(376),
	    }),
	    __metadata("design:paramtypes", [product_service_1.ProductService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        pager_service_1.PagerService,
	        common_1.Location,
	        platform_browser_1.Title,
	        cart_service_1.CartService])
	], ProductComponent);
	exports.ProductComponent = ProductComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 373:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var http_1 = __webpack_require__(31);
	var Rx_1 = __webpack_require__(34);
	__webpack_require__(227);
	__webpack_require__(150);
	var ProductService = (function () {
	    function ProductService(http) {
	        this.http = http;
	        this.productUrl = 'api/get-product-by-category/';
	        this.productSearchUrl = 'api/get-search-product/';
	        this.categoryUrl = 'api/get-category-attribute/';
	        this.attributeUrl = 'api/get-all-attribute/';
	    }
	    ProductService.prototype.getProduct = function (slug, page, sort_by, search) {
	        var url = this.productUrl + slug + '?page=' + page;
	        if (sort_by) {
	            url = url + "&sort_by=" + sort_by;
	        }
	        return this.http.post("" + url, { data: search })
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    ProductService.prototype.getSearchProduct = function (slug, page, sort_by, search) {
	        var url = this.productSearchUrl + slug + '?page=' + page;
	        if (sort_by) {
	            url = url + "&sort_by=" + sort_by;
	        }
	        return this.http.post("" + url, { data: search })
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    ProductService.prototype.getAttribute = function (slug) {
	        var url = this.categoryUrl + slug;
	        return this.http.get("" + url)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    ProductService.prototype.getAllAttribute = function (slug) {
	        var url = this.attributeUrl + slug;
	        return this.http.get("" + url)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    ProductService.prototype.getProductDetail = function (slug, userId) {
	        return this.http.post('api/get-product-detail', { slug: slug, userId: userId })
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    ProductService.prototype.getWishlistProduct = function (uId, page) {
	        return this.http.post('api/get-product-wishlist?page=' + page, { uId: uId })
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    return ProductService;
	}());
	ProductService = __decorate([
	    core_1.Injectable(),
	    __metadata("design:paramtypes", [http_1.Http])
	], ProductService);
	exports.ProductService = ProductService;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 374:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var _ = __webpack_require__(375);
	var PagerService = (function () {
	    function PagerService() {
	    }
	    PagerService.prototype.getPager = function (totalItems, currentPage, pageSize) {
	        if (currentPage === void 0) { currentPage = 1; }
	        if (pageSize === void 0) { pageSize = 10; }
	        var totalPages = Math.ceil(totalItems / pageSize);
	        var startPage, endPage;
	        if (totalPages <= 10) {
	            startPage = 1;
	            endPage = totalPages;
	        }
	        else {
	            if (currentPage <= 6) {
	                startPage = 1;
	                endPage = 10;
	            }
	            else if (currentPage + 4 >= totalPages) {
	                startPage = totalPages - 9;
	                endPage = totalPages;
	            }
	            else {
	                startPage = currentPage - 5;
	                endPage = currentPage + 4;
	            }
	        }
	        var startIndex = (currentPage - 1) * pageSize;
	        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
	        var pages = _.range(startPage, endPage + 1);
	        return {
	            totalItems: totalItems,
	            currentPage: currentPage,
	            pageSize: pageSize,
	            totalPages: totalPages,
	            startPage: startPage,
	            endPage: endPage,
	            startIndex: startIndex,
	            endIndex: endIndex,
	            pages: pages
	        };
	    };
	    return PagerService;
	}());
	exports.PagerService = PagerService;


/***/ },

/***/ 375:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;//     Underscore.js 1.8.3
	//     http://underscorejs.org
	//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
	//     Underscore may be freely distributed under the MIT license.
	
	(function() {
	
	  // Baseline setup
	  // --------------
	
	  // Establish the root object, `window` in the browser, or `exports` on the server.
	  var root = this;
	
	  // Save the previous value of the `_` variable.
	  var previousUnderscore = root._;
	
	  // Save bytes in the minified (but not gzipped) version:
	  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;
	
	  // Create quick reference variables for speed access to core prototypes.
	  var
	    push             = ArrayProto.push,
	    slice            = ArrayProto.slice,
	    toString         = ObjProto.toString,
	    hasOwnProperty   = ObjProto.hasOwnProperty;
	
	  // All **ECMAScript 5** native function implementations that we hope to use
	  // are declared here.
	  var
	    nativeIsArray      = Array.isArray,
	    nativeKeys         = Object.keys,
	    nativeBind         = FuncProto.bind,
	    nativeCreate       = Object.create;
	
	  // Naked function reference for surrogate-prototype-swapping.
	  var Ctor = function(){};
	
	  // Create a safe reference to the Underscore object for use below.
	  var _ = function(obj) {
	    if (obj instanceof _) return obj;
	    if (!(this instanceof _)) return new _(obj);
	    this._wrapped = obj;
	  };
	
	  // Export the Underscore object for **Node.js**, with
	  // backwards-compatibility for the old `require()` API. If we're in
	  // the browser, add `_` as a global object.
	  if (true) {
	    if (typeof module !== 'undefined' && module.exports) {
	      exports = module.exports = _;
	    }
	    exports._ = _;
	  } else {
	    root._ = _;
	  }
	
	  // Current version.
	  _.VERSION = '1.8.3';
	
	  // Internal function that returns an efficient (for current engines) version
	  // of the passed-in callback, to be repeatedly applied in other Underscore
	  // functions.
	  var optimizeCb = function(func, context, argCount) {
	    if (context === void 0) return func;
	    switch (argCount == null ? 3 : argCount) {
	      case 1: return function(value) {
	        return func.call(context, value);
	      };
	      case 2: return function(value, other) {
	        return func.call(context, value, other);
	      };
	      case 3: return function(value, index, collection) {
	        return func.call(context, value, index, collection);
	      };
	      case 4: return function(accumulator, value, index, collection) {
	        return func.call(context, accumulator, value, index, collection);
	      };
	    }
	    return function() {
	      return func.apply(context, arguments);
	    };
	  };
	
	  // A mostly-internal function to generate callbacks that can be applied
	  // to each element in a collection, returning the desired result — either
	  // identity, an arbitrary callback, a property matcher, or a property accessor.
	  var cb = function(value, context, argCount) {
	    if (value == null) return _.identity;
	    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
	    if (_.isObject(value)) return _.matcher(value);
	    return _.property(value);
	  };
	  _.iteratee = function(value, context) {
	    return cb(value, context, Infinity);
	  };
	
	  // An internal function for creating assigner functions.
	  var createAssigner = function(keysFunc, undefinedOnly) {
	    return function(obj) {
	      var length = arguments.length;
	      if (length < 2 || obj == null) return obj;
	      for (var index = 1; index < length; index++) {
	        var source = arguments[index],
	            keys = keysFunc(source),
	            l = keys.length;
	        for (var i = 0; i < l; i++) {
	          var key = keys[i];
	          if (!undefinedOnly || obj[key] === void 0) obj[key] = source[key];
	        }
	      }
	      return obj;
	    };
	  };
	
	  // An internal function for creating a new object that inherits from another.
	  var baseCreate = function(prototype) {
	    if (!_.isObject(prototype)) return {};
	    if (nativeCreate) return nativeCreate(prototype);
	    Ctor.prototype = prototype;
	    var result = new Ctor;
	    Ctor.prototype = null;
	    return result;
	  };
	
	  var property = function(key) {
	    return function(obj) {
	      return obj == null ? void 0 : obj[key];
	    };
	  };
	
	  // Helper for collection methods to determine whether a collection
	  // should be iterated as an array or as an object
	  // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
	  // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
	  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
	  var getLength = property('length');
	  var isArrayLike = function(collection) {
	    var length = getLength(collection);
	    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
	  };
	
	  // Collection Functions
	  // --------------------
	
	  // The cornerstone, an `each` implementation, aka `forEach`.
	  // Handles raw objects in addition to array-likes. Treats all
	  // sparse array-likes as if they were dense.
	  _.each = _.forEach = function(obj, iteratee, context) {
	    iteratee = optimizeCb(iteratee, context);
	    var i, length;
	    if (isArrayLike(obj)) {
	      for (i = 0, length = obj.length; i < length; i++) {
	        iteratee(obj[i], i, obj);
	      }
	    } else {
	      var keys = _.keys(obj);
	      for (i = 0, length = keys.length; i < length; i++) {
	        iteratee(obj[keys[i]], keys[i], obj);
	      }
	    }
	    return obj;
	  };
	
	  // Return the results of applying the iteratee to each element.
	  _.map = _.collect = function(obj, iteratee, context) {
	    iteratee = cb(iteratee, context);
	    var keys = !isArrayLike(obj) && _.keys(obj),
	        length = (keys || obj).length,
	        results = Array(length);
	    for (var index = 0; index < length; index++) {
	      var currentKey = keys ? keys[index] : index;
	      results[index] = iteratee(obj[currentKey], currentKey, obj);
	    }
	    return results;
	  };
	
	  // Create a reducing function iterating left or right.
	  function createReduce(dir) {
	    // Optimized iterator function as using arguments.length
	    // in the main function will deoptimize the, see #1991.
	    function iterator(obj, iteratee, memo, keys, index, length) {
	      for (; index >= 0 && index < length; index += dir) {
	        var currentKey = keys ? keys[index] : index;
	        memo = iteratee(memo, obj[currentKey], currentKey, obj);
	      }
	      return memo;
	    }
	
	    return function(obj, iteratee, memo, context) {
	      iteratee = optimizeCb(iteratee, context, 4);
	      var keys = !isArrayLike(obj) && _.keys(obj),
	          length = (keys || obj).length,
	          index = dir > 0 ? 0 : length - 1;
	      // Determine the initial value if none is provided.
	      if (arguments.length < 3) {
	        memo = obj[keys ? keys[index] : index];
	        index += dir;
	      }
	      return iterator(obj, iteratee, memo, keys, index, length);
	    };
	  }
	
	  // **Reduce** builds up a single result from a list of values, aka `inject`,
	  // or `foldl`.
	  _.reduce = _.foldl = _.inject = createReduce(1);
	
	  // The right-associative version of reduce, also known as `foldr`.
	  _.reduceRight = _.foldr = createReduce(-1);
	
	  // Return the first value which passes a truth test. Aliased as `detect`.
	  _.find = _.detect = function(obj, predicate, context) {
	    var key;
	    if (isArrayLike(obj)) {
	      key = _.findIndex(obj, predicate, context);
	    } else {
	      key = _.findKey(obj, predicate, context);
	    }
	    if (key !== void 0 && key !== -1) return obj[key];
	  };
	
	  // Return all the elements that pass a truth test.
	  // Aliased as `select`.
	  _.filter = _.select = function(obj, predicate, context) {
	    var results = [];
	    predicate = cb(predicate, context);
	    _.each(obj, function(value, index, list) {
	      if (predicate(value, index, list)) results.push(value);
	    });
	    return results;
	  };
	
	  // Return all the elements for which a truth test fails.
	  _.reject = function(obj, predicate, context) {
	    return _.filter(obj, _.negate(cb(predicate)), context);
	  };
	
	  // Determine whether all of the elements match a truth test.
	  // Aliased as `all`.
	  _.every = _.all = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var keys = !isArrayLike(obj) && _.keys(obj),
	        length = (keys || obj).length;
	    for (var index = 0; index < length; index++) {
	      var currentKey = keys ? keys[index] : index;
	      if (!predicate(obj[currentKey], currentKey, obj)) return false;
	    }
	    return true;
	  };
	
	  // Determine if at least one element in the object matches a truth test.
	  // Aliased as `any`.
	  _.some = _.any = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var keys = !isArrayLike(obj) && _.keys(obj),
	        length = (keys || obj).length;
	    for (var index = 0; index < length; index++) {
	      var currentKey = keys ? keys[index] : index;
	      if (predicate(obj[currentKey], currentKey, obj)) return true;
	    }
	    return false;
	  };
	
	  // Determine if the array or object contains a given item (using `===`).
	  // Aliased as `includes` and `include`.
	  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
	    if (!isArrayLike(obj)) obj = _.values(obj);
	    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
	    return _.indexOf(obj, item, fromIndex) >= 0;
	  };
	
	  // Invoke a method (with arguments) on every item in a collection.
	  _.invoke = function(obj, method) {
	    var args = slice.call(arguments, 2);
	    var isFunc = _.isFunction(method);
	    return _.map(obj, function(value) {
	      var func = isFunc ? method : value[method];
	      return func == null ? func : func.apply(value, args);
	    });
	  };
	
	  // Convenience version of a common use case of `map`: fetching a property.
	  _.pluck = function(obj, key) {
	    return _.map(obj, _.property(key));
	  };
	
	  // Convenience version of a common use case of `filter`: selecting only objects
	  // containing specific `key:value` pairs.
	  _.where = function(obj, attrs) {
	    return _.filter(obj, _.matcher(attrs));
	  };
	
	  // Convenience version of a common use case of `find`: getting the first object
	  // containing specific `key:value` pairs.
	  _.findWhere = function(obj, attrs) {
	    return _.find(obj, _.matcher(attrs));
	  };
	
	  // Return the maximum element (or element-based computation).
	  _.max = function(obj, iteratee, context) {
	    var result = -Infinity, lastComputed = -Infinity,
	        value, computed;
	    if (iteratee == null && obj != null) {
	      obj = isArrayLike(obj) ? obj : _.values(obj);
	      for (var i = 0, length = obj.length; i < length; i++) {
	        value = obj[i];
	        if (value > result) {
	          result = value;
	        }
	      }
	    } else {
	      iteratee = cb(iteratee, context);
	      _.each(obj, function(value, index, list) {
	        computed = iteratee(value, index, list);
	        if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
	          result = value;
	          lastComputed = computed;
	        }
	      });
	    }
	    return result;
	  };
	
	  // Return the minimum element (or element-based computation).
	  _.min = function(obj, iteratee, context) {
	    var result = Infinity, lastComputed = Infinity,
	        value, computed;
	    if (iteratee == null && obj != null) {
	      obj = isArrayLike(obj) ? obj : _.values(obj);
	      for (var i = 0, length = obj.length; i < length; i++) {
	        value = obj[i];
	        if (value < result) {
	          result = value;
	        }
	      }
	    } else {
	      iteratee = cb(iteratee, context);
	      _.each(obj, function(value, index, list) {
	        computed = iteratee(value, index, list);
	        if (computed < lastComputed || computed === Infinity && result === Infinity) {
	          result = value;
	          lastComputed = computed;
	        }
	      });
	    }
	    return result;
	  };
	
	  // Shuffle a collection, using the modern version of the
	  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
	  _.shuffle = function(obj) {
	    var set = isArrayLike(obj) ? obj : _.values(obj);
	    var length = set.length;
	    var shuffled = Array(length);
	    for (var index = 0, rand; index < length; index++) {
	      rand = _.random(0, index);
	      if (rand !== index) shuffled[index] = shuffled[rand];
	      shuffled[rand] = set[index];
	    }
	    return shuffled;
	  };
	
	  // Sample **n** random values from a collection.
	  // If **n** is not specified, returns a single random element.
	  // The internal `guard` argument allows it to work with `map`.
	  _.sample = function(obj, n, guard) {
	    if (n == null || guard) {
	      if (!isArrayLike(obj)) obj = _.values(obj);
	      return obj[_.random(obj.length - 1)];
	    }
	    return _.shuffle(obj).slice(0, Math.max(0, n));
	  };
	
	  // Sort the object's values by a criterion produced by an iteratee.
	  _.sortBy = function(obj, iteratee, context) {
	    iteratee = cb(iteratee, context);
	    return _.pluck(_.map(obj, function(value, index, list) {
	      return {
	        value: value,
	        index: index,
	        criteria: iteratee(value, index, list)
	      };
	    }).sort(function(left, right) {
	      var a = left.criteria;
	      var b = right.criteria;
	      if (a !== b) {
	        if (a > b || a === void 0) return 1;
	        if (a < b || b === void 0) return -1;
	      }
	      return left.index - right.index;
	    }), 'value');
	  };
	
	  // An internal function used for aggregate "group by" operations.
	  var group = function(behavior) {
	    return function(obj, iteratee, context) {
	      var result = {};
	      iteratee = cb(iteratee, context);
	      _.each(obj, function(value, index) {
	        var key = iteratee(value, index, obj);
	        behavior(result, value, key);
	      });
	      return result;
	    };
	  };
	
	  // Groups the object's values by a criterion. Pass either a string attribute
	  // to group by, or a function that returns the criterion.
	  _.groupBy = group(function(result, value, key) {
	    if (_.has(result, key)) result[key].push(value); else result[key] = [value];
	  });
	
	  // Indexes the object's values by a criterion, similar to `groupBy`, but for
	  // when you know that your index values will be unique.
	  _.indexBy = group(function(result, value, key) {
	    result[key] = value;
	  });
	
	  // Counts instances of an object that group by a certain criterion. Pass
	  // either a string attribute to count by, or a function that returns the
	  // criterion.
	  _.countBy = group(function(result, value, key) {
	    if (_.has(result, key)) result[key]++; else result[key] = 1;
	  });
	
	  // Safely create a real, live array from anything iterable.
	  _.toArray = function(obj) {
	    if (!obj) return [];
	    if (_.isArray(obj)) return slice.call(obj);
	    if (isArrayLike(obj)) return _.map(obj, _.identity);
	    return _.values(obj);
	  };
	
	  // Return the number of elements in an object.
	  _.size = function(obj) {
	    if (obj == null) return 0;
	    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
	  };
	
	  // Split a collection into two arrays: one whose elements all satisfy the given
	  // predicate, and one whose elements all do not satisfy the predicate.
	  _.partition = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var pass = [], fail = [];
	    _.each(obj, function(value, key, obj) {
	      (predicate(value, key, obj) ? pass : fail).push(value);
	    });
	    return [pass, fail];
	  };
	
	  // Array Functions
	  // ---------------
	
	  // Get the first element of an array. Passing **n** will return the first N
	  // values in the array. Aliased as `head` and `take`. The **guard** check
	  // allows it to work with `_.map`.
	  _.first = _.head = _.take = function(array, n, guard) {
	    if (array == null) return void 0;
	    if (n == null || guard) return array[0];
	    return _.initial(array, array.length - n);
	  };
	
	  // Returns everything but the last entry of the array. Especially useful on
	  // the arguments object. Passing **n** will return all the values in
	  // the array, excluding the last N.
	  _.initial = function(array, n, guard) {
	    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
	  };
	
	  // Get the last element of an array. Passing **n** will return the last N
	  // values in the array.
	  _.last = function(array, n, guard) {
	    if (array == null) return void 0;
	    if (n == null || guard) return array[array.length - 1];
	    return _.rest(array, Math.max(0, array.length - n));
	  };
	
	  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
	  // Especially useful on the arguments object. Passing an **n** will return
	  // the rest N values in the array.
	  _.rest = _.tail = _.drop = function(array, n, guard) {
	    return slice.call(array, n == null || guard ? 1 : n);
	  };
	
	  // Trim out all falsy values from an array.
	  _.compact = function(array) {
	    return _.filter(array, _.identity);
	  };
	
	  // Internal implementation of a recursive `flatten` function.
	  var flatten = function(input, shallow, strict, startIndex) {
	    var output = [], idx = 0;
	    for (var i = startIndex || 0, length = getLength(input); i < length; i++) {
	      var value = input[i];
	      if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
	        //flatten current level of array or arguments object
	        if (!shallow) value = flatten(value, shallow, strict);
	        var j = 0, len = value.length;
	        output.length += len;
	        while (j < len) {
	          output[idx++] = value[j++];
	        }
	      } else if (!strict) {
	        output[idx++] = value;
	      }
	    }
	    return output;
	  };
	
	  // Flatten out an array, either recursively (by default), or just one level.
	  _.flatten = function(array, shallow) {
	    return flatten(array, shallow, false);
	  };
	
	  // Return a version of the array that does not contain the specified value(s).
	  _.without = function(array) {
	    return _.difference(array, slice.call(arguments, 1));
	  };
	
	  // Produce a duplicate-free version of the array. If the array has already
	  // been sorted, you have the option of using a faster algorithm.
	  // Aliased as `unique`.
	  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
	    if (!_.isBoolean(isSorted)) {
	      context = iteratee;
	      iteratee = isSorted;
	      isSorted = false;
	    }
	    if (iteratee != null) iteratee = cb(iteratee, context);
	    var result = [];
	    var seen = [];
	    for (var i = 0, length = getLength(array); i < length; i++) {
	      var value = array[i],
	          computed = iteratee ? iteratee(value, i, array) : value;
	      if (isSorted) {
	        if (!i || seen !== computed) result.push(value);
	        seen = computed;
	      } else if (iteratee) {
	        if (!_.contains(seen, computed)) {
	          seen.push(computed);
	          result.push(value);
	        }
	      } else if (!_.contains(result, value)) {
	        result.push(value);
	      }
	    }
	    return result;
	  };
	
	  // Produce an array that contains the union: each distinct element from all of
	  // the passed-in arrays.
	  _.union = function() {
	    return _.uniq(flatten(arguments, true, true));
	  };
	
	  // Produce an array that contains every item shared between all the
	  // passed-in arrays.
	  _.intersection = function(array) {
	    var result = [];
	    var argsLength = arguments.length;
	    for (var i = 0, length = getLength(array); i < length; i++) {
	      var item = array[i];
	      if (_.contains(result, item)) continue;
	      for (var j = 1; j < argsLength; j++) {
	        if (!_.contains(arguments[j], item)) break;
	      }
	      if (j === argsLength) result.push(item);
	    }
	    return result;
	  };
	
	  // Take the difference between one array and a number of other arrays.
	  // Only the elements present in just the first array will remain.
	  _.difference = function(array) {
	    var rest = flatten(arguments, true, true, 1);
	    return _.filter(array, function(value){
	      return !_.contains(rest, value);
	    });
	  };
	
	  // Zip together multiple lists into a single array -- elements that share
	  // an index go together.
	  _.zip = function() {
	    return _.unzip(arguments);
	  };
	
	  // Complement of _.zip. Unzip accepts an array of arrays and groups
	  // each array's elements on shared indices
	  _.unzip = function(array) {
	    var length = array && _.max(array, getLength).length || 0;
	    var result = Array(length);
	
	    for (var index = 0; index < length; index++) {
	      result[index] = _.pluck(array, index);
	    }
	    return result;
	  };
	
	  // Converts lists into objects. Pass either a single array of `[key, value]`
	  // pairs, or two parallel arrays of the same length -- one of keys, and one of
	  // the corresponding values.
	  _.object = function(list, values) {
	    var result = {};
	    for (var i = 0, length = getLength(list); i < length; i++) {
	      if (values) {
	        result[list[i]] = values[i];
	      } else {
	        result[list[i][0]] = list[i][1];
	      }
	    }
	    return result;
	  };
	
	  // Generator function to create the findIndex and findLastIndex functions
	  function createPredicateIndexFinder(dir) {
	    return function(array, predicate, context) {
	      predicate = cb(predicate, context);
	      var length = getLength(array);
	      var index = dir > 0 ? 0 : length - 1;
	      for (; index >= 0 && index < length; index += dir) {
	        if (predicate(array[index], index, array)) return index;
	      }
	      return -1;
	    };
	  }
	
	  // Returns the first index on an array-like that passes a predicate test
	  _.findIndex = createPredicateIndexFinder(1);
	  _.findLastIndex = createPredicateIndexFinder(-1);
	
	  // Use a comparator function to figure out the smallest index at which
	  // an object should be inserted so as to maintain order. Uses binary search.
	  _.sortedIndex = function(array, obj, iteratee, context) {
	    iteratee = cb(iteratee, context, 1);
	    var value = iteratee(obj);
	    var low = 0, high = getLength(array);
	    while (low < high) {
	      var mid = Math.floor((low + high) / 2);
	      if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
	    }
	    return low;
	  };
	
	  // Generator function to create the indexOf and lastIndexOf functions
	  function createIndexFinder(dir, predicateFind, sortedIndex) {
	    return function(array, item, idx) {
	      var i = 0, length = getLength(array);
	      if (typeof idx == 'number') {
	        if (dir > 0) {
	            i = idx >= 0 ? idx : Math.max(idx + length, i);
	        } else {
	            length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
	        }
	      } else if (sortedIndex && idx && length) {
	        idx = sortedIndex(array, item);
	        return array[idx] === item ? idx : -1;
	      }
	      if (item !== item) {
	        idx = predicateFind(slice.call(array, i, length), _.isNaN);
	        return idx >= 0 ? idx + i : -1;
	      }
	      for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
	        if (array[idx] === item) return idx;
	      }
	      return -1;
	    };
	  }
	
	  // Return the position of the first occurrence of an item in an array,
	  // or -1 if the item is not included in the array.
	  // If the array is large and already in sort order, pass `true`
	  // for **isSorted** to use binary search.
	  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
	  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);
	
	  // Generate an integer Array containing an arithmetic progression. A port of
	  // the native Python `range()` function. See
	  // [the Python documentation](http://docs.python.org/library/functions.html#range).
	  _.range = function(start, stop, step) {
	    if (stop == null) {
	      stop = start || 0;
	      start = 0;
	    }
	    step = step || 1;
	
	    var length = Math.max(Math.ceil((stop - start) / step), 0);
	    var range = Array(length);
	
	    for (var idx = 0; idx < length; idx++, start += step) {
	      range[idx] = start;
	    }
	
	    return range;
	  };
	
	  // Function (ahem) Functions
	  // ------------------
	
	  // Determines whether to execute a function as a constructor
	  // or a normal function with the provided arguments
	  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
	    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
	    var self = baseCreate(sourceFunc.prototype);
	    var result = sourceFunc.apply(self, args);
	    if (_.isObject(result)) return result;
	    return self;
	  };
	
	  // Create a function bound to a given object (assigning `this`, and arguments,
	  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
	  // available.
	  _.bind = function(func, context) {
	    if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
	    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
	    var args = slice.call(arguments, 2);
	    var bound = function() {
	      return executeBound(func, bound, context, this, args.concat(slice.call(arguments)));
	    };
	    return bound;
	  };
	
	  // Partially apply a function by creating a version that has had some of its
	  // arguments pre-filled, without changing its dynamic `this` context. _ acts
	  // as a placeholder, allowing any combination of arguments to be pre-filled.
	  _.partial = function(func) {
	    var boundArgs = slice.call(arguments, 1);
	    var bound = function() {
	      var position = 0, length = boundArgs.length;
	      var args = Array(length);
	      for (var i = 0; i < length; i++) {
	        args[i] = boundArgs[i] === _ ? arguments[position++] : boundArgs[i];
	      }
	      while (position < arguments.length) args.push(arguments[position++]);
	      return executeBound(func, bound, this, this, args);
	    };
	    return bound;
	  };
	
	  // Bind a number of an object's methods to that object. Remaining arguments
	  // are the method names to be bound. Useful for ensuring that all callbacks
	  // defined on an object belong to it.
	  _.bindAll = function(obj) {
	    var i, length = arguments.length, key;
	    if (length <= 1) throw new Error('bindAll must be passed function names');
	    for (i = 1; i < length; i++) {
	      key = arguments[i];
	      obj[key] = _.bind(obj[key], obj);
	    }
	    return obj;
	  };
	
	  // Memoize an expensive function by storing its results.
	  _.memoize = function(func, hasher) {
	    var memoize = function(key) {
	      var cache = memoize.cache;
	      var address = '' + (hasher ? hasher.apply(this, arguments) : key);
	      if (!_.has(cache, address)) cache[address] = func.apply(this, arguments);
	      return cache[address];
	    };
	    memoize.cache = {};
	    return memoize;
	  };
	
	  // Delays a function for the given number of milliseconds, and then calls
	  // it with the arguments supplied.
	  _.delay = function(func, wait) {
	    var args = slice.call(arguments, 2);
	    return setTimeout(function(){
	      return func.apply(null, args);
	    }, wait);
	  };
	
	  // Defers a function, scheduling it to run after the current call stack has
	  // cleared.
	  _.defer = _.partial(_.delay, _, 1);
	
	  // Returns a function, that, when invoked, will only be triggered at most once
	  // during a given window of time. Normally, the throttled function will run
	  // as much as it can, without ever going more than once per `wait` duration;
	  // but if you'd like to disable the execution on the leading edge, pass
	  // `{leading: false}`. To disable execution on the trailing edge, ditto.
	  _.throttle = function(func, wait, options) {
	    var context, args, result;
	    var timeout = null;
	    var previous = 0;
	    if (!options) options = {};
	    var later = function() {
	      previous = options.leading === false ? 0 : _.now();
	      timeout = null;
	      result = func.apply(context, args);
	      if (!timeout) context = args = null;
	    };
	    return function() {
	      var now = _.now();
	      if (!previous && options.leading === false) previous = now;
	      var remaining = wait - (now - previous);
	      context = this;
	      args = arguments;
	      if (remaining <= 0 || remaining > wait) {
	        if (timeout) {
	          clearTimeout(timeout);
	          timeout = null;
	        }
	        previous = now;
	        result = func.apply(context, args);
	        if (!timeout) context = args = null;
	      } else if (!timeout && options.trailing !== false) {
	        timeout = setTimeout(later, remaining);
	      }
	      return result;
	    };
	  };
	
	  // Returns a function, that, as long as it continues to be invoked, will not
	  // be triggered. The function will be called after it stops being called for
	  // N milliseconds. If `immediate` is passed, trigger the function on the
	  // leading edge, instead of the trailing.
	  _.debounce = function(func, wait, immediate) {
	    var timeout, args, context, timestamp, result;
	
	    var later = function() {
	      var last = _.now() - timestamp;
	
	      if (last < wait && last >= 0) {
	        timeout = setTimeout(later, wait - last);
	      } else {
	        timeout = null;
	        if (!immediate) {
	          result = func.apply(context, args);
	          if (!timeout) context = args = null;
	        }
	      }
	    };
	
	    return function() {
	      context = this;
	      args = arguments;
	      timestamp = _.now();
	      var callNow = immediate && !timeout;
	      if (!timeout) timeout = setTimeout(later, wait);
	      if (callNow) {
	        result = func.apply(context, args);
	        context = args = null;
	      }
	
	      return result;
	    };
	  };
	
	  // Returns the first function passed as an argument to the second,
	  // allowing you to adjust arguments, run code before and after, and
	  // conditionally execute the original function.
	  _.wrap = function(func, wrapper) {
	    return _.partial(wrapper, func);
	  };
	
	  // Returns a negated version of the passed-in predicate.
	  _.negate = function(predicate) {
	    return function() {
	      return !predicate.apply(this, arguments);
	    };
	  };
	
	  // Returns a function that is the composition of a list of functions, each
	  // consuming the return value of the function that follows.
	  _.compose = function() {
	    var args = arguments;
	    var start = args.length - 1;
	    return function() {
	      var i = start;
	      var result = args[start].apply(this, arguments);
	      while (i--) result = args[i].call(this, result);
	      return result;
	    };
	  };
	
	  // Returns a function that will only be executed on and after the Nth call.
	  _.after = function(times, func) {
	    return function() {
	      if (--times < 1) {
	        return func.apply(this, arguments);
	      }
	    };
	  };
	
	  // Returns a function that will only be executed up to (but not including) the Nth call.
	  _.before = function(times, func) {
	    var memo;
	    return function() {
	      if (--times > 0) {
	        memo = func.apply(this, arguments);
	      }
	      if (times <= 1) func = null;
	      return memo;
	    };
	  };
	
	  // Returns a function that will be executed at most one time, no matter how
	  // often you call it. Useful for lazy initialization.
	  _.once = _.partial(_.before, 2);
	
	  // Object Functions
	  // ----------------
	
	  // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
	  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
	  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
	                      'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];
	
	  function collectNonEnumProps(obj, keys) {
	    var nonEnumIdx = nonEnumerableProps.length;
	    var constructor = obj.constructor;
	    var proto = (_.isFunction(constructor) && constructor.prototype) || ObjProto;
	
	    // Constructor is a special case.
	    var prop = 'constructor';
	    if (_.has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);
	
	    while (nonEnumIdx--) {
	      prop = nonEnumerableProps[nonEnumIdx];
	      if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
	        keys.push(prop);
	      }
	    }
	  }
	
	  // Retrieve the names of an object's own properties.
	  // Delegates to **ECMAScript 5**'s native `Object.keys`
	  _.keys = function(obj) {
	    if (!_.isObject(obj)) return [];
	    if (nativeKeys) return nativeKeys(obj);
	    var keys = [];
	    for (var key in obj) if (_.has(obj, key)) keys.push(key);
	    // Ahem, IE < 9.
	    if (hasEnumBug) collectNonEnumProps(obj, keys);
	    return keys;
	  };
	
	  // Retrieve all the property names of an object.
	  _.allKeys = function(obj) {
	    if (!_.isObject(obj)) return [];
	    var keys = [];
	    for (var key in obj) keys.push(key);
	    // Ahem, IE < 9.
	    if (hasEnumBug) collectNonEnumProps(obj, keys);
	    return keys;
	  };
	
	  // Retrieve the values of an object's properties.
	  _.values = function(obj) {
	    var keys = _.keys(obj);
	    var length = keys.length;
	    var values = Array(length);
	    for (var i = 0; i < length; i++) {
	      values[i] = obj[keys[i]];
	    }
	    return values;
	  };
	
	  // Returns the results of applying the iteratee to each element of the object
	  // In contrast to _.map it returns an object
	  _.mapObject = function(obj, iteratee, context) {
	    iteratee = cb(iteratee, context);
	    var keys =  _.keys(obj),
	          length = keys.length,
	          results = {},
	          currentKey;
	      for (var index = 0; index < length; index++) {
	        currentKey = keys[index];
	        results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
	      }
	      return results;
	  };
	
	  // Convert an object into a list of `[key, value]` pairs.
	  _.pairs = function(obj) {
	    var keys = _.keys(obj);
	    var length = keys.length;
	    var pairs = Array(length);
	    for (var i = 0; i < length; i++) {
	      pairs[i] = [keys[i], obj[keys[i]]];
	    }
	    return pairs;
	  };
	
	  // Invert the keys and values of an object. The values must be serializable.
	  _.invert = function(obj) {
	    var result = {};
	    var keys = _.keys(obj);
	    for (var i = 0, length = keys.length; i < length; i++) {
	      result[obj[keys[i]]] = keys[i];
	    }
	    return result;
	  };
	
	  // Return a sorted list of the function names available on the object.
	  // Aliased as `methods`
	  _.functions = _.methods = function(obj) {
	    var names = [];
	    for (var key in obj) {
	      if (_.isFunction(obj[key])) names.push(key);
	    }
	    return names.sort();
	  };
	
	  // Extend a given object with all the properties in passed-in object(s).
	  _.extend = createAssigner(_.allKeys);
	
	  // Assigns a given object with all the own properties in the passed-in object(s)
	  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
	  _.extendOwn = _.assign = createAssigner(_.keys);
	
	  // Returns the first key on an object that passes a predicate test
	  _.findKey = function(obj, predicate, context) {
	    predicate = cb(predicate, context);
	    var keys = _.keys(obj), key;
	    for (var i = 0, length = keys.length; i < length; i++) {
	      key = keys[i];
	      if (predicate(obj[key], key, obj)) return key;
	    }
	  };
	
	  // Return a copy of the object only containing the whitelisted properties.
	  _.pick = function(object, oiteratee, context) {
	    var result = {}, obj = object, iteratee, keys;
	    if (obj == null) return result;
	    if (_.isFunction(oiteratee)) {
	      keys = _.allKeys(obj);
	      iteratee = optimizeCb(oiteratee, context);
	    } else {
	      keys = flatten(arguments, false, false, 1);
	      iteratee = function(value, key, obj) { return key in obj; };
	      obj = Object(obj);
	    }
	    for (var i = 0, length = keys.length; i < length; i++) {
	      var key = keys[i];
	      var value = obj[key];
	      if (iteratee(value, key, obj)) result[key] = value;
	    }
	    return result;
	  };
	
	   // Return a copy of the object without the blacklisted properties.
	  _.omit = function(obj, iteratee, context) {
	    if (_.isFunction(iteratee)) {
	      iteratee = _.negate(iteratee);
	    } else {
	      var keys = _.map(flatten(arguments, false, false, 1), String);
	      iteratee = function(value, key) {
	        return !_.contains(keys, key);
	      };
	    }
	    return _.pick(obj, iteratee, context);
	  };
	
	  // Fill in a given object with default properties.
	  _.defaults = createAssigner(_.allKeys, true);
	
	  // Creates an object that inherits from the given prototype object.
	  // If additional properties are provided then they will be added to the
	  // created object.
	  _.create = function(prototype, props) {
	    var result = baseCreate(prototype);
	    if (props) _.extendOwn(result, props);
	    return result;
	  };
	
	  // Create a (shallow-cloned) duplicate of an object.
	  _.clone = function(obj) {
	    if (!_.isObject(obj)) return obj;
	    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
	  };
	
	  // Invokes interceptor with the obj, and then returns obj.
	  // The primary purpose of this method is to "tap into" a method chain, in
	  // order to perform operations on intermediate results within the chain.
	  _.tap = function(obj, interceptor) {
	    interceptor(obj);
	    return obj;
	  };
	
	  // Returns whether an object has a given set of `key:value` pairs.
	  _.isMatch = function(object, attrs) {
	    var keys = _.keys(attrs), length = keys.length;
	    if (object == null) return !length;
	    var obj = Object(object);
	    for (var i = 0; i < length; i++) {
	      var key = keys[i];
	      if (attrs[key] !== obj[key] || !(key in obj)) return false;
	    }
	    return true;
	  };
	
	
	  // Internal recursive comparison function for `isEqual`.
	  var eq = function(a, b, aStack, bStack) {
	    // Identical objects are equal. `0 === -0`, but they aren't identical.
	    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
	    if (a === b) return a !== 0 || 1 / a === 1 / b;
	    // A strict comparison is necessary because `null == undefined`.
	    if (a == null || b == null) return a === b;
	    // Unwrap any wrapped objects.
	    if (a instanceof _) a = a._wrapped;
	    if (b instanceof _) b = b._wrapped;
	    // Compare `[[Class]]` names.
	    var className = toString.call(a);
	    if (className !== toString.call(b)) return false;
	    switch (className) {
	      // Strings, numbers, regular expressions, dates, and booleans are compared by value.
	      case '[object RegExp]':
	      // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
	      case '[object String]':
	        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
	        // equivalent to `new String("5")`.
	        return '' + a === '' + b;
	      case '[object Number]':
	        // `NaN`s are equivalent, but non-reflexive.
	        // Object(NaN) is equivalent to NaN
	        if (+a !== +a) return +b !== +b;
	        // An `egal` comparison is performed for other numeric values.
	        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
	      case '[object Date]':
	      case '[object Boolean]':
	        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
	        // millisecond representations. Note that invalid dates with millisecond representations
	        // of `NaN` are not equivalent.
	        return +a === +b;
	    }
	
	    var areArrays = className === '[object Array]';
	    if (!areArrays) {
	      if (typeof a != 'object' || typeof b != 'object') return false;
	
	      // Objects with different constructors are not equivalent, but `Object`s or `Array`s
	      // from different frames are.
	      var aCtor = a.constructor, bCtor = b.constructor;
	      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
	                               _.isFunction(bCtor) && bCtor instanceof bCtor)
	                          && ('constructor' in a && 'constructor' in b)) {
	        return false;
	      }
	    }
	    // Assume equality for cyclic structures. The algorithm for detecting cyclic
	    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
	
	    // Initializing stack of traversed objects.
	    // It's done here since we only need them for objects and arrays comparison.
	    aStack = aStack || [];
	    bStack = bStack || [];
	    var length = aStack.length;
	    while (length--) {
	      // Linear search. Performance is inversely proportional to the number of
	      // unique nested structures.
	      if (aStack[length] === a) return bStack[length] === b;
	    }
	
	    // Add the first object to the stack of traversed objects.
	    aStack.push(a);
	    bStack.push(b);
	
	    // Recursively compare objects and arrays.
	    if (areArrays) {
	      // Compare array lengths to determine if a deep comparison is necessary.
	      length = a.length;
	      if (length !== b.length) return false;
	      // Deep compare the contents, ignoring non-numeric properties.
	      while (length--) {
	        if (!eq(a[length], b[length], aStack, bStack)) return false;
	      }
	    } else {
	      // Deep compare objects.
	      var keys = _.keys(a), key;
	      length = keys.length;
	      // Ensure that both objects contain the same number of properties before comparing deep equality.
	      if (_.keys(b).length !== length) return false;
	      while (length--) {
	        // Deep compare each member
	        key = keys[length];
	        if (!(_.has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
	      }
	    }
	    // Remove the first object from the stack of traversed objects.
	    aStack.pop();
	    bStack.pop();
	    return true;
	  };
	
	  // Perform a deep comparison to check if two objects are equal.
	  _.isEqual = function(a, b) {
	    return eq(a, b);
	  };
	
	  // Is a given array, string, or object empty?
	  // An "empty" object has no enumerable own-properties.
	  _.isEmpty = function(obj) {
	    if (obj == null) return true;
	    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
	    return _.keys(obj).length === 0;
	  };
	
	  // Is a given value a DOM element?
	  _.isElement = function(obj) {
	    return !!(obj && obj.nodeType === 1);
	  };
	
	  // Is a given value an array?
	  // Delegates to ECMA5's native Array.isArray
	  _.isArray = nativeIsArray || function(obj) {
	    return toString.call(obj) === '[object Array]';
	  };
	
	  // Is a given variable an object?
	  _.isObject = function(obj) {
	    var type = typeof obj;
	    return type === 'function' || type === 'object' && !!obj;
	  };
	
	  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError.
	  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
	    _['is' + name] = function(obj) {
	      return toString.call(obj) === '[object ' + name + ']';
	    };
	  });
	
	  // Define a fallback version of the method in browsers (ahem, IE < 9), where
	  // there isn't any inspectable "Arguments" type.
	  if (!_.isArguments(arguments)) {
	    _.isArguments = function(obj) {
	      return _.has(obj, 'callee');
	    };
	  }
	
	  // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
	  // IE 11 (#1621), and in Safari 8 (#1929).
	  if (typeof /./ != 'function' && typeof Int8Array != 'object') {
	    _.isFunction = function(obj) {
	      return typeof obj == 'function' || false;
	    };
	  }
	
	  // Is a given object a finite number?
	  _.isFinite = function(obj) {
	    return isFinite(obj) && !isNaN(parseFloat(obj));
	  };
	
	  // Is the given value `NaN`? (NaN is the only number which does not equal itself).
	  _.isNaN = function(obj) {
	    return _.isNumber(obj) && obj !== +obj;
	  };
	
	  // Is a given value a boolean?
	  _.isBoolean = function(obj) {
	    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
	  };
	
	  // Is a given value equal to null?
	  _.isNull = function(obj) {
	    return obj === null;
	  };
	
	  // Is a given variable undefined?
	  _.isUndefined = function(obj) {
	    return obj === void 0;
	  };
	
	  // Shortcut function for checking if an object has a given property directly
	  // on itself (in other words, not on a prototype).
	  _.has = function(obj, key) {
	    return obj != null && hasOwnProperty.call(obj, key);
	  };
	
	  // Utility Functions
	  // -----------------
	
	  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
	  // previous owner. Returns a reference to the Underscore object.
	  _.noConflict = function() {
	    root._ = previousUnderscore;
	    return this;
	  };
	
	  // Keep the identity function around for default iteratees.
	  _.identity = function(value) {
	    return value;
	  };
	
	  // Predicate-generating functions. Often useful outside of Underscore.
	  _.constant = function(value) {
	    return function() {
	      return value;
	    };
	  };
	
	  _.noop = function(){};
	
	  _.property = property;
	
	  // Generates a function for a given object that returns a given property.
	  _.propertyOf = function(obj) {
	    return obj == null ? function(){} : function(key) {
	      return obj[key];
	    };
	  };
	
	  // Returns a predicate for checking whether an object has a given set of
	  // `key:value` pairs.
	  _.matcher = _.matches = function(attrs) {
	    attrs = _.extendOwn({}, attrs);
	    return function(obj) {
	      return _.isMatch(obj, attrs);
	    };
	  };
	
	  // Run a function **n** times.
	  _.times = function(n, iteratee, context) {
	    var accum = Array(Math.max(0, n));
	    iteratee = optimizeCb(iteratee, context, 1);
	    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
	    return accum;
	  };
	
	  // Return a random integer between min and max (inclusive).
	  _.random = function(min, max) {
	    if (max == null) {
	      max = min;
	      min = 0;
	    }
	    return min + Math.floor(Math.random() * (max - min + 1));
	  };
	
	  // A (possibly faster) way to get the current timestamp as an integer.
	  _.now = Date.now || function() {
	    return new Date().getTime();
	  };
	
	   // List of HTML entities for escaping.
	  var escapeMap = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#x27;',
	    '`': '&#x60;'
	  };
	  var unescapeMap = _.invert(escapeMap);
	
	  // Functions for escaping and unescaping strings to/from HTML interpolation.
	  var createEscaper = function(map) {
	    var escaper = function(match) {
	      return map[match];
	    };
	    // Regexes for identifying a key that needs to be escaped
	    var source = '(?:' + _.keys(map).join('|') + ')';
	    var testRegexp = RegExp(source);
	    var replaceRegexp = RegExp(source, 'g');
	    return function(string) {
	      string = string == null ? '' : '' + string;
	      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
	    };
	  };
	  _.escape = createEscaper(escapeMap);
	  _.unescape = createEscaper(unescapeMap);
	
	  // If the value of the named `property` is a function then invoke it with the
	  // `object` as context; otherwise, return it.
	  _.result = function(object, property, fallback) {
	    var value = object == null ? void 0 : object[property];
	    if (value === void 0) {
	      value = fallback;
	    }
	    return _.isFunction(value) ? value.call(object) : value;
	  };
	
	  // Generate a unique integer id (unique within the entire client session).
	  // Useful for temporary DOM ids.
	  var idCounter = 0;
	  _.uniqueId = function(prefix) {
	    var id = ++idCounter + '';
	    return prefix ? prefix + id : id;
	  };
	
	  // By default, Underscore uses ERB-style template delimiters, change the
	  // following template settings to use alternative delimiters.
	  _.templateSettings = {
	    evaluate    : /<%([\s\S]+?)%>/g,
	    interpolate : /<%=([\s\S]+?)%>/g,
	    escape      : /<%-([\s\S]+?)%>/g
	  };
	
	  // When customizing `templateSettings`, if you don't want to define an
	  // interpolation, evaluation or escaping regex, we need one that is
	  // guaranteed not to match.
	  var noMatch = /(.)^/;
	
	  // Certain characters need to be escaped so that they can be put into a
	  // string literal.
	  var escapes = {
	    "'":      "'",
	    '\\':     '\\',
	    '\r':     'r',
	    '\n':     'n',
	    '\u2028': 'u2028',
	    '\u2029': 'u2029'
	  };
	
	  var escaper = /\\|'|\r|\n|\u2028|\u2029/g;
	
	  var escapeChar = function(match) {
	    return '\\' + escapes[match];
	  };
	
	  // JavaScript micro-templating, similar to John Resig's implementation.
	  // Underscore templating handles arbitrary delimiters, preserves whitespace,
	  // and correctly escapes quotes within interpolated code.
	  // NB: `oldSettings` only exists for backwards compatibility.
	  _.template = function(text, settings, oldSettings) {
	    if (!settings && oldSettings) settings = oldSettings;
	    settings = _.defaults({}, settings, _.templateSettings);
	
	    // Combine delimiters into one regular expression via alternation.
	    var matcher = RegExp([
	      (settings.escape || noMatch).source,
	      (settings.interpolate || noMatch).source,
	      (settings.evaluate || noMatch).source
	    ].join('|') + '|$', 'g');
	
	    // Compile the template source, escaping string literals appropriately.
	    var index = 0;
	    var source = "__p+='";
	    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
	      source += text.slice(index, offset).replace(escaper, escapeChar);
	      index = offset + match.length;
	
	      if (escape) {
	        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
	      } else if (interpolate) {
	        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
	      } else if (evaluate) {
	        source += "';\n" + evaluate + "\n__p+='";
	      }
	
	      // Adobe VMs need the match returned to produce the correct offest.
	      return match;
	    });
	    source += "';\n";
	
	    // If a variable is not specified, place data values in local scope.
	    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';
	
	    source = "var __t,__p='',__j=Array.prototype.join," +
	      "print=function(){__p+=__j.call(arguments,'');};\n" +
	      source + 'return __p;\n';
	
	    try {
	      var render = new Function(settings.variable || 'obj', '_', source);
	    } catch (e) {
	      e.source = source;
	      throw e;
	    }
	
	    var template = function(data) {
	      return render.call(this, data, _);
	    };
	
	    // Provide the compiled source as a convenience for precompilation.
	    var argument = settings.variable || 'obj';
	    template.source = 'function(' + argument + '){\n' + source + '}';
	
	    return template;
	  };
	
	  // Add a "chain" function. Start chaining a wrapped Underscore object.
	  _.chain = function(obj) {
	    var instance = _(obj);
	    instance._chain = true;
	    return instance;
	  };
	
	  // OOP
	  // ---------------
	  // If Underscore is called as a function, it returns a wrapped object that
	  // can be used OO-style. This wrapper holds altered versions of all the
	  // underscore functions. Wrapped objects may be chained.
	
	  // Helper function to continue chaining intermediate results.
	  var result = function(instance, obj) {
	    return instance._chain ? _(obj).chain() : obj;
	  };
	
	  // Add your own custom functions to the Underscore object.
	  _.mixin = function(obj) {
	    _.each(_.functions(obj), function(name) {
	      var func = _[name] = obj[name];
	      _.prototype[name] = function() {
	        var args = [this._wrapped];
	        push.apply(args, arguments);
	        return result(this, func.apply(_, args));
	      };
	    });
	  };
	
	  // Add all of the Underscore functions to the wrapper object.
	  _.mixin(_);
	
	  // Add all mutator Array functions to the wrapper.
	  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
	    var method = ArrayProto[name];
	    _.prototype[name] = function() {
	      var obj = this._wrapped;
	      method.apply(obj, arguments);
	      if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
	      return result(this, obj);
	    };
	  });
	
	  // Add all accessor Array functions to the wrapper.
	  _.each(['concat', 'join', 'slice'], function(name) {
	    var method = ArrayProto[name];
	    _.prototype[name] = function() {
	      return result(this, method.apply(this._wrapped, arguments));
	    };
	  });
	
	  // Extracts the result from a wrapped and chained object.
	  _.prototype.value = function() {
	    return this._wrapped;
	  };
	
	  // Provide unwrapping proxy for some methods used in engine operations
	  // such as arithmetic and JSON stringification.
	  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;
	
	  _.prototype.toString = function() {
	    return '' + this._wrapped;
	  };
	
	  // AMD registration happens at the end for compatibility with AMD loaders
	  // that may not enforce next-turn semantics on modules. Even though general
	  // practice for AMD registration is to be anonymous, underscore registers
	  // as a named module because, like jQuery, it is a base library that is
	  // popular enough to be bundled in a third party lib, but not be part of
	  // an AMD load request. Those cases could generate an error when an
	  // anonymous define() is called outside of a loader request.
	  if (true) {
	    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
	      return _;
	    }.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  }
	}.call(this));


/***/ },

/***/ 376:
/***/ function(module, exports) {

	module.exports = "<section class=\"inner-slider\">\n    <img class=\"img-full\" src=\"{{categories.image}}\"/>   \n</section> \n\n<section class=\"listing-breadcrumb\">\n    <div class=\"container\">\n        <div class=\"row div-hide-on-large-only\">\n            <div class=\"col m6 s6 center-align\">\n                <a href=\"#modalFilter\">\n                    <i class=\"fa fa-filter fa_filter\"></i>\n                    FILTERS </a>\n            </div>\n            <div class=\"col m6 s6 center-align\">\n                <a href=\"#modalSort\" >\n                    <i class=\"fa fa-sort fa_sort_by\" ></i>\n                    SORT BY \n                </a>\n            </div>\n\n\n\n        </div>\n        <div class=\"row \" >\n            <div class=\"col m6 s12 div-hide-on-med-and-down\">\n                <div class=\"sort-by\">\n                    <ul>\n                        <li><b>Sort By:</b></li>\n                        <li [ngClass]=\"{active:sortBy == 'popular'}\" ><a  (click)=\"setsortBy('popular')\"> Popular</a></li>\n                        <li [ngClass]=\"{active:sortBy =='new'}\" ><a  (click)=\"setsortBy('new')\"> New</a></li>\n                        <li [ngClass]=\"{active:sortBy =='price_high'}\" ><a  (click)=\"setsortBy('price_high')\">Price High</a></li>\n                        <li [ngClass]=\"{active:sortBy =='price_low'}\" ><a  (click)=\"setsortBy('price_low')\">Price Low</a></li>\n                        <div class=\"clearfix\"></div>\n                    </ul>\n                </div>\n\n            </div>\n            <div id=\"modalSort\" class=\"modal div-hide-on-large-only\">\n                <div class=\"container\">\n                    <div class=\"row\">\n                        <div class=\"col s12\"><span class=\"modal-action modal-close\" ><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span></div>\n                        <div class=\"col s12\">\n\n                            <ul>\n                                <li><b>SORT BY</b></li>\n                                <li [ngClass]=\"{active:sortBy == 'popular'}\" ><a  (click)=\"setsortBy('popular')\"> Popular</a></li>\n                                <li [ngClass]=\"{active:sortBy =='new'}\" ><a  (click)=\"setsortBy('new')\"> New</a></li>\n                                <li [ngClass]=\"{active:sortBy =='price_low'}\" ><a  (click)=\"setsortBy('price_low')\">  Price : Low to High</a></li>\n                                <li [ngClass]=\"{active:sortBy =='price_high'}\" ><a  (click)=\"setsortBy('price_high')\">Price : High to Low</a></li>\n                                <div class=\"clearfix\"></div>\n                            </ul>\n\n\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col m6 s12 pagination_full\">\n\n\n\n                <!-- pager -->\n                <ul  *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination right\">\n                    <!--<li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                        <a (click)=\"setPage(1)\">First</a>\n                    </li> -->\n                    <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                        <a  (click)=\"setPage(pager.currentPage - 1)\"><i class=\"material-icons\">chevron_left</i></a>\n                    </li>\n                    <li class=\"waves-effect\"  *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                        <a (click)=\"setPage(page)\">{{page}}</a>\n                    </li>\n                    <li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                        <a (click)=\"setPage(pager.currentPage + 1)\"><i class=\"material-icons\">chevron_right</i></a>\n                    </li>\n                    <!--\n                    <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                        <a (click)=\"setPage(pager.totalPages)\">Last</a>\n                    </li> -->\n                </ul>\n            </div> \n        </div>\n    </div>  \n</section>\n\n<section class=\"product-category\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m6 s6\">\n                <h5>PRODUCT CATEGORY</h5>\n            </div>\n            <div class=\"col m6 s6\">\n                <div class=\"product-category-breadcrumb right\">\n                    <ul>\n                        <li ><a [routerLink]=\"['/']\" >Home</a></li>\n                        <li class=\"disabled\"><a >{{categories['name'] | capitalize}}</a></li>\n                    </ul> \n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n<section class=\"filters-product\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m4 s6 div-hide-on-med-and-down\">\n\n                <div class=\"filters-list filters-list-2\">\n                    <ul class=\"collapsible\" data-collapsible=\"accordion\">\n                        <li *ngFor=\"let filter of filters\">\n                            <div class=\"collapsible-header\" [ngClass]=\"{show_bullet:attributes.indexOf(filter.slug) > -1}\">{{filter.name | capitalize}} <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                            </div>\n                            <div class=\"collapsible-body\">\n                                <ul>\n                                    <li *ngFor=\"let attribute of filter.attribute_value\">\n                                        <a [ngClass]=\"{active:valuses.indexOf(attribute.slug) > -1}\" (click)=\"setfilter(filter.slug,attribute.slug)\">\n                                            {{attribute.name | capitalize}}\n                                        </a>\n                                    </li>\n\n                                </ul>\n                            </div>\n                        </li>\n\n                    </ul>\n                </div>\n            </div>\n\n\n            <div id=\"modalFilter\" class=\"modal div-hide-on-large-only\">\n                <div class=\"container\">\n                    <div class=\"row\">\n                        <div class=\"col s12\"><span class=\"modal-action modal-close\" ><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span></div>\n                        <div class=\"col s12\">\n\n                            <div class=\"filters-list filters-list-2\">\n                                <ul class=\"collapsible\" data-collapsible=\"accordion\">\n                                    <li *ngFor=\"let filter of filters\">\n                                        <div class=\"collapsible-header\" [ngClass]=\"{show_bullet:attributes.indexOf(filter.slug) > -1}\">{{filter.name | capitalize}} <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                                        </div>\n                                        <div class=\"collapsible-body\">\n                                            <ul>\n                                                <li *ngFor=\"let attribute of filter.attribute_value\">\n                                                    <a [ngClass]=\"{active:valuses.indexOf(attribute.slug) > -1}\" (click)=\"setfilter(filter.slug,attribute.slug)\">\n                                                        {{attribute.name | capitalize}}\n                                                    </a>\n                                                </li>\n\n                                            </ul>\n                                        </div>\n                                    </li>\n\n                                </ul>\n                            </div>\n\n\n\n                        </div>\n\n                        <div class=\"col s12\">\n                            <div class=\"row\">\n\n                                <div class=\"col m6 s6 center-align\">\n                                    <a  onclick=\"clearFilter()\" class=\"clear_filter waves-effect fabivo-btn btn\"> CLEAR \n                                    </a>\n                                </div>\n\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n\n            <div class=\"col m8 s6\">\n                <div class=\"category-content\">\n                    <div class=\"row\">\n                        <div class=\"col m4 s6 \" *ngFor=\"let product of products.data\">\n                             <div class=\"product-bx card-panel hoverable \">\n                                <div class=\"product-img\">\n                                    <a [routerLink]=\"['../../product',product.slug]\">\n                                        <img class=\"responsive-img\" src=\"{{product.image}}\">\n                                    </a>\n\n                                    <a *ngIf=\"cartService.wishlistArray.indexOf(product.id) > -1\" class=\"is_wishlist\" href=\"javascript:void(0);\"   ><i  class=\"fa fa-heart\" aria-hidden=\"true\" (click)=\"cartService.removeToWishlist(product.id)\"></i></a> \n                                    <a *ngIf=\"cartService.wishlistArray.indexOf(product.id) ==-1\" class=\"is_wishlist\" href=\"javascript:void(0);\"  ><i class=\"fa fa-heart-o\" aria-hidden=\"true\" (click)=\"cartService.addToWishlist(product.id)\" ></i></a>\n\n\n\n\n\n\n                                </div>\n                                <div class=\"product-details center-align\">\n                                    <a [routerLink]=\"['../../product',product.slug]\">\n                                        <h5>{{product.title}}</h5>\n                                        <!--                                     <h6 [innerHTML]=\"product.description\"></h6>-->\n                                        <div class=\"prize\">\n                                            <!--                                                             <del>&#x20B9; 1500</del>-->\n                                            <span [innerHTML]=\"product.price\"></span>\n                                        </div>\n                                    </a>\n                                    <div class=\"clearfix\"></div>\n                                    <!--                                    <a [routerLink]=\"['../../product',product.slug]\" class=\"waves-effect btn fabivo-btn-3\">\n                                                                            Shop now\n                                                                        </a>-->\n                                </div> \n                            </div>\n\n\n                        </div>\n                        <div class=\"emptyPage\" *ngIf=\"products.total==0\" style=\"\"> \n                            <div><i class=\"fa fa-search\" aria-hidden=\"true\"></i></div> \n                            <div>No Match Found</div> \n                        </div>\n\n                    </div>\n\n                </div>\n            </div>\n        </div>\n    </div>\n</section>"

/***/ },

/***/ 377:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var platform_browser_1 = __webpack_require__(21);
	var router_1 = __webpack_require__(361);
	var product_service_1 = __webpack_require__(373);
	var cart_service_1 = __webpack_require__(370);
	var authentication_service_1 = __webpack_require__(369);
	var global_1 = __webpack_require__(363);
	var page_service_1 = __webpack_require__(33);
	var user_service_1 = __webpack_require__(378);
	var ProductDetailComponent = (function () {
	    function ProductDetailComponent(productService, cartService, pageService, route, authenticationService, titleService, sanitizer, userService) {
	        this.productService = productService;
	        this.cartService = cartService;
	        this.pageService = pageService;
	        this.route = route;
	        this.authenticationService = authenticationService;
	        this.titleService = titleService;
	        this.sanitizer = sanitizer;
	        this.userService = userService;
	        this.productDetail = [];
	        this.category = [];
	        this.product = [];
	        this.add_product = {};
	        this.images = [];
	        this.thumbnail = [];
	        this.settings = [];
	        this.authDatas = [];
	        this.select_size = '';
	        this.repeat_size = '';
	        this.size_error = false;
	        this.success = '';
	        this.error = '';
	        this.pinerror = 0;
	        this.add_wishlist = {};
	        this.productWishlist = [];
	        this.wish_flag = true;
	        this.loading = false;
	        this.sizes = '';
	        this.toggalFlage = 0;
	        this.pincode = '';
	        this.pincode_data = [];
	        this.refreshFlag = 0;
	        this.currentSlider = 0;
	        this.repoUrl = '';
	        this.imageUrl = '';
	        this.site_title = '';
	        this.whatsapp = '';
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	    }
	    ProductDetailComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        global_1.modalBlockUI();
	        this.route.params.subscribe(function (params) {
	            var slug = params['slug'];
	            var userId = '';
	            if (_this.authenticationService.isLoggedIn()) {
	                userId = _this.authDatas.id;
	            }
	            var url = window.location.href.toString();
	            _this.repoUrl = url.split("?")[0];
	            _this.productService.getProductDetail(slug, userId).subscribe(function (products) {
	                global_1.modalBlockUI();
	                _this.refreshFlag = 0;
	                _this.select_size = '';
	                _this.size_error = false;
	                jQuery("html, body").animate({ scrollTop: 10 }, "slow");
	                _this.productDetail = products;
	                _this.product = products['product'];
	                _this.images = products['images'];
	                _this.category = products['category'];
	                jQuery('meta[property="og:url"]').attr('content', _this.repoUrl);
	                jQuery('meta[property="og:title"]').attr('content', products['product']['title']);
	                jQuery('meta[property="og:description"]').attr('content', products['product']['description']);
	                jQuery('meta[name="title"]').attr('content', products['product']['meta_title']);
	                jQuery('meta[name="description"]').attr('content', products['product']['meta_description']);
	                jQuery('meta[name="keywords"]').attr('content', products['product']['meta_keywords']);
	                if (products['no_images'] == '') {
	                    jQuery('meta[property="og:image"]').attr('content', products['images'][0]['orignail_image']);
	                    _this.imageUrl = products['images'][0]['orignail_image'];
	                }
	                else {
	                    jQuery('meta[property="og:image"]').attr('content', products['no_images']);
	                    _this.imageUrl = products['no_images'];
	                }
	                var wurl = 'whatsapp://send?text=' + products['product']['title'] + ' : ' + _this.repoUrl;
	                _this.whatsapp = _this.sanitize(wurl);
	                _this.titleService.setTitle(products['product']['title']);
	                _this.thumbnail = products['thumbnail'];
	                _this.sizes = products['size'];
	                if (_this.product['in_wishlist'] == 1) {
	                    _this.wish_flag = false;
	                }
	                jQuery(document).ready(function () {
	                    if (jQuery('.slider-for').hasClass('slick-initialized')) {
	                        jQuery('.slider-for').slick('unslick');
	                        ;
	                    }
	                    if (jQuery('.slider-nav').hasClass('slick-initialized')) {
	                        jQuery('.slider-nav').slick('unslick');
	                    }
	                    if (jQuery('.slider-for-zoom').hasClass('slick-initialized')) {
	                        jQuery('.slider-for-zoom').slick('unslick');
	                    }
	                });
	                jQuery(document).scroll(function () {
	                    if (jQuery(window).width() < 641) {
	                        if (jQuery('#sticker').offset().top + jQuery('#sticker').height() >= jQuery('#description').offset().top - 5) {
	                            jQuery('#sticker').removeClass('fixed_div');
	                        }
	                        if (jQuery(document).scrollTop() + window.innerHeight < jQuery('#description').offset().top) {
	                            jQuery('#sticker').addClass('fixed_div');
	                        }
	                    }
	                    else {
	                        jQuery('#sticker').removeClass('fixed_div');
	                    }
	                });
	                jQuery(window).resize(function () {
	                    if (jQuery(window).width() < 641) {
	                        if (jQuery('#sticker').offset().top + jQuery('#sticker').height() >= jQuery('#description').offset().top - 5) {
	                            jQuery('#sticker').removeClass('fixed_div');
	                        }
	                        if (jQuery(document).scrollTop() + window.innerHeight < jQuery('#description').offset().top) {
	                            jQuery('#sticker').addClass('fixed_div');
	                        }
	                    }
	                    else {
	                        jQuery('#sticker').removeClass('fixed_div');
	                    }
	                });
	                if (products['images']) {
	                    jQuery(document).ready(function () {
	                        var _this = this;
	                        setTimeout(function () {
	                            jQuery('.slider-for').slick({
	                                slidesToShow: 1,
	                                slidesToScroll: 1,
	                                lazyLoad: 'ondemand',
	                                speed: 500,
	                                arrows: true,
	                                fade: true,
	                                asNavFor: '.slider-nav',
	                                mobileFirst: true,
	                                responsive: [
	                                    {
	                                        breakpoint: 1024,
	                                        settings: {
	                                            arrows: false,
	                                        }
	                                    },
	                                    {
	                                        breakpoint: 768,
	                                        settings: {
	                                            arrows: false,
	                                        }
	                                    },
	                                    {
	                                        breakpoint: 767,
	                                        settings: {
	                                            arrows: true,
	                                        }
	                                    },
	                                    {
	                                        breakpoint: 360,
	                                        settings: {
	                                            arrows: true,
	                                        }
	                                    }
	                                ]
	                            });
	                            var self = _this;
	                            jQuery('.slider-nav').slick({
	                                slidesToShow: 5,
	                                slidesToScroll: 1,
	                                speed: 500,
	                                asNavFor: '.slider-for',
	                                dots: false,
	                                centerMode: false,
	                                focusOnSelect: true,
	                                slide: 'div',
	                                arrows: false,
	                                variableWidth: false,
	                                vertical: true,
	                                verticalSwiping: true,
	                                waitForAnimate: true,
	                                zIndex: 1000,
	                                slideWidth: '400',
	                                swipe: false,
	                                adaptiveHeight: true,
	                            });
	                            jQuery('.slider-for-zoom').slick({
	                                slidesToShow: 1,
	                                slidesToScroll: 1,
	                                speed: 500,
	                                arrows: true,
	                                fade: true,
	                                mobileFirst: true,
	                                dots: true,
	                            });
	                            jQuery("#currnt_slide").val(0);
	                            jQuery('.slider-for').on('afterChange', function (event, slick, currentSlide, nextSlide) {
	                                jQuery(".slider-for-zoom").slick('slickGoTo', currentSlide);
	                                jQuery("#currnt_slide").val(currentSlide);
	                            });
	                            if (jQuery('#modal4 table').length) {
	                                jQuery('#modal4 table').addClass("bordered centered striped responsive-table");
	                            }
	                        }, 1000);
	                    });
	                }
	                if (_this.productDetail['related_product']) {
	                    jQuery(document).ready(function () {
	                        if (typeof jQuery('#owl-demo').data('owlCarousel') != 'undefined') {
	                            jQuery('#owl-demo').data('owlCarousel').destroy();
	                            jQuery('#owl-demo').removeClass('owl-carousel');
	                        }
	                        setTimeout(function () {
	                            jQuery("#owl-demo").owlCarousel({
	                                autoPlay: 3000,
	                                items: 4,
	                                loop: true,
	                                itemsDesktop: [1199, 3],
	                                itemsDesktopSmall: [979, 3]
	                            });
	                            jQuery(".next").click(function () {
	                                jQuery("#owl-demo").trigger('owl.next');
	                            });
	                            jQuery(".prev").click(function () {
	                                jQuery("#owl-demo").trigger('owl.prev');
	                            });
	                        }, 1000);
	                    });
	                }
	                setTimeout(function () {
	                    global_1.modalUnBlockUI();
	                }, 2000);
	            }, function (err) {
	                console.log(err);
	            });
	        });
	        this.pageService.getSettings().subscribe(function (settings) {
	            _this.settings = settings;
	            _this.site_title = settings['CONFIG_SITE_TITLE'];
	            setTimeout(function () {
	                jQuery(document).ready(function () {
	                    jQuery('.tabs').tabs();
	                    jQuery('.modal').modal();
	                });
	            }, 1000);
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    ProductDetailComponent.prototype.setSize = function (size) {
	        this.select_size = size;
	        this.size_error = false;
	    };
	    ProductDetailComponent.prototype.sanitize = function (url) {
	        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	    };
	    ProductDetailComponent.prototype.addToCart = function (choose_size) {
	        var _this = this;
	        if (this.authenticationService.isLoggedIn()) {
	            if (this.select_size === '') {
	                this.size_error = true;
	            }
	            else {
	                global_1.blockUI();
	                this.add_product['user_id'] = this.authDatas.id;
	                this.add_product['product_id'] = this.product.id;
	                this.add_product['product_size'] = this.select_size;
	                this.cartService.addCart(this.add_product)
	                    .subscribe(function (result) {
	                    if (result['status_code'] === 1) {
	                        _this.repeat_size = choose_size;
	                        _this.success = result['message'];
	                        global_1.unblockUI();
	                        global_1.toast(_this.success, 'success');
	                    }
	                    else {
	                        _this.error = result['message'];
	                        global_1.unblockUI();
	                        global_1.toast(_this.error, 'error');
	                    }
	                });
	                if (this.toggalFlage == 1) {
	                    jQuery(document).ready(function () {
	                        jQuery('#zoomModal').modal('close');
	                    });
	                    this.toggalFlage = 0;
	                }
	            }
	        }
	        else {
	            jQuery(document).ready(function () {
	                jQuery('#modal1').modal('open');
	            });
	        }
	    };
	    ProductDetailComponent.prototype.addToWishlist = function () {
	        var _this = this;
	        if (this.authenticationService.isLoggedIn()) {
	            global_1.blockUI();
	            this.loading = true;
	            var userData = this.authenticationService.authData;
	            this.add_wishlist['product_id'] = this.product.id;
	            this.add_wishlist['user_id'] = userData['id'];
	            this.add_wishlist['userToken'] = userData['token'];
	            this.cartService.addWishlist(this.add_wishlist).subscribe(function (result) {
	                global_1.unblockUI();
	                _this.loading = false;
	                if (result['status_code'] === 1) {
	                    _this.success = result['message'];
	                    global_1.toast(_this.success, 'success');
	                    _this.wish_flag = false;
	                }
	                else {
	                    _this.error = result['message'];
	                    global_1.toast(_this.error, 'error');
	                }
	            });
	        }
	        else {
	            jQuery('#modal1').modal('open');
	        }
	    };
	    ProductDetailComponent.prototype.ngAfterViewInit = function () {
	        setTimeout(function () {
	        }, 2000);
	    };
	    ProductDetailComponent.prototype.toggleZoom = function () {
	        var _this = this;
	        global_1.blockUI();
	        jQuery('#zoomModal').modal('open');
	        setTimeout(function () {
	            if (_this.refreshFlag == 0) {
	                jQuery(document).ready(function () {
	                    jQuery('.slider-for-zoom').slick('refresh');
	                    jQuery(".slider-for-zoom").slick('slickGoTo', jQuery("#currnt_slide").val());
	                });
	                _this.refreshFlag = 1;
	            }
	            jQuery(document).ready(function () {
	                jQuery("#zoomModal").mCustomScrollbar({
	                    autoDraggerLength: false,
	                    auto: "yes",
	                    mouseWheel: {
	                        scrollAmount: 128
	                    }
	                });
	            });
	            global_1.unblockUI();
	        }, 1000);
	        this.toggalFlage = 1;
	    };
	    ProductDetailComponent.prototype.CheckPinCode = function () {
	        var _this = this;
	        this.pincode = this.pincode.replace(/[^0-9]/g, '');
	        if (this.pincode != '' && this.pincode.length == 6) {
	            this.userService.getPincodeAvalability(this.pincode)
	                .subscribe(function (result) {
	                if (result['status_code'] === 1) {
	                    _this.pincode_data = result;
	                    if (result['cash'] == 'n') {
	                        _this.pinerror = 1;
	                    }
	                    else {
	                        _this.pinerror = 0;
	                    }
	                }
	                else {
	                    _this.pincode_data = result;
	                    _this.pinerror = 1;
	                }
	            });
	        }
	        else {
	            this.pincode_data = [];
	            this.pinerror = 1;
	        }
	    };
	    return ProductDetailComponent;
	}());
	ProductDetailComponent = __decorate([
	    core_1.Component({
	        selector: 'product-detail',
	        template: __webpack_require__(379),
	        inputs: ['product'],
	    }),
	    __metadata("design:paramtypes", [product_service_1.ProductService,
	        cart_service_1.CartService,
	        page_service_1.PageService,
	        router_1.ActivatedRoute,
	        authentication_service_1.AuthenticationService,
	        platform_browser_1.Title,
	        platform_browser_1.DomSanitizer,
	        user_service_1.UserService])
	], ProductDetailComponent);
	exports.ProductDetailComponent = ProductDetailComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 378:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var http_1 = __webpack_require__(31);
	var authentication_service_1 = __webpack_require__(369);
	var Rx_1 = __webpack_require__(34);
	__webpack_require__(227);
	__webpack_require__(150);
	var UserService = (function () {
	    function UserService(http, authenticationService) {
	        this.http = http;
	        this.authenticationService = authenticationService;
	        this.pageUrl = 'api/check-reset-password/';
	        this.userGetUrl = 'api/get-user-detail/';
	        this.usersGetUrl = 'api/get-user-details/';
	        this.addressGetUrl = 'api/get-user-address/';
	        this.addressUrl = 'api/delete-address/';
	    }
	    UserService.prototype.getUsers = function () {
	        var headers = new http_1.Headers({ 'Authorization': 'Bearer ' + this.authenticationService.authData });
	        var options = new http_1.RequestOptions({ headers: headers });
	        return this.http.get('/api/users', options)
	            .map(function (response) { return response.json(); });
	    };
	    UserService.prototype.checkResetToken = function (token) {
	        return this.http.get("" + this.pageUrl + token + "/")
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    UserService.prototype.submitResetPassword = function (modelData) {
	        return this.http.post('api/save-reset-password', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    UserService.prototype.getUserDetail = function (id, token) {
	        return this.http.get(this.userGetUrl + id + '/' + token)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    UserService.prototype.getUserDetails = function (id, token) {
	        return this.http.get(this.usersGetUrl + id + '/' + token)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    UserService.prototype.profileSave = function (modelData) {
	        return this.http.post('api/update-profile', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    UserService.prototype.getUserAddress = function (id, token) {
	        return this.http.get(this.addressGetUrl + id + '/' + token)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    UserService.prototype.addressSave = function (modelData) {
	        return this.http.post('api/save-address', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    UserService.prototype.setAddressDefault = function (modelData) {
	        return this.http.post('api/set-address-default', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    UserService.prototype.deleteUserAddress = function (address_id, user_id) {
	        return this.http.get(this.addressUrl + address_id + '/' + user_id)
	            .map(function (res) { return res.json(); })
	            .catch(function (error) { return Rx_1.Observable.throw(error.json().error || 'Server error'); });
	    };
	    UserService.prototype.editAddressSave = function (modelData) {
	        return this.http.post('api/update-address', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    UserService.prototype.changePasswordSave = function (modelData) {
	        return this.http.post('api/change-password', { data: modelData })
	            .map(function (response) {
	            if (response.json().status_code == 1) {
	                return response.json();
	            }
	            else {
	                return response.json();
	            }
	        });
	    };
	    UserService.prototype.getPincodeDetail = function (pincode) {
	        return this.http.get('api/get-pincode-detail/' + pincode)
	            .map(function (response) {
	            return response.json();
	        });
	    };
	    UserService.prototype.getPincodeAvalability = function (pincode) {
	        return this.http.get('api/get-pincode-avalability/' + pincode)
	            .map(function (response) {
	            return response.json();
	        });
	    };
	    return UserService;
	}());
	UserService = __decorate([
	    core_1.Injectable(),
	    __metadata("design:paramtypes", [http_1.Http,
	        authentication_service_1.AuthenticationService])
	], UserService);
	exports.UserService = UserService;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 379:
/***/ function(module, exports) {

	module.exports = "<div class=\"div-loader\">\n    <section class=\"product-category breadcrumb-detail\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m6 s6\">\n                <h5>PRODUCT DETAIL</h5>\n            </div>\n            <div class=\"col m6 s6\">\n                <div class=\"product-category-breadcrumb right\">\n                    <ul>\n                        \n                        <li ><a [routerLink]=\"['/']\" >Home</a></li>\n                        <li ><a [routerLink]=\"['/category',category['slug']]\">{{category['name']}}</a></li>\n                        <li class=\"disabled\"><a>{{product.title}}</a></li>\n                    </ul> \n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n    <section class=\"product-detail modal-dialog\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col m6 s12\">\n                    <div class=\"product-view row\">\n\n                        <div *ngIf=\"productDetail.no_images == ''\" class=\"col m2 s12 slider slider-nav \">\n                            <div *ngFor=\"let thumbnailImg of thumbnail\">\n                                <img class=\"\" src=\"{{thumbnailImg}}\">\n                            </div>\n                        </div>\n\n                        <div *ngIf=\"productDetail.no_images == ''\" class=\"col m10 s12 slider slider-for\">\n                            <div *ngFor=\"let image of images;let i = index;\" >\n                                <img class=\"zoom\"  src=\"{{image.name}}\"  (click)=\"toggleZoom()\" [attr.data-zoom-image]=\"image.orignail_image\">\n                            </div>\n                            <div class=\"clearfix\"></div>\n                        </div>\n\n                        <div *ngIf=\"productDetail.no_images != ''\" class=\"col m12 s12 \">\n                            <img src=\"{{productDetail.no_images}}\" >\n                        </div>\n\n                    </div>\n                </div>\n\n                <div class=\"col m6 s12\">\n                    <div class=\"product-info\">\n                        <h5>{{product.title | capitalize}}</h5>\n                        <h6>{{product.short_description}}</h6>\n                        <div class=\"prize\" [innerHTML]=\"product.price\">\n\n                        </div>\n                        <div class=\"product-size\">\n                            <h6 *ngIf=\"!size_error\" class=\"left\">Select Size</h6>\n                            <div *ngIf=\"size_error\" class=\"err-msg\">Please select size</div>\n                            <a class=\"right\" href=\"#modal4\">\n                                <span>Not Sure?</span>\n                                See Size\n                            </a>\n                            <div class=\"clearfix\"></div>\n                            <ul>\n\n                                <li *ngFor=\"let size of sizes\" [class.active]=\"select_size === size.slug\" (click)=\"setSize(size.slug)\"><a class=\"waves-effect btn\">{{size.name }}</a></li>\n\n                            </ul>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n                        <div class=\"cart-wish-btn\" id='sticker'>\n                            <a class=\"waves-effect fabivo-btn btn\" href=\"javascript:void(0);\" (click)=\"addToCart(select_size)\">ADD TO CART</a>\n\n                            <a *ngIf=\"cartService.wishlistArray.indexOf(product.id)  ==-1\" (click)=\"cartService.addToWishlist(product.id)\" class=\"waves-effect fabivo-btn btn\">ADD TO WISHLIST</a>\n\n                            <a *ngIf=\"cartService.wishlistArray.indexOf(product.id) > -1\" [routerLink]=\"['../../wishlist']\" class=\"waves-effect fabivo-btn btn\">VIEW WISHLIST</a>\n                        </div>\n                        <div class=\"all-btn\" id='description'>\n                            <button class ='waves-effect fabivo-btn facebook-btn btn' ceiboShare  [facebook]=\"{u: repoUrl}\"> <i class=\"fa fa-facebook\"> </i> </button>\n\n\n                            <button class ='waves-effect fabivo-btn twitter-btn btn'  ceiboShare  [twitter]=\"{url:repoUrl, text:product.title, hashtags:site_title}\"> <i class=\"fa fa-twitter\"></i> </button>   \n\n                      \n\n                           <a    class ='waves-effect fabivo-btn whatsapp-btn btn' [href]=\"whatsapp\"   data-action=\"share/whatsapp/share\"><i class=\"fa fa-whatsapp\"></i></a> \n\n                     \n\n\n\n                            <a class ='waves-effect fabivo-btn envelope-btn btn'  href=\"mailto:?subject={{settings['CONFIG_SITE_TITLE']}}&body={{repoUrl}}\"  ><i class=\"fa fa-envelope\"></i> </a>\n\n\n                        </div> \n                        <div class=\"clearfix\"></div>\n                        <div class=\"description\" >\n                            <h5>Delivery time & Cash/Card delivery</h5>\n                            <p>Please enter your PIN code to check delivery time and\n                                Cash/Card on Delivery Avalability</p>\n                            <form>\n                                <div class=\"input-field\">\n                                    <input id=\"search\" [(ngModel)]=\"pincode\" (keyup)='CheckPinCode()' name='pincode' placeholder=\"Enter Pincode\" maxlength=\"6\" type=\"search\">                   \n                                        <a (click)='CheckPinCode()'>Check Pin Code</a> \n                                        \n                                </div>  \n                                 <div class=\"clearfix\"></div>\n                                  <p *ngIf=\"pincode_data && pincode_data['status_code']=1\"> {{pincode_data['expected_delivery_date']}}</p>  \n                                  <p *ngIf=\"pincode_data && pincode_data['message']!=''\" class=\"h-heading \" [ngClass]=\"{failed:pinerror}\"  > {{pincode_data['message']}}</p>  \n\n\n\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </section> \n\n    <section class=\"review-description\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col s12\">\n                    <ul class=\"tabs\">\n                        <li class=\"tab\">\n                            <a class=\"active\" href=\"#descriptions\">DESCRIPTION</a></li>\n\n                        <li class=\"tab\"><a href=\"#return\">RETURN & DELIVERY</a></li>\n                    </ul>\n                </div>\n                <div id=\"descriptions\" class=\"col s12\" [innerHTML]=\"product.description | sanitizeHtml\"></div>\n\n\n\n                <div id=\"return\" class=\"col s12\" [innerHTML]=\"settings['CONFIG_RETURN_AND_DELIVERY'] | sanitizeHtml\">\n\n                </div>\n            </div>\n        </div>\n    </section> \n\n    <section class=\"product-slider\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col m12 s12\">\n                    <div class=\"row product-slider-heading\">\n                        <div class=\"col s3\"><span></span></div>\n                        <div class=\"col s6\">\n                            <h5 class=\"center-align\">You May Also Like</h5>\n                        </div>\n                        <div class=\"col s3\"><span></span></div>\n                        <div class=\"clearfix\"></div>\n                    </div>  \n                    <div class=\"customNavigation\">\n                        <a class=\"prev\"></a>\n                        <a class=\"next\"></a>\n                    </div>\n                    <div id=\"owl-demo\" class=\"owl-carousel\" >             \n\n                        <div class=\"item\" *ngFor=\"let relatedData of productDetail.related_product\" >\n                             <a [routerLink]=\"['../',relatedData.slug]\">\n                                <img class=\"responsive-img\" src=\"{{relatedData.image}}\" alt=\"{{relatedData.title}}\" />\n                                <h6>{{relatedData.title}}</h6>\n                            </a>\n                        </div>\n\n                    </div>  \n                </div>\n            </div>\n        </div>\n    </section>\n</div>\n<!--size chart-->\n<div id=\"modal4\" class=\"modal login-bx\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <div class=\"login-form\">\n                    <div class=\"row\">\n                        <div class=\"input-field col s12 center\">\n                            <div class=\"contant\">\n                                <span class=\"modal-action modal-close\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                                <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                                <h4 class=\"center login-form-text\">Size Chart</h4>\n                                <div class=\"chart\" [innerHTML]=\"product.size_chart | sanitizeHtml\"></div>\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div id=\"zoomModal\" class=\"modal zoomModal\">\n    <input type=\"hidden\" id='currnt_slide' value=\"0\" >\n    <span class=\"modal-action modal-close\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n\n\n\n    <div  class=\" slider-for-zoom\">\n        <div *ngFor=\"let image of images\">\n            <img class=\"\"  src=\"{{image.orignail_image}}\"  >\n        </div>\n    </div>\n\n    <div class=\"zoomStrip  \">\n\n        <div class=\"zoomStripContent\"> \n            <span class=\"prize\" [innerHTML]=\"product.price\">\n\n            </span>\n            <span class=\"sizes\" >\n                <ul>\n\n                    <li *ngFor=\"let size of sizes\" [class.active]=\"select_size === size.slug\" (click)=\"setSize(size.slug)\"><a class=\"waves-effect  \">{{size.name }}</a></li>\n                    <div class=\"clearfix\"></div>\n\n                </ul>\n\n            </span>\n            <span>  <a class=\"waves-effect fabivo-btn btn\" href=\"javascript:void(0);\" (click)=\"addToCart(select_size);\">ADD TO CART</a>\n            </span>\n            <div class=\"clearfix\"></div>\n        </div>\n        <div *ngIf=\"size_error\" class=\"err-msg\">Please select size</div>\n    </div>\n\n\n\n</div> "

/***/ },

/***/ 380:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var NotFoundComponent = (function () {
	    function NotFoundComponent() {
	    }
	    return NotFoundComponent;
	}());
	NotFoundComponent = __decorate([
	    core_1.Component({
	        selector: 'not-found',
	        template: __webpack_require__(381),
	    }),
	    __metadata("design:paramtypes", [])
	], NotFoundComponent);
	exports.NotFoundComponent = NotFoundComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 381:
/***/ function(module, exports) {

	module.exports = " <section class=\"faq-section\">\n       <div class=\"container\">\n             <div class=\"row\">\n                <div class=\"col m12 s12\">\n\n\t\t\t\t\t\t<h1><strong>Oops! Error 400</strong></h1><br />\n\t\t\t\t\t\t<h4><strong>We could not find the page you are looking for ! Sorry, we could not find the page you are trying to reach. You've probably arrived here by accident, or the page you're looking for has been moved or is no longer available. You're most welcome to navigate our web site via the links on the top.</strong></h4>\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n</section>"

/***/ },

/***/ 382:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var page_service_1 = __webpack_require__(33);
	var platform_browser_1 = __webpack_require__(21);
	var global_1 = __webpack_require__(363);
	var ContactUsComponent = (function () {
	    function ContactUsComponent(pageService, route, router, sanitizer) {
	        this.pageService = pageService;
	        this.route = route;
	        this.router = router;
	        this.sanitizer = sanitizer;
	        this.settings = [];
	        this.contactModel = {};
	        this.loading = false;
	        this.error = '';
	        this.success = '';
	    }
	    ContactUsComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.pageService.getSettings().subscribe(function (settings) {
	            _this.settings = settings;
	            _this.settings['CONFIG_GOOGLE_MAP'] = _this.sanitizer.bypassSecurityTrustHtml(settings['CONFIG_GOOGLE_MAP']);
	            setTimeout(function () {
	                jQuery('.map iframe').height("100%");
	                jQuery('.map iframe').width("100%");
	                setTimeout('resizeequalheight()', 250);
	                jQuery(window).resize(function () {
	                    setTimeout('resizeequalheight()', 250);
	                });
	            }, 1000);
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    ContactUsComponent.prototype.savecontactus = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.pageService.contactUs(this.contactModel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.contactModel = '';
	                _this.error = '';
	                global_1.unblockUI();
	                _this.router.navigate(['/']);
	                global_1.toast("Your Conatct enquery Send successfully", 'success');
	            }
	            else {
	                _this.error = result['errors'];
	                global_1.toast("Required field can't empty", 'error');
	                _this.loading = false;
	                global_1.unblockUI();
	            }
	        });
	    };
	    return ContactUsComponent;
	}());
	ContactUsComponent = __decorate([
	    core_1.Component({
	        selector: 'contact-us',
	        template: __webpack_require__(383)
	    }),
	    __metadata("design:paramtypes", [page_service_1.PageService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        platform_browser_1.DomSanitizer])
	], ContactUsComponent);
	exports.ContactUsComponent = ContactUsComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 383:
/***/ function(module, exports) {

	module.exports = "<section class=\"contact-us-section\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m4 s12\">\n                <div class=\"contact-info center-align makeequal\">\n                    <i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>\n                    <h5>Our Location</h5>\n                    <p>{{settings['CONFIG_ADDRESS']}}</p>  \n                </div>\n            </div>\n            <div class=\"col m4 s12\">\n                <div class=\"contact-info center-align makeequal\">\n                    <i class=\"fa fa-phone\" aria-hidden=\"true\"></i>\n                    <h5>Telephone</h5>\n                    <p>{{settings['CONFIG_PHONE_NUMBER']}}</p>  \n                </div>\n            </div>\n            <div class=\"col m4 s12\">\n                <div class=\"contact-info center-align makeequal\">\n                    <i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>\n                    <h5>Support Mail</h5>\n                    <p>{{settings['CONFIG_SUPPORT_MAIL']}}</p>  \n                </div>\n            </div> \n\n            <div class=\"col m12 s12\">\n                <div class=\"map\"  [innerHtml]=\"settings['CONFIG_GOOGLE_MAP']\">\n\n                </div>\n                <form class=\"contact-form\"  (ngSubmit)=\"contact.form.valid && savecontactus()\"  #contact=\"ngForm\" novalidate>\n                      <div class=\"row\">\n                        <div class=\"col m12 s12 center-align\">\n                            <div class=\"heading\">\n                                <h4>Contact Form</h4>\n                                <span class=\"sepretar\"></span>\n                                <span class=\"sepretar sepretar-down\"></span>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"input-field col m6 s12\">\n                            <input id=\"first_name\" class=\"validate\" type=\"text\" name='name' [(ngModel)]=\"contactModel.name\" #name=\"ngModel\" required>\n                            <label for=\"first_name\">Your Name</label>\n                            <div *ngIf=\"contact.submitted && !name.valid\" class=\"help-block err-msg\">The name is required</div>\n                             <div *ngIf=\"error['name'] && name.valid\" class=\"help-block  err-msg\">{{error['name']['0']}}</div>\n                        </div>\n                        <div class=\"input-field col m6 s12\">\n                            <input id=\"subject\" class=\"validate\" name=\"subject\" type=\"text\" [(ngModel)]=\"contactModel.subject\" #subject=\"ngModel\" required>\n                            <label for=\"subject\">Subject</label>\n                            <div *ngIf=\"contact.submitted && !subject.valid\" class=\"help-block err-msg\">The subject is required</div>\n                             <div *ngIf=\"error['subject'] && subject.valid\" class=\"help-block  err-msg\">{{error['subject']['0']}}</div>\n                        </div>\n                    </div>   <div class=\"row\">\n\n                        <div class=\"input-field col m6 s12\">\n                            <input id=\"email\" class=\"validate\" type=\"text\"  name=\"email\" type=\"text\" [(ngModel)]=\"contactModel.email\" #email=\"ngModel\" required pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\">\n                            <div *ngIf=\"contact.submitted && email.errors && email.errors.required\" class=\"help-block err-msg\">The Email is required</div>\n                            <div *ngIf=\"contact.submitted && email.errors && email.errors.pattern\" class=\"help-block  err-msg\">The Email is invalid</div>\n                            <div *ngIf=\"error['email'] && email.valid\" class=\"help-block err-ms\">{{error['email']['0']}}</div>\n                            <label for=\"email\">Email</label>\n                        </div>\n\n                        <div class=\"input-field col m6 s12\">\n                            <input id=\"phone_number\" class=\"validate\" type=\"text\" name=\"phone_number\" type=\"text\" [(ngModel)]=\"contactModel.phone_number\" #phone_number=\"ngModel\" required pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\">\n                            <label for=\"phone_number\">Phone Number</label>\n                            <div *ngIf=\"contact.submitted && phone_number.errors && phone_number.errors.required\" class=\"help-block err-msg\">The Phone Number  is required</div>\n                            <div *ngIf=\"contact.submitted && phone_number.errors && phone_number.errors.pattern\" class=\"help-block err-msg\"> The Phone number format is invalid</div>\n                            <div *ngIf=\"error['phone_number'] && phone_number.valid\" class=\"help-block err-msg\">{{error['phone_number']['0']}}</div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col m12 s12\"> \n                            <div class=\"input-field\">\n                                <textarea id=\"icon_prefix2\" class=\"materialize-textarea\" name='message' [(ngModel)]=\"contactModel.message\" #message=\"ngModel\" required></textarea>\n                                <label for=\"icon_prefix2\">Message</label>\n                                 <div *ngIf=\"contact.submitted && !message.valid\" class=\"help-block err-msg\">The message is required</div>\n                                   <div *ngIf=\"error['message'] && message.valid\" class=\"help-block  err-msg\">{{error['message']['0']}}</div>\n                            </div>\n                        </div>\n\n                        <div class=\"col m12 s12 center-align\">\n                            <button class=\"waves-effect fabivo-btn-2 btn\">Submit</button>\n                            \n                        </div>\n                    </div>  \n                </form>\n            </div>\n        </div>        \n    </div>\n</section>"

/***/ },

/***/ 384:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var platform_browser_1 = __webpack_require__(21);
	var global_1 = __webpack_require__(363);
	var ResetPasswordComponent = (function () {
	    function ResetPasswordComponent(userService, route, router, sanitizer) {
	        this.userService = userService;
	        this.route = route;
	        this.router = router;
	        this.sanitizer = sanitizer;
	        this.resetmodel = {};
	        this.token = '';
	        this.loading = false;
	        this.error = '';
	        this.success = '';
	    }
	    ResetPasswordComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.route.params.subscribe(function (params) {
	            var token = params['token'];
	            _this.token = token;
	            _this.userService.checkResetToken(token).subscribe(function (tokenResponse) {
	                console.log(tokenResponse);
	                if (tokenResponse['status_code'] === 1) {
	                    return true;
	                }
	                else {
	                    _this.router.navigate(['/']);
	                    global_1.toast(tokenResponse['message'], 'error');
	                }
	            }, function (err) {
	                console.log(err);
	            });
	        });
	    };
	    ResetPasswordComponent.prototype.reset_password = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.resetmodel.token = this.token;
	        this.userService.submitResetPassword(this.resetmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.resetmodel = '';
	                _this.error = '';
	                global_1.unblockUI();
	                _this.router.navigate(['/login']);
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                _this.error = result['errors'];
	                global_1.toast(result['message'], 'error');
	                _this.loading = false;
	                global_1.unblockUI();
	            }
	        });
	    };
	    return ResetPasswordComponent;
	}());
	ResetPasswordComponent = __decorate([
	    core_1.Component({
	        selector: 'reset-password',
	        template: __webpack_require__(385)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        platform_browser_1.DomSanitizer])
	], ResetPasswordComponent);
	exports.ResetPasswordComponent = ResetPasswordComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 385:
/***/ function(module, exports) {

	module.exports = "<div class=\"container\">\n    <div class=\" login-bx login-page  \">\n    <div class=\" row   \">\n      <div class=\"col s12 \">\n      \n        <form class=\"login-form\" id=\"resetPasswordForm\" name=\"reset-passord\" (ngSubmit)=\"resetPassword.form.valid && reset_password()\" #resetPassword=\"ngForm\" novalidate>\n        <div class=\"row\">\n          <div class=\"input-field col s12 center\">\n               <div class=\"contant\">\n            <h4 class=\"center login-form-text\">Reset Passowrd</h4>\n               </div>\n          </div>\n        </div>\n        \n        <div class=\"row\" [ngClass]=\"{ 'has-error': resetPassword.submitted && !password.valid }\">\n          <div class=\"input-field col s12\">\n            <i class=\" fa fa-lock\"></i>\n            <input id=\"password\" type=\"password\" name=\"password\" [(ngModel)]=\"resetmodel.password\" #password=\"ngModel\" required>\n            <label for=\"password\" class=\"\">Password</label>\n            <div *ngIf=\"resetPassword.submitted  && password.errors && password.errors.required\" class=\"help-block err-msg\">The password field is required.</div>\n            <div *ngIf=\"error['password']\" class=\"err-msg\">{{error['password']['0']}}</div>\n          </div>\n        </div>\n              \n        <div class=\"row\" [ngClass]=\"{ 'has-error': resetPassword.submitted && !password.valid }\">\n          <div class=\"input-field col s12\">\n            <i class=\" fa fa-lock\"></i>\n            <input id=\"confirm_password\" type=\"password\" name=\"confirm_password\" [(ngModel)]=\"resetmodel.confirm_password\" #confirm_password=\"ngModel\" required>\n            <label for=\"confirm_password\" class=\"\">Confirm Password</label>\n            <div *ngIf=\"resetPassword.submitted && confirm_password.errors && confirm_password.errors.required\" class=\"help-block err-msg\">The confirm password field is required.</div>\n            <div *ngIf=\"error['confirm_password']\" class=\"err-msg\">{{error['confirm_password']['0']}}</div>\n          </div>\n        </div>\n        \n        \n        <div class=\"row\">\n          <span class=\"input-field col  login-btn\">\n            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">Submit</button>\n          </span>\n        </div>\n        \n\n      </form>\n    </div>\n    </div>\n</div>\n</div>"

/***/ },

/***/ 386:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var common_1 = __webpack_require__(22);
	var MyAccountComponent = (function () {
	    function MyAccountComponent(userService, route, location) {
	        this.userService = userService;
	        this.route = route;
	        this.location = location;
	        this.authDatas = [];
	        this.url_path = '';
	    }
	    MyAccountComponent.prototype.ngOnInit = function () {
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        setTimeout(function () {
	            jQuery(document).ready(function () {
	                jQuery('.tabs').tabs();
	            });
	        }, 100);
	    };
	    MyAccountComponent.prototype.isActive = function (path) {
	        return this.location.path().indexOf(path) > -1;
	    };
	    return MyAccountComponent;
	}());
	MyAccountComponent = __decorate([
	    core_1.Component({
	        selector: 'my-account',
	        template: __webpack_require__(387)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        common_1.Location])
	], MyAccountComponent);
	exports.MyAccountComponent = MyAccountComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 387:
/***/ function(module, exports) {

	module.exports = "<section class=\"product-detail myaccount\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <ul class=\"tabs\">\n                    <li class=\"tab col\"><a [routerLinkActive]=\"['active']\" [routerLink]=\"['profile']\">profile</a></li>\n                    <li class=\"tab col\"><a [routerLinkActive]=\"['active']\" [routerLink]=\"['address']\">address</a></li>\n                    <li class=\"tab col\" *ngIf=\"!isActive('/myaccount/order-detail')\"><a [routerLinkActive]=\"['active']\" [routerLink]=\"['order']\">my orders</a></li>\n                    <li class=\"tab col\"  *ngIf=\"isActive('/myaccount/order-detail')\"><a class='active' [routerLink]=\"['order']\">my orders</a></li>\n                    <li class=\"tab col\"><a [routerLinkActive]=\"['active']\" [routerLink]=\"['wallet']\">my wallet</a></li>\n                    <li class=\"tab col\"><a [routerLinkActive]=\"['active']\" [routerLink]=\"['change-password']\">change password</a></li>\n                </ul>\n            </div>  \n            <div class=\"clearfix\"></div>\n            <div class=\"tabsin\">\n\n                <router-outlet></router-outlet>\n            </div>\n        </div>\n    </div>\n</section>\n"

/***/ },

/***/ 388:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var global_1 = __webpack_require__(363);
	var authentication_service_1 = __webpack_require__(369);
	var MyProfileComponent = (function () {
	    function MyProfileComponent(userService, authenticationService, route) {
	        this.userService = userService;
	        this.authenticationService = authenticationService;
	        this.route = route;
	        this.authDatas = [];
	        this.userData = [];
	        this.profilemodel = {};
	        this.number_model = {};
	        this.loading = false;
	        this.error = '';
	        this.success = '';
	        this.phone_verify = '';
	        this.phone_number = '';
	        this.user_id = '';
	        this.new_number = 0;
	        this.enble_sms = 0;
	    }
	    MyProfileComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.authDatas = this.authenticationService.authData;
	        this.userService.getUserDetail(this.authDatas.id, this.authDatas.token).subscribe(function (userDetail) {
	            _this.userData = userDetail['user_data'];
	            _this.profilemodel['first_name'] = _this.userData['first_name'];
	            _this.profilemodel['last_name'] = _this.userData['last_name'];
	            _this.profilemodel['phone'] = _this.userData['phone'];
	            _this.phone_number = _this.userData['phone'];
	            _this.profilemodel['email'] = _this.userData['email'];
	            _this.profilemodel['gender'] = _this.userData['gender'];
	            _this.phone_verify = _this.userData['phone_verify'];
	            _this.enble_sms = userDetail['enble_sms'];
	            setTimeout(function () {
	                Materialize.updateTextFields();
	            }, 1000);
	        }, function (err) {
	            console.log(err);
	        });
	        setTimeout(function () {
	            jQuery(document).ready(function () {
	                jQuery('.tabs').tabs();
	                jQuery('.modal').modal();
	            });
	        }, 100);
	    };
	    MyProfileComponent.prototype.phoneVerify = function () {
	        if (this.enble_sms) {
	            this.user_id = this.authDatas.id;
	            global_1.blockUI();
	            this.authenticationService.resendOtpPassword(this.user_id)
	                .subscribe(function (result) {
	                if (result['status_code'] == 1) {
	                    jQuery('#modal_otp_myfrofile').modal('open');
	                    global_1.unblockUI();
	                    global_1.toast(result['message'], 'success');
	                }
	                else {
	                    global_1.unblockUI();
	                    global_1.toast(result['message'], 'error');
	                }
	            });
	        }
	    };
	    MyProfileComponent.prototype.profileSubmit = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.profilemodel['id'] = this.authDatas.id;
	        this.profilemodel['token'] = this.authDatas.token;
	        this.userService.profileSave(this.profilemodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.new_number = result['new_number'];
	                _this.phone_number = _this.profilemodel['phone'];
	                if (result['new_number'] == 1) {
	                    _this.phone_number = result['mobile_number'];
	                    _this.user_id = _this.authDatas.id;
	                    jQuery('#modal_otp_myfrofile').modal('open');
	                }
	                _this.error = '';
	                global_1.unblockUI();
	                _this.loading = false;
	                global_1.toast(_this.success, 'success');
	                var auth_data = JSON.stringify(result['user_data']);
	                if (auth_data) {
	                    localStorage.setItem('loginUser', auth_data);
	                    _this.authenticationService.setAuthData();
	                    _this.authDatas = _this.authenticationService.authData;
	                }
	            }
	            else {
	                _this.error = result['errors'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    MyProfileComponent.prototype.resendOtp = function () {
	        global_1.blockUI();
	        this.authenticationService.resendOtpPassword(this.user_id)
	            .subscribe(function (result) {
	            if (result['status_code'] == 1) {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    MyProfileComponent.prototype.close_model = function (id) {
	        jQuery('#verified_number')[0].reset();
	        jQuery('#verified_number').find('label').removeClass('active');
	        this.error = '';
	        this.number_model = {};
	    };
	    MyProfileComponent.prototype.submitOtp = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.number_model['user'] = this.user_id;
	        this.authenticationService.mobileNumberVerified(this.number_model)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.number_model = '';
	                _this.error = '';
	                jQuery('#modal_otp_myfrofile').modal('close');
	                jQuery('#verified_number')[0].reset();
	                global_1.unblockUI();
	                _this.phone_verify = '1';
	                _this.loading = false;
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['message'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    return MyProfileComponent;
	}());
	__decorate([
	    core_1.Input(),
	    __metadata("design:type", Object)
	], MyProfileComponent.prototype, "model", void 0);
	MyProfileComponent = __decorate([
	    core_1.Component({
	        selector: 'my-profile',
	        template: __webpack_require__(389)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        authentication_service_1.AuthenticationService,
	        router_1.ActivatedRoute])
	], MyProfileComponent);
	exports.MyProfileComponent = MyProfileComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 389:
/***/ function(module, exports) {

	module.exports = "<div id=\"pro1\" class=\"col s12\">\n    <div class=\"row\">\n    \n        <form class=\"col s8\" id=\"editProfile\" (ngSubmit)=\"editProfile.form.valid && profileSubmit()\" #editProfile=\"ngForm\" novalidate>\n              <div class=\"row\">\n                <div class=\"input-field col s6\">\n                    <input id=\"first_name\" type=\"text\" name='first_name' class=\"validate\" [(ngModel)]=\"profilemodel.first_name\" #first_name=\"ngModel\" required  pattern=\"[a-zA-Z ]*\">\n                    <label for=\"first_name\" class=\"active\">First Name</label>\n                 \n                      <div *ngIf=\"editProfile.submitted && first_name.errors && first_name.errors.required\" class=\"help-block err-msg\">The first name field is required.</div>\n                        <div *ngIf=\"editProfile.submitted && first_name.errors && first_name.errors.pattern\" class=\"help-block err-msg\"> Enter valid first name.</div>\n                    <div *ngIf=\"error && error['first_name'] && first_name.valid\" class=\"err-msg\">{{error['first_name']['0']}}</div>\n                </div>\n\n                <div class=\"input-field col s6\">\n                    <input id=\"last_name\" name ='last_name' type=\"text\" class=\"validate\" [(ngModel)]=\"profilemodel.last_name\" #last_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"last_name\" class=\"active\">Last Name</label>\n                   \n                       <div *ngIf=\"editProfile.submitted && last_name.errors && last_name.errors.required\" class=\"help-block err-msg\">The last name field is required.</div>\n                        <div *ngIf=\"editProfile.submitted && last_name.errors && last_name.errors.pattern\" class=\"help-block err-msg\"> Enter valid last name.</div>\n                    <div *ngIf=\"error && error['last_name'] && last_name.valid\" class=\"err-msg\">{{error['last_name']['0']}}</div>\n                      \n                </div>\n            </div>\n<div class=\"clearfix\"></div>\n            <div class=\"row\">\n                <div class=\"input-field col s6\">\n                    <input id=\"email\" type=\"text\" class=\"validate\" name='email' [(ngModel)]=\"profilemodel.email\" #email=\"ngModel\" disabled=\"disabled\">\n                    <label for=\"email\" class=\"active\">Email</label>\n                </div>\n                <div *ngIf=\"phone_verify==1 && phone_number != ''\" class=\"input-field col s6\">\n                    <input id=\"phone\" type=\"text\" class=\"validate\" name='phone' [(ngModel)]=\"profilemodel.phone\" #phone=\"ngModel\" disabled=\"disabled\">\n                    <label for=\"phone\" class=\"active\">Phone</label>\n                </div>\n                <div *ngIf=\"phone_verify==0 && phone_number == ''\" class=\"input-field col s6\">\n                    <input id=\"phone\" type=\"text\" class=\"validate\" name='phone' [(ngModel)]=\"profilemodel.phone\" #phone=\"ngModel\" required  pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\">\n                    <label for=\"phone\" class=\"active\">Phone</label>\n                   <div *ngIf=\"editProfile.submitted && phone.errors && phone.errors.required\" class=\"help-block err-msg\">The phone number field is required.</div>\n                   <div *ngIf=\"editProfile.submitted && phone.errors && phone.errors.pattern\" class=\"help-block err-msg\"> The phone number format is invalid</div>\n                   <div *ngIf=\"error && error['phone'] && first_name.valid\" class=\"err-msg\">{{error['phone']['0']}}</div>\n                </div>\n                 <div *ngIf=\"phone_verify==0 && phone_number != ''\" class=\"input-field col s3\">\n                    <input id=\"phone\" type=\"text\" class=\"validate\" name='phone' [(ngModel)]=\"profilemodel.phone\" #phone=\"ngModel\" disabled=\"disabled\">\n                    <label for=\"phone\" class=\"active\">Phone</label>\n                   \n                </div>\n                <div *ngIf=\"phone_verify==0 && phone_number != '' && new_number==0\" class=\"input-field col s3\"> <a class=\"waves-effect fabivo-btn btn\" (click)='phoneVerify()'>Verifiy Number</a></div>\n\n            </div>\n            <div class=\"clearfix\"></div>\n            <div class=\"row\">\n                <div class=\"col s12 gander\">\n                    Gender\n                </div>\n                <div class=\"col s2\">\n                    <p>\n                        <input class=\"with-gap\" name=\"gender\" type=\"radio\" id=\"male1\" value='male' [(ngModel)]=\"profilemodel.gender\" #gender=\"ngModel\" required/>\n                        <label for=\"male1\">Male</label>\n                    </p>\n                </div>\n                <div class=\"col s2\">\n                    <p>\n                        <input class=\"with-gap\" name=\"gender\" type=\"radio\" id=\"female1\" value='female' [(ngModel)]=\"profilemodel.gender\" #gender=\"ngModel\" required/>\n                        <label for=\"female1\">Female</label>\n                    </p>\n                </div>\n                <div *ngIf=\"editProfile.submitted && gender.errors && gender.errors.required\" class=\"help-block err-msg\">The gender field is required.</div>\n            </div>  \n            <div class=\"row\">\n                <div class=\"col s8 button\">\n                    <button [disabled]=\"loading\" class=\"waves-effect fabivo-btn btn\">Submit<i class=\"material-icons right\">send</i></button>\n                </div>\n            </div>\n        </form>\n    </div>\n</div>\n<div id=\"modal_otp_myfrofile\" class=\"modal login-bx\">\n    <div  class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <form class=\"login-form\" name=\"verified-form\" id=\"verified_number\" (ngSubmit)=\"verified_number.form.valid && submitOtp()\"  #verified_number=\"ngForm\" novalidate>\n                      <div class=\"row\">\n                        <div class=\"input-field col s12 center\">\n                            <div class=\"contant\">\n                                <span class=\"modal-action modal-close\" (click)=\"close_model('modal_otp_myfrofile')\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                                <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                                <h4 class=\"center login-form-text\">Verify Mobile Number</h4>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <p>OTP sent to {{phone_number}}. Enter it below to verify.<p>\n                                <input id=\"otp\" type=\"text\" name='otp' [(ngModel)]=\"number_model.otp\" #otp=\"ngModel\" required  pattern=\"^[0-9]{6}$\">\n                            <div *ngIf=\"verified_number.submitted && otp.errors && otp.errors.required\" class=\"help-block err-msg\">The Otp field is required.</div>\n                            <div *ngIf=\"verified_number.submitted && otp.errors && otp.errors.pattern\" class=\"help-block err-msg\">Enter 6 digit OTP.</div>\n\n\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"input-field col s12 login-btn\">\n                            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">VERIFY MOBILE NUMBER</button>\n                            <a  class=\"btn waves-effect fabivo-btn\" (click)=\"resendOtp()\">Resend Otp</a>\n\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ },

/***/ 390:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var global_1 = __webpack_require__(363);
	var MyAddressComponent = (function () {
	    function MyAddressComponent(userService, route) {
	        this.userService = userService;
	        this.route = route;
	        this.open_address = false;
	        this.edit_address = false;
	        this.authDatas = [];
	        this.addressData = [];
	        this.search = [];
	        this.addressmodel = {};
	        this.editAddressmodel = {};
	        this.defaultAddress = {};
	        this.loading = false;
	        this.error = '';
	        this.errors = '';
	        this.success = '';
	    }
	    MyAddressComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(function (addressDetail) {
	            _this.addressData = addressDetail['data'];
	        }, function (err) {
	            console.log(err);
	        });
	        setTimeout(function () {
	            jQuery(document).ready(function () {
	                jQuery('.tabs').tabs();
	            });
	        }, 100);
	    };
	    MyAddressComponent.prototype.addressSubmit = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.addressmodel['user_id'] = this.authDatas.id;
	        this.addressmodel['token'] = this.authDatas.token;
	        this.userService.addressSave(this.addressmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.error = '';
	                jQuery('#userAddress')[0].reset();
	                jQuery('#userAddress').find('label').removeClass('active');
	                _this.open_address = false;
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.addressData.push(result['data']);
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['error'];
	                _this.errors = result['message'];
	                _this.loading = false;
	                _this.open_address = true;
	                global_1.unblockUI();
	                global_1.toast(_this.errors, 'error');
	            }
	        });
	    };
	    MyAddressComponent.prototype.getdetail = function (modelName, pincode) {
	        var _this = this;
	        this.userService.getPincodeDetail(pincode)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                if (modelName == 'addressmodel') {
	                    _this.addressmodel['state'] = result['data']['state'];
	                    _this.addressmodel['city'] = result['data']['city'];
	                }
	                else {
	                    _this.editAddressmodel['edit_state'] = result['data']['state'];
	                    _this.editAddressmodel['edit_city'] = result['data']['city'];
	                }
	                setTimeout(function () {
	                    Materialize.updateTextFields();
	                }, 100);
	            }
	        });
	    };
	    MyAddressComponent.prototype.editAddressSubmit = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.editAddressmodel['user_id'] = this.authDatas.id;
	        this.editAddressmodel['token'] = this.authDatas.token;
	        this.edit_address = false;
	        this.userService.editAddressSave(this.editAddressmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.error = '';
	                jQuery('#editAddress')[0].reset();
	                jQuery('#editAddress').find('label').removeClass('active');
	                _this.edit_address = false;
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.addressData = result['data'];
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['error'];
	                _this.errors = result['message'];
	                _this.loading = false;
	                _this.edit_address = true;
	                global_1.unblockUI();
	                global_1.toast(_this.errors, 'error');
	            }
	        });
	    };
	    MyAddressComponent.prototype.showEdit = function (address_id) {
	        this.editAddressmodel = this.addressData.filter(function (address1) { return address1.id == address_id; })[0];
	        this.editAddressmodel.edit_first_name = this.editAddressmodel.first_name;
	        this.editAddressmodel.edit_last_name = this.editAddressmodel.last_name;
	        this.editAddressmodel.edit_address_1 = this.editAddressmodel.address_1;
	        this.editAddressmodel.edit_address_2 = this.editAddressmodel.address_2;
	        this.editAddressmodel.edit_mobile = this.editAddressmodel.mobile;
	        this.editAddressmodel.edit_pin_code = this.editAddressmodel.pin_code;
	        this.editAddressmodel.edit_city = this.editAddressmodel.city;
	        this.editAddressmodel.edit_state = this.editAddressmodel.state;
	        jQuery('#editAddress').find('label').addClass('active');
	    };
	    MyAddressComponent.prototype.setDefaultAddress = function (address_id, is_default) {
	        var _this = this;
	        if (is_default == 0) {
	            this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	            global_1.blockUI();
	            this.defaultAddress['address_id'] = address_id;
	            this.defaultAddress['user_id'] = this.authDatas.id;
	            this.userService.setAddressDefault(this.defaultAddress)
	                .subscribe(function (result) {
	                if (result['status_code'] === 1) {
	                    _this.addressData = result['data'];
	                    _this.success = result['message'];
	                    global_1.unblockUI();
	                    global_1.toast(_this.success, 'success');
	                }
	                else {
	                    _this.errors = result['message'];
	                    global_1.unblockUI();
	                    global_1.toast(_this.errors, 'error');
	                }
	            });
	        }
	        else {
	            global_1.toast('Already set default', 'error');
	            return false;
	        }
	    };
	    MyAddressComponent.prototype.deleteAddress = function (address_id) {
	        var _this = this;
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        global_1.blockUI();
	        this.userService.deleteUserAddress(address_id, this.authDatas.id)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.addressData = result['data'];
	                _this.success = result['message'];
	                global_1.unblockUI();
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.errors = result['message'];
	                global_1.unblockUI();
	                global_1.toast(_this.errors, 'error');
	            }
	        });
	    };
	    MyAddressComponent.prototype.close_address_form = function (mode) {
	        var _this = this;
	        this.open_address = !mode;
	        this.edit_address = !mode;
	        jQuery('#userAddress')[0].reset();
	        jQuery('#editAddress')[0].reset();
	        jQuery('#userAddress').find('label').removeClass('active');
	        jQuery('#editAddress').find('label').removeClass('active');
	        this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(function (addressDetail) {
	            _this.addressData = addressDetail['data'];
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    return MyAddressComponent;
	}());
	MyAddressComponent = __decorate([
	    core_1.Component({
	        selector: 'my-address',
	        template: __webpack_require__(391)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute])
	], MyAddressComponent);
	exports.MyAddressComponent = MyAddressComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 391:
/***/ function(module, exports) {

	module.exports = "<div id=\"pro2\" class=\"col s12\">\n    <div class=\"row\">\n\n        <div class=\"limit_scroll\" [hidden]=\"(open_address || edit_address)\"> \n            <div class=\"col s4\" [ngClass]=\"{'defaultAddress':address.is_default === 1}\" *ngFor=\"let address of addressData\">\n                <div> \n                    <div class=\"panel panel-box address-box simple-hover-box highlight-box\" [ngClass]=\"{'highlight-box':address.is_default === 1}\">\n                        <div class=\"panel-body\"> \n                            <h3 class=\"fixed_dynamic_content\">\n                                <div class=\"dynamic-left\"> {{address['first_name']}} {{address['last_name']}} </div> \n                                \n                                <div class=\"icon-group fixed-right\" style=\"width: 104px\">\n                                    <i class=\"pd-5 pull-right\" [ngClass]=\"address['is_default'] === 1 ? 'fa fa-check-square-o': 'fa fa-square-o'\" (click)=\"setDefaultAddress(address.id, address.is_default)\" title=\"Set As Default\"></i>\n                                    <i class=\"fa fa-trash-o pd-5 pull-right\" (click)=\"deleteAddress(address.id)\"></i>\n                                    <i class=\"fa fa-edit pd-5 pull-right\" (click)=\"showEdit(address.id);edit_address = !edit_address\"></i>\n                                </div> \n                            </h3> \n\n                            <div class=\"text-grayed-12\"> \n                                <br>{{address['address_1']}} \n                                <br>{{address['address_2']}} \n                                <br>{{address['city']}}, {{address['pin_code']}}\n                                <br>{{address['state']}}\n                                <br> \n                                <div>Mobile No. {{address['mobile']}}</div> \n                                <br> \n                            </div> \n                        </div>\n                    </div> \n                </div>\n            </div>\n        </div>\n\n        <div class=\"col s12\" [hidden]=\"(open_address || edit_address)\">\n             <a class=\"addrase-btn\" (click)=\"open_address = !open_address\">\n                    <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                    Add New Address\n             </a>\n        </div>\n        \n        <div class=\"col s12\" [hidden]=\"!open_address\">\n            <form id=\"userAddress\" (ngSubmit)=\"userAddress.form.valid && addressSubmit()\" #userAddress=\"ngForm\" novalidate>\n                <div class=\"row\">\n                  <div class=\"input-field col s4\">\n                    <input id=\"first_name\" type=\"text\" name=\"first_name\" class=\"validate\" [(ngModel)]=\"addressmodel.first_name\" #first_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"first_name\">First Name</label>\n                          <div *ngIf=\"userAddress.submitted && first_name.errors && first_name.errors.required\" class=\"help-block err-msg\">The first name field is required.</div>\n                        <div *ngIf=\"userAddress.submitted && first_name.errors && first_name.errors.pattern\" class=\"help-block err-msg\">Enter valid first name.</div>\n                    <div *ngIf=\"error && error['first_name'] && first_name.valid\" class=\"err-msg\">{{error['first_name']['0']}}</div>\n                  </div>\n                  <div class=\"input-field col s4\">\n                    <input id=\"last_name\" name =\"last_name\" type=\"text\" class=\"validate\" [(ngModel)]=\"addressmodel.last_name\" #last_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"last_name\">Last Name</label>\n                 <div *ngIf=\"userAddress.submitted && last_name.errors && last_name.errors.required\" class=\"help-block err-msg\">The last name field is required.</div>\n                         <div *ngIf=\"userAddress.submitted && last_name.errors && last_name.errors.pattern\" class=\"help-block err-msg\">Enter valid last name.</div>\n                    <div *ngIf=\"error && error['last_name'] && last_name.valid\" class=\"err-msg\">{{error['last_name']['0']}}</div>\n                  </div>\n                </div>\n                  \n                <div class=\"row mar-top\">\n                  <div class=\"input-field col s8\">\n                    <input id=\"address_1\" type=\"text\" name=\"address_1\" class=\"validate\" [(ngModel)]=\"addressmodel.address_1\" #address_1=\"ngModel\" required>\n                    <label for=\"address_1\">Address 1</label>\n                    <div *ngIf=\"userAddress.submitted && !address_1.valid\" class=\"help-block err-msg\">The address 1 field is required.</div>\n                    <div *ngIf=\"error && error['address_1'] && address_1.valid\" class=\"err-msg\">{{error['address_1']['0']}}</div>\n                  </div>\n                </div>\n                \n                <div class=\"row\">\n                  <div class=\"input-field col s8\">\n                    <input id=\"address_2\" type=\"text\" name=\"address_2\" class=\"validate\" [(ngModel)]=\"addressmodel.address_2\" #address_2=\"ngModel\" required>\n                    <label for=\"address_2\">Address 2</label>\n                    <div *ngIf=\"userAddress.submitted && !address_2.valid\" class=\"help-block err-msg\">The address 2 field is required.</div>\n                    <div *ngIf=\"error && error['address_2'] && address_2.valid\" class=\"err-msg\">{{error['address_2']['0']}}</div>\n                  </div>\n                </div>\n                \n                <div class=\"row\">\n                    <div class=\"input-field col s4\">\n                        <input id=\"mobile\" type=\"text\" name=\"mobile\" class=\"validate\" [(ngModel)]=\"addressmodel.mobile\" #mobile=\"ngModel\" required  pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\">\n                        <label for=\"mobile\" >Phone Number</label>\n                        <div *ngIf=\"userAddress.submitted && mobile.errors && mobile.errors.required\" class=\"help-block err-msg\">The mobile number field is required.</div>\n                        <div *ngIf=\"userAddress.submitted && mobile.errors && mobile.errors.pattern\" class=\"help-block err-msg\"> The mobile number format is invalid</div>\n                        <div *ngIf=\"error && error['mobile']\" class=\"err-msg\">{{error['mobile']['0']}}</div>\n                    </div>\n                    \n                    <div class=\"input-field col s4\">\n                       <input id=\"pin_code\" type=\"text\" name=\"pin_code\" (keyup)=\"getdetail('addressmodel',addressmodel.pin_code)\" class=\"validate\" [(ngModel)]=\"addressmodel.pin_code\" #pin_code=\"ngModel\" required pattern=\"^[1-9][0-9]{5}$\">\n                        <label for=\"pin_code\">Pincode</label>\n                      <div *ngIf=\"userAddress.submitted &&  pin_code.errors && pin_code.errors.required\" class=\"help-block err-msg\">The pincode field is required.</div>\n                           <div *ngIf=\"userAddress.submitted &&  pin_code.errors && pin_code.errors.pattern\" class=\"help-block err-msg\">Enter valid pincode.</div>\n                        <div *ngIf=\"error && error['pin_code'] && pin_code.valid\" class=\"err-msg\">{{error['pin_code']['0']}}</div>\n                    </div>\n                </div>\n                \n                <div class=\"row\">\n                  <div class=\"input-field col s4\">\n                    <input id=\"city\" type=\"text\" name=\"city\" class=\"validate\" [(ngModel)]=\"addressmodel.city\" #city=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"city\">City</label>\n                    <div *ngIf=\"userAddress.submitted && city.errors && city.errors.required\" class=\"help-block err-msg\">The city field is required.</div>\n                    <div *ngIf=\"userAddress.submitted &&  city.errors && city.errors.pattern\" class=\"help-block err-msg\">Enter valid city name.</div>\n                    <div *ngIf=\"error && error['city'] && city.valid\" class=\"err-msg\">{{error['city']['0']}}</div>\n                  </div>\n                    \n                  <div class=\"input-field col s4\">\n                    <input id=\"state\" type=\"text\" name=\"state\" class=\"validate\" [(ngModel)]=\"addressmodel.state\" #state=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"state\">State</label>\n                    <div *ngIf=\"userAddress.submitted  && state.errors && state.errors.required\" class=\"help-block err-msg\">The state field is required.</div>\n                     <div *ngIf=\"userAddress.submitted &&  state.errors && state.errors.pattern\" class=\"help-block err-msg\">Enter valid state name.</div>\n                    <div *ngIf=\"error && error['state'] && state.valid\" class=\"err-msg\">{{error['state']['0']}}</div>\n                  </div>\n                </div>\n                  \n                <div class=\"row\">\n                     <div class=\"col s8 button\">\n                         <a href=\"javascript:void(0);\" class=\"waves-effect fabivo-btn btn\" (click)=\"close_address_form(open_address)\">\n                             Cancel\n                         </a> \n                         <button class=\"waves-effect fabivo-btn btn pro2-btn\">\n                              Submit<i class=\"material-icons right\">send</i>\n                          </button>\n                     </div>\n                </div>\n            </form>\n        </div>\n        \n        <div class=\"col s12\" [hidden]=\"!edit_address\">\n            <form id=\"editAddress\" (ngSubmit)=\"editAddress.form.valid && editAddressSubmit()\" #editAddress=\"ngForm\" novalidate>\n                <div class=\"row\">\n                  <div class=\"input-field col s4\">\n                    <input id=\"first_name\" type=\"text\" name=\"edit_first_name\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_first_name\" #edit_first_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"first_name\">First Name</label>\n                    <div *ngIf=\"editAddress.submitted && edit_first_name.errors && edit_first_name.errors.required\" class=\"help-block err-msg\">The first name field is required.</div>\n                    <div *ngIf=\"editAddress.submitted && edit_first_name.errors && edit_first_name.errors.pattern\" class=\"help-block err-msg\">Enter valid first name.</div>\n                    <div *ngIf=\"error && error['edit_first_name'] && edit_first_name.valid\" class=\"err-msg\">{{error['edit_first_name']['0']}}</div>\n                  </div>\n                  <div class=\"input-field col s4\">\n                    <input id=\"last_name\" name =\"edit_last_name\" type=\"text\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_last_name\" #edit_last_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"last_name\">Last Name</label>\n                    <div *ngIf=\"editAddress.submitted && edit_last_name.errors && edit_last_name.errors.required\" class=\"help-block err-msg\">The last name field is required.</div>\n                     <div *ngIf=\"editAddress.submitted && edit_last_name.errors && edit_last_name.errors.pattern\" class=\"help-block err-msg\">Enter valid last name.</div>\n                    <div *ngIf=\"error && error['edit_last_name'] && edit_last_name.valid\" class=\"err-msg\">{{error['edit_last_name']['0']}}</div>\n                  </div>\n                </div>\n                  \n                <div class=\"row mar-top\">\n                  <div class=\"input-field col s8\">\n                    <input id=\"address_1\" type=\"text\" name=\"edit_address_1\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_address_1\" #edit_address_1=\"ngModel\" required>\n                    <label for=\"address_1\">Address 1</label>\n                    <div *ngIf=\"editAddress.submitted && !edit_address_1.valid\" class=\"help-block err-msg\">The address 1 field is required.</div>\n                    <div *ngIf=\"error && error['edit_address_1'] && edit_address_1.valid\" class=\"err-msg\">{{error['edit_address_1']['0']}}</div>\n                  </div>\n                </div>\n                \n                <div class=\"row\">\n                  <div class=\"input-field col s8\">\n                    <input id=\"address_2\" type=\"text\" name=\"edit_address_2\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_address_2\" #edit_address_2=\"ngModel\" required>\n                    <label for=\"address_2\">Address 2</label>\n                    <div *ngIf=\"editAddress.submitted && !edit_address_2.valid\" class=\"help-block err-msg\">The address 2 field is required.</div>\n                    <div *ngIf=\"error && error['edit_address_2'] && edit_address_2.valid\" class=\"err-msg\">{{error['edit_address_2']['0']}}</div>\n                  </div>\n                </div>\n                \n                <div class=\"row\">\n                    <div class=\"input-field col s4\">\n                        <input id=\"mobile\" type=\"text\" name=\"edit_mobile\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_mobile\" #edit_mobile=\"ngModel\" required  pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\" >\n                        <label for=\"mobile\" >Phone Number</label>\n                        <div *ngIf=\"editAddress.submitted && edit_mobile.errors && edit_mobile.errors.required\" class=\"help-block err-msg\">The mobile number field is required.</div>\n                        <div *ngIf=\"editAddress.submitted && edit_mobile.errors && edit_mobile.errors.pattern\" class=\"help-block err-msg\"> The mobile number format is invalid</div>\n                        <div *ngIf=\"error && error['edit_mobile']\" class=\"err-msg\">{{error['edit_mobile']['0']}}</div>\n                    </div>\n                    \n                    <div class=\"input-field col s4\">\n                       <input id=\"pin_code\" type=\"text\" name=\"edit_pin_code\" (keyup)=\"getdetail('editAddressmodel',editAddressmodel.edit_pin_code)\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_pin_code\" #edit_pin_code=\"ngModel\" required pattern=\"^[1-9][0-9]{5}$\">\n                        <label for=\"pin_code\">Pincode</label>\n                        <div *ngIf=\"editAddress.submitted &&  edit_pin_code.errors && edit_pin_code.errors.required\" class=\"help-block err-msg\">The pincode field is required.</div>\n                         <div *ngIf=\"editAddress.submitted &&  edit_pin_code.errors && edit_pin_code.errors.pattern\" class=\"help-block err-msg\">Enter valid pincode.</div>\n                        <div *ngIf=\"error && error['edit_pin_code'] && edit_pin_code.valid\" class=\"err-msg\">{{error['edit_pin_code']['0']}}</div>\n                    </div>\n                </div>\n                \n                <div class=\"row\">\n                  <div class=\"input-field col s4\">\n                    <input id=\"city\" type=\"text\" name=\"edit_city\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_city\" #edit_city=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"city\">City</label>\n                    <div *ngIf=\"editAddress.submitted && edit_city.errors && edit_city.errors.required\" class=\"help-block err-msg\">The city field is required.</div>\n                    <div *ngIf=\"editAddress.submitted &&  edit_city.errors && edit_city.errors.pattern\" class=\"help-block err-msg\">Enter valid city name.</div>\n                    <div *ngIf=\"error && error['edit_city'] && edit_city.valid\" class=\"err-msg\">{{error['edit_city']['0']}}</div>\n                  </div>\n                    \n                  <div class=\"input-field col s4\">\n                    <input id=\"state\" type=\"text\" name=\"edit_state\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_state\" #edit_state=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                    <label for=\"state\">State</label>\n                     <div *ngIf=\"editAddress.submitted  && edit_state.errors && edit_state.errors.required\" class=\"help-block err-msg\">The state field is required.</div>\n                     <div *ngIf=\"editAddress.submitted &&  edit_state.errors && edit_state.errors.pattern\" class=\"help-block err-msg\">Enter valid state name.</div>\n                    <div *ngIf=\"error && error['edit_state'] && edit_state.valid\" class=\"err-msg\">{{error['edit_state']['0']}}</div>\n                  </div>\n                </div>\n                  \n                <div class=\"row\">\n                     <div class=\"col s8 button\">\n                         <a href=\"javascript:void(0);\" class=\"waves-effect fabivo-btn btn\" (click)=\"close_address_form(edit_address)\">\n                             Cancel\n                         </a> \n                         <button class=\"waves-effect fabivo-btn btn pro2-btn\">\n                              Submit<i class=\"material-icons right\">send</i>\n                          </button>\n                     </div>\n                </div>\n            </form>\n        </div>\n        \n    </div>\n</div>"

/***/ },

/***/ 392:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var global_1 = __webpack_require__(363);
	var pager_service_1 = __webpack_require__(374);
	var page_service_1 = __webpack_require__(33);
	var MyOrderComponent = (function () {
	    function MyOrderComponent(userService, route, router, pagerService, pageService) {
	        this.userService = userService;
	        this.route = route;
	        this.router = router;
	        this.pagerService = pagerService;
	        this.pageService = pageService;
	        this.page = 1;
	        this.pager = {};
	        this.orders = [];
	        this.authDatas = {};
	    }
	    MyOrderComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.router.routerState.root.queryParams.subscribe(function (params) {
	            if (params['page']) {
	                _this.page = parseInt(params['page']);
	            }
	            _this.getList();
	        });
	        setTimeout(function () {
	            jQuery(document).ready(function () {
	                jQuery('.tabs').tabs();
	            });
	        }, 100);
	    };
	    MyOrderComponent.prototype.setPage = function (page) {
	        if (page < 1 || page > this.pager.totalPages) {
	            return;
	        }
	        this.page = page;
	        this.getList();
	    };
	    MyOrderComponent.prototype.getList = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        this.pageService.getOrder(this.authDatas.id, this.page).subscribe(function (orderData) {
	            _this.orders = orderData['data'];
	            _this.total = orderData['total'];
	            _this.per_page = orderData['per_page'];
	            ;
	            _this.pager = _this.pagerService.getPager(_this.total, _this.page, _this.per_page);
	        }, function (err) {
	            console.log(err);
	        });
	        global_1.unblockUI();
	    };
	    return MyOrderComponent;
	}());
	MyOrderComponent = __decorate([
	    core_1.Component({
	        selector: 'my-order',
	        template: __webpack_require__(393)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        pager_service_1.PagerService,
	        page_service_1.PageService])
	], MyOrderComponent);
	exports.MyOrderComponent = MyOrderComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 393:
/***/ function(module, exports) {

	module.exports = "<div id=\"pro3\" class=\"col s12\">\n    <div class=\"row\">\n        <div class=\"col s12\">\n\n            <div class=\"pro3details\" *ngFor=\"let order of orders\">\n                 <div class=\"track-bx\">\n                    <div class=\"row\">\n                        <div class=\"col s4\">\n                            <a [routerLink]=\"['../order-detail', order.order_id]\" class=\"track-id-bx\">\n                                {{order.order_id}}\n                        </a>\n                    </div>\n\n                    <div class=\"col s8 right-align\">\n                        <span class=\"h-heading left-align\">Payment By :- <span class=\"color-txt-2\">{{order.payment_by}}</span></span><br>\n                        <span class=\"h-heading  left-align\">Payment Status :- <span class=\"color-txt-1\">{{order.payment_status | capitalize}}</span></span>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"row \" >\n                 <div class=\"col s4\">\n                    <div class=\"row pro3-product-details-bx\"  *ngFor=\"let products of order.order_detail\">\n                        <div class=\"col s3\">\n                            <img class=\"responsive-img\" src=\"{{products.product.image}}\">        \n                        </div>\n                        <div class=\"col s9\">  \n                            <p><a [routerLink]=\"['../../product',products.product.slug]\">{{products.product.title}}</a></p>\n                            <p><b>Size:</b>{{products.product_size}}</p>\n                            <p><b>Quantity:</b>{{products.quantity}}</p> \n                            <p><b>price:</b><span [innerHTML]=\"products.price\"></span></p> \n                        </div>\n                    </div>\n                </div>\n                <div class=\"col s8\">\n\n                    <ul class=\"timeline\" id=\"timeline\">\n                        <li class=\"li complete\">\n                            <div class=\"timestamp\">\n\n                                <span class=\"date\">{{order.status['ordered']}}<span>\n                                    </span></span></div>\n                            <div class=\"status\">\n                                <h4> Ordered </h4>\n                            </div>\n                        </li>\n\n\n                    \n\n                        <li class=\"li complete\"  *ngIf=\"order.order_status=='reject'\">\n                            <div class=\"timestamp\">\n\n                                <span class=\"date\">{{(order.status['reject']!='')?order.status['reject']:''}}<span>\n                                    </span></span></div>\n                            <div class=\"status failed\" style=\"border:none\">\n                                <h4 class=\"failed\"> Rejected </h4>\n                            </div>\n                        </li>\n                          <li class=\"li complete\"   *ngIf=\"order.order_status=='cancel'\">\n                            <div class=\"timestamp\">\n\n                                <span class=\"date\">{{(order.status['cancel']!='')?order.status['cancel']:''}}<span>\n                                    </span></span></div>\n                            <div class=\"status failed\" style=\"border:none\">\n                                <h4 class=\"failed\"> Cancelled </h4>\n                            </div>\n                        </li>\n                           <li class=\"li complete\"  *ngIf=\"order.order_status=='failed'\">\n                            <div class=\"timestamp\">\n\n                                <span class=\"date\">{{(order.status['failed']!='')?order.status['failed']:''}}<span>\n                                    </span></span></div>\n                            <div class=\"status\" style=\"border:none\">\n                                <h4 class=\"failed\"> Failed </h4>\n                            </div>\n                        </li>\n                        <li class=\"li \" [ngClass]=\"{complete:order.status['inprocess']!= ''}\" *ngIf=\"order.order_status!='failed' && order.order_status!='reject' && order.order_status!='cancel'\">\n                            <div class=\"timestamp\">\n\n                                <span class=\"date\">{{(order.status['inprocess']!='')?order.status['inprocess']:''}}<span>\n                                    </span></span></div>\n                            <div class=\"status\">\n                                <h4> In Process </h4>\n                            </div>\n                        </li>\n                        <li class=\"li \" [ngClass]=\"{complete:order.status['shipped']!= ''}\" *ngIf=\"order.order_status!='failed' && order.order_status!='reject' && order.order_status!='cancel'\">\n                            <div class=\"timestamp\">\n\n                            <span class=\"date\">{{(order.status['shipped']!='')?order.status['shipped']:''}}<span>\n                                    </span></span></div>\n                            <div class=\"status\">\n                                <h4> Shipped </h4>\n                            </div>\n                        </li>\n                        <li class=\"li\" [ngClass]=\"{complete:order.status['delivered']!= ''}\" *ngIf=\"order.order_status!='failed' && order.order_status!='reject' && order.order_status!='cancel'\">\n                            <div class=\"timestamp\">\n\n                                <span class=\"date\">{{(order.status['delivered']!='')?order.status['delivered']:''}}<span>\n                                    </span></span></div>\n                            <div class=\"status\" style=\"border:none\">\n                                <h4> Delivered </h4>\n                            </div>\n                        </li>\n                    </ul>\n\n                </div>\n\n            </div>\n\n\n            <div class=\"total-bx\">\n                <div class=\"row\">\n                    <div class=\"col s3\">\n                        <p><b>Order Date:</b>{{order.created_at}}</p>\n                        <div *ngIf=\"order.order_status!='failed' && order.order_status!='reject' && order.order_status!='cancel'\">\n                            <p *ngIf=\"order.delivery_date\"><b>Delivery Date:</b>{{order.delivery_date}}</p>\n                            <p *ngIf=\"!order.delivery_date\"><b>Expected Delivery Date:</b>{{order.expected_delivery_date}}</p>\n                        </div>\n                        <p *ngIf=\"order.order_status=='pending'\" class=\"h-heading\">Your order is  pending</p>\n                        <p *ngIf=\"order.order_status=='failed'\" class='h-heading failed'>Your order is  failed</p>\n                        <p *ngIf=\"order.order_status=='inprocess'\" class='h-heading success'>Your order is  in process</p>\n                        <p *ngIf=\"order.order_status=='reject'\" class='h-heading failed'>Your order is rejected</p>\n                        <p *ngIf=\"order.order_status=='shipped'\" class='h-heading success'>Your order is shipped</p>\n                        <p *ngIf=\"order.order_status=='cancel'\" class='h-heading failed'>Your order is Cancelled</p>\n                        <p *ngIf=\"order.order_status=='delivered'\" class='h-heading success'>Your order is delivered</p>\n                        <p *ngIf=\"order.order_status=='success'\" class='h-heading success'>Your order is successfully placed.</p>\n\n\n                    </div>\n                    <div class=\"col s6\">\n                        <div *ngIf=\"order.order_status!='failed' &&  order.order_status!='reject' &&  order.order_status!='cancel'\">  \n                            <p *ngIf=\"order.is_cash_back && order.is_cash_back_transfer==0\" class='success'>Cash back of <span [innerHTML]=\"order.cash_back_amount\"></span> for Promo Code {{order.coupon_code|ucupper}} has been successfully added to your Fabivo Wallet on after shipped successfully. Happy Shopping!</p>\n                            <p *ngIf=\"order.is_cash_back && order.is_cash_back_transfer==1\" class='success'>Cash back of <span [innerHTML]=\"order.cash_back_amount\"></span> for Promo Code {{order.coupon_code|ucupper}} has been successfully added to your Fabivo Wallet</p>\n                        </div>\n                    </div>\n                    <div class=\"col s3 left-align\">\n                        <p><b> Sub Total:</b><span [innerHTML]=\"order.total_price\" ></span></p>\n                        <p *ngIf=\"order.enable_shipping_charge\"><b>Shipping:</b><span [innerHTML]=\"order.shipping_charge\" ></span></p>\n                        <p *ngIf=\"order.enable_tax\"><b>Tax:</b><span [innerHTML]=\"order.total_tax\" ></span></p>\n                        <p *ngIf=\"order.is_discount\"><b>Discount</b><span [innerHTML]=\"order.discount\" ></span></p>\n                        <p *ngIf=\"order.is_cod\"><b>Cod Money</b><span [innerHTML]=\"order.cod_money\" ></span></p>\n                        <p><b>Total:</b><span [innerHTML]=\"order.payble_amount\" ></span></p>\n\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"emptyPage\" *ngIf=\"total==0\" style=\"\"> \n            <div><i class=\"fa fa-search\" aria-hidden=\"true\"></i></div> \n            <div>No Order Found </div>    \n            <a class=\"waves-effect fabivo-btn btn\" [routerLink]=\"['/']\">\n                        <i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i>CONTINUE SHOPPING\n                    </a>\n        </div>\n        <div class=\"listing-breadcrumb\"> \n            <div class=\"col m12 s12 pagination_full\">\n\n\n\n                <!-- pager -->\n                <ul  *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination right\">\n                    <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                        <a  (click)=\"setPage(pager.currentPage - 1)\"><i class=\"material-icons\">chevron_left</i></a>\n                    </li>\n                    <li class=\"waves-effect\"  *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                        <a (click)=\"setPage(page)\">{{page}}</a>\n                    </li>\n                    <li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                        <a (click)=\"setPage(pager.currentPage + 1)\"><i class=\"material-icons\">chevron_right</i></a>\n                    </li>\n\n                </ul>\n            </div> \n        </div> \n\n    </div>\n</div>\n</div>"

/***/ },

/***/ 394:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var pager_service_1 = __webpack_require__(374);
	var global_1 = __webpack_require__(363);
	var page_service_1 = __webpack_require__(33);
	var MyWalletComponent = (function () {
	    function MyWalletComponent(userService, route, router, pageService, pagerService) {
	        this.userService = userService;
	        this.route = route;
	        this.router = router;
	        this.pageService = pageService;
	        this.pagerService = pagerService;
	        this.authDatas = {};
	        this.walletDetail = [];
	        this.page = 1;
	        this.wallet_amount = '';
	        this.pager = {};
	    }
	    MyWalletComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.router.routerState.root.queryParams.subscribe(function (params) {
	            if (params['page']) {
	                _this.page = parseInt(params['page']);
	            }
	            _this.getList();
	        });
	        setTimeout(function () {
	            jQuery(document).ready(function () {
	                jQuery('.tabs').tabs();
	            });
	        }, 100);
	    };
	    MyWalletComponent.prototype.setPage = function (page) {
	        if (page < 1 || page > this.pager.totalPages) {
	            return;
	        }
	        this.page = page;
	        this.getList();
	    };
	    MyWalletComponent.prototype.getList = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        this.pageService.getWallet(this.authDatas.id, this.page).subscribe(function (walletData) {
	            _this.walletDetail = walletData['data'];
	            _this.total = walletData['total'];
	            _this.per_page = walletData['per_page'];
	            ;
	            _this.wallet_amount = walletData['wallet_amount'];
	            ;
	            _this.pager = _this.pagerService.getPager(_this.total, _this.page, _this.per_page);
	        }, function (err) {
	            console.log(err);
	        });
	        global_1.unblockUI();
	    };
	    return MyWalletComponent;
	}());
	MyWalletComponent = __decorate([
	    core_1.Component({
	        selector: 'my-wallet',
	        template: __webpack_require__(395)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        page_service_1.PageService,
	        pager_service_1.PagerService])
	], MyWalletComponent);
	exports.MyWalletComponent = MyWalletComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 395:
/***/ function(module, exports) {

	module.exports = "<div id=\"pro4\" class=\"col s12\">\n    <div class=\"row\">\n        <div class=\"col s12\">\n            <div class=\"wallet-bx\">\n                <div class=\"money-info\">\n                    <span class=\"left\">Fabivo Wallet: <strong [innerHTML]=\"wallet_amount\"></strong></span>\n                    <a class=\"right\" [routerLink]=\"['/page','how-to-use-wallet']\">How to use wallet?</a>\n\n                    <div class=\"clearfix\"></div>\n                </div>\n                <div class=\"clearfix\">\n                    <table class=\" bordered striped highlight responsive-table\">\n                        <thead>\n                            <tr>\n                                <th data-field=\"description\">Description</th>\n                                <th data-field=\"withdrawal\">Withdrawal</th>\n                                <th data-field=\"deposit\">Deposit</th>\n                                <th data-field=\"date\">Date</th>\n                            </tr>\n                        </thead>\n                        <tbody>\n                            <tr *ngFor=\"let wallet of walletDetail\">\n                                <td>{{wallet['description']}}</td>\n                                <td [innerHTML]=\"wallet['flag'] == 'dr'?wallet['cash']:'-'\"></td>\n                                <td [innerHTML]=\"wallet['flag'] == 'cr'?wallet['cash']:'-'\"></td>\n                                <td>{{wallet['created_at']}}</td>\n                            </tr>\n                            <tr>\n                                <td  *ngIf=\"total==0\" colspan='4'>\n\n                                    No Data Found\n                                </td>\n\n                            </tr>\n\n                        </tbody>\n                    </table>\n\n                    <div class=\"listing-breadcrumb\"> \n                        <div class=\"col m12 s12 pagination_full\">\n                            <!-- pager -->\n                            <ul  *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination right\">\n                                <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                    <a  (click)=\"setPage(pager.currentPage - 1)\"><i class=\"material-icons\">chevron_left</i></a>\n                                </li>\n                                <li class=\"waves-effect\"  *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                                    <a (click)=\"setPage(page)\">{{page}}</a>\n                                </li>\n                                <li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                    <a (click)=\"setPage(pager.currentPage + 1)\"><i class=\"material-icons\">chevron_right</i></a>\n                                </li>\n                                \n                            </ul>\n                        </div> \n                    </div> \n                </div>\n            </div>\n        </div>\n\n    </div>\n</div>"

/***/ },

/***/ 396:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var global_1 = __webpack_require__(363);
	var authentication_service_1 = __webpack_require__(369);
	var ChangePasswordComponent = (function () {
	    function ChangePasswordComponent(userService, route, authenticationService) {
	        this.userService = userService;
	        this.route = route;
	        this.authenticationService = authenticationService;
	        this.changepasswordmodel = {};
	        this.authDatas = [];
	        this.loading = false;
	        this.error = '';
	        this.success = '';
	        this.is_socail = 0;
	    }
	    ChangePasswordComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.authDatas = this.authenticationService.authData;
	        setTimeout(function () {
	            jQuery(document).ready(function () {
	                jQuery('.tabs').tabs();
	            });
	        }, 100);
	        this.userService.getUserDetails(this.authDatas.id, this.authDatas.token).subscribe(function (userDetail) {
	            _this.is_socail = userDetail['is_socail'];
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    ChangePasswordComponent.prototype.changePasswordSubmit = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        this.changepasswordmodel['user_id'] = this.authDatas.id;
	        this.changepasswordmodel['token'] = this.authDatas.token;
	        this.userService.changePasswordSave(this.changepasswordmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.error = '';
	                jQuery('#changePassword')[0].reset();
	                jQuery('#changePassword').find('label').removeClass('active');
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.is_socail = 0;
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['error'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    return ChangePasswordComponent;
	}());
	ChangePasswordComponent = __decorate([
	    core_1.Component({
	        selector: 'change-password',
	        template: __webpack_require__(397)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        authentication_service_1.AuthenticationService])
	], ChangePasswordComponent);
	exports.ChangePasswordComponent = ChangePasswordComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 397:
/***/ function(module, exports) {

	module.exports = "<div id=\"pro5\" class=\"col s12\">\n    <div class=\"row\">\n        <form class=\"col s8\" id=\"changePassword\" (ngSubmit)=\"changePassword.form.valid && changePasswordSubmit()\" #changePassword=\"ngForm\" novalidate>\n            <div class=\"row\" *ngIf=\"!is_socail\">\n              <div class=\"input-field col s12\">\n                <input id=\"old_password\" type=\"password\" class=\"validate\" name='old_password' [(ngModel)]=\"changepasswordmodel.old_password\" #old_password=\"ngModel\" required>\n                <label for=\"old_password\">Old Password</label>\n                <div *ngIf=\"changePassword.submitted  && old_password.errors && old_password.errors.required\" class=\"help-block err-msg\">The old password field is required.</div>\n                <div *ngIf=\"error && error['old_password']\" class=\"err-msg\">{{error['old_password']['0']}}</div>\n              </div>\n            </div>\n                  \n            <div class=\"row\">\n                <div class=\"input-field col s6\">\n                    <input id=\"password\" type=\"password\" class=\"validate\" name='password' [(ngModel)]=\"changepasswordmodel.password\" #password=\"ngModel\" required  validateEqual=\"confirm_password\" reverse=\"true\">\n                    <label for=\"password\">New Password</label>\n                    <div *ngIf=\"changePassword.submitted  && password.errors && password.errors.required\" class=\"help-block err-msg\">The password field is required.</div>\n        \n                    <div *ngIf=\"error && error['password']\" class=\"err-msg\">{{error['password']['0']}}</div>\n                </div>\n\n                <div class=\"input-field col s6\">\n                    <input id=\"confirm_password\" type=\"password\" class=\"validate\" name='confirm_password' [(ngModel)]=\"changepasswordmodel.confirm_password\" #confirm_password=\"ngModel\" required validateEqual=\"password\" reverse=\"false\">\n                    <label for=\"confirm_password\">Confirm Password</label>\n                    <div *ngIf=\"changePassword.submitted && confirm_password.errors && confirm_password.errors.required\" class=\"help-block err-msg\">The confirm password field is required.</div>       \n                    <div *ngIf=\"changePassword.submitted && confirm_password.errors && !confirm_password.errors.validateEqual\" class=\"help-block err-msg\"> The confirm password and password must match.</div>\n                   \n  \n        \n                    <div *ngIf=\"error && error['confirm_password']\" class=\"err-msg\">{{error['confirm_password']['0']}}</div>\n                </div>\n            </div>\n                      \n            <div class=\"row\">\n                <div class=\"col s8 button\">\n                    <button [disabled]=\"loading\" class=\"waves-effect fabivo-btn btn\">\n                        Submit<i class=\"material-icons right\">send</i>\n                    </button>\n                </div>\n            </div>\n        </form>\n    </div>\n</div>  "

/***/ },

/***/ 398:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var product_service_1 = __webpack_require__(373);
	var pager_service_1 = __webpack_require__(374);
	var authentication_service_1 = __webpack_require__(369);
	var cart_service_1 = __webpack_require__(370);
	var global_1 = __webpack_require__(363);
	var common_1 = __webpack_require__(22);
	var WishlistComponent = (function () {
	    function WishlistComponent(productService, route, router, pagerService, location, authenticationService, cartService) {
	        this.productService = productService;
	        this.route = route;
	        this.router = router;
	        this.pagerService = pagerService;
	        this.location = location;
	        this.authenticationService = authenticationService;
	        this.cartService = cartService;
	        this.authData = [];
	        this.wish_products = [];
	        this.add_to_cart = {};
	        this.page = 1;
	        this.pager = {};
	        this.show_size = false;
	        this.size_error = false;
	        this.select_size = '';
	        this.show_product = '';
	        this.repeat_size = '';
	        this.remove_product = {};
	    }
	    WishlistComponent.prototype.ngOnInit = function () {
	        this.authData = JSON.parse(localStorage.getItem('loginUser'));
	        if (this.authData && this.authData.id) {
	            this.getList();
	        }
	    };
	    WishlistComponent.prototype.getList = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.productService.getWishlistProduct(this.authData.id, this.page).subscribe(function (result) {
	            _this.show_product = '';
	            _this.wish_products = result['data'];
	            _this.total = result['total'];
	            _this.per_page = result['per_page'];
	            ;
	            _this.pager = _this.pagerService.getPager(_this.total, _this.page, _this.per_page);
	            if (_this.page > 1) {
	                _this.changeUrl();
	            }
	        }, function (err) {
	            console.log(err);
	        });
	        global_1.unblockUI();
	    };
	    WishlistComponent.prototype.setPage = function (page) {
	        if (page < 1 || page > this.pager.totalPages) {
	            return;
	        }
	        this.page = page;
	        this.getList();
	    };
	    WishlistComponent.prototype.removeToWishlist = function (product_id) {
	        this.cartService.removeToWishlist(product_id);
	        this.getList();
	    };
	    WishlistComponent.prototype.showSize = function (product_id) {
	        this.show_product = product_id;
	    };
	    WishlistComponent.prototype.setSize = function (size, product_id) {
	        var _this = this;
	        global_1.blockUI();
	        this.add_to_cart['user_id'] = this.authData.id;
	        this.add_to_cart['product_id'] = product_id;
	        this.add_to_cart['product_size'] = size;
	        this.cartService.addCart(this.add_to_cart)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.repeat_size = _this.select_size;
	                _this.success = result['message'];
	                _this.remove_product['product_id'] = product_id;
	                _this.remove_product['user_id'] = _this.authData.id;
	                _this.cartService.removeWishlist(_this.remove_product).subscribe(function (result) {
	                    _this.cartService.getToWishlist(_this.authData.id);
	                    _this.getList();
	                });
	                global_1.unblockUI();
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['message'];
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    WishlistComponent.prototype.changeUrl = function () {
	        var url = 'wishlist?page=' + this.page;
	        this.location.go(url);
	    };
	    return WishlistComponent;
	}());
	WishlistComponent = __decorate([
	    core_1.Component({
	        selector: 'wish-list',
	        template: __webpack_require__(399),
	    }),
	    __metadata("design:paramtypes", [product_service_1.ProductService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        pager_service_1.PagerService,
	        common_1.Location,
	        authentication_service_1.AuthenticationService,
	        cart_service_1.CartService])
	], WishlistComponent);
	exports.WishlistComponent = WishlistComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 399:
/***/ function(module, exports) {

	module.exports = "<section class=\"listing-breadcrumb\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m6 s12\">\n\n            </div>\n\n        </div>\n    </div>  \n</section>\n\n<section class=\"filters-product\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m12 s6\">\n                <div class=\"category-content\">\n                    <div class=\"row\">\n                        <div class=\"col m3 s6\" *ngFor=\"let product_w of wish_products\">\n\n                             <div class=\"product-bx wish-product-bx card-panel hoverable \">\n\n                                <div class=\"product-img\">\n                                    <a [routerLink]=\"['../product',product_w.slug]\">\n                                        <img class=\"responsive-img\" src=\"{{product_w.image}}\">\n                                    </a>\n                                    <a href=\"javascript:void(0);\" (click)=\"removeToWishlist(product_w.id)\"   class=\"is_wishlist\">\n                                        <i aria-hidden=\"true\" class=\"fa fa-close\"></i>\n                                    </a>\n\n                                </div>\n\n                                <div class=\"product-details center-align\">\n                                    <a [routerLink]=\"['../product',product_w.slug]\">\n\n                                        <h5>{{product_w.title}}</h5>\n\n                                        <div class=\"prize\">\n                                            <span [innerHTML]=\"product_w.price\"></span>\n                                        </div>\n                                    </a>\n                                    <div class=\"clearfix\"></div>\n\n                                    <div *ngIf=\"product_w.id== show_product\" class=\"product-detail product-size product-size1\">\n                                        <ul class=\"product-info\">\n\n                                            <li *ngFor=\"let attribute of product_w.size\" [class.active]=\"select_size == attribute.slug\" (click)=\"setSize(attribute.slug,product_w.id)\">\n                                                <a class=\"waves-effect btn\">{{attribute.name}}</a>\n                                            </li>\n\n\n                                        </ul>\n\n\n                                    </div>\n\n                                    <div class=\"clearfix\"></div>\n\n\n\n                                    <a *ngIf=\"product_w.id!= show_product\" (click)=\"showSize(product_w.id)\"  class=\"waves-effect btn fabivo-btn-3\">Add To Bag</a>\n\n                                    <div class=\"clearfix\"></div>\n\n\n                                </div> \n                            </div>\n\n                        </div>\n\n                        <div class=\"emptyPage\" *ngIf=\"!wish_products || wish_products.length == 0\" style=\"\"> \n                            <div><i class=\"fa fa-search\" aria-hidden=\"true\"></i></div> \n                            <div>No Match Found</div> \n                        </div>\n                    </div>\n\n                    <div class=\"listing-breadcrumb\"> \n                        <div class=\"col m12 s12 pagination_full\">\n\n\n\n                            <!-- pager -->\n                            <ul  *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination right\">\n                                <!--<li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                    <a (click)=\"setPage(1)\">First</a>\n                                </li> -->\n                                <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                                    <a  (click)=\"setPage(pager.currentPage - 1)\"><i class=\"material-icons\">chevron_left</i></a>\n                                </li>\n                                <li class=\"waves-effect\"  *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                                    <a (click)=\"setPage(page)\">{{page}}</a>\n                                </li>\n                                <li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                    <a (click)=\"setPage(pager.currentPage + 1)\"><i class=\"material-icons\">chevron_right</i></a>\n                                </li>\n                                <!--\n                                <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                                    <a (click)=\"setPage(pager.totalPages)\">Last</a>\n                                </li> -->\n                            </ul>\n                        </div> \n                    </div> \n\n                </div>\n            </div>\n        </div>\n    </div>\n</section>"

/***/ },

/***/ 400:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var user_service_1 = __webpack_require__(378);
	var authentication_service_1 = __webpack_require__(369);
	var cart_service_1 = __webpack_require__(370);
	var global_1 = __webpack_require__(363);
	var router_1 = __webpack_require__(361);
	var customEvent = document.createEvent('Event');
	var CartComponent = (function () {
	    function CartComponent(userService, authenticationService, cartService, router) {
	        this.userService = userService;
	        this.authenticationService = authenticationService;
	        this.cartService = cartService;
	        this.router = router;
	        this.loading = false;
	        this.error = '';
	        this.success = '';
	        this.authData = [];
	        this.carts = [];
	        this.add_product = {};
	        this.remove_product = {};
	        this.coupan_code = '';
	        this.coupen_code_apply = 0;
	        this.coupen_error = '';
	        this.coupen_error_status = 0;
	        this.coupen_massage = 0;
	        this.add_wishlist = {};
	        this.controlEnabled = true;
	    }
	    CartComponent.prototype.ngOnInit = function () {
	        this.authData = JSON.parse(localStorage.getItem('loginUser'));
	        if (this.authData && this.authData.id) {
	            this.allCartItem();
	        }
	    };
	    CartComponent.prototype.ngAfterViewInit = function () {
	    };
	    CartComponent.prototype.allCartItem = function () {
	        var _this = this;
	        var coupan_code = '';
	        if (this.coupen_code_apply) {
	            coupan_code = this.coupan_code;
	        }
	        this.cartService.getcartItem(this.authData.id, coupan_code).subscribe(function (cartsData) {
	            _this.carts = cartsData['data'];
	            _this.cart_length = cartsData['data']['products'].length;
	            _this.materialSelectBox();
	            _this.coupanCodeValidate(cartsData['data']);
	        }, function (err) {
	            console.log(err);
	        });
	        this.cartService.getCart(this.authData.id).subscribe(function (cartData) {
	            _this.cartService.cart = cartData['data'];
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    CartComponent.prototype.changeCart = function (cart_id, size, user_id, product_id, quantity, flag) {
	        var _this = this;
	        global_1.blockUI();
	        this.add_product['cart_id'] = cart_id;
	        this.add_product['user_id'] = user_id;
	        this.add_product['product_id'] = product_id;
	        this.add_product['product_size'] = size;
	        this.add_product['quantity'] = quantity;
	        this.add_product['flag'] = flag;
	        if (this.coupen_code_apply) {
	            this.add_product['coupan_code'] = this.coupan_code;
	        }
	        else {
	            this.add_product['coupan_code'] = '';
	        }
	        this.cartService.updateCart(this.add_product)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.carts = result['data'];
	                _this.coupanCodeValidate(result['data']);
	                _this.cart_length = result['data']['products'].length;
	                _this.success = result['message'];
	                _this.materialSelectBox();
	                setTimeout(function () {
	                    global_1.unblockUI();
	                }, 500);
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.carts = result['data'];
	                _this.coupanCodeValidate(result['data']);
	                _this.cart_length = result['data']['products'].length;
	                _this.error = result['message'];
	                _this.materialSelectBox();
	                setTimeout(function () {
	                    global_1.unblockUI();
	                }, 500);
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    CartComponent.prototype.removeCart = function (cart_id) {
	        var _this = this;
	        global_1.blockUI();
	        this.remove_product['cart_id'] = cart_id;
	        this.remove_product['user_id'] = this.authData.id;
	        if (this.coupen_code_apply) {
	            this.remove_product['coupan_code'] = this.coupan_code;
	        }
	        else {
	            this.remove_product['coupan_code'] = '';
	        }
	        this.cartService.removeCart(this.remove_product)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.cartService.getCart(_this.authData.id).subscribe(function (cartData) {
	                    _this.cartService.cart = cartData['data'];
	                }, function (err) {
	                    console.log(err);
	                });
	                _this.carts = result['data'];
	                _this.coupanCodeValidate(result['data']);
	                if (result['data']['products'] && result['data']['products'].length > 0) {
	                    _this.cart_length = result['data']['products'].length;
	                }
	                else {
	                    _this.cart_length = 0;
	                }
	                _this.success = result['message'];
	                _this.materialSelectBox();
	                setTimeout(function () {
	                    global_1.unblockUI();
	                }, 500);
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['message'];
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    CartComponent.prototype.materialSelectBox = function () {
	        setTimeout(function () {
	            jQuery('select').material_select();
	            jQuery("select").on('change', function () {
	                var id = jQuery(this).attr("id");
	                customEvent.initEvent('change', true, true);
	                jQuery("#" + id)[0].dispatchEvent(customEvent);
	            });
	            jQuery(".cart-product-info-bx").mCustomScrollbar('destroy');
	            if (jQuery(window).width() > 641) {
	                jQuery(".cart-product-info-bx").mCustomScrollbar({
	                    autoDraggerLength: false,
	                    mouseWheel: {
	                        scrollAmount: 128
	                    }
	                });
	            }
	            jQuery(window).resize(function () {
	                if (window.innerWidth > 641) {
	                    jQuery(".cart-product-info-bx").mCustomScrollbar({
	                        autoDraggerLength: false,
	                        mouseWheel: {
	                            scrollAmount: 128
	                        }
	                    });
	                }
	                else {
	                    jQuery(".cart-product-info-bx").mCustomScrollbar('destroy');
	                }
	            });
	        }, 500);
	    };
	    CartComponent.prototype.coupanCodeValidate = function (data) {
	        if (this.coupan_code != '') {
	            if (this.coupen_code_apply == 1) {
	                if (data['coupon_code_error'] != '') {
	                    this.coupen_error = data['coupon_code_error'];
	                    this.coupen_code_apply = 0;
	                    this.coupen_error_status = 1;
	                }
	            }
	            else {
	            }
	        }
	    };
	    CartComponent.prototype.checkCoupanCode = function () {
	        var _this = this;
	        if (this.coupan_code != '') {
	            global_1.blockUI();
	            this.coupen_error = '';
	            this.cartService.checkCode(this.authData.id, this.coupan_code)
	                .subscribe(function (result) {
	                if (result['status_code'] === 1) {
	                    _this.coupen_massage = result['message'];
	                    _this.coupen_code_apply = 1;
	                    _this.coupen_error_status = 0;
	                    _this.allCartItem();
	                    setTimeout(function () {
	                        global_1.unblockUI();
	                    }, 500);
	                }
	                else {
	                    _this.coupen_error = result['message'];
	                    _this.coupen_code_apply = 0;
	                    _this.coupen_error_status = 1;
	                    _this.allCartItem();
	                    setTimeout(function () {
	                        global_1.unblockUI();
	                    }, 500);
	                }
	            });
	        }
	    };
	    CartComponent.prototype.RemoveCoupanCode = function () {
	        if (this.coupan_code != '') {
	            global_1.blockUI();
	            this.coupen_error = '';
	            this.coupen_error = '';
	            this.coupen_code_apply = 0;
	            this.coupen_error_status = 0;
	            this.allCartItem();
	            setTimeout(function () {
	                global_1.unblockUI();
	            }, 500);
	        }
	    };
	    CartComponent.prototype.checkout = function () {
	        if (this.carts['stock_error'] == 1) {
	            global_1.toast('Some item out off stock', 'error');
	        }
	        else {
	            this.cartService.orderData = this.carts;
	            this.router.navigate(['../checkout']);
	        }
	    };
	    CartComponent.prototype.addToWishlist = function (product_id) {
	        var _this = this;
	        if (this.authenticationService.isLoggedIn()) {
	            global_1.blockUI();
	            this.loading = true;
	            var userData = this.authenticationService.authData;
	            this.add_wishlist['product_id'] = product_id;
	            this.add_wishlist['user_id'] = userData['id'];
	            this.add_wishlist['userToken'] = userData['token'];
	            alert("jkdhsakd");
	            console.log(this.add_wishlist);
	            this.cartService.addWishlist(this.add_wishlist).subscribe(function (result) {
	                global_1.unblockUI();
	                _this.loading = false;
	                if (result['status_code'] === 1) {
	                    _this.success = result['message'];
	                    global_1.toast(_this.success, 'success');
	                }
	                else {
	                    _this.error = result['message'];
	                    global_1.toast(_this.error, 'error');
	                }
	            });
	        }
	        else {
	            jQuery('#modal1').modal('open');
	        }
	    };
	    return CartComponent;
	}());
	CartComponent = __decorate([
	    core_1.Component({
	        selector: 'my-cart',
	        template: __webpack_require__(401)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        authentication_service_1.AuthenticationService,
	        cart_service_1.CartService,
	        router_1.Router])
	], CartComponent);
	exports.CartComponent = CartComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 401:
/***/ function(module, exports) {

	module.exports = "<section class=\"cart-section\">\n    <div class=\"container\">\n        <div class=\"row\" *ngIf=\"cart_length && cart_length > 0\">\n            <div class=\"col m7 s12\">\n                <div class=\"cart-product-info-bx\">    \n                  \n                    <div class=\"cart-product-info\" *ngFor=\"let cart_data of carts['products']; let i = index\">\n                        <div class=\"row\">\n                            <div class=\"col s5\">\n                                <div class=\"cart-product-img\">\n                                    <a [routerLink]=\"['../product',cart_data['product']['slug']]\">\n                                        <img class=\"responsive-img\" src=\"{{cart_data['product']['image']}}\"/>\n                                    </a>\n                                  \n                                    <div class=\"cart-remove\">\n                                        <a *ngIf=\"cartService.wishlistArray.indexOf(cart_data['product_id']) > -1\"  href=\"javascript:void(0);\"   ><i  class=\"fa fa-heart\" aria-hidden=\"true\" (click)=\"cartService.removeToWishlist(cart_data['product_id'])\"></i></a> \n                                        <a *ngIf=\"cartService.wishlistArray.indexOf(cart_data['product_id']) ==-1\" href=\"javascript:void(0);\"  ><i class=\"fa fa-heart-o\" aria-hidden=\"true\" (click)=\"cartService.addToWishlist(cart_data['product_id'])\" ></i></a>\n                                        <a href=\"javascript:void(0);\"><i class=\"fa fa-times\" (click)=\"removeCart(cart_data['id'])\" aria-hidden=\"true\"></i></a>\n                                    </div>\n                                </div>\n                            </div>\n                            <div class=\"col s7\">\n                                <div class=\"cart-details\">\n                                    <a [routerLink]=\"['../product',cart_data['product']['slug']]\">\n                                        <h5>{{cart_data['product']['title']}}</h5>\n                                    </a>\n                                    <div class=\"size-qty\">\n                                        <div class=\"left\">\n                                            <div class=\"heading left\">Qty:</div>\n                                            <div class=\"input-field left\">\n                                                <select id=\"quantity_{{cart_data['id']}}\" [(ngModel)]=\"cart_data['quantity']\" #quantity (change)=\"changeCart(cart_data['id'], size.value, cart_data['user_id'], cart_data['product_id'], quantity.value, 'quantity')\">\n                                                    <option value=1>1</option>\n                                                    <option value=2>2</option>\n                                                    <option value=3>3</option>\n                                                    <option value=4>4</option>\n                                                    <option value=5>5</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                        <div class=\"right\">\n                                            <div class=\"heading left\">Size:</div>\n                                            <div class=\"input-field left\">\n                                                <select  id=\"size_{{cart_data['id']}}\" class=\"\"  [(ngModel)]=\"cart_data['product_size']\" #size (change)=\"changeCart(cart_data['id'], size.value, cart_data['user_id'], cart_data['product_id'], quantity.value, 'size')\">\n<!--                                                    <option [value]=\"i\" *ngFor=\"let i of sizes\">{{i}}</option>-->\n                                                     <option *ngFor=\"let size of cart_data['product']['size']\" [value]=\"size.slug\" >{{size.name }}</option>\n                                                </select>\n                                                                                          </div>\n                                        </div>\n                                        <div class=\"clearfix\"></div>\n                                    </div>  \n                                    \n                                    <h6><span class=\"heading\">Price:</span><span class=\"heading\" [innerHTML]=\"cart_data['product']['price']\"></span></h6>\n                                    <p *ngIf=\"cart_data['in_stock']==0\" class=\"failed\">Out of stock</p>\n                                    <p *ngIf=\"cart_data['in_stock']==1 && cart_data['stock'] < cart_data['quantity']\" class=\"failed\">Only {{cart_data['stock']}} item is in stock</p>\n\n           \n                                </div> \n                            </div>\n                        </div>\n\n\n\n          \n                    </div>\n                    \n                </div> \n                <div class=\"cart-itam\">\n                    <i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i>\n                    {{cart_length}} item in your shopping bag\n                </div>\n            </div>\n            <div class=\" col m5 s12\">\n                <div class=\"cart-product-prize\">\n                    <div class=\"code\">\n                        <div class=\"input-field left\">\n                            <input  [disabled]=\"coupen_code_apply || carts['stock_error'] == 1  \" id=\"search\" [(ngModel)]=\"coupan_code\" placeholder=\"Enter Voucher Code\" required=\"\" type=\"search\">        \n                        </div>\n                        <a *ngIf=\"!coupen_code_apply\" class=\"waves-effect btn fabivo-btn-3 right\" [attr.disabled]=\"carts['stock_error'] == 1 ? true:null\"  (click)=\"checkCoupanCode()\">Check</a>\n                        <a *ngIf=\"coupen_code_apply\" class=\"waves-effect btn fabivo-btn-3 right\" (click)=\"RemoveCoupanCode()\">Remove</a>\n                        <div class=\"clearfix\"></div>   \n                        <div class=\"help-block err-msg\"  *ngIf=\"coupen_error_status\" [innerHTML]=\"coupen_error\"></div>\n                        <div class=\"success-msg\"  *ngIf=\"coupen_code_apply\">{{coupen_massage}}</div>\n                    </div>\n                    <div class=\"pay-details\" *ngIf=\"carts['enable_cashback']\"  >\n                        <h5>Cashback Summary</h5>\n                        <span class=\"left\">{{carts['coupon_code']}}</span>\n                        <span class=\"right\" [innerHTML]=\"carts['cash_back_amount']\" ></span>\n                        <div class=\"clearfix\"></div>\n                        <p [innerHTML]=\"carts['coupon_description']  | sanitizeHtml\" ></p>\n                    </div>\n                    <span class=\"border\"></span>\n                    <div class=\"pay-details\">\n                        <h5>Summary</h5>\n                        <span class=\"left\">Sub Total</span>\n                        <span class=\"right\" [innerHTML]=\"carts['total_price'] ? carts['total_price'] : 0\"></span>\n                        <div class=\"clearfix\"></div>\n                            <div *ngIf=\"carts['enable_shipping_charge']\">\n                            \n                                    \n                            <span class=\"left\">Shipping</span>\n                            <span class=\"right\" [innerHTML]=\"carts['shipping_charge']\"></span>\n                            <div class=\"clearfix\"></div>\n                            \n                        </div>\n                        <div *ngIf=\"carts['enable_tax']\">\n                        <div  *ngFor=\"let tax of carts['tax']\">\n                                \n                        <span class=\"left\">{{tax['name']}}</span>\n                        <span class=\"right\" [innerHTML]=\"tax['amount']\"></span>\n                        <div class=\"clearfix\"></div>\n                        </div>\n                        </div>\n                        <div *ngIf=\"carts['enable_discount']\">\n                            \n                                    \n                            <span class=\"left\">Discount</span>\n                            <span class=\"right\" [innerHTML]=\"carts['discount']\"></span>\n                            <div class=\"clearfix\"></div>\n                            \n                        </div>\n\n                        <!--                                     <span class=\"left\">Sub Total</span>\n                                                             <span class=\"right\">&#x20B9; -100</span>\n                                                             <div class=\"clearfix\"></div>-->\n                    </div>\n                    <span class=\"border\"></span>\n                    <div class=\"net-details\">\n                        <h5 class=\"left\">Payable Amount</h5><span class=\"right\" [innerHTML]=\"carts['payble_amount'] ? carts['payble_amount'] : 0\"></span>\n                        <div class=\"clearfix\"></div>\n                    </div>\n                </div>\n                <div class=\"pay-option\">\n                    <a class=\"waves-effect fabivo-btn btn\" (click)=\"checkout()\"  [attr.disabled]=\"carts['stock_error'] == 1 ? true:null\" >Place  order</a>\n                    <ul>\n                        <li><a class=\"card-n card-1\" href=\"javascript:void(0);\"></a></li>\n                        <li><a class=\"card-n card-2\" href=\"javascript:void(0);\"></a></li>\n                        <li><a class=\"card-n card-3\" href=\"javascript:void(0);\"></a></li>\n                        <li><a class=\"card-n card-4\" href=\"javascript:void(0);\"></a></li>\n                        <div class=\"clearfix\"></div>\n                    </ul>  \n                </div>                         \n            </div>\n        </div>\n        \n        <div class=\"row\" *ngIf=\"!cart_length || cart_length === 0\">\n            <div class=\"col m12 s12\">\n                <div class=\"cart-itam\">\n                    <i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i>\n                    No item in your shopping bag.\n                    <a class=\"waves-effect fabivo-btn btn\" [routerLink]=\"['/']\">\n                        CONTINUE SHOPPING\n                    </a>\n                </div>\n            </div>\n        </div>\n    </div>              \n</section> "

/***/ },

/***/ 402:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var user_service_1 = __webpack_require__(378);
	var router_1 = __webpack_require__(361);
	var authentication_service_1 = __webpack_require__(369);
	var cart_service_1 = __webpack_require__(370);
	var common_1 = __webpack_require__(22);
	var CheckoutComponent = (function () {
	    function CheckoutComponent(userService, authenticationService, cartService, location, router) {
	        this.userService = userService;
	        this.authenticationService = authenticationService;
	        this.cartService = cartService;
	        this.location = location;
	        this.router = router;
	        this.loading = false;
	        this.second_step = false;
	        this.error = '';
	        this.errors = '';
	        this.type = '';
	        this.success = '';
	        this.authData = [];
	        this.carts = [];
	        this.selected_address = [];
	        this.order_data = [];
	    }
	    CheckoutComponent.prototype.ngOnInit = function () {
	        if (Object.keys(this.cartService.orderData).length > 0) {
	            if (!this.isDisabled('address')) {
	                this.router.navigate(['./checkout', 'address']);
	            }
	            this.authData = JSON.parse(localStorage.getItem('loginUser'));
	            if (this.authData && this.authData.id) {
	                this.carts = this.cartService.orderData;
	                this.cart_length = this.cartService.orderData['products'].length;
	                this.order_data['email'] = this.authData.email;
	                this.order_data = this.cartService.orderData;
	            }
	        }
	        else {
	            this.router.navigate(['/']);
	        }
	    };
	    CheckoutComponent.prototype.changeUrl = function (url) {
	        if (this.second_step) {
	            this.router.navigate(['./checkout', url]);
	        }
	        if (url === 'address') {
	            this.second_step = false;
	        }
	    };
	    CheckoutComponent.prototype.isDisabled = function (path) {
	        return this.location.path().indexOf(path) > -1;
	    };
	    return CheckoutComponent;
	}());
	CheckoutComponent = __decorate([
	    core_1.Component({
	        selector: 'checkout',
	        template: __webpack_require__(403)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        authentication_service_1.AuthenticationService,
	        cart_service_1.CartService,
	        common_1.Location,
	        router_1.Router])
	], CheckoutComponent);
	exports.CheckoutComponent = CheckoutComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 403:
/***/ function(module, exports) {

	module.exports = "<section class=\"chackout-form\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m4 s12\">\n                <div class=\"login-details\" (click)=\"changeUrl('address')\">\n                    <div class=\"heading\">\n                        <h5>\n                            <span>1</span>\n                            SHIPMENT ADDRESS \n                        </h5>\n                    </div>\n                    <div class=\"mail-id\" *ngIf=\"!selected_address.id\">\n                        <p>Select a Shipping Address</p>\n                    </div>\n                    \n                    <div class=\"mail-id\" *ngIf=\"selected_address.id\">\n                        <div class=\"panel-box\"> \n                            <h3 class=\"fixed_dynamic_content\">\n                                <div class=\"dynamic-left\"> {{selected_address.first_name}} {{selected_address.last_name}} </div> \n                            </h3> \n\n                            <div class=\"text-grayed-12\"> \n                                <br>{{selected_address.address_1}} \n                                <br>{{selected_address.address_2}} \n                                <br>{{selected_address.city}}, {{selected_address.pin_code}}\n                                <br>{{selected_address.state}}\n                                <br> \n                                <div>Mobile No. {{selected_address.mobile}}</div> \n                                <br> \n                            </div> \n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"login-details \" (click)=\"changeUrl('summary')\" [class.disabled]=\"isDisabled('/address')\" [class.order_summary_xl]=\"isDisabled('/address')\">\n                    <div class=\"heading\">\n                        <h5>\n                            <span>2</span>\n                            ORDER SUMMARY\n                        </h5>\n                    </div>\n                    <div class=\"mail-id\">\n                        <h6>You have {{cart_length > 1 ? cart_length + ' items' : cart_length + ' item'}} in your bag</h6>\n                        <div class=\"order-1\">\n                            <span class=\"left\">Sub Total</span>\n                            <span class=\"right\" [innerHTML]=\"carts['total_price'] ? carts['total_price'] : 0\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n                        <div  *ngIf=\"carts['enable_shipping_charge']\" class=\"order-1\">\n                            <span class=\"left\">Shipping</span>\n                            <span class=\"right\" [innerHTML]=\"carts['shipping_charge'] ? carts['shipping_charge'] : 0\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n                        \n                        <div class=\"order-1\" *ngIf=\"carts['enable_tax']\">\n                                  <div  *ngFor=\"let tax of carts['tax']\">\n                                \n                        <span class=\"left\">{{tax['name']}}</span>\n                        <span class=\"right\" [innerHTML]=\"tax['amount']\"></span>\n                        <div class=\"clearfix\"></div>\n                        </div>\n                        </div>\n                        <div class=\"clearfix\"></div>\n                             <div *ngIf=\"carts['enable_discount']\" class=\"order-1\"  >\n                            \n                                    \n                            <span class=\"left\">Discount</span>\n                            <span class=\"right\" [innerHTML]=\"carts['discount']\"></span>\n                            <div class=\"clearfix\"></div>\n                            \n                          </div> \n                      <div *ngIf=\"type == 'COD' && carts['is_cod_money']\" class=\"order-1\"  >\n                            \n                                    \n                            <span class=\"left\">COD</span>\n                            <span class=\"right\" [innerHTML]=\"carts['cod_money']\"></span>\n                            <div class=\"clearfix\"></div>\n                            \n                          </div>\n                        \n                        <div class=\"order-2\">\n                            <span class=\"left\">Net Amount</span>\n                            <span class=\"right\" [innerHTML]=\"type != 'COD'?carts['payble_amount']:carts['cod_payble_amount']\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n                    </div>\n                </div>\n            </div>\n\n            <router-outlet></router-outlet>\n             <div class=\"col m4 s12 \" *ngIf=\"isDisabled('/address')\"  [class.order_summary_sm]=\"isDisabled('/address')\">\n          \n\n                <div class=\"login-details\" (click)=\"changeUrl('summary')\" [class.disabled]=\"isDisabled('/address')\">\n                    <div class=\"heading\">\n                        <h5>\n                            <span>2</span>\n                            ORDER SUMMARY\n                        </h5>\n                    </div>\n                    <div class=\"mail-id\">\n                        <h6>You have {{cart_length > 1 ? cart_length + ' items' : cart_length + ' item'}} in your bag</h6>\n                        <div class=\"order-1\">\n                            <span class=\"left\">Sub Total</span>\n                            <span class=\"right\" [innerHTML]=\"carts['total_price'] ? carts['total_price'] : 0\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n                        <div  *ngIf=\"carts['enable_shipping_charge']\" class=\"order-1\">\n                            <span class=\"left\">Shipping</span>\n                            <span class=\"right\" [innerHTML]=\"carts['shipping_charge'] ? carts['shipping_charge'] : 0\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n                        \n                        <div class=\"order-1\" *ngIf=\"carts['enable_tax']\">\n                                  <div  *ngFor=\"let tax of carts['tax']\">\n                                \n                        <span class=\"left\">{{tax['name']}}</span>\n                        <span class=\"right\" [innerHTML]=\"tax['amount']\"></span>\n                        <div class=\"clearfix\"></div>\n                        </div>\n                        </div>\n                        <div class=\"clearfix\"></div>\n                             <div *ngIf=\"carts['enable_discount']\" class=\"order-1\"  >\n                            \n                                    \n                            <span class=\"left\">Discount</span>\n                            <span class=\"right\" [innerHTML]=\"carts['discount']\"></span>\n                            <div class=\"clearfix\"></div>\n                            \n                          </div> \n                      <div *ngIf=\"type == 'COD' && carts['is_cod_money']\" class=\"order-1\"  >\n                            \n                                    \n                            <span class=\"left\">COD</span>\n                            <span class=\"right\" [innerHTML]=\"carts['cod_money']\"></span>\n                            <div class=\"clearfix\"></div>\n                            \n                          </div>\n                        \n                        <div class=\"order-2\">\n                            <span class=\"left\">Net Amount</span>\n                            <span class=\"right\" [innerHTML]=\"type != 'COD'?carts['payble_amount']:carts['cod_payble_amount']\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n                    </div>\n                </div>\n            </div>\n\n        </div>\n    </div>\n</section>"

/***/ },

/***/ 404:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var global_1 = __webpack_require__(363);
	var checkout_component_1 = __webpack_require__(402);
	var cart_service_1 = __webpack_require__(370);
	var CheckoutAddressComponent = (function () {
	    function CheckoutAddressComponent(userService, route, router, checkout, cartService) {
	        this.userService = userService;
	        this.route = route;
	        this.router = router;
	        this.checkout = checkout;
	        this.cartService = cartService;
	        this.open_address = false;
	        this.edit_address = false;
	        this.authDatas = [];
	        this.addressData = [];
	        this.search = [];
	        this.addressmodel = {};
	        this.editAddressmodel = {};
	        this.loading = false;
	        this.error = '';
	        this.errors = '';
	        this.success = '';
	    }
	    CheckoutAddressComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        if (Object.keys(this.cartService.orderData).length > 0) {
	            this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	            this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(function (addressDetail) {
	                _this.addressData = addressDetail['data'];
	                if (_this.addressData[0]['is_default']) {
	                    _this.checkout.selected_address = _this.addressData[0];
	                    _this.checkout.order_data['address'] = _this.addressData[0];
	                    _this.checkout.cod_charge = 0;
	                    _this.checkout.second_step = true;
	                }
	                console.log(_this.addressData[0]);
	            }, function (err) {
	                console.log(err);
	            });
	        }
	        else {
	            this.router.navigate(['/']);
	        }
	    };
	    CheckoutAddressComponent.prototype.addressSubmit = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.addressmodel['user_id'] = this.authDatas.id;
	        this.addressmodel['token'] = this.authDatas.token;
	        this.userService.addressSave(this.addressmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.error = '';
	                jQuery('#userAddress')[0].reset();
	                jQuery('#userAddress').find('label').removeClass('active');
	                _this.open_address = false;
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.addressData.push(result['data']);
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['error'];
	                _this.errors = result['message'];
	                _this.loading = false;
	                _this.open_address = true;
	                global_1.unblockUI();
	                global_1.toast(_this.errors, 'error');
	            }
	        });
	    };
	    CheckoutAddressComponent.prototype.editAddressSubmit = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.editAddressmodel['user_id'] = this.authDatas.id;
	        this.editAddressmodel['token'] = this.authDatas.token;
	        this.edit_address = false;
	        this.userService.editAddressSave(this.editAddressmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.error = '';
	                jQuery('#editAddress')[0].reset();
	                jQuery('#editAddress').find('label').removeClass('active');
	                _this.edit_address = false;
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.addressData = result['data'];
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['error'];
	                _this.errors = result['message'];
	                _this.loading = false;
	                _this.edit_address = true;
	                global_1.unblockUI();
	                global_1.toast(_this.errors, 'error');
	            }
	        });
	    };
	    CheckoutAddressComponent.prototype.showEdit = function (address_id) {
	        this.editAddressmodel = this.addressData.filter(function (address1) { return address1.id == address_id; })[0];
	        this.editAddressmodel.edit_first_name = this.editAddressmodel.first_name;
	        this.editAddressmodel.edit_last_name = this.editAddressmodel.last_name;
	        this.editAddressmodel.edit_address_1 = this.editAddressmodel.address_1;
	        this.editAddressmodel.edit_address_2 = this.editAddressmodel.address_2;
	        this.editAddressmodel.edit_mobile = this.editAddressmodel.mobile;
	        this.editAddressmodel.edit_pin_code = this.editAddressmodel.pin_code;
	        this.editAddressmodel.edit_city = this.editAddressmodel.city;
	        this.editAddressmodel.edit_state = this.editAddressmodel.state;
	        jQuery('#editAddress').find('label').addClass('active');
	    };
	    CheckoutAddressComponent.prototype.close_address_form = function (mode) {
	        var _this = this;
	        this.open_address = !mode;
	        this.edit_address = !mode;
	        jQuery('#userAddress')[0].reset();
	        jQuery('#editAddress')[0].reset();
	        jQuery('#userAddress').find('label').removeClass('active');
	        jQuery('#editAddress').find('label').removeClass('active');
	        this.userService.getUserAddress(this.authDatas.id, this.authDatas.token).subscribe(function (addressDetail) {
	            _this.addressData = addressDetail['data'];
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    CheckoutAddressComponent.prototype.setAddress = function (address) {
	        this.checkout.selected_address = address;
	        this.checkout.order_data['address'] = address;
	        this.checkout.cod_charge = 0;
	        this.checkout.second_step = true;
	        this.router.navigate(['./checkout', 'summary']);
	    };
	    CheckoutAddressComponent.prototype.getdetail = function (modelName, pincode) {
	        var _this = this;
	        this.userService.getPincodeDetail(pincode)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                if (modelName == 'addressmodel') {
	                    _this.addressmodel['state'] = result['data']['state'];
	                    _this.addressmodel['city'] = result['data']['city'];
	                    jQuery('#userAddress').find('input').focus();
	                    ;
	                }
	                else {
	                    _this.editAddressmodel['edit_state'] = result['data']['state'];
	                    _this.editAddressmodel['edit_city'] = result['data']['city'];
	                    jQuery('#editAddress').find('input').focus();
	                    ;
	                }
	            }
	        });
	    };
	    return CheckoutAddressComponent;
	}());
	CheckoutAddressComponent = __decorate([
	    core_1.Component({
	        selector: 'checkout-address',
	        template: __webpack_require__(405)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        checkout_component_1.CheckoutComponent,
	        cart_service_1.CartService])
	], CheckoutAddressComponent);
	exports.CheckoutAddressComponent = CheckoutAddressComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 405:
/***/ function(module, exports) {

	module.exports = "<div class=\"col m8 s12\">\n\n    <div class=\"\"> \n        <div class=\"fill-form\" [hidden]=\"(open_address || edit_address)\"> \n             <div class=\"heading\">\n                <h5>SHIPMENT ADDRESS </h5>\n            </div>\n            <div class=\"fill-deatils\">\n                <div class=\"row\">\n                    <div class=\"col m6 s12\" [ngClass]=\"{'defaultAddress':address.is_default === 1}\" *ngFor=\"let address of addressData\">\n\n                         <div class=\"login-details panel panel-box address-box simple-hover-box highlight-box\" [ngClass]=\"{'highlight-box':address.is_default === 1}\">\n                            <div class=\"icon-group\">\n<!--                                        <i class=\"pd-5 pull-right fa fa-check-square-o\" title=\"Select Address\" (click)=\"setAddress(address)\"></i>-->\n                                        <i class=\"fa fa-edit pd-5 pull-right\" (click)=\"showEdit(address.id);edit_address = !edit_address\"></i>\n                                    </div> \n                             <div class=\"panel-body\" (click)=\"setAddress(address)\"> \n                                <h3 class=\"fixed_dynamic_content\">\n                                    <div class=\"dynamic-left\"> {{address['first_name']}} {{address['last_name']}} </div> \n\n                                    \n                                </h3> \n\n                                <div class=\"text-grayed-12\"> \n                                    <br>{{address['address_1']}} \n                                    <br>{{address['address_2']}} \n                                    <br>{{address['city']}}, {{address['pin_code']}}\n                                    <br>{{address['state']}}\n                                    <br> \n                                    <div>Mobile No. {{address['mobile']}}</div> \n                                    <br> \n                                </div> \n                            </div>\n                        </div> \n                    </div>\n\n                    <div class=\"col s12 myaccount\" [hidden]=\"(open_address || edit_address)\">\n                         <a class=\"addrase-btn\" (click)=\"open_address = !open_address\">\n                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\n                            Add New Address\n                        </a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"\" [hidden]=\"!open_address\">\n        <form class=\"fill-form\" id=\"userAddress\" (ngSubmit)=\"userAddress.form.valid && addressSubmit()\" #userAddress=\"ngForm\" novalidate>\n            <div class=\"heading\">\n                <h5>SHIPMENT ADDRESS </h5>\n            </div>\n            <div class=\"fill-deatils\">\n                <div class=\"row\">\n                    <div class=\"input-field col s6\">\n                        <input placeholder=\"First Name*\" id=\"first_name\" type=\"text\" name=\"first_name\" class=\"validate\" [(ngModel)]=\"addressmodel.first_name\" #first_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                        <div *ngIf=\"userAddress.submitted && first_name.errors && first_name.errors.required\" class=\"help-block err-msg\">The first name field is required.</div>\n                        <div *ngIf=\"userAddress.submitted && first_name.errors && first_name.errors.pattern\" class=\"help-block err-msg\">Enter valid first name.</div>\n                        <div *ngIf=\"error && error['first_name'] && first_name.valid\" class=\"err-msg\">{{error['first_name']['0']}}</div>\n                    </div>\n\n                    <div class=\"input-field col s6\">\n                        <input placeholder=\"Last Name*\" id=\"last_name\" type=\"text\" name=\"last_name\" class=\"validate\" [(ngModel)]=\"addressmodel.last_name\" #last_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                        <div *ngIf=\"userAddress.submitted && last_name.errors && last_name.errors.required\" class=\"help-block err-msg\">The last name field is required.</div>\n                         <div *ngIf=\"userAddress.submitted && last_name.errors && last_name.errors.pattern\" class=\"help-block err-msg\">Enter valid last name.</div>\n                        <div *ngIf=\"error && error['last_name'] && last_name.valid\" class=\"err-msg\">{{error['last_name']['0']}}</div>\n                    </div>\n                    </div>        <div class=\"row\">\n\n                    <div class=\"input-field col s12\">\n                        <input placeholder=\"Flat / House No. / Floor / Building*\" id=\"address_1\" type=\"text\" name=\"address_1\" class=\"validate\" [(ngModel)]=\"addressmodel.address_1\" #address_1=\"ngModel\" required>\n                        <div *ngIf=\"userAddress.submitted && !address_1.valid\" class=\"help-block err-msg\">The address 1 field is required.</div>\n                        <div *ngIf=\"error && error['address_1'] && address_1.valid\" class=\"err-msg\">{{error['address_1']['0']}}</div>\n                    </div>\n                      </div>        <div class=\"row\">\n\n                    <div class=\"input-field col s12\">\n                        <input placeholder=\"Colony / Street / Locality*\" id=\"address_2\" type=\"text\" name=\"address_2\" class=\"validate\" [(ngModel)]=\"addressmodel.address_2\" #address_2=\"ngModel\" required>\n                        <div *ngIf=\"userAddress.submitted && !address_2.valid\" class=\"help-block err-msg\">The address 2 field is required.</div>\n                        <div *ngIf=\"error && error['address_2'] && address_2.valid\" class=\"err-msg\">{{error['address_2']['0']}}</div>\n                    </div>\n  </div>        <div class=\"row\">\n                    <div class=\"input-field col s6\">\n                        <input placeholder=\"Mobile Number*\" id=\"mobile\" type=\"text\" name=\"mobile\" class=\"validate\" [(ngModel)]=\"addressmodel.mobile\" #mobile=\"ngModel\" required  pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\">\n                        <div *ngIf=\"userAddress.submitted && mobile.errors && mobile.errors.required\" class=\"help-block err-msg\">The mobile number field is required.</div>\n                        <div *ngIf=\"userAddress.submitted && mobile.errors && mobile.errors.pattern\" class=\"help-block err-msg\"> The mobile number format is invalid</div>\n                        <div *ngIf=\"error && error['mobile']\" class=\"err-msg\">{{error['mobile']['0']}}</div>\n                    </div>\n\n                    <div class=\"input-field col s6\">\n                        <input placeholder=\"Pincode\" id=\"pin_code\" type=\"text\" name=\"pin_code\" (keyup)=\"getdetail('addressmodel',addressmodel.pin_code)\" class=\"validate\" [(ngModel)]=\"addressmodel.pin_code\" #pin_code=\"ngModel\" required pattern=\"^[1-9][0-9]{5}$\"> \n                        <div *ngIf=\"userAddress.submitted &&  pin_code.errors && pin_code.errors.required\" class=\"help-block err-msg\">The pincode field is required.</div>\n                           <div *ngIf=\"userAddress.submitted &&  pin_code.errors && pin_code.errors.pattern\" class=\"help-block err-msg\">Enter valid pincode.</div>\n                        <div *ngIf=\"error && error['pin_code'] && pin_code.valid\" class=\"err-msg\">{{error['pin_code']['0']}}</div>\n                    </div>\n                </div>        <div class=\"row\">\n                    <div class=\"input-field col s6\">\n                        <input placeholder=\"City*\" id=\"city\" type=\"text\" name=\"city\" class=\"validate\" [(ngModel)]=\"addressmodel.city\" #city=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                        <div *ngIf=\"userAddress.submitted && city.errors && city.errors.required\" class=\"help-block err-msg\">The city field is required.</div>\n                         <div *ngIf=\"userAddress.submitted &&  city.errors && city.errors.pattern\" class=\"help-block err-msg\">Enter valid city name.</div>\n                        <div *ngIf=\"error && error['city'] && city.valid\" class=\"err-msg\">{{error['city']['0']}}</div>\n                    </div>\n\n                    <div class=\"input-field col s6\">\n                        <input placeholder=\"State*\" id=\"state\" type=\"text\" name=\"state\" class=\"validate\" [(ngModel)]=\"addressmodel.state\" #state=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                       <div *ngIf=\"userAddress.submitted  && state.errors && state.errors.required\" class=\"help-block err-msg\">The state field is required.</div>\n                     <div *ngIf=\"userAddress.submitted &&  state.errors && state.errors.pattern\" class=\"help-block err-msg\">Enter valid state name.</div>\n                        <div *ngIf=\"error && error['state'] && state.valid\" class=\"err-msg\">{{error['state']['0']}}</div>\n                    </div>\n                    </div>\n            <div class=\"row\">\n                    <div class=\"col s12 center-align btn-box\">\n                        <a href=\"javascript:void(0);\" class=\"waves-effect fabivo-btn btn\" (click)=\"close_address_form(open_address)\">\n                            Cancel\n                        </a> \n                        <button class=\"waves-effect fabivo-btn btn pro2-btn\">\n                            Submit\n                        </button>\n                    </div> \n                </div>\n            </div>\n        </form>\n    </div>\n\n    <div class=\"\" [hidden]=\"!edit_address\">\n        <form class=\"fill-form\" id=\"editAddress\" (ngSubmit)=\"editAddress.form.valid && editAddressSubmit()\" #editAddress=\"ngForm\" novalidate>\n                <div class=\"heading\">\n                    <h5>SHIPMENT ADDRESS </h5>\n                </div>\n                <div class=\"fill-deatils\">\n                    <div class=\"row\">\n                        <div class=\"input-field col s6\">\n                            <input placeholder=\"First Name*\" id=\"first_name\" type=\"text\" name=\"edit_first_name\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_first_name\" #edit_first_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                        <div *ngIf=\"editAddress.submitted && edit_first_name.errors && edit_first_name.errors.required\" class=\"help-block err-msg\">The first name field is required.</div>\n                    <div *ngIf=\"editAddress.submitted && edit_first_name.errors && edit_first_name.errors.pattern\" class=\"help-block err-msg\">Enter valid first name.</div>\n                            <div *ngIf=\"error && error['edit_first_name'] && edit_first_name.valid\" class=\"err-msg\">{{error['edit_first_name']['0']}}</div>\n                        </div>\n                        <div class=\"input-field col s6\">\n                            <input placeholder=\"Last Name*\" id=\"last_name\" name =\"edit_last_name\" type=\"text\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_last_name\" #edit_last_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                           <div *ngIf=\"editAddress.submitted && edit_last_name.errors && edit_last_name.errors.required\" class=\"help-block err-msg\">The last name field is required.</div>\n                     <div *ngIf=\"editAddress.submitted && edit_last_name.errors && edit_last_name.errors.pattern\" class=\"help-block err-msg\">Enter valid last name.</div>\n                            <div *ngIf=\"error && error['edit_last_name'] && edit_last_name.valid\" class=\"err-msg\">{{error['edit_last_name']['0']}}</div>\n                        </div>\n                </div>        <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <input placeholder=\"Flat / House No. / Floor / Building*\" id=\"address_1\" type=\"text\" name=\"edit_address_1\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_address_1\" #edit_address_1=\"ngModel\" required>\n                            <div *ngIf=\"editAddress.submitted && !edit_address_1.valid\" class=\"help-block err-msg\">The address 1 field is required.</div>\n                            <div *ngIf=\"error && error['edit_address_1'] && edit_address_1.valid\" class=\"err-msg\">{{error['edit_address_1']['0']}}</div>\n                        </div>\n                          </div>        <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <input placeholder=\"Colony / Street / Locality*\" id=\"address_2\" type=\"text\" name=\"edit_address_2\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_address_2\" #edit_address_2=\"ngModel\" required>\n                            <div *ngIf=\"editAddress.submitted && !edit_address_2.valid\" class=\"help-block err-msg\">The address 2 field is required.</div>\n                            <div *ngIf=\"error && error['edit_address_2'] && edit_address_2.valid\" class=\"err-msg\">{{error['edit_address_2']['0']}}</div>\n                        </div>\n                      </div>        <div class=\"row\">\n                        <div class=\"input-field col s6\">\n                            <input placeholder=\"Mobile Number*\" id=\"mobile\" type=\"text\" name=\"edit_mobile\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_mobile\" #edit_mobile=\"ngModel\" required  pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\" >\n                            <div *ngIf=\"editAddress.submitted && edit_mobile.errors && edit_mobile.errors.required\" class=\"help-block err-msg\">The mobile number field is required.</div>\n                            <div *ngIf=\"editAddress.submitted && edit_mobile.errors && edit_mobile.errors.pattern\" class=\"help-block err-msg\"> The mobile number format is invalid</div>\n                            <div *ngIf=\"error && error['edit_mobile']\" class=\"err-msg\">{{error['edit_mobile']['0']}}</div>\n                        </div>\n\n                        <div class=\"input-field col s6\">\n                            <input placeholder=\"Pincode*\" id=\"pin_code\" type=\"text\" name=\"edit_pin_code\" (keyup)=\"getdetail('editAddressmodel',editAddressmodel.edit_pin_code)\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_pin_code\" #edit_pin_code=\"ngModel\" required pattern=\"^[1-9][0-9]{5}$\">\n                            <div *ngIf=\"editAddress.submitted &&  edit_pin_code.errors && edit_pin_code.errors.required\" class=\"help-block err-msg\">The pincode field is required.</div>\n                         <div *ngIf=\"editAddress.submitted &&  edit_pin_code.errors && edit_pin_code.errors.pattern\" class=\"help-block err-msg\">Enter valid pincode.</div>\n                            <div *ngIf=\"error && error['edit_pin_code'] && edit_pin_code.valid\" class=\"err-msg\">{{error['edit_pin_code']['0']}}</div>\n                        </div>\n                        </div>       <div class=\"row\">\n                    \n                        <div class=\"input-field col s6\">\n                            <input placeholder=\"City*\" id=\"city\" type=\"text\" name=\"edit_city\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_city\" #edit_city=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                            <div *ngIf=\"editAddress.submitted && edit_city.errors && edit_city.errors.required\" class=\"help-block err-msg\">The city field is required.</div>\n                    <div *ngIf=\"editAddress.submitted &&  edit_city.errors && edit_city.errors.pattern\" class=\"help-block err-msg\">Enter valid city name.</div>\n                            <div *ngIf=\"error && error['edit_city'] && edit_city.valid\" class=\"err-msg\">{{error['edit_city']['0']}}</div>\n                        </div>\n\n                        <div class=\"input-field col s6\">\n                            <input placeholder=\"State*\" id=\"state\" type=\"text\" name=\"edit_state\" class=\"validate\" [(ngModel)]=\"editAddressmodel.edit_state\" #edit_state=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                <div *ngIf=\"editAddress.submitted  && edit_state.errors && edit_state.errors.required\" class=\"help-block err-msg\">The state field is required.</div>\n                     <div *ngIf=\"editAddress.submitted &&  edit_state.errors && edit_state.errors.pattern\" class=\"help-block err-msg\">Enter valid state name.</div>\n                            <div *ngIf=\"error && error['edit_state'] && edit_state.valid\" class=\"err-msg\">{{error['edit_state']['0']}}</div>\n                        </div>\n                        </div>\n                        <div class=\"row\">\n                        <div class=\"col s12 center-align btn-box\">\n                            <a href=\"javascript:void(0);\" class=\"waves-effect fabivo-btn btn\" (click)=\"close_address_form(edit_address)\">\n                                Cancel\n                            </a> \n                            <button class=\"waves-effect fabivo-btn btn pro2-btn\">\n                                 Submit<i class=\"material-icons right\">send</i>\n                             </button>\n                        </div>\n                    </div>\n                </div>\n            </form>\n    </div>\n\n</div>"

/***/ },

/***/ 406:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var cart_service_1 = __webpack_require__(370);
	var checkout_component_1 = __webpack_require__(402);
	var router_2 = __webpack_require__(361);
	var OrderSummaryComponent = (function () {
	    function OrderSummaryComponent(userService, route, checkoutComponent, router, cartService) {
	        this.userService = userService;
	        this.route = route;
	        this.checkoutComponent = checkoutComponent;
	        this.router = router;
	        this.cartService = cartService;
	        this.authData = [];
	        this.carts = [];
	    }
	    OrderSummaryComponent.prototype.ngOnInit = function () {
	        if (Object.keys(this.cartService.orderData).length > 0) {
	            this.authData = JSON.parse(localStorage.getItem('loginUser'));
	            if (this.authData && this.authData.id) {
	                this.carts = this.checkoutComponent.order_data;
	                this.cart_length = this.checkoutComponent.order_data['products'].length;
	            }
	        }
	        else {
	            this.router.navigate(['/']);
	        }
	    };
	    return OrderSummaryComponent;
	}());
	OrderSummaryComponent = __decorate([
	    core_1.Component({
	        selector: 'order-summary',
	        template: __webpack_require__(407)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        checkout_component_1.CheckoutComponent,
	        router_2.Router,
	        cart_service_1.CartService])
	], OrderSummaryComponent);
	exports.OrderSummaryComponent = OrderSummaryComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 407:
/***/ function(module, exports) {

	module.exports = "<div class=\"col m8 s12\">\n    <div class=\"\"> \n        <div class=\"fill-form\"> \n            <div class=\"heading\">\n                <h5>ORDER SUMMARY</h5>\n            </div>\n            <div class=\"fill-deatils\">\n                <div class=\"row\">\n                    <div class=\"col m7 s12\"> \n                        <div class=\"account-details-bx\">\n                                <div class=\"product-details-bx\" *ngFor=\"let cart_data of carts['products']; let i = index\">\n                                    <div class=\"row\">\n                                        <div class=\"col m4 s4 img-bx\">\n                                            <img class=\"img-full\" src=\"{{cart_data['product']['image']}}\">\n                                        </div>\n                                        <div class=\"col m8 s8 price-detail\">\n                                            <h6>{{cart_data['product']['title']}}</h6>\n                                            <h6 >Price: <span [innerHTML]=\"cart_data['product']['price']\"></span></h6>\n                                            <div class=\"row\">\n                                                <div class=\"col m6\">Size: {{cart_data['product_size']}}</div>\n                                                <div class=\"col m6\">Qty: {{cart_data['quantity']}}</div>\n                                            </div>\n                                            <span>Expected delivery: {{ carts['expected_delivery_date']}}</span>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                    </div>\n                    <div class=\"col m5 s12\"> \n                        <div class=\"order-details\">\n                            <h6>You have {{cart_length > 1 ? cart_length + ' items' : cart_length + ' item'}} in your bag </h6>\n     <div class=\"order-1\">\n                            <span class=\"left\">Sub Total</span>\n                            <span class=\"right\" [innerHTML]=\"carts['total_price'] ? carts['total_price'] : 0\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n                        <div  *ngIf=\"carts['enable_shipping_charge']\" class=\"order-1\">\n                            <span class=\"left\">Shipping</span>\n                            <span class=\"right\" [innerHTML]=\"carts['shipping_charge'] ? carts['shipping_charge'] : 0\"></span>\n                        </div>\n                        <div class=\"clearfix\"></div>\n\n                        \n                        <div class=\"order-1\" *ngIf=\"carts['enable_tax']\">\n                                  <div  *ngFor=\"let tax of carts['tax']\">\n                                \n                        <span class=\"left\">{{tax['name']}}</span>\n                        <span class=\"right\" [innerHTML]=\"tax['amount']\"></span>\n                        <div class=\"clearfix\"></div>\n                        </div>\n                        </div>\n                        <div class=\"clearfix\"></div>\n                             <div *ngIf=\"carts['enable_discount']\" class=\"order-1\"  >\n                            \n                                    \n                            <span class=\"left\">Discount</span>\n                            <span class=\"right\" [innerHTML]=\"carts['discount']\"></span>\n                            <div class=\"clearfix\"></div>\n                            \n                        </div>\n                            \n                            <div class=\"order-2\">\n                                <span>Payable Amount</span>\n                                <span class=\"pull-right\" [innerHTML]=\"carts['payble_amount'] ? carts['payble_amount'] : 0\"></span>\n                                <div class=\"clearfix\"></div>\n                            </div>\n                            <div class=\"pull-right\">\n                                <a class=\"btn waves-effect fabivo-btn\" href=\"javascript:void(0);\" (click)=\"checkoutComponent.changeUrl('payment')\">\n                                    Continue\n                                </a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ },

/***/ 408:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var global_1 = __webpack_require__(363);
	var pager_service_1 = __webpack_require__(374);
	var page_service_1 = __webpack_require__(33);
	var common_1 = __webpack_require__(22);
	var OrderDetailComponent = (function () {
	    function OrderDetailComponent(userService, route, router, pagerService, pageService, location) {
	        this.userService = userService;
	        this.route = route;
	        this.router = router;
	        this.pagerService = pagerService;
	        this.pageService = pageService;
	        this.location = location;
	        this.page = 1;
	        this.trackdisable = 0;
	        this.pager = {};
	        this.orderDetail = [];
	        this.status = [];
	        this.authDatas = {};
	        this.cancelmodel = {};
	    }
	    OrderDetailComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
	        this.route.params.subscribe(function (params) {
	            var order_id = params['order_id'];
	            _this.router.routerState.root.queryParams.subscribe(function (params) {
	                if (params['status']) {
	                    if (params['status'] == 'success') {
	                        global_1.toast('Order successfully placed', 'success');
	                    }
	                    else {
	                        global_1.toast('Your order is ' + params['status'], 'error');
	                    }
	                    _this.location.go('myaccount/order-detail/' + order_id);
	                }
	            });
	            _this.pageService.getOrderDetail(_this.authDatas.id, order_id).subscribe(function (orderDetail) {
	                _this.orderDetail = orderDetail['data'];
	                _this.status = orderDetail['data']['status'];
	                setTimeout(function () {
	                    jQuery(document).ready(function () {
	                        jQuery('.tooltipped').tooltip({ delay: 50 });
	                        jQuery('#cancle_order').modal();
	                    });
	                    setTimeout('resizeequalheight()', 50);
	                }, 1000);
	            }, function (err) {
	                console.log(err);
	            });
	        });
	        setTimeout(function () {
	            jQuery(document).ready(function () {
	                jQuery('.tabs').tabs();
	            });
	        }, 100);
	        global_1.unblockUI();
	    };
	    OrderDetailComponent.prototype.cancelOrder = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.pageService.cancelOrder(this.authDatas.id, this.orderDetail.order_id, this.cancelmodel).subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.orderDetail.order_status = 'cancel';
	                _this.orderDetail.cancel_reason = _this.cancelmodel.cancel_reason;
	                jQuery('#cancle_order').modal('close');
	                _this.cancelmodel.cancel_reason = '';
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    return OrderDetailComponent;
	}());
	__decorate([
	    core_1.Input(),
	    __metadata("design:type", Object)
	], OrderDetailComponent.prototype, "model", void 0);
	OrderDetailComponent = __decorate([
	    core_1.Component({
	        selector: 'order-detail',
	        template: __webpack_require__(409)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        pager_service_1.PagerService,
	        page_service_1.PageService,
	        common_1.Location])
	], OrderDetailComponent);
	exports.OrderDetailComponent = OrderDetailComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 409:
/***/ function(module, exports) {

	module.exports = "<div id=\"pro4\" class=\"col s12\">\n    <div class=\"row\">\n        <div class=\"col s12\">\n            <div class=\"pro3details\">\n                <div class=\"track-bx\">\n                    <div class=\"row\">\n                        <div class=\"col s6\">\n                            <a  [routerLink]=\"['/myaccount','order']\" class=\"track-id-bx tp-btn\">\n                                <i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i>\n                                Back\n                            </a>\n                                 <a href=\"#cancle_order\" *ngIf=\"orderDetail.order_status=='success'\"   class=\"track-id-bx tp-btn\">\n                                Cancel Order\n                            </a>\n                            <a *ngIf=\"orderDetail.order_status=='inprocess' || orderDetail.order_status=='inprocess'\"  href=\"javascript:void(0)\"    class=\"track-id-bx tooltipped tp-btn\" data-position=\"bottom\" data-delay=\"50\" data-tooltip=\"You will track the package,once the shipment is created\">\n                                <i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>\n                                Track\n                            </a>\n                            <a *ngIf=\"(orderDetail.order_status=='shipped' || orderDetail.order_status=='delivered') && orderDetail.shipped_by=='delhivery'  \"  href=\"{{orderDetail.delivery.link}}\"   target='_blank' class=\"track-id-bx tp-btn\" >\n                                <i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>\n                                Track\n                            </a>\n                        </div>\n\n                        <div class=\"col s6 right-align\">\n                            <a href=\"javascript:void();\" class=\"track-id-bx tp-btn-full\">\n                                {{orderDetail.order_id}}\n                            </a>\n\n                       \n                        </div>\n                    </div>\n                </div>\n                <div class=\"row pro3-product-details-bx\"  >\n                    <div class=\"col m3 s12\">\n                        <div class=\"row\">\n\n                            <div class=\"panel panel-box address-box simple-hover-box highlight-box makeequal\" >\n                                <div class=\"panel-body track-box \"> \n                                    <h3>{{orderDetail.first_name | capitalize}} {{orderDetail.last_name | capitalize}}</h3>\n                                    <div class=\"text-grayed-12\"> \n                                        <br>{{orderDetail.address_1}}\n                                        <br>{{orderDetail.address_2}} \n                                        <br>{{orderDetail.city}} , {{orderDetail.pin_code}} \n                                        <br>{{orderDetail.state}} \n                                        <br> \n                                        <div>Mobile No. {{orderDetail.mobile}} </div> \n                                        <br> \n                                    </div> \n                                </div>\n\n\n\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"col m9 s12\">\n\n\n\n                       \n\n                        <div class=\"panel panel-box  simple-hover-box highlight-box makeequal\" *ngIf=\"orderDetail.shipped_by=='delhivery' && orderDetail.delivery.scan.length!=0\"  >\n                             <div class=\"panel-body track-box\"  >   <div class=\"row\">  <div class=\"col s6 left-align\"><h3><span class=\"left-align\">Way bill Number:{{orderDetail.delivery.waybill}} </span></h3> </div> <div class=\"col s6 right-align \"><h3><span class=\"right-align\">Status:{{orderDetail.delivery.status}}</span></h3></div></div>\n                                <div *ngIf=\"orderDetail.delivery.scan.length!=0\">\n                                    <table class=\" bordered striped highlight responsive-table\">\n                                        <thead>\n                                            <tr>\n                                                <th >Status Type</th>\n                                                <th >Status</th>\n                                                <th >Status Date</th>\n                                                <th >Remarks</th>\n                                                <th >Location</th>\n\n\n                                            </tr>\n\n\n                                        </thead>\n                                        <tbody>\n                                            <tr *ngFor=\"let scans of orderDetail.delivery.scan\">\n                                                <td>{{scans.ScanType}}</td>\n                                                <td>{{scans.Scan}}</td>\n                                                <td>{{scans.StatusDateTime}}</td>\n                                                <td>{{scans.Instructions}}</td>\n                                                <td>{{scans.ScannedLocation}}</td>\n\n                                            </tr>\n                                            <tr *ngIf=\"orderDetail.delivery.scan.length==0\">\n                                                <td  colspan=\"6\"><div class=\"center-align\"> Data Not Found </div></td>\n\n                                            </tr>\n\n                                        </tbody>\n\n                                    </table>\n\n                                </div></div>\n                                       \n                        </div>\n                        <div class=\"panel panel-box  simple-hover-box highlight-box makeequal\" *ngIf=\"(orderDetail.order_status=='shipped' || orderDetail.order_status=='delivered') && orderDetail.shipped_by=='postal'\" >\n                            \n                        <div class=\"panel-body track-box\"  >   <div class=\"row\">  <div class=\"col s6 left-align\"><h3><span class=\"left-align\">AWB Number:{{orderDetail.awb_number}} </span></h3> </div> <div class=\"col s6 right-align \"><h3><span class=\"right-align\">Shipped Via:{{orderDetail.post_by | capitalize}}</span></h3></div></div>\n\n                        </div>\n\n\n\n                        </div>\n\n                    </div>\n                </div>\n                   <div class=\"row \" >\n                 <div class=\"col s4\">\n                <div class=\"row pro3-product-details-bx\" *ngFor=\"let products of orderDetail.order_detail\" >\n                    \n                            <div class=\"col s3\">\n                                <img class=\"responsive-img\" src=\"{{products.product.image}}\">        \n                            </div>\n                            <div class=\"col s9\">  \n                                <p><a [routerLink]=\"['/product',products.product.slug]\">{{products.product.title}}</a></p>\n                                <p><b>Size:</b>{{products.product_size}}</p>\n                                <p><b>Quantity:</b>{{products.quantity}}</p> \n                                 <p><b>price:</b><span [innerHTML]=\"products.price\"></span></p> \n\n                          \n                    </div>\n                   \n                </div>\n                  </div>\n                <div class=\"col s8\">\n                     <ul class=\"timeline\" id=\"timeline\">\n                            <li class=\"li complete first_complete\">\n                                <div class=\"timestamp\">\n\n                                    <span class=\"date\">{{status['ordered']}}<span>\n                                        </span></span></div>\n                                <div class=\"status\">\n                                    <h4> Ordered </h4>\n                                </div>\n                            </li>\n\n\n\n\n                            <li class=\"li complete last_complete\"  *ngIf=\"orderDetail.order_status=='reject'\">\n                                <div class=\"timestamp\">\n\n                                    <span class=\"date\">{{(status['reject']!='')?status['reject']:''}}<span>\n                                        </span></span></div>\n                                <div class=\"status failed\">\n                                    <h4 class=\"failed\"> Rejected </h4>\n                                </div>\n                            </li>\n                            <li class=\"li complete last_complete\"   *ngIf=\"orderDetail.order_status=='cancel'\">\n                                <div class=\"timestamp\">\n\n                                    <span class=\"date\">{{(status['cancel']!='')?status['cancel']:''}}<span>\n                                        </span></span></div>\n                                <div class=\"status failed\">\n                                    <h4 class=\"failed\"> Cancelled </h4>\n                                </div>\n                            </li>\n                            <li class=\"li complete last_complete\"  *ngIf=\"orderDetail.order_status=='failed'\">\n                                <div class=\"timestamp\">\n\n                                    <span class=\"date\">{{(status['failed']!='')?status['failed']:''}}<span>\n                                        </span></span></div>\n                                <div class=\"status\">\n                                    <h4 class=\"failed\"> Failed </h4>\n                                </div>\n                            </li>\n                            <li class=\"li \" [ngClass]=\"{complete:status['inprocess']!= ''}\" *ngIf=\"orderDetail.order_status!='failed' && orderDetail.order_status!='reject' && orderDetail.order_status!='cancel'\">\n                                <div class=\"timestamp\">\n\n                                    <span class=\"date\">{{(status['inprocess']!='')?status['inprocess']:''}}<span>\n                                        </span></span></div>\n                                <div class=\"status\">\n                                    <h4> In Process </h4>\n                                </div>\n                            </li>\n                            <li class=\"li \" [ngClass]=\"{complete:status['shipped']!= ''}\" *ngIf=\"orderDetail.order_status!='failed' && orderDetail.order_status!='reject' && orderDetail.order_status!='cancel'\">\n                                <div class=\"timestamp\">\n\n                                    <span class=\"date\">{{(status['shipped']!='')?status['shipped']:''}}<span>\n                                        </span></span></div>\n                                <div class=\"status\">\n                                    <h4> Shipped </h4>\n                                </div>\n                            </li>\n                            <li class=\"li last_complete\" [ngClass]=\"{complete:status['delivered']!= ''}\" *ngIf=\"orderDetail.order_status!='failed' && orderDetail.order_status!='reject' && orderDetail.order_status!='cancel'\">\n                                <div class=\"timestamp\">\n\n                                    <span class=\"date\">{{(status['delivered']!='')?status['delivered']:''}}<span>\n                                        </span></span></div>\n                                <div class=\"status\">\n                                    <h4> Delivered </h4>\n                                </div>\n                            </li>\n                        </ul>\n\n\n                </div>\n                </div>\n                <div class=\"total-bx\">\n                    <div class=\"row\">\n                        <div class=\"col s3\">\n                            <p><b>Order Date:</b>{{orderDetail.created_at}}</p>\n                            <div *ngIf=\"orderDetail.order_status!='failed' &&  orderDetail.order_status!='reject' &&  orderDetail.order_status!='cancel'\">\n                                <p *ngIf=\"orderDetail.delivery_date\"><b>Delivery  Date:</b>{{orderDetail.delivery_date}}</p>\n                                <p *ngIf=\"!orderDetail.delivery_date\"><b>Expected Delivery Date:</b>{{orderDetail.expected_delivery_date}}</p>\n                            </div>\n                            <p *ngIf=\"orderDetail.order_status=='pending'\">Your order is  pending</p>\n                            <p *ngIf=\"orderDetail.order_status=='failed'\" class='failed'>Your order is  failed</p>\n                            <p *ngIf=\"orderDetail.order_status=='inprocess'\" class='success'>Your order is  in process</p>\n                            <p *ngIf=\"orderDetail.order_status=='reject'\" class='failed'>Your order is rejected</p>\n                             <p *ngIf=\"orderDetail.order_status=='reject' || orderDetail.order_status=='failed'\" class=''>Reason :- {{orderDetail.reject_reason}}</p>\n                            <p *ngIf=\"orderDetail.order_status=='shipped'\" class='success'>Your order is shipped</p>\n                            <p *ngIf=\"orderDetail.order_status=='cancel'\" class='failed'>Your order is Cancelled</p>\n                            <p *ngIf=\"orderDetail.order_status=='cancel'\" class=''>Reason :- {{orderDetail.cancel_reason}}</p>\n                           \n                            <p *ngIf=\"orderDetail.order_status=='delivered'\" class='success'>Your order is delivered</p>\n                            <p *ngIf=\"orderDetail.order_status=='success'\" class='success'>Your order is successfully placed.</p>\n\n                        </div>\n                        <div class=\"col s6\" >\n                            <div *ngIf=\"orderDetail.order_status!='failed' &&  orderDetail.order_status!='reject' &&  orderDetail.order_status!='cancel'\">\n\n\n\n                                <p *ngIf=\"orderDetail.is_cash_back && orderDetail.is_cash_back_transfer==0\" class='success'>Cash back of <span [innerHTML]=\"orderDetail.cash_back_amount\"></span> for Promo Code {{orderDetail.coupon_code|ucupper}} has been successfully added to your Fabivo Wallet on after shipped successfully. Happy Shopping!</p>\n\n\n                                <p *ngIf=\"orderDetail.is_cash_back && orderDetail.is_cash_back_transfer==1\" class='success'>Cash back of <span [innerHTML]=\"orderDetail.cash_back_amount\"></span> for Promo Code {{orderDetail.coupon_code|ucupper}} has been successfully added to your Fabivo Wallet</p>\n                            </div>\n                        </div>\n                        <div class=\"col s3 left-align\">\n                            <p><b> Sub Total:</b><span [innerHTML]=\"orderDetail.total_price\" ></span></p>\n                            <p *ngIf=\"orderDetail.enable_shipping_charge\"><b>Shipping:</b><span [innerHTML]=\"orderDetail.shipping_charge\" ></span></p>\n                            <p *ngIf=\"orderDetail.enable_tax\"><b>Tax:</b><span [innerHTML]=\"orderDetail.total_tax\" ></span></p>\n                            <p *ngIf=\"orderDetail.is_discount\"><b>Discount</b><span [innerHTML]=\"orderDetail.discount\" ></span></p>\n                            <p *ngIf=\"orderDetail.is_cod\"><b>Cod Money</b><span [innerHTML]=\"orderDetail.cod_money\" ></span></p>\n                            <p><b>Total:</b><span [innerHTML]=\"orderDetail.payble_amount\" ></span></p>\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- Modal Structure -->\n<div id=\"cancle_order\" class=\"modal\">\n    <form class=\"\"  id=\"cancelForm\" (ngSubmit)=\"cancelForm.form.valid && cancelOrder()\" #cancelForm=\"ngForm\" novalidate>\n    <div class=\"modal-content\">\n        <div class=\"row\">\n            <div class=\"col s12 \" >\n                  <div class=\"row\">\n                    <div class=\"input-field col s12\">\n                        <textarea id=\"cancel_reason\" name=\"cancel_reason\" [(ngModel)]=\"cancelmodel.cancel_reason\" #cancel_reason=\"ngModel\" required class=\"materialize-textarea\"></textarea>\n                       <div *ngIf=\"cancelForm.submitted && !cancel_reason.valid\" class=\"help-block err-msg\">The Reason field is required.</div>\n                        \n                        <label for=\"cancel_reason\">Reason</label>\n                    </div>\n                </div>\n                </div>\n        </div>\n    </div>\n    <div class=\"modal-footer\">\n\n\n        <button class=\"btn waves-effect fabivo-btn\">Submit</button>\n        <a href=\"javascript:void(0)\" class=\"modal-action modal-close waves-effect fabivo-btn \">Close</a>\n    </div>\n                      </form>\n\n</div>"

/***/ },

/***/ 410:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var user_service_1 = __webpack_require__(378);
	var checkout_component_1 = __webpack_require__(402);
	var page_service_1 = __webpack_require__(33);
	var cart_service_1 = __webpack_require__(370);
	var router_2 = __webpack_require__(361);
	var global_1 = __webpack_require__(363);
	var PaymentComponent = (function () {
	    function PaymentComponent(userService, route, checkoutComponent, pageService, cartService, router) {
	        this.userService = userService;
	        this.route = route;
	        this.checkoutComponent = checkoutComponent;
	        this.pageService = pageService;
	        this.cartService = cartService;
	        this.router = router;
	        this.type = 'PAYU';
	        this.settings = [];
	        this.carts = [];
	        this.cart_length = [];
	        this.paytm = [];
	        this.payu = [];
	        this.PayUmoney = [];
	        this.order = [];
	        this.authData = [];
	        this.wallet = {};
	        this.enable_cod = 0;
	        this.enable_paytm = 0;
	        this.enable_payu = 0;
	        this.is_wallet = 0;
	        this.enable_wallet = 0;
	        this.phone_number = '';
	        this.number_model = {};
	        this.loading = false;
	        this.error = '';
	        this.success = '';
	    }
	    PaymentComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        if (Object.keys(this.cartService.orderData).length > 0) {
	            this.authData = JSON.parse(localStorage.getItem('loginUser'));
	            this.checkwallet();
	            this.pageService.getSettings().subscribe(function (settings) {
	                _this.settings = settings;
	                _this.enable_payu = Number(_this.settings['CONFIG_ENABLE_PAYU']);
	                _this.enable_paytm = Number(_this.settings['CONFIG_ENABLE_PAYTM']);
	                _this.enable_cod = Number(_this.settings['CONFIG_ENABLE_COD']);
	                _this.carts = _this.checkoutComponent.order_data;
	                _this.cart_length = _this.checkoutComponent.order_data['products'].length;
	                _this.userService.getPincodeAvalability(_this.checkoutComponent.order_data['address']['pin_code'])
	                    .subscribe(function (result) {
	                    if (result['status_code'] === 1) {
	                        if (result['cash'] == 'y') {
	                            _this.enable_cod = 1;
	                        }
	                        else {
	                            _this.enable_cod = 0;
	                        }
	                    }
	                    else {
	                        _this.enable_cod = 0;
	                    }
	                    setTimeout(function () {
	                        jQuery('.collapsible').collapsible();
	                    }, 1000);
	                });
	            }, function (err) {
	                console.log(err);
	            });
	        }
	        else {
	            this.router.navigate(['/']);
	        }
	    };
	    PaymentComponent.prototype.setPaymentType = function (type) {
	        this.type = type;
	        this.checkoutComponent.type = type;
	        if (type == 'COD') {
	            this.enable_wallet = 0;
	            this.is_wallet = 0;
	        }
	        else {
	            if (this.wallet['enable_wallet'] == 1) {
	                this.enable_wallet = 1;
	            }
	        }
	    };
	    PaymentComponent.prototype.paymentConfirm = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.checkoutComponent.order_data['type'] = this.type;
	        this.checkoutComponent.order_data['user_id'] = this.authData.id;
	        this.checkoutComponent.order_data['token'] = this.authData.token;
	        if (this.is_wallet) {
	            this.wallet['is_wallet'] = 1;
	            this.checkoutComponent.order_data['wallet'] = this.wallet;
	            if (this.wallet['after_wallet_pay'] == 0) {
	                this.checkoutComponent.order_data['type'] = 'wallet';
	                this.type = 'wallet';
	            }
	        }
	        if (this.type == 'COD') {
	            this.pageService.checkMobileNumberVerified(this.authData.id).subscribe(function (result) {
	                if (result['status_code'] == 1) {
	                    if (result['phone_verify'] == 1) {
	                        _this.submitPayemntData();
	                    }
	                    else {
	                        if (result['send_otp'] == 0) {
	                            _this.submitPayemntData();
	                        }
	                        else {
	                            _this.phone_number = result['mobile'];
	                            jQuery('#modal_otp_cod').modal();
	                            jQuery('#modal_otp_cod').modal('open');
	                            global_1.unblockUI();
	                        }
	                    }
	                }
	                else {
	                    if (result['message'] != '') {
	                        global_1.toast(result['message'], 'error');
	                    }
	                    global_1.unblockUI();
	                }
	            }, function (err) {
	                console.log(err);
	            });
	        }
	        else {
	            this.submitPayemntData();
	        }
	    };
	    PaymentComponent.prototype.resendOtp = function () {
	        global_1.blockUI();
	        this.pageService.resendOtpCodNumber(this.authData.id)
	            .subscribe(function (result) {
	            if (result['status_code'] == 1) {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    PaymentComponent.prototype.close_model = function (id) {
	        jQuery('#verified_number')[0].reset();
	        jQuery('#verified_number').find('label').removeClass('active');
	        this.error = '';
	        this.number_model = {};
	    };
	    PaymentComponent.prototype.submitOtp = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.number_model['address'] = this.checkoutComponent.order_data['address'];
	        this.pageService.mobileNumberVerified(this.number_model, this.authData.id)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.error = '';
	                jQuery('#modal_otp_cod').modal('close');
	                jQuery('#verified_number')[0].reset();
	                global_1.unblockUI();
	                _this.loading = false;
	                global_1.toast(_this.success, 'success');
	                _this.submitPayemntData();
	            }
	            else {
	                _this.error = result['message'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    PaymentComponent.prototype.submitPayemntData = function () {
	        var _this = this;
	        this.cartService.payment(this.checkoutComponent.order_data).subscribe(function (result) {
	            if (result['status_code'] == 1) {
	                _this.paytm = result['paytm'];
	                _this.payu = result['payu'];
	                _this.PayUmoney = result['PayUmoney'];
	                _this.type = result['payment_type'];
	                setTimeout(function () {
	                    if (_this.type == "PAYU") {
	                        jQuery("#payuForm").submit();
	                    }
	                    if (_this.type == "PAYTM") {
	                        jQuery("#paytmform").submit();
	                    }
	                    if (_this.type == "PayUmoney") {
	                        jQuery("#PayUmoneyForm").submit();
	                    }
	                }, 1000);
	                if (_this.type == "wallet" || _this.type == "COD") {
	                    _this.cartService.getCart(_this.authData.id).subscribe(function (cartData) {
	                        _this.cartService.cart = cartData['data'];
	                    }, function (err) {
	                        console.log(err);
	                    });
	                    global_1.toast(result['message'], 'success');
	                    _this.router.navigate(['./myaccount/order']);
	                    global_1.unblockUI();
	                }
	            }
	            else {
	                global_1.unblockUI();
	            }
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    PaymentComponent.prototype.checkwallet = function () {
	        var _this = this;
	        this.checkoutComponent.order_data['type'] = this.type;
	        this.checkoutComponent.order_data['token'] = this.authData.token;
	        this.cartService.checkwalletBalance(this.authData.id, this.checkoutComponent.order_data).subscribe(function (result) {
	            if (result['status_code'] == 1) {
	                _this.wallet['enable_wallet'] = result['enable_wallet'];
	                _this.wallet['wallet'] = result['wallet'];
	                _this.wallet['after_wallet_pay_rs'] = result['after_wallet_pay_rs'];
	                _this.wallet['after_wallet_pay'] = result['after_wallet_pay'];
	                _this.wallet['remaining_balance'] = result['remaining_balance'];
	                _this.wallet['remaining_balance_rs'] = result['remaining_balance_rs'];
	                _this.enable_wallet = result['enable_wallet'];
	                if (_this.enable_wallet == 1) {
	                    _this.is_wallet = 1;
	                    _this.wallet['is_wallet'] = 1;
	                }
	                else {
	                    _this.is_wallet = 0;
	                    _this.wallet['is_wallet'] = 0;
	                }
	            }
	            else {
	                _this.wallet = [];
	                _this.enable_wallet = 0;
	                _this.wallet['enable_wallet'] = 0;
	                _this.wallet['is_wallet'] = 0;
	                _this.is_wallet = 0;
	            }
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    return PaymentComponent;
	}());
	PaymentComponent = __decorate([
	    core_1.Component({
	        selector: 'payment',
	        template: __webpack_require__(411)
	    }),
	    __metadata("design:paramtypes", [user_service_1.UserService,
	        router_1.ActivatedRoute,
	        checkout_component_1.CheckoutComponent,
	        page_service_1.PageService,
	        cart_service_1.CartService,
	        router_2.Router])
	], PaymentComponent);
	exports.PaymentComponent = PaymentComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 411:
/***/ function(module, exports) {

	module.exports = "<div class=\"col m8 s12\">\n\n    <div  *ngIf=\"enable_wallet \"  class=\"\"> \n        <div class=\"fill-form fabivo_wallet\"> \n            <div class=\"heading\">\n                <h5>fabivo wallet</h5>\n            </div>\n            <div>\n\n                <p class=\"wallet-balance\">\n                    <input [(ngModel)]=\"is_wallet\"  type=\"checkbox\" class=\"filled-in\" id=\"filled-in-box\" checked=\"{{(is_wallet)?checked:''}}\" />\n                    <label for=\"filled-in-box\">Use Fabivo Wallet (<span *ngIf=\"is_wallet==1\" [innerHTML]=\"wallet['remaining_balance_rs']\"></span><span *ngIf=\"is_wallet==0\" [innerHTML]=\"wallet['wallet']\"></span>)</label>\n                </p>\n            </div>\n            <div *ngIf=\"wallet['after_wallet_pay']==0 && is_wallet==1\" class='wallet_box_head'>\n\n                <div class=\"row\">\n                    <div class=\"col m3\"><div class=\"wallet_box\">\n                            <p>Fabivo Wallet</p>\n                            <p class=\"show_price\" [innerHTML]=\"wallet['wallet']\" ></p>\n\n                        </div></div>\n                    <div class=\"col m1\"><span class=\"minus_sign\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i>\n                        </span></div>\n                    <div class=\"col m3\"><div class=\"wallet_box\"> <p>Payment to be made</p>\n                            <p class=\"show_price\" [innerHTML]=\"carts['payble_amount']\" ></p>\n                        </div>\n                    </div>\n                    <div class=\"col m5\"> <a (click)=\"paymentConfirm()\"  class=\"btn waves-effect fabivo-btn\"  href=\"javascript:void(0);\">\n                            PAY NOW\n                        </a></div>\n\n                </div>\n            </div>\n            <div *ngIf=\"wallet['after_wallet_pay']>0 && is_wallet==1\" class='wallet_box_head'>\n\n                <div class=\"row\">\n                    <div class=\"col m3\">\n                        <div class=\"wallet_box\">\n                            <p>Payment to be made</p>\n                            <p class=\"show_price\" [innerHTML]=\"carts['payble_amount']\" ></p>\n                        </div>\n                    </div>\n\n                    <div class=\"col m1\"><span class=\"minus_sign\"><i class=\"fa fa-minus\" aria-hidden=\"true\"></i>\n                        </span></div>\n                    <div class=\"col m3\"><div class=\"wallet_box\">\n                            <p>\n                                Money in Your Fabivo Wallet</p>\n                            <p class=\"show_price\" [innerHTML]=\"wallet['wallet']\" ></p>\n\n\n                        </div>\n                    </div>\n\n                    <div class=\"col m1\"><span class=\"minus_sign\"><i class=\"fa fa-equal\" aria-hidden=\"true\">=</i>\n                        </span></div>\n                    <div class=\"col m4\"> \n                        <div class=\"wallet_box\"><p>Select an option to pay balance</p> </div>\n                        <p class=\"show_price\" [innerHTML]=\"wallet['after_wallet_pay_rs']\" ></p>\n                    </div>\n\n                </div>\n            </div>\n\n        </div>\n\n    </div>\n\n\n\n    <div [class.disabled]=\"enable_wallet==1 && is_wallet==1 && wallet['after_wallet_pay']==0\" class=\"\"> \n        <div class=\"fill-form\"> \n            <div class=\"heading\">\n                <h5>PAYMENT DETAILS</h5>\n            </div>\n\n            <div class=\"fill-deatils fill-deatils-bx\">\n                <div class=\"row\">\n                    <div class=\"tab-div\">\n\n                        <div class=\"col m4\">\n                            <ul class=\"tabs\">\n                                <li *ngIf=\"enable_payu\" [class.active]=\"type == 'PAYU'\" (click)=\"setPaymentType('PAYU')\" class=\"tab\"><a href=\"javascript:void(0);\">Credit/Debit Card</a></li>\n                                <li *ngIf=\"enable_payu\" [class.active]=\"type == 'PayUmoney'\" (click)=\"setPaymentType('PayUmoney')\" class=\"tab\"><a href=\"javascript:void(0);\">PayUmoney</a></li>\n                                <li *ngIf=\"enable_paytm\" [class.active]=\"type == 'PAYTM'\" (click)=\"setPaymentType('PAYTM')\" class=\"tab\"><a href=\"javascript:void(0);\" >Paytm</a></li>\n                                <li *ngIf=\"enable_cod ==1 && is_wallet==0\" [class.active]=\"type == 'COD'\" (click)=\"setPaymentType('COD')\" class=\"tab\"><a href=\"javascript:void(0);\" >Cash on Delivery</a></li>\n                            </ul>\n                        </div>\n\n                        <div class=\"col m8 tab-details\">                        \n                            <div *ngIf=\"enable_payu\" id=\"deatils1\" class=\"col s12\" [hidden]=\"type != 'PAYU'\">\n\n\n\n                            </div> \n                            <div *ngIf=\"enable_payu\" id=\"deatils1\" class=\"col s12 center-text\" [hidden]=\"type != 'PayUmoney'\">\n                                <img class=\"img-responsive\" src=\"public/img/payumoney-logo.png\">\n                                <div [innerHTML]=\"settings['CONFIG_PAYUMONEY_DESCRIPTION']\"></div>\n\n\n                            </div>\n\n                            <div *ngIf=\"enable_paytm\" id=\"deatils2\" class=\"col s12 center-text\" [hidden]=\"type != 'PAYTM'\">\n                                <img class=\"img-responsive\" src=\"public/img/paytm-logo.png\">\n                                <div class=\"montlight\">       <div [innerHTML]=\"settings['CONFIG_PAYTM_DESCRIPTION']\"></div></div>\n\n                            </div>\n\n\n                            <div *ngIf=\"enable_cod==1 && is_wallet==0\"  id=\"deatils3\" class=\"col s12 center-text\" [hidden]=\"type != 'COD'\" >\n                                <div [innerHTML]=\"settings['CONFIG_COD_DESCRIPTION']\"></div>\n                            </div>\n                        </div>\n\n                    </div>\n\n                    <div *ngIf=\"enable_payu\">\n                        <form action=\"{{payu['redirect_url']}}\" id='payuForm'  method=\"post\" name=\"f2\" class=\"fill-deatils\">\n\n\n                            <input  *ngFor=\"let payudata of payu['data']\" type=\"hidden\" name=\"{{payudata['name']}}\" value=\"{{payudata['value']}}\">\n\n\n                        </form>\n                    </div>\n                    <div *ngIf=\"enable_payu\">\n                        <form action=\"{{PayUmoney['redirect_url']}}\" id='PayUmoneyForm'  method=\"post\" name=\"f2\" class=\"fill-deatils\">\n\n\n                            <input  *ngFor=\"let payumdata of PayUmoney['data']\" type=\"hidden\" name=\"{{payumdata['name']}}\" value=\"{{payumdata['value']}}\">\n\n\n                        </form>\n                    </div>\n                    <div *ngIf=\"enable_paytm\">\n                        <form action=\"{{paytm['redirect_url']}}\" id='paytmform'  method=\"post\" name=\"f1\">\n\n\n                                    <input  *ngFor=\"let paytmdata of paytm['data']\" type=\"hidden\" name=\"{{paytmdata['name']}}\" value=\"{{paytmdata['value']}}\">\n                                 \n                        </form>\n                    </div>\n\n                    <div class=\"accordion-div\">\n                        <ul class=\"collapsible collapsible-tab\" data-collapsible=\"accordion\">\n                                \n                            <li *ngIf=\"enable_payu\">\n                                <div class=\"collapsible-header\" [class.payment_active]=\"type == 'PAYU'\" (click)=\"setPaymentType('PAYU')\">Credit/Debit Card</div>\n                                <div class=\"collapsible-body\">\n\n                                    <div [innerHTML]=\"settings['CONFIG_PAYU_DESCRIPTION']\"></div>\n\n                                </div>\n                            </li>\n                            <li *ngIf=\"enable_payu\">\n                                <div class=\"collapsible-header\" [class.payment_active]=\"type == 'PayUmoney'\" (click)=\"setPaymentType('PayUmoney')\">PayUmoney</div>\n                                <div class=\"collapsible-body\">\n                                    <img class=\"img-responsive\" src=\"public/img/payumoney-logo.png\">\n                                    <div [innerHTML]=\"settings['CONFIG_PAYUMONEY_DESCRIPTION']\"></div>\n\n\n                                </div>\n                            </li>\n                            <li *ngIf=\"enable_paytm\">\n                                <div class=\"collapsible-header\" [class.payment_active]=\"type == 'PAYTM'\" (click)=\"setPaymentType('PAYTM')\">Paytm</div>\n                                <div class=\"collapsible-body\">\n\n                                    <img class=\"img-responsive\" src=\"public/img/paytm-logo.png\">\n                                    <div class=\"montlight\">       <div [innerHTML]=\"settings['CONFIG_PAYTM_DESCRIPTION']\"></div></div>\n\n\n                                </div>\n                            </li>\n                            <li *ngIf=\"enable_cod==1 && is_wallet==0\">\n                                <div class=\"collapsible-header\" [class.payment_active]=\"type == 'COD'\" (click)=\"setPaymentType('COD')\">Cash on Delivery</div>\n                                <div class=\"collapsible-body\"><div [innerHTML]=\"settings['CONFIG_COD_DESCRIPTION']\"></div></div>\n                            </li>\n                        </ul>\n                    </div>\n\n\n\n\n                    <div class=\"col m12 details-subit-bx\">\n\n\n\n                        <span *ngIf=\"wallet['after_wallet_pay']>0 && is_wallet==1\"><b>Payable Amount <span [innerHTML]=\"wallet['after_wallet_pay_rs']\"></span></b></span>\n\n                        <span *ngIf=\"is_wallet==0\"><b>Payable Amount <span [innerHTML]=\"type != 'COD'?carts['payble_amount']:carts['cod_payble_amount']\"></span></b></span>\n\n\n\n                        <a (click)=\"paymentConfirm()\"  class=\"btn waves-effect fabivo-btn\"  href=\"javascript:void(0);\">\n                            CONFIRM ORDER\n                        </a>\n                        <p [hidden]=\"type == 'COD' && carts['enable_cashback']==1\" >\n                            <b>You'll receive <span [innerHTML]=\"carts['cash_back_amount']\"></span> cashback in Fabivo wallet!</b>\n                        </p>\n                    </div> \n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div id=\"modal_otp_cod\" class=\"modal login-bx\">\n    <div  class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <form class=\"login-form\" name=\"verified-form\" id=\"verified_number\" (ngSubmit)=\"verified_number.form.valid && submitOtp()\"  #verified_number=\"ngForm\" novalidate>\n                      <div class=\"row\">\n                        <div class=\"input-field col s12 center\">\n                            <div class=\"contant\">\n                                <span class=\"modal-action modal-close\" (click)=\"close_model('modal_otp_cod1')\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                                <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                                <h4 class=\"center login-form-text\">Verify Mobile Number</h4>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <p>OTP sent to {{phone_number}}. Enter it below to verify.<p>\n                                <input id=\"otp\" type=\"text\" name='otp' [(ngModel)]=\"number_model.otp\" #otp=\"ngModel\" required  pattern=\"^[0-9]{6}$\" >\n                            <div *ngIf=\"verified_number.submitted && otp.errors && otp.errors.required\" class=\"help-block err-msg\">The Otp field is required.</div>\n                            <div *ngIf=\"verified_number.submitted && otp.errors && otp.errors.pattern\" class=\"help-block err-msg\">Enter 6 digit OTP.</div>\n\n\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"input-field col s12 login-btn\">\n                            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">VERIFY MOBILE NUMBER</button>\n                            <a  class=\"btn waves-effect fabivo-btn\" (click)=\"resendOtp()\">Resend Otp</a>\n\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ },

/***/ 412:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var TahnkYouComponent = (function () {
	    function TahnkYouComponent() {
	    }
	    return TahnkYouComponent;
	}());
	TahnkYouComponent = __decorate([
	    core_1.Component({
	        selector: 'thanks',
	        template: __webpack_require__(413),
	    }),
	    __metadata("design:paramtypes", [])
	], TahnkYouComponent);
	exports.TahnkYouComponent = TahnkYouComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 413:
/***/ function(module, exports) {

	module.exports = " <section class=\"faq-section\">\n       <div class=\"container\">\n             <div class=\"row\">\n                <div class=\"col m12 s12\">\n\n\t\t\t\t\t\n\t\t\t\t\t\t<h4 class=\"center-align\"><strong>Thank you for confirm your email.</strong></h4>\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n</section>"

/***/ },

/***/ 414:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var router_1 = __webpack_require__(361);
	var AuthGuard = (function () {
	    function AuthGuard(router) {
	        this.router = router;
	    }
	    AuthGuard.prototype.canActivate = function () {
	        if (localStorage.getItem('loginUser')) {
	            return true;
	        }
	        this.router.navigate(['/login']);
	        return false;
	    };
	    return AuthGuard;
	}());
	AuthGuard = __decorate([
	    core_1.Injectable(),
	    __metadata("design:paramtypes", [router_1.Router])
	], AuthGuard);
	exports.AuthGuard = AuthGuard;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 415:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var platform_browser_1 = __webpack_require__(21);
	var router_1 = __webpack_require__(361);
	var product_service_1 = __webpack_require__(373);
	var pager_service_1 = __webpack_require__(374);
	var global_1 = __webpack_require__(363);
	var common_1 = __webpack_require__(22);
	var cart_service_1 = __webpack_require__(370);
	var _ = __webpack_require__(375);
	var SearchComponent = (function () {
	    function SearchComponent(productService, route, router, pagerService, location, titleService, cartService) {
	        this.productService = productService;
	        this.route = route;
	        this.router = router;
	        this.pagerService = pagerService;
	        this.location = location;
	        this.titleService = titleService;
	        this.cartService = cartService;
	        this.products = [];
	        this.filters = [];
	        this.categories = [];
	        this.page = 1;
	        this.search = [];
	        this.searchCat = [];
	        this.pager = {};
	        this.sortBy = 'popular';
	        this.attributes = [];
	        this.valuses = [];
	        this.PostData = {};
	        this.ChildCategory = [];
	    }
	    SearchComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.route.params.subscribe(function (params) {
	            _this.slug = params['search_text'];
	            _this.router.routerState.root.queryParams.subscribe(function (params) {
	                if (_this.location.path().indexOf('search/') > -1) {
	                    if (params['page']) {
	                        _this.page = parseInt(params['page']);
	                    }
	                    if (params['sortBy']) {
	                        _this.sortBy = params['sortBy'];
	                    }
	                    if (params['filter']) {
	                        var filter = params['filter'];
	                        var filter_data = filter.split(";");
	                        for (var _i = 0, filter_data_1 = filter_data; _i < filter_data_1.length; _i++) {
	                            var attribute_data = filter_data_1[_i];
	                            var split_array = attribute_data.split("--");
	                            _this.search.push({ attribute: split_array[0], data: split_array[1] });
	                            _this.attributes.push(split_array[0]);
	                            var split_value = split_array[1].split(",");
	                            for (var _a = 0, split_value_1 = split_value; _a < split_value_1.length; _a++) {
	                                var value = split_value_1[_a];
	                                _this.valuses.push(value);
	                            }
	                        }
	                    }
	                    else {
	                        _this.search = [];
	                        _this.attributes = [];
	                        _this.valuses = [];
	                    }
	                    if (params['category']) {
	                        var category = params['category'];
	                        _this.ChildCategory = category.split(",");
	                    }
	                    else {
	                        _this.ChildCategory = [];
	                    }
	                    _this.getList();
	                }
	            });
	            _this.productService.getAllAttribute(_this.slug).subscribe(function (attributedata) {
	                _this.filters = attributedata['filter'];
	                _this.categories = attributedata['category'];
	                setTimeout(function () {
	                    jQuery('.collapsible').collapsible();
	                }, 2000);
	            }, function (err) {
	                console.log(err);
	            });
	        });
	    };
	    SearchComponent.prototype.setsortBy = function (sort_by) {
	        this.sortBy = sort_by;
	        this.page = 1;
	        jQuery('#modalSort').modal('close');
	        this.getList();
	    };
	    SearchComponent.prototype.setCategory = function (category) {
	        var index = _.indexOf(this.ChildCategory, category);
	        if (index == -1) {
	            this.ChildCategory.push(category);
	        }
	        else {
	            this.ChildCategory = _.without(this.ChildCategory, category);
	        }
	        this.page = 1;
	        jQuery('#modalFilter').modal('close');
	        this.getList();
	    };
	    SearchComponent.prototype.setfilter = function (attribute, value) {
	        var attribute_data = this.search.filter(function (search) { return search.attribute === attribute; }).pop();
	        if (attribute_data) {
	            var data = attribute_data.data;
	            var spitdata = data.split(",");
	            var index = _.indexOf(spitdata, value);
	            if (index == -1) {
	                spitdata.push(value);
	                this.valuses.push(value);
	            }
	            else {
	                spitdata = _.without(spitdata, value);
	                this.valuses = _.without(this.valuses, value);
	            }
	            if (spitdata.length > 0) {
	                data = spitdata.join();
	                Object.assign(attribute_data, { data: data });
	            }
	            else {
	                this.search = this.search.filter(function (search) { return search.attribute !== attribute; });
	                this.attributes = _.without(this.attributes, attribute);
	            }
	        }
	        else {
	            this.search.push({ attribute: attribute, data: value });
	            this.attributes.push(attribute);
	            this.valuses.push(value);
	        }
	        this.page = 1;
	        jQuery('#modalFilter').modal('close');
	        this.getList();
	    };
	    SearchComponent.prototype.clearFilter = function () {
	        this.search = [];
	        this.searchCat = [];
	        this.attributes = [];
	        this.valuses = [];
	        this.getList();
	    };
	    SearchComponent.prototype.setPage = function (page) {
	        if (page < 1 || page > this.pager.totalPages) {
	            return;
	        }
	        this.page = page;
	        this.getList();
	    };
	    SearchComponent.prototype.getList = function () {
	        var _this = this;
	        global_1.blockUI();
	        if (this.ChildCategory.length) {
	            this.PostData['category'] = this.ChildCategory;
	        }
	        this.PostData['search'] = this.search;
	        this.productService.getSearchProduct(this.slug, this.page, this.sortBy, this.PostData).subscribe(function (productsdata) {
	            _this.products = productsdata['products'];
	            _this.total = productsdata['products']['total'];
	            _this.per_page = productsdata['products']['per_page'];
	            ;
	            _this.pager = _this.pagerService.getPager(_this.total, _this.page, _this.per_page);
	            _this.changeUrl();
	            setTimeout(function () {
	                jQuery(document).ready(function () {
	                    jQuery('.collapsible').collapsible();
	                    jQuery('.modal').modal();
	                });
	            }, 1000);
	        }, function (err) {
	            console.log(err);
	        });
	        global_1.unblockUI();
	    };
	    SearchComponent.prototype.changeUrl = function () {
	        var url = 'search/' + this.slug + '?page=' + this.page + '&sortBy=' + this.sortBy;
	        if (this.search) {
	            var filter = '';
	            var i = 0;
	            for (var _i = 0, _a = this.search; _i < _a.length; _i++) {
	                var entry = _a[_i];
	                if (i > 0) {
	                    filter = filter + ';';
	                }
	                filter = filter + entry.attribute + "--" + entry.data;
	                i = i + 1;
	            }
	            if (filter) {
	                url = url + "&filter=" + filter;
	            }
	        }
	        if (this.ChildCategory) {
	            var catfilter = this.ChildCategory.join();
	            if (catfilter) {
	                url = url + "&category=" + catfilter;
	            }
	        }
	        var count = 0;
	        var query_string = '';
	        this.location.go(url);
	    };
	    return SearchComponent;
	}());
	SearchComponent = __decorate([
	    core_1.Component({
	        selector: 'product-listing',
	        template: __webpack_require__(416),
	    }),
	    __metadata("design:paramtypes", [product_service_1.ProductService,
	        router_1.ActivatedRoute,
	        router_1.Router,
	        pager_service_1.PagerService,
	        common_1.Location,
	        platform_browser_1.Title,
	        cart_service_1.CartService])
	], SearchComponent);
	exports.SearchComponent = SearchComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 416:
/***/ function(module, exports) {

	module.exports = "\n<section class=\"listing-breadcrumb\">\n    <div class=\"container\">\n        <div *ngIf=\"categories.length>0\"  class=\"row div-hide-on-large-only\">\n            <div class=\"col m6 s6 center-align\">\n                <a href=\"#modalFilter\">\n                    <i class=\"fa fa-filter fa_filter\"></i>\n                    FILTERS </a>\n            </div>\n            <div class=\"col m6 s6 center-align\">\n                <a href=\"#modalSort\" >\n                    <i class=\"fa fa-sort fa_sort_by\" ></i>\n                    SORT BY \n                </a>\n            </div>\n\n\n\n        </div>\n        <div class=\"row \" >\n            <div class=\"col m6 s12 div-hide-on-med-and-down\">\n                <div class=\"sort-by\">\n                    <ul>\n                        <li><b>Sort By:</b></li>\n                        <li [ngClass]=\"{active:sortBy == 'popular'}\" ><a  (click)=\"setsortBy('popular')\"> Popular</a></li>\n                        <li [ngClass]=\"{active:sortBy =='new'}\" ><a  (click)=\"setsortBy('new')\"> New</a></li>\n                        <li [ngClass]=\"{active:sortBy =='price_high'}\" ><a  (click)=\"setsortBy('price_high')\">Price High</a></li>\n                        <li [ngClass]=\"{active:sortBy =='price_low'}\" ><a  (click)=\"setsortBy('price_low')\">Price Low</a></li>\n                        <div class=\"clearfix\"></div>\n                    </ul>\n                </div>\n\n            </div>\n            <div id=\"modalSort\" class=\"modal div-hide-on-large-only\">\n                <div class=\"container\">\n                    <div class=\"row\">\n                        <div class=\"col s12\"><span class=\"modal-action modal-close\" ><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span></div>\n                        <div class=\"col s12\">\n\n                            <ul>\n                                <li><b>SORT BY</b></li>\n                                <li [ngClass]=\"{active:sortBy == 'popular'}\" ><a  (click)=\"setsortBy('popular')\"> Popular</a></li>\n                                <li [ngClass]=\"{active:sortBy =='new'}\" ><a  (click)=\"setsortBy('new')\"> New</a></li>\n                                <li [ngClass]=\"{active:sortBy =='price_high'}\" ><a  (click)=\"setsortBy('price_high')\">  Price : Low to High</a></li>\n                                <li [ngClass]=\"{active:sortBy =='price_low'}\" ><a  (click)=\"setsortBy('price_low')\">Price : High to Low</a></li>\n                                <div class=\"clearfix\"></div>\n                            </ul>\n\n\n\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col m6 s12 pagination_full\">\n\n\n\n                <!-- pager -->\n                <ul  *ngIf=\"pager.pages && pager.pages.length\" class=\"pagination right\">\n                    <!--<li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                        <a (click)=\"setPage(1)\">First</a>\n                    </li> -->\n                    <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === 1}\">\n                        <a  (click)=\"setPage(pager.currentPage - 1)\"><i class=\"material-icons\">chevron_left</i></a>\n                    </li>\n                    <li class=\"waves-effect\"  *ngFor=\"let page of pager.pages\" [ngClass]=\"{active:pager.currentPage === page}\">\n                        <a (click)=\"setPage(page)\">{{page}}</a>\n                    </li>\n                    <li class=\"waves-effect\" [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                        <a (click)=\"setPage(pager.currentPage + 1)\"><i class=\"material-icons\">chevron_right</i></a>\n                    </li>\n                    <!--\n                    <li class=\"waves-effect\"  [ngClass]=\"{disabled:pager.currentPage === pager.totalPages}\">\n                        <a (click)=\"setPage(pager.totalPages)\">Last</a>\n                    </li> -->\n                </ul>\n            </div> \n        </div>\n    </div>  \n</section>\n\n<section class=\"product-category\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col m6 s6\">\n                <h5>PRODUCT SEARCH</h5>\n            </div>\n            <div class=\"col m6 s6\">\n                <div class=\"product-category-breadcrumb right\">\n                   Search Results for: \"{{slug}}\"\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n<section class=\"filters-product\">\n    <div class=\"container\">\n        <div class=\"row\" *ngIf=\"categories.length>0\" >\n            <div class=\"col m4 s6 div-hide-on-med-and-down\">\n\n                <div class=\"filters-list filters-list-2\">\n                    <ul class=\"collapsible\" data-collapsible=\"accordion\">\n                         <li >\n                            <div class=\"collapsible-header\" [ngClass]=\"{show_bullet:ChildCategory.length>0}\">Category <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                            </div>\n                            <div class=\"collapsible-body\">\n                                <ul>\n                                    <li *ngFor=\"let category of categories\">\n                                        <a [ngClass]=\"{active:ChildCategory.indexOf(category.slug) > -1}\" (click)=\"setCategory(category.slug)\">\n                                            {{category.name | capitalize}}\n                                        </a>\n                                    </li>\n\n                                </ul>\n                            </div>\n                        </li>\n                        <li *ngFor=\"let filter of filters\">\n                            <div class=\"collapsible-header\" [ngClass]=\"{show_bullet:attributes.indexOf(filter.slug) > -1}\">{{filter.name | capitalize}} <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                            </div>\n                            <div class=\"collapsible-body\">\n                                <ul>\n                                    <li *ngFor=\"let attribute of filter.attribute_value\">\n                                        <a [ngClass]=\"{active:valuses.indexOf(attribute.slug) > -1}\" (click)=\"setfilter(filter.slug,attribute.slug)\">\n                                            {{attribute.name | capitalize}}\n                                        </a>\n                                    </li>\n\n                                </ul>\n                            </div>\n                        </li>       \n\n\n                   \n\n                    </ul>\n                </div>\n            </div>\n\n\n            <div id=\"modalFilter\" class=\"modal div-hide-on-large-only\">\n                <div class=\"container\">\n                    <div class=\"row\">\n                        <div class=\"col s12\"><span class=\"modal-action modal-close\" ><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span></div>\n                        <div class=\"col s12\">\n\n                            <div class=\"filters-list filters-list-2\">\n                                <ul class=\"collapsible\" data-collapsible=\"accordion\">\n                                    <li >\n                                        <div class=\"collapsible-header\" [ngClass]=\"{show_bullet:ChildCategory.length>0}\">Category <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                                        </div>\n                                        <div class=\"collapsible-body\">\n                                            <ul>\n                                                <li *ngFor=\"let category of categories\">\n                                                    <a [ngClass]=\"{active:ChildCategory.indexOf(category.slug) > -1}\" (click)=\"setCategory(category.slug)\">\n                                                        {{category.name | capitalize}}\n                                                    </a>\n                                                </li>\n\n                                            </ul>\n                                        </div>\n                                    </li>\n                                    <li *ngFor=\"let filter of filters\">\n                                        <div class=\"collapsible-header\" [ngClass]=\"{show_bullet:attributes.indexOf(filter.slug) > -1}\">{{filter.name | capitalize}} <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>\n                                        </div>\n                                        <div class=\"collapsible-body\">\n                                            <ul>\n                                                <li *ngFor=\"let attribute of filter.attribute_value\">\n                                                    <a [ngClass]=\"{active:valuses.indexOf(attribute.slug) > -1}\" (click)=\"setfilter(filter.slug,attribute.slug)\">\n                                                        {{attribute.name | capitalize}}\n                                                    </a>\n                                                </li>\n\n                                            </ul>\n                                        </div>\n                                    </li>\n\n                                </ul>\n                            </div>\n\n\n\n                        </div>\n\n                        <div class=\"col s12\">\n                            <div class=\"row\">\n\n                                <div class=\"col m6 s6 center-align\">\n                                    <a  onclick=\"clearFilter()\" class=\"clear_filter waves-effect fabivo-btn btn\"> CLEAR \n                                    </a>\n                                </div>\n\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n\n\n            <div class=\"col m8 s6\">\n                <div class=\"category-content\">\n                    <div class=\"row\">\n                        <div class=\"col m4 s6 \" *ngFor=\"let product of products.data\">\n                             <div class=\"product-bx card-panel hoverable \">\n                                <div class=\"product-img\">\n                                    <a [routerLink]=\"['../../product',product.slug]\">\n                                        <img class=\"responsive-img\" src=\"{{product.image}}\">\n                                    </a>\n\n                                    <a *ngIf=\"cartService.wishlistArray.indexOf(product.id) > -1\" class=\"is_wishlist\" href=\"javascript:void(0);\"   ><i  class=\"fa fa-heart\" aria-hidden=\"true\" (click)=\"cartService.removeToWishlist(product.id)\"></i></a> \n                                    <a *ngIf=\"cartService.wishlistArray.indexOf(product.id) ==-1\" class=\"is_wishlist\" href=\"javascript:void(0);\"  ><i class=\"fa fa-heart-o\" aria-hidden=\"true\" (click)=\"cartService.addToWishlist(product.id)\" ></i></a>\n\n\n\n\n\n\n                                </div>\n                                <div class=\"product-details center-align\">\n                                    <a [routerLink]=\"['../../product',product.slug]\">\n                                        <h5>{{product.title}}</h5>\n                                        <!--                                     <h6 [innerHTML]=\"product.description\"></h6>-->\n                                        <div class=\"prize\">\n                                            <!--                                                             <del>&#x20B9; 1500</del>-->\n                                            <span [innerHTML]=\"product.price\"></span>\n                                        </div>\n                                    </a>\n                                    <div class=\"clearfix\"></div>\n                                    <!--                                    <a [routerLink]=\"['../../product',product.slug]\" class=\"waves-effect btn fabivo-btn-3\">\n                                                                            Shop now\n                                                                        </a>-->\n                                </div> \n                            </div>\n\n\n                        </div>\n                        <div class=\"emptyPage\" *ngIf=\"products.total==0\" style=\"\"> \n                            <div><i class=\"fa fa-search\" aria-hidden=\"true\"></i></div> \n                            <div>No Match Found</div> \n                        </div>\n\n                    </div>\n\n                </div>\n            </div>\n        </div>\n\n        <div class=\"row\" *ngIf=\"categories.length==0\">\n\n                <div class=\"col m12 s6 emptyPage\" style=\"\"> \n                            <div><i class=\"fa fa-search\" aria-hidden=\"true\"></i></div> \n                            <div>No Match Found</div> \n                        </div></div>\n    </div>\n</section>"

/***/ },

/***/ 417:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var page_service_1 = __webpack_require__(33);
	var router_1 = __webpack_require__(361);
	var authentication_service_1 = __webpack_require__(369);
	var global_1 = __webpack_require__(363);
	var common_1 = __webpack_require__(22);
	var socialuser_1 = __webpack_require__(418);
	var cart_service_1 = __webpack_require__(370);
	var HeaderComponent = (function () {
	    function HeaderComponent(pageService, router, authenticationService, location, cartService) {
	        this.pageService = pageService;
	        this.router = router;
	        this.authenticationService = authenticationService;
	        this.location = location;
	        this.cartService = cartService;
	        this.menus = [];
	        this.userData = [];
	        this.model = {};
	        this.loading = false;
	        this.error = [];
	        this.sign_up_error = [];
	        this.success = '';
	        this.authData = [];
	        this.signupmodel = {};
	        this.forgotpasswordmodel = {};
	        this.number_model = {};
	        this.mobile_number = '';
	        this.PhoneNumberModel = {};
	        this.user_id = '';
	        this.emailId = '';
	        this.loged = false;
	        this.user = { name: 'Hello' };
	        this.settings = [];
	        this.cartItem = [];
	        this.FacebookAppId = '';
	        this.GoogleAppId = '';
	    }
	    HeaderComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.authData = this.authenticationService.authData;
	        this.pageService.getMenus().subscribe(function (menuData) {
	            console.log(menuData['data'][0]['parent']);
	            _this.menus = menuData['data'][0]['parent'];
	            _this.settings = menuData['settings'];
	            var _self = _this;
	            FB.init({
	                appId: _self.FacebookAppId,
	                cookie: false,
	                xfbml: true,
	                version: 'v2.5'
	            });
	        });
	        if (localStorage.getItem('remember_me') == '1') {
	            this.model.loginEmail = localStorage.getItem('loginEmail');
	            this.model.remember_me = localStorage.getItem('remember_me');
	            this.model.loginPassword = localStorage.getItem('loginPassword');
	            setTimeout(function () {
	                jQuery(document).ready(function () {
	                    Materialize.updateTextFields();
	                });
	            }, 100);
	        }
	    };
	    HeaderComponent.prototype.signin = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.authenticationService.login(this.model.loginEmail, this.model.loginPassword)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.error = [];
	                _this.authData = JSON.parse(localStorage.getItem('loginUser'));
	                if (_this.model.remember_me) {
	                    localStorage.setItem('loginEmail', _this.model.loginEmail);
	                    localStorage.setItem('loginPassword', _this.model.loginPassword);
	                    localStorage.setItem('remember_me', '1');
	                }
	                else {
	                    localStorage.removeItem('remember_me');
	                    localStorage.removeItem('loginEmail');
	                    localStorage.removeItem('loginPassword');
	                }
	                jQuery('#modal1').modal('close');
	                jQuery('#loginForm')[0].reset();
	                jQuery('#loginForm').find('label').removeClass('active');
	                _this.getCartItem();
	                global_1.unblockUI();
	                _this.loading = false;
	                global_1.toast(_this.success, 'success');
	                setTimeout(function () {
	                    jQuery('.dropdown-button').dropdown({
	                        inDuration: 300,
	                        outDuration: 225,
	                        constrain_width: false,
	                        hover: true,
	                        gutter: 0,
	                        belowOrigin: false,
	                        alignment: 'left'
	                    });
	                    jQuery("#nav-mobile .child-menu ul li a").click(function () {
	                        jQuery('.button-collapse').sideNav('hide');
	                    });
	                }, 2000);
	            }
	            else {
	                _this.error = result['message'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    HeaderComponent.prototype.logout = function () {
	        global_1.blockUI();
	        this.authenticationService.logout();
	        this.authData = null;
	        global_1.unblockUI();
	        global_1.toast("Logout successfully", 'success');
	        this.cartService.cart = [];
	        this.cartItem = [];
	        this.cartService.wishlist = [];
	        this.cartService.wishlistArray = [];
	        this.router.navigate(['/']);
	        setTimeout(function () {
	            jQuery("#nav-mobile .child-menu ul li a").click(function () {
	                jQuery('.button-collapse').sideNav('hide');
	            });
	        }, 2000);
	        if (localStorage.getItem('remember_me') == '1') {
	            this.model.loginEmail = localStorage.getItem('loginEmail');
	            this.model.remember_me = localStorage.getItem('remember_me');
	            this.model.loginPassword = localStorage.getItem('loginPassword');
	            setTimeout(function () {
	                jQuery(document).ready(function () {
	                    Materialize.updateTextFields();
	                });
	            }, 1000);
	        }
	    };
	    HeaderComponent.prototype.isLoggedIn = function () {
	        return this.authenticationService.isLoggedIn();
	    };
	    HeaderComponent.prototype.getUserName = function () {
	        var userData = this.authenticationService.authData;
	        return userData['first_name'] + " " + userData['last_name'];
	    };
	    HeaderComponent.prototype.getCartItem = function () {
	        var _this = this;
	        if (this.authData && this.authData.id) {
	            this.cartService.getCart(this.authData.id).subscribe(function (cartData) {
	                _this.cartItem = cartData['data'];
	            }, function (err) {
	                console.log(err);
	            });
	            this.cartService.getToWishlist(this.authData.id);
	        }
	    };
	    HeaderComponent.prototype.getWishlistItem = function () {
	        var _this = this;
	        if (this.authData && this.authData.id) {
	            this.cartService.getWishlist(this.authData.id).subscribe(function (wishData) {
	                _this.cartService.wishlist = wishData['data'];
	            }, function (err) {
	                console.log(err);
	            });
	        }
	    };
	    HeaderComponent.prototype.usersignup = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.authenticationService.signup(this.signupmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.signupmodel = '';
	                _this.sign_up_error = [];
	                jQuery('#modal2').modal('close');
	                jQuery('#signupform')[0].reset();
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.router.navigate(['/']);
	                global_1.toast(_this.success, 'success');
	                if (result['phone_verify'] == 1) {
	                    _this.mobile_number = result['mobile_number'];
	                    _this.user_id = result['user_id'];
	                    jQuery('#modal_otp').modal('open');
	                }
	                else { }
	            }
	            else {
	                _this.sign_up_error = result['errors'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    HeaderComponent.prototype.forgot_passowrd = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.authenticationService.forgotPassword(this.forgotpasswordmodel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.forgotpasswordmodel = {};
	                _this.error = [];
	                jQuery('#modal3').modal('close');
	                jQuery('#forgotPassword')[0].reset();
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.router.navigate(['/']);
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['message'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    HeaderComponent.prototype.close_model = function (id) {
	        jQuery('#' + id)[0].reset();
	        jQuery('#' + id).find('label').removeClass('active');
	        this.error = [];
	    };
	    HeaderComponent.prototype.submitOtp = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.number_model['user'] = this.user_id;
	        this.authenticationService.mobileNumberVerified(this.number_model)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.number_model = '';
	                _this.error = [];
	                jQuery('#modal_otp').modal('close');
	                jQuery('#verified_number')[0].reset();
	                global_1.unblockUI();
	                _this.loading = false;
	                _this.location.go('/');
	                _this.router.navigate(['/']);
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['message'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(_this.error, 'error');
	            }
	        });
	    };
	    HeaderComponent.prototype.submitPhoneNumber = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.loading = true;
	        this.PhoneNumberModel['user'] = this.user_id;
	        this.authenticationService.mobileNumberSaved(this.PhoneNumberModel)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.success = result['message'];
	                _this.PhoneNumberModel = '';
	                _this.error = [];
	                jQuery('#PhoneModalBox').modal('close');
	                jQuery('#PhoneNumberForm')[0].reset();
	                if (result['phone_verify'] == 1) {
	                    _this.mobile_number = result['mobile_number'];
	                    jQuery('#modal_otp').modal('open');
	                }
	                _this.authenticationService.setMobileNumberONAuth(result['mobile_number']);
	                global_1.unblockUI();
	                _this.loading = false;
	                global_1.toast(_this.success, 'success');
	            }
	            else {
	                _this.error = result['error'];
	                _this.loading = false;
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    HeaderComponent.prototype.resendOtp = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.authenticationService.resendOtpPassword(this.user_id)
	            .subscribe(function (result) {
	            if (result['status_code'] == 1) {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	                _this.router.navigate(['/']);
	            }
	        });
	    };
	    HeaderComponent.prototype.fblogin = function () {
	        var _self = this;
	        global_1.blockUI();
	        FB.getLoginStatus(function (response) {
	            if (response.status === 'connected') {
	                if (response.authResponse) {
	                    FB.api('/me?fields=id,email,name,first_name,last_name,gender', function (result) {
	                        if (result && !result.error) {
	                            _self.user = result;
	                            _self.saveSocailData(result, 'facebook');
	                        }
	                        else {
	                            console.log(result.error);
	                        }
	                    });
	                    global_1.unblockUI();
	                }
	                else {
	                    console.log('User cancelled login or did not fully authorize.');
	                    global_1.unblockUI();
	                }
	            }
	            else {
	                FB.login(function (response) {
	                    if (response.authResponse) {
	                        FB.api('/me?fields=id,email,name,first_name,last_name,gender', function (result) {
	                            if (result && !result.error) {
	                                _self.user = result;
	                                _self.saveSocailData(result, 'facebook');
	                            }
	                            else {
	                                console.log(result.error);
	                            }
	                        });
	                        global_1.unblockUI();
	                    }
	                    else {
	                        console.log('User cancelled login or did not fully authorize.');
	                        global_1.unblockUI();
	                    }
	                });
	            }
	        });
	    };
	    HeaderComponent.prototype.saveSocailData = function (data, login_type) {
	        var _this = this;
	        global_1.blockUI();
	        this.authenticationService.socialLoginSaveData(data, login_type)
	            .subscribe(function (response) {
	            if (response['status_code'] == 1) {
	                var result = _this.authenticationService.socialLogin(response['user_data']);
	                if (result) {
	                    _this.error = [];
	                    _this.authData = _this.authenticationService.authData;
	                    _this.emailId = response['user_data']['email'];
	                    _this.user_id = _this.authData.id;
	                    _this.getCartItem();
	                    jQuery('#modal1').modal('close');
	                    jQuery('#loginForm')[0].reset();
	                    global_1.unblockUI();
	                    setTimeout(function () {
	                        jQuery('.dropdown-button').dropdown({
	                            inDuration: 300,
	                            outDuration: 225,
	                            constrain_width: false,
	                            hover: true,
	                            gutter: 0,
	                            belowOrigin: false,
	                            alignment: 'left'
	                        });
	                    }, 2000);
	                    global_1.toast(response['message'], 'success');
	                    if (response['user_data']['phone'] == '' && response['user_data']['phone_verify'] == 0) {
	                        jQuery('#PhoneModalBox').modal('open');
	                    }
	                    global_1.unblockUI();
	                }
	                else {
	                    global_1.toast(response['message'], 'error');
	                    global_1.unblockUI();
	                }
	            }
	            else {
	                _this.router.navigate(['/']);
	                global_1.toast(response['message'], 'error');
	                global_1.unblockUI();
	            }
	        });
	    };
	    HeaderComponent.prototype.googleInit = function () {
	        var that = this;
	        gapi.load('auth2', function () {
	            that.auth2 = gapi.auth2.init({
	                client_id: that.GoogleAppId,
	                cookiepolicy: 'single_host_origin',
	                scope: 'profile email'
	            });
	            that.attachSignin(document.getElementById('googleBtn'));
	        });
	    };
	    HeaderComponent.prototype.attachSignin = function (element) {
	        var _self = this;
	        this.auth2.attachClickHandler(element, {}, function (googleUser) {
	            var profile = googleUser.getBasicProfile();
	            var userData = new socialuser_1.SocialUser(profile.getId(), profile.getName(), profile.getGivenName(), profile.getFamilyName(), profile.getEmail());
	            _self.saveSocailData(userData, 'google');
	        }, function (error) {
	        });
	    };
	    HeaderComponent.prototype.ngAfterViewInit = function () {
	    };
	    HeaderComponent.prototype.isDisabled = function (path) {
	        return this.location.path().indexOf(path) > -1;
	    };
	    HeaderComponent.prototype.Search = function (search_text) {
	        if (search_text != '') {
	            this.router.navigate(['search', search_text]);
	        }
	    };
	    HeaderComponent.prototype.redirectback = function () {
	        this.location.back();
	    };
	    return HeaderComponent;
	}());
	HeaderComponent = __decorate([
	    core_1.Component({
	        selector: 'my-header',
	        template: __webpack_require__(419)
	    }),
	    __metadata("design:paramtypes", [page_service_1.PageService,
	        router_1.Router,
	        authentication_service_1.AuthenticationService,
	        common_1.Location,
	        cart_service_1.CartService])
	], HeaderComponent);
	exports.HeaderComponent = HeaderComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 418:
/***/ function(module, exports) {

	"use strict";
	var SocialUser = (function () {
	    function SocialUser(id, name, first_name, last_name, email) {
	        this.id = id;
	        this.name = name;
	        this.first_name = first_name;
	        this.last_name = last_name;
	        this.email = email;
	    }
	    return SocialUser;
	}());
	exports.SocialUser = SocialUser;


/***/ },

/***/ 419:
/***/ function(module, exports) {

	module.exports = "<section class=\"header\">\n    <!--<div class=\"header-top\" [hidden]=\"isDisabled('/checkout')\">-->\n    <div class=\"header-top\">\n         <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col m6 s6 xs6 col-1\">\n<!--                    <a href=\"#\">\n                        <i class=\"fa fa-mobile\" aria-hidden=\"true\"></i>\n                        <span>Download App</span>\n                    </a>-->\n                \n                </div>\n                <div class=\"col m6 s6 xs6 col-2\">\n                    <div class=\"row right\">\n                        <div class=\"col order-box\">\n                            <ul>\n                                <li> \n                                    <a href=\"#\"> \n                                        <i class=\"fa fa-search\" aria-hidden=\"true\"></i> \n                                    </a> \n                                </li>\n                                \n                                <li *ngIf=\"!isLoggedIn()\">\n                                    <a class=\"waves-effect fabivo-btn btn user-icon\" href=\"#modal1\">\n                                        <i class=\"fa fa-user\" aria-hidden=\"true\"></i>\n                                    </a> \n                                </li>\n                                \n                                <li *ngIf=\"isLoggedIn()\">\n                                    <a class=\"dropdown-button waves-effect fabivo-btn btn user-icon\" href=\"javascript:void(0);\" data-activates='dropdownnew'>\n                                        <i class=\"fa fa-user\" aria-hidden=\"true\"></i>\n                                    </a>\n                                    <ul id='dropdownnew' class='dropdown-content'>  \n                                        <li><a [routerLink]=\"['./myaccount', 'profile']\">Hi, {{ this.authenticationService.authData ? this.authenticationService.authData['first_name'] + \" \" + this.authenticationService.authData['last_name'] : this.authenticationService.getUserName() }}</a></li>\n                                        <li><a [routerLink]=\"['./myaccount', 'order']\">My Order</a></li>\n                                        <li><a [routerLink]=\"['./myaccount', 'wallet']\">My Wallet</a></li>\n                                        <li><a href=\"javascript:void(0);\" (click)=\"logout()\">Logout</a></li>\n                                    </ul>\n                                </li>\n                                \n                                <li> \n                                    <a class=\"cart-field\" [routerLink]=\"['./cart']\">\n                                        <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i>\n                                        <div *ngIf=\"(cartService.cart && cartService.cart.length > 0) || (cartItem && cartItem.length > 0)\" class=\"num\">{{cartService.cart && cartService.cart.length > 0 ? cartService.cart.length : cartItem.length}}</div>\n                                    </a>\n                                </li>\n                            </ul>\n                        </div>\n                        \n<!--                        <div class=\"col cart-box\">\n                            <div class=\"search-box-mobile\">\n                                <button class=\"mobile_search\">\n                                    <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\n                                </button>\n                                <div class=\"nav-wrapper\">\n                                    <form>\n                                        <div class=\"input-field\">\n                                            <input placeholder=\"Search....\" id=\"search\" name=\"search_text\" [(ngModel)]=\"search_text\" type=\"text\" (keyup.enter)=\"Search(search_text)\">\n                                        </div>\n                                    </form>\n                                </div>\n                            </div>\n                        </div>-->\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    \n    <nav class=\"header-custom lighten-1\"  [ngClass]=\"{res_header: isDisabled('/checkout')==1}\"  role=\"navigation\">\n        <div class=\"nav-wrapper container\">\n            <a id=\"logo-container\" class=\"brand-logo\" [routerLink]=\"['/']\">\n               <img class=\"responsive-img\" src=\"public/img/logo.png\"/>   \n            </a>\n\n            <div class=\"right\"  [hidden]=\"isDisabled('/checkout')\">\n                <ul class=\"left hide-on-med-and-down\">\n<!--                    <li *ngFor=\"let menu of menus; let i = index\">\n                        <a class=\"dropdown-button\" href=\"javascript:void(0);\" attr.data-activates=\"dropdown{{i}}\" *ngIf=\"menu.name\">\n                            {{menu.name}}\n                        </a>   \n                        <div id=\"dropdown{{i}}\" class=\"dropdown-content dropdown-full\" *ngIf=\"menu.parent.length > 0\">\n                            <div class=\"drop-in\" *ngFor=\"let submenu of menu.parent\">\n                                 <h4 *ngIf=\"submenu.name\"> {{submenu.name}} </h4>\n                                <span class=\"sapreter\"></span>\n                                <ul *ngIf=\"submenu.children.length > 0\">\n                                    <li *ngFor=\"let childmenu of submenu.children\">\n                                        <a [routerLink]=\"['./category',childmenu.slug]\" *ngIf=\"childmenu.name\">{{childmenu.name}}</a>\n                                    </li>\n                                </ul>\n                            </div>\n                        </div>  \n                    </li>-->\n\n                    <li> <a class=\"dropdown-button\" [routerLink]=\"['/']\">Home</a> </li>\n                    \n                    <li *ngFor=\"let menu of menus; let i = index\"> \n                        <a class=\"dropdown-button\" href=\"javascript:void(0);\" attr.data-activates=\"dropdown{{i}}\" *ngIf=\"menu.name\">\n                            {{menu.name}}\n                        </a> \n                        <ul class=\"drop-nav\" *ngIf=\"menu.children.length > 0\">\n                            <li *ngFor=\"let submenu of menu.children\">\n                                <a [routerLink]=\"['./category',submenu.slug]\" *ngIf=\"submenu.name\">{{submenu.name}}</a>\n                            </li>\n<!--                            <li>\n                                <a href=\"#\">demo</a>\n                            </li>\n                            <li>\n                                <a href=\"#\">demo</a>\n                            </li>-->\n                        </ul>\n                    </li>\n                    \n<!--                    <li> <a class=\"dropdown-button\" href=\"#!\">Earrings</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown4\">Bracelet</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown5\">Necklaces</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown6\">Color We Mad</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>-->\n                    <li> <a class=\"dropdown-button\" [routerLink]=\"['./contact-us']\">Contact Us</a> </li>\n\n                </ul>\n                 \n                <ul id=\"nav-mobile\" class=\"collapsible side-nav\" data-collapsible=\"accordion\">\n<!--                    <li *ngFor=\"let menu of menus; let i = index\">\n                        <div class=\"collapsible-header\" *ngIf=\"menu.name\">\n                            <a href=\"javascript:void(0);\">{{menu.name}}<i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i></a>\n                        </div>\n                        <div class=\"collapsible-body child-menu\" *ngIf=\"menu.parent.length > 0\">\n                            <div *ngFor=\"let submenu of menu.parent\">\n                                <div class=\"respnsive-menu-heading\">\n                                    <h4 *ngIf=\"submenu.name\"> {{submenu.name}} </h4>\n                                    <span class=\"sapreter\"></span>\n                                </div>\n                                <ul *ngIf=\"submenu.children.length > 0\">\n                                    <li *ngFor=\"let childmenu of submenu.children\">\n                                        <a [routerLink]=\"['./category',childmenu.slug]\" *ngIf=\"childmenu.name\">{{childmenu.name}}</a>\n                                    </li>\n                                </ul>\n                            </div>\n                        </div>\n                    </li>-->\n\n                    <li> <a class=\"dropdown-button\" href=\"#!\">HOME</a> </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\">Rings</a> </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\">Earrings</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown4\">Bracelet</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown5\">Necklaces</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\" data-activates=\"dropdown6\">Color We Mad</a>\n                        <ul class=\"drop-nav\">\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                            <li><a href=\"#\">demo</a></li>\n                        </ul>\n                    </li>\n                    <li> <a class=\"dropdown-button\" href=\"#!\">Contact Us</a> </li>\n\n                    <div class=\"child-menu\" *ngIf=\"!isLoggedIn()\">\n                        <ul class=\"my-account-bx-mobile\">\n                            <li><a href=\"#modal1\">Login</a></li>\n                            <li><a href=\"#modal2\">Sign Up</a></li>\n                        </ul>\n                    </div>\n\n                    <div class=\"my-account-bx child-menu\" *ngIf=\"isLoggedIn()\"> \n                        <ul class=\"my-account-bx-mobile\">  \n                            <li><a [routerLink]=\"['./myaccount', 'profile']\">My Account</a></li>\n                            <li><a [routerLink]=\"['./myaccount', 'order']\">My Order</a></li>\n                            <li><a [routerLink]=\"['./myaccount', 'wallet']\">My Wallet</a></li>\n                            <li><a href=\"javascript:void(0);\" (click)=\"logout()\">Logout</a></li>\n                        </ul>\n                    </div>\n                </ul>\n                 \n                <a [hidden]=\"isDisabled('/product')\" href=\"#\" data-activates=\"nav-mobile\" class=\"button-collapse\"><i class=\"material-icons\">menu</i></a> \n                <a [hidden]=\"!isDisabled('/product')\" href=\"javascript:void(0);\" (click)=\"redirectback()\" data-activates=\"nav-mobile\" class=\"back_link_nav\"><i class=\"fa fa-angle-left\" aria-hidden=\"true\"></i></a> \n<!--                <div class=\"search-box\">\n                    <form>\n                        <div class=\"input-field\">\n                            <button>\n                                <i class=\"fa fa-search\" aria-hidden=\"true\"></i>          </button>\n    <input placeholder=\"Search....\" id=\"search1\" type=\"search\"  name=\"search_text\" [(ngModel)]=\"search_text\" (keyup.enter)=\"Search(search_text)\"  >\n                            <i class=\"material-icons\">close</i>  \n                                  </div>\n                    </form>\n                </div>-->\n<!--                <div class=\"login-box\" *ngIf=\"!isLoggedIn()\">\n                    <a class=\"waves-effect fabivo-btn btn\" href=\"#modal1\"><i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i>Login</a>\n                    <a class=\"waves-effect fabivo-btn btn\" href=\"#modal2\"><i class=\"fa fa-user\" aria-hidden=\"true\"></i>Sign Up</a>     \n                </div>\n                <div class=\"login-box\" *ngIf=\"isLoggedIn()\">\n                    <div class=\"my-account-bx\">\n                        <a class='dropdown-button btn waves-effect fabivo-btn' href='javascript:void(0);' data-activates='dropdownnew'>\n                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> My Account\n                        </a>\n\n                        <ul id='dropdownnew' class='dropdown-content'>  \n                            <li><a [routerLink]=\"['./myaccount', 'profile']\">Hi, {{ this.authenticationService.authData ? this.authenticationService.authData['first_name'] + \" \" + this.authenticationService.authData['last_name'] : this.authenticationService.getUserName() }}</a></li>\n                            <li><a [routerLink]=\"['./myaccount', 'order']\">My Order</a></li>\n                            <li><a [routerLink]=\"['./myaccount', 'wallet']\">My Wallet</a></li>\n                            <li><a href=\"javascript:void(0);\" (click)=\"logout()\">Logout</a></li>\n                        </ul>\n                    </div>\n                </div>-->\n            </div>\n            \n            <div class=\"right\" *ngIf=\"isDisabled('/checkout')\" style=\"color:#9D812E;\">\n                 <div class=\"col m6 s6 xs6 col-2\">\n                    <div class=\"row right\">\n                        <div class=\"col order-box2\">\n                            <a  href=\"tel:{{settings['CONFIG_PHONE_NUMBER']}}\">\n                                <i class=\"fa fa-phone\" aria-hidden=\"true\"></i>\n                                <span>{{settings['CONFIG_PHONE_NUMBER']}}</span>\n                            </a>\n                            <a href=\"mailto:{{settings['CONFIG_SUPPORT_MAIL']}}\">\n                                <i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>\n                                <span>{{settings['CONFIG_SUPPORT_MAIL']}}</span>\n                            </a>\n                        </div>    \n                  \n                    </div>\n                </div>\n            </div>\n            <div class=\"clearfix\"></div>    \n        </div>\n    </nav>\n\n</section>\n\n<div id=\"modal1\" class=\"modal login-bx\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <form class=\"login-form\" id=\"loginForm\" name=\"form\" (ngSubmit)=\"f.form.valid && signin()\" #f=\"ngForm\" novalidate>\n                      <div class=\"row\">\n                        <div class=\"input-field col s12 center\">\n                            <div class=\"contant\">\n                                <span class=\"modal-action modal-close\" (click)=\"close_model('loginForm')\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                                <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                                <h4 class=\"center login-form-text\">Login</h4>\n\n                            </div>\n                        </div>\n                    </div>\n<!--                    <div class=\"row social-btn-bx\">\n                        <div class=\"col m6 s12\">\n                            <a class=\"btn  fabivo-btn  waves-effect btn-social btn-facebook\" (click)=\"fblogin()\">\n                                <i class=\"fa fa-facebook\"></i><span>Sign in with Facebook</span>\n                            </a>\n                        </div>\n                        <div class=\"col m6 s12\">\n                            <a id='googleBtn' class=\"btn fabivo-btn  waves-effect btn-social btn-google\" >\n                                <i class=\"fa fa-google\"></i><span> Sign in with Google</span>\n                            </a>\n\n                        </div>\n                    </div>-->\n                    <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <i class=\"fa fa-user\"></i>\n                            <input id=\"loginEmail\" type=\"text\" name=\"loginEmail\" [(ngModel)]=\"model.loginEmail\" #loginEmail=\"ngModel\" required pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\">\n                            <label for=\"loginEmail\" >Email</label>\n                            <div *ngIf=\"f.submitted && loginEmail.errors && loginEmail.errors.required\" class=\"help-block err-msg\">Email is required</div>\n                            <div *ngIf=\"f.submitted && loginEmail.errors && loginEmail.errors.pattern\" class=\"help-block err-msg\"> Email is invalid</div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <i class=\" fa fa-lock\"></i>\n                            <input id=\"loginPassword\" type=\"password\" name=\"loginPassword\" [(ngModel)]=\"model.loginPassword\" #loginPassword=\"ngModel\" required>\n                            <label for=\"password\" class=\"\">Password</label>\n                            <div *ngIf=\"f.submitted && !loginPassword.valid\" class=\"help-block err-msg\">Password is required</div>\n                        </div>\n                    </div>\n                    <div class=\"row\">          \n                        <div class=\"input-field col s12 m12 l12  login-text\">\n                            <div class=\"row\">\n                                <div class=\"col m6\">\n                                    <input id=\"remember_me\" type=\"checkbox\" name=\"remember_me\"  [(ngModel)]=\"model.remember_me\" #remember_me=\"ngModel\" >\n                                    <label class=\"mr-lf\" for=\"remember_me\" >Remember me</label>\n                                </div>\n                                <div class=\"col m6\">\n                                    <p class=\"margin right-align medium-small\">\n                                        <a href=\"#modal3\" class=\"modal-close\">Forgot password ?</a>\n                                    </p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">          \n                        <div class=\"input-field col s12 m12 l12  login-text\">\n                            <div class=\"row\">\n                                <div class=\"col m12\">\n                                    <p class=\"margin left-align medium-small\">\n                                        <i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i>\n                                        <a class=\"allready modal-close\" href=\"#modal2\">Don't have an account? Sign up</a>\n                                    </p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"input-field col s12 login-btn\">\n                            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">Sign in</button>\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"modal2\" class=\"modal login-bx\" aria-hidden=\"true\">\n    <div class=\"row\">\n        <div class=\"col s12\">\n            <form class=\"login-form\" id=\"signupform\" (ngSubmit)=\"signup.form.valid && usersignup(signup)\"  #signup=\"ngForm\" novalidate>\n                  <div class=\"row\">\n                    <div class=\"input-field col s12 center\">\n                        <div class=\"contant\">\n                            <span class=\"modal-action modal-close\" (click)=\"close_model('signupform')\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                            <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                            <h4 class=\"center login-form-text\">Sign Up</h4>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"input-field col s6\">\n                        <i class=\"fa fa-user\"></i>\n                        <input id=\"first_name\" type=\"text\" name='first_name' [(ngModel)]=\"signupmodel.first_name\" #first_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                        <label for=\"first_name\" >First Name</label>\n                        <div *ngIf=\"signup.submitted && first_name.errors && first_name.errors.required\" class=\"help-block err-msg\">The first name field is required.</div>\n                        <div *ngIf=\"signup.submitted && first_name.errors && first_name.errors.pattern\" class=\"help-block err-msg\"> Enter valid first name.</div>\n\n\n                        <div *ngIf=\"sign_up_error['first_name'] && first_name.valid\" class=\"err-msg\">{{sign_up_error['first_name']['0']}}</div>\n                    </div>\n                    <div class=\"input-field col s6\">\n                        <input id=\"last_name\" name ='last_name' type=\"text\"  [(ngModel)]=\"signupmodel.last_name\" #last_name=\"ngModel\" required pattern=\"[a-zA-Z ]*\">\n                        <label for=\"last_name\" >Last Name</label>\n                       \n                        <div *ngIf=\"signup.submitted && last_name.errors && last_name.errors.required\" class=\"help-block err-msg\">The last name field is required.</div>\n                        <div *ngIf=\"signup.submitted && last_name.errors && last_name.errors.pattern\" class=\"help-block err-msg\"> Enter valid last name.</div>\n                        <div *ngIf=\"sign_up_error['last_name'] && last_name.valid\" class=\"err-msg\">{{sign_up_error['last_name']['0']}}</div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"input-field col s12\">\n                        <i class=\" fa fa-envelope\"></i>\n                        <input id=\"email\" type=\"text\" name='email' [(ngModel)]=\"signupmodel.email\" #email=\"ngModel\" required  pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\">\n                        <label for=\"email\" >Email</label>\n                        <div *ngIf=\"signup.submitted && email.errors && email.errors.required\" class=\"help-block err-msg\">The email field is required.</div>\n                        <div *ngIf=\"signup.submitted && email.errors && email.errors.pattern\" class=\"help-block err-msg\"> Email is invalid.</div>\n                        <div *ngIf=\"sign_up_error['email'] && email.valid\" class=\"err-msg\">{{sign_up_error['email']['0']}}</div>\n                       \n                    </div>\n                </div> \n                <div class=\"row\">\n                    <div class=\"input-field col s6\">\n                        <i class=\" fa fa-lock\"></i>\n                        <input id=\"password\" type=\"password\" name='password' [(ngModel)]=\"signupmodel.password\" #password=\"ngModel\" required validateEqual=\"confirm_password\" reverse=\"true\">\n                        <label for=\"password\" >Password</label>\n                        <div *ngIf=\"signup.submitted  && password.errors && password.errors.required\" class=\"help-block err-msg\">The password field is required.</div>\n                        <div *ngIf=\"sign_up_error['password']  && password.valid\" class=\"err-msg\">{{sign_up_error['password']['0']}}</div>\n                    </div>\n\n                    <div class=\"input-field col s6\">\n                        <i class=\" fa fa-lock\"></i>\n                        <input id=\"confirm_password\" type=\"password\" name='confirm_password' [(ngModel)]=\"signupmodel.confirm_password\" #confirm_password=\"ngModel\" required validateEqual=\"password\">\n                        <label for=\"confirm_password\" >Confirm Password</label>\n                        <div *ngIf=\"signup.submitted && confirm_password.errors && confirm_password.errors.required\" class=\"help-block err-msg\">The confirm password field is required.</div>\n                         <div *ngIf=\"signup.submitted && confirm_password.errors && !confirm_password.errors.validateEqual && !confirm_password.errors.required\" class=\"help-block err-msg\"> The confirm password and password must match.</div>                       \n                        <div *ngIf=\"sign_up_error['confirm_password']  && confirm_password.valid\" class=\"err-msg\">{{sign_up_error['confirm_password']['0']}}</div>\n                    </div>\n                </div>\n                <div class=\"row\">\n                    <div class=\"gender-bx col s6\">\n                        <i class=\"fa fa-venus-mars\" aria-hidden=\"true\"></i>\n                        <label>Gender</label>\n                        <p>\n                            <input class=\"with-gap\" name=\"gender\" type=\"radio\" id=\"male\" value='male' [(ngModel)]=\"signupmodel.gender\" #gender=\"ngModel\" required  />\n                            <label for=\"male\">Male</label>\n\n                            <input class=\"with-gap\" name=\"gender\" type=\"radio\" id=\"female\" value='female' [(ngModel)]=\"signupmodel.gender\" #gender=\"ngModel\" required/>\n                            <label for=\"female\">Female</label>\n                        </p>\n                        <div *ngIf=\"signup.submitted && gender.errors && gender.errors.required\" class=\"help-block err-msg\">The gender field is required.</div>\n                    </div>\n\n                    <div class=\"input-field col s6\">\n                        <i class=\" fa fa-phone\"></i>\n\n\n                        <input id=\"phone\" type=\"text\" name='phone' [(ngModel)]=\"signupmodel.phone\" #phone=\"ngModel\" required  pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\" >\n                        <label for=\"phone\" >Phone Number</label>\n                        <div *ngIf=\"signup.submitted && phone.errors && phone.errors.required\" class=\"help-block err-msg\">The phone number field is required.</div>\n                        <div *ngIf=\"signup.submitted && phone.errors && phone.errors.pattern\" class=\"help-block err-msg\"> The phone number format is invalid</div>\n                        <div *ngIf=\"sign_up_error['phone']  && phone.valid\" class=\"err-msg\">{{sign_up_error['phone']['0']}}</div>\n                    </div>\n                </div>\n                <div class=\"row\">          \n                    <div class=\"input-field col s12 m12 l12  login-text\">\n                        <div class=\"row\">\n                            <div class=\"col m12\">\n                                <p class=\"margin left-align medium-small\"><i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i>\n                                    <a  class=\"allready modal-close\" href=\"#modal1\">Already have an account? Sign in</a></p>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"row\">\n                    <div class=\"input-field col s12 login-btn\">\n                        <button  class=\"btn waves-effect fabivo-btn\">Sign up</button>\n                    </div>\n                </div>\n\n            </form>\n        </div>\n    </div>\n</div>\n\n<div id=\"modal3\" class=\"modal login-bx\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <form class=\"login-form\" name=\"forgot-form\" id=\"forgotPassword\" (ngSubmit)=\"forgotpassword.form.valid && forgot_passowrd()\"  #forgotpassword=\"ngForm\" novalidate>\n                      <div class=\"row\">\n                        <div class=\"input-field col s12 center\">\n                            <div class=\"contant\">\n                                <span class=\"modal-action modal-close\" (click)=\"close_model('forgotPassword')\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                                <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                                <h4 class=\"center login-form-text\">Forgot Password</h4>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <i class=\"fa fa-user\"></i>\n                            <input id=\"emails\" type=\"text\" name='email_add' [(ngModel)]=\"forgotpasswordmodel.email_add\" #email_add=\"ngModel\" required  pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\">\n                            <label for=\"emails\" >Email</label>\n                            <div *ngIf=\"forgotpassword.submitted && email_add.errors && email_add.errors.required\" class=\"help-block err-msg\">The email field is required.</div>\n                            <div *ngIf=\"forgotpassword.submitted && email_add.errors && email_add.errors.pattern\" class=\"help-block err-msg\"> Email is invalid.</div>\n                            <div *ngIf=\"error['email']\" class=\"err-msg\">{{error['email_add']['0']}}</div>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">          \n                        <div class=\"input-field col s12 m12 l12  login-text\">\n                            <div class=\"row\">\n                                <div class=\"col m12\">\n                                    <p class=\"margin left-align medium-small\"><i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i>\n                                        <a  class=\"allready modal-close\" href=\"#modal1\">Sign in</a></p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"input-field col s12 login-btn\">\n                            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">Submit</button>\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"modal_otp\" class=\"modal login-bx\">\n    <div  class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <form class=\"login-form\" name=\"verified-form\" id=\"verified_number\" (ngSubmit)=\"verified_number.form.valid && submitOtp()\"  #verified_number=\"ngForm\" novalidate>\n                      <div class=\"row\">\n                        <div class=\"input-field col s12 center\">\n                            <div class=\"contant\">\n                                <span class=\"modal-action modal-close\" (click)=\"close_model('verified_number')\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                                <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                                <h4 class=\"center login-form-text\">Verify Mobile Number</h4>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"input-field col s12\">\n                            <p>OTP sent to {{mobile_number}}. Enter it below to verify.<p>\n                                <input id=\"otp\" type=\"text\" name='otp' [(ngModel)]=\"number_model.otp\" #otp=\"ngModel\" required pattern=\"^[0-9]{6}$\"  >\n                            <div *ngIf=\"verified_number.submitted && otp.errors && otp.errors.required\" class=\"help-block err-msg\">The Otp field is required.</div>\n                            <div *ngIf=\"verified_number.submitted && otp.errors && otp.errors.pattern\" class=\"help-block err-msg\">Enter 6 digit OTP.</div>\n\n\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"input-field col s12 login-btn\">\n                            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">VERIFY MOBILE NUMBER</button>\n                            <a  class=\"btn waves-effect fabivo-btn\" (click)=\"resendOtp()\">Resend Otp</a>\n\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div id=\"PhoneModalBox\" class=\"modal login-bx\">\n    <div  class=\"container\">\n        <div class=\"row\">\n            <div class=\"col s12\">\n                <form class=\"login-form\" name=\"phonne-verified-form\" id=\"PhoneNumberForm\" (ngSubmit)=\"PhoneNumberForm.form.valid && submitPhoneNumber()\"  #PhoneNumberForm=\"ngForm\" novalidate>\n                      <div class=\"row\">\n                        <div class=\"input-field col s12 center\">\n                            <div class=\"contant\">\n                                <span class=\"modal-action modal-close\" (click)=\"close_model('PhoneNumberForm')\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></span>\n                                <img class=\"responsive-img\" src=\"public/img/logo.png\"/>  \n                                <h4 class=\"center login-form-text\">Verify Mobile Number</h4>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\" col s12\">\n                            <p>You are logged in with {{emailId}}.<br>\n                                Please provide your mobile number for verification</p>\n                        </div>\n                        <div class=\"input-field col s12\">\n                            <i class=\" fa fa-phone\"></i>\n\n\n                            <input id=\"phone_number\" type=\"text\" name='phone_number' [(ngModel)]=\"PhoneNumberModel.phone_number\" #phone_number=\"ngModel\" required  pattern=\"^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$\" >\n                            <label for=\"phone_number\" >Phone Number</label>\n                            <div *ngIf=\"PhoneNumberForm.submitted && phone_number.errors && phone_number.errors.required\" class=\"help-block err-msg\">The phone number field is required.</div>\n                            <div *ngIf=\"PhoneNumberForm.submitted && phone_number.errors && phone_number.errors.pattern\" class=\"help-block err-msg\"> The phone number format is invalid</div>\n                            <div *ngIf=\"error['phone']\" class=\"err-msg\">{{error['phone']['0']}}</div>\n\n\n\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"input-field col s12 login-btn\">\n                            <button [disabled]=\"loading\" class=\"btn waves-effect fabivo-btn\">Send Otp</button>\n\n\n                        </div>\n                    </div>\n\n\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n\n<!--<div class=\"hearder_top\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-lg-4\">\n        <select id=\"mounth\">\n          <option value=\"hide\">USD</option>\n          <option value=\"option\">Option1</option>\n          <option value=\"option\">Option2</option>\n        </select>\n        <select id=\"year\">\n          <option value=\"hide\">ENG</option>\n          <option value=\"2010\">Option</option>\n          <option value=\"2011\">Option</option>\n        </select>\n      </div>\n      <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-6\"></div>\n      <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-6\">\n        <div class=\"left_side\"><a href=\"#\"><i class=\"fa fa-user\" aria-hidden=\"true\"></i></a> <a href=\"#\"><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i>\n          <div class=\"num\">0</div>\n          </a>\n          <p>$0.00</p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<header>\n<nav id='cssmenu' class=\"topnav\">\n<div class=\"logo\"><a href=\"\"><img src=\"public/img/hearder_logo.png\" alt=\"logo\"> </a></div>\n<div id=\"head-mobile\"></div>\n<div class=\"button\"></div>\n<ul>\n<li class='active'><a href='#'>HOME</a></li>\n<li><a href='#'>Rings</a></li>\n<li><a href='#'>Earrings</a>\n   <ul>\n      <li><a href='#'>Product 1</a>\n         <ul>\n            <li><a href='#'>Sub Product</a></li>\n            <li><a href='#'>Sub Product</a></li>\n         </ul>\n      </li>\n      <li><a href='#'>Product 2</a>\n         <ul>\n            <li><a href='#'>Sub Product</a></li>\n            <li><a href='#'>Sub Product</a></li>\n         </ul>\n      </li>\n   </ul>\n</li>\n<li><a href='#'>Bracelet</a></li>\n<li><a href='#'>Necklaces</a></li>\n<li><a href='#'>Collections</a></li>\n<li><a href='#'>Contact Us</a></li>\n <li><a href=\"#\" id=\"searchtoggl\"><img src=\"public/img/search_icon.png\" alt=\"\" /></a></li>\n  <li><a href=\"#\" id=\"searchtoggl\"><img src=\"public/img/shooping.png\" alt=\"\" /></a></li>\n</ul>\n</nav>\n</header>\n<div id=\"searchbar\" class=\"clearfix\">\n    <form id=\"searchform\" method=\"get\" action=\"searchpage.php\">\n      <button type=\"submit\" id=\"searchsubmit\" class=\"fa fa-search fa-4x\"></button>\n      <input type=\"search\" name=\"s\" id=\"s\" placeholder=\"Keywords...\" autocomplete=\"off\">\n\n    </form>\n</div>-->\n"

/***/ },

/***/ 420:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var page_service_1 = __webpack_require__(33);
	var authentication_service_1 = __webpack_require__(369);
	var common_1 = __webpack_require__(22);
	var global_1 = __webpack_require__(363);
	var FooterComponent = (function () {
	    function FooterComponent(pageService, location, authenticationService) {
	        this.pageService = pageService;
	        this.location = location;
	        this.authenticationService = authenticationService;
	        this.cmspages1 = [];
	        this.cmspages2 = [];
	        this.cmspages3 = [];
	        this.settings = [];
	        this.newslatter = {};
	    }
	    FooterComponent.prototype.ngOnInit = function () {
	        var _this = this;
	        this.pageService.getPages().subscribe(function (pages) {
	            _this.cmspages1 = pages['data'][1];
	            _this.cmspages2 = pages['data'][2];
	            _this.cmspages3 = pages['data'][3];
	        }, function (err) {
	            console.log(err);
	        });
	        this.pageService.getSettings().subscribe(function (settings) {
	            _this.settings = settings;
	        }, function (err) {
	            console.log(err);
	        });
	    };
	    FooterComponent.prototype.isLoggedIn = function () {
	        return this.authenticationService.isLoggedIn();
	    };
	    FooterComponent.prototype.isDisabled = function (path) {
	        return this.location.path().indexOf(path) > -1;
	    };
	    FooterComponent.prototype.newslattersaved = function () {
	        var _this = this;
	        global_1.blockUI();
	        this.pageService.newslatterSubscription(this.newslatter)
	            .subscribe(function (result) {
	            if (result['status_code'] === 1) {
	                _this.newslatter = {};
	                jQuery('#newsletter')[0].reset();
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'success');
	            }
	            else {
	                global_1.unblockUI();
	                global_1.toast(result['message'], 'error');
	            }
	        });
	    };
	    return FooterComponent;
	}());
	FooterComponent = __decorate([
	    core_1.Component({
	        selector: 'my-footer',
	        template: __webpack_require__(421)
	    }),
	    __metadata("design:paramtypes", [page_service_1.PageService,
	        common_1.Location,
	        authentication_service_1.AuthenticationService])
	], FooterComponent);
	exports.FooterComponent = FooterComponent;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 421:
/***/ function(module, exports) {

	module.exports = "<!--<footer class=\"footer\">\n    <div class=\"footer-overlay\">\n        <div class=\"container\">\n            <div class=\"row\" *ngIf=\"!isDisabled('/checkout')\">\n                <div class=\"col m3 s12\">\n                    <div class=\"footer-heading\">\n                        <img class=\"img-responsive\" src=\"public/img/footer-logo.png\">             \n                        <span class=\"sapreter\"></span>\n                    </div>\n                    <ul class=\"footer-link\">\n                        <li *ngFor=\"let cmspage1 of cmspages1\"><a [routerLink]=\"['./page',cmspage1['slug']]\">{{cmspage1[\"title\"]}}</a></li>\n\n                                  </ul>\n                </div>\n                <div class=\"col m3 s12\">\n                    <div class=\"footer-heading\">\n                        <h4>WHY BUY FROM US</h4>       \n                        <span class=\"sapreter\"></span>\n                    </div>\n                    <ul class=\"footer-link\">\n                        <li *ngFor=\"let cmspage2 of cmspages2\"><a [routerLink]=\"['./page',cmspage2['slug']]\">{{cmspage2[\"title\"]}}</a></li>\n              \n                    </ul>\n                </div>\n                <div class=\"col m3 s12\">\n                    <div class=\"footer-heading\">\n                        <h4>MY ACCOUNT</h4>       \n                        <span class=\"sapreter\"></span>\n                    </div>   \n                    <ul class=\"footer-link\">\n                        <li *ngIf=\"!isLoggedIn()\"><a href=\"#modal1\">Sign In</a></li>\n                        <li *ngIf=\"isLoggedIn()\"><a [routerLink]=\"['./myaccount', 'profile']\">My Account</a></li>\n\n                        <li><a [routerLink]=\"['./wishlist']\">Wish List</a></li>\n                        <li><a [routerLink]=\"['./cart']\">View Cart</a></li>\n                        <li><a [routerLink]=\"['./myaccount', 'order']\">Track My Order</a></li>\n                                                                   <li><a href=\"#\">Help</a></li> \n                        <li><a [routerLink]=\"['./faq']\">Faq</a></li> \n\n                        <li *ngFor=\"let cmspage3 of cmspages3\"><a [routerLink]=\"['./page',cmspage3['slug']]\">{{cmspage3[\"title\"]}}</a></li>\n                    </ul>\n                    <div class=\"social-box\">\n                        <ul>\n                            <li><a href=\"{{settings['CONFIG_FACEBOOK_LINK']}}\" target=\"_blank\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>\n                            <li><a href=\"{{settings['CONFIG_TWITTER_LINK']}}\" target=\"_blank\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>                                       \n                            <li><a href=\"{{settings['CONFIG_GOOGLE_PLUS_LINK']}}\" target=\"_blank\"><i class=\"fa fa-google-plus\" aria-hidden=\"true\"></i></a></li>\n                            <li><a href=\"{{settings['CONFIG_PINTEREST_LINK']}}\" target=\"_blank\"><i class=\"fa fa-pinterest\" aria-hidden=\"true\"></i></a></li>                                       \n                        </ul>\n                    </div>\n                </div>\n                <div class=\"col m3 s12\">\n                    <div class=\"footer-heading\">\n                        <h4>NEWSLETTER</h4>       \n                        <span class=\"sapreter\"></span>\n                    </div>\n                    <div class=\"news-bx\">\n                    <form  name=\"newsletter\" id=\"newsletters\" (ngSubmit)=\"newsletter.form.valid && newslattersaved()\"  #newsletter=\"ngForm\" novalidate>\n                        <div class=\"input-field\">          \n                            <input id=\"search\" placeholder=\"Your Email Address\" type=\"search\" name='email_news' [(ngModel)]=\"newslatter.email_news\" #email_news=\"ngModel\" required  pattern=\"^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$\">\n                            <button class=\"waves-effect fabivo-btn btn\" type=\"submit\">Subscribe</button>\n                        </div>\n                                  <div *ngIf=\"newsletter.submitted && email_news.errors && email_news.errors.required\" class=\"help-block err-msg\">The email field is required.</div>\n                            <div *ngIf=\"newsletter.submitted && email_news.errors && email_news.errors.pattern\" class=\"help-block err-msg\"> Email is invalid.</div>\n\n                </form>\n                    </div>\n                    <div class=\"store-bx\">\n                        <h4>DOWNLOAD THE APP</h4>\n                        <a class=\"aap-store waves-effect waves-light\" href=\"{{settings['CONFIG_IOS_APP_LINK']}}\" target=\"_blank\"></a>\n                        <a class=\"play-store waves-effect waves-light\" href=\"{{settings['CONFIG_ANDROID_APP_LINK']}}\" target=\"_blank\"></a>               </div>\n                </div>   \n\n                <div class=\"col m12 s12 footer-mobile center-align\">\n                    <ul>\n                        <li><a [routerLink]=\"['./faq']\">Faq</a></li>\n                        <li><a [routerLink]=\"['./page','shipping-return']\">Shipping & Return</a></li>\n                        <li><a [routerLink]=\"['./page','help']\">Help</a></li>\n                        <li><a [routerLink]=\"['./page','privacy-policy']\">Privacy Policy</a></li>\n                    </ul>\n                </div> \n\n                <div class=\"col m12 s12\">\n                    <span class=\"footer-sapreter\"></span>\n                </div> \n            </div>\n            <div class=\"footer-copyright\">\n                <div class=\"row\">\n                    <div class=\"copyright\">\n                        <div class=\"col m6 s12 col-1\">\n                            <a [routerLink]=\"['/']\" [innerHTML]=\"settings['CONFIG_COPYRIGHT']\"></a>\n                        </div>\n                        <div class=\"col m6  s12 right col-2\">\n                            <ul>\n                                <li><a class=\"card-n card-1\" href=\"javascript:void(0);\"></a></li>\n                                <li><a class=\"card-n card-2\" href=\"javascript:void(0);\"></a></li>\n                                <li><a class=\"card-n card-3\" href=\"javascript:void(0);\"></a></li>\n                                <li><a class=\"card-n card-4\" href=\"javascript:void(0);\"></a></li>\n                            </ul>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>    \n</footer>-->\n\n\n<footer class=\"footer\">\n  <div class=\"footer-overlay\">\n    <div class=\"container\">\n      <div class=\"row\" *ngIf=\"!isDisabled('/checkout')\">\n           \n        <div class=\"col m3 s12\">\n          <div class=\"footer-heading\"> \n                <img class=\"img-responsive\" src=\"public/img/logo_footer.png\" alt=\"\"> \n                <span class=\"sapreter\"></span>\n                <ul class=\"footer-link\">\n                    <li *ngFor=\"let cmspage2 of cmspages2\"><a [routerLink]=\"['./page',cmspage2['slug']]\">{{cmspage2[\"title\"]}}</a></li>\n                </ul>\n          </div>\n          <div class=\"social-box\">\n            <ul>\n                  <li><a href=\"{{settings['CONFIG_FACEBOOK_LINK']}}\" target=\"_blank\"><i class=\"fa fa-facebook\" aria-hidden=\"true\"></i></a></li>\n                  <li><a href=\"{{settings['CONFIG_TWITTER_LINK']}}\" target=\"_blank\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a></li>                                       \n                  <li><a href=\"{{settings['CONFIG_GOOGLE_PLUS_LINK']}}\" target=\"_blank\"><i class=\"fa fa-google-plus\" aria-hidden=\"true\"></i></a></li>\n                  <li><a href=\"{{settings['CONFIG_PINTEREST_LINK']}}\" target=\"_blank\"><i class=\"fa fa-pinterest\" aria-hidden=\"true\"></i></a></li>                                       \n              </ul>\n          </div>\n        </div>\n           \n        <div class=\"col m3 s12 foot-link\">\n          <div class=\"footer-heading\">\n            <h4>MY ACOUNT</h4>\n            <span class=\"sapreter\"></span> </div>\n            <ul class=\"footer-link\">\n                <li *ngIf=\"!isLoggedIn()\"><a href=\"#modal1\">Sign In</a></li>\n                <li *ngIf=\"isLoggedIn()\"><a [routerLink]=\"['./myaccount', 'profile']\">My Account</a></li>\n\n                <li><a [routerLink]=\"['./wishlist']\">Wish List</a></li>\n                <li><a [routerLink]=\"['./cart']\">View Cart</a></li>\n                <li><a [routerLink]=\"['./myaccount', 'order']\">Track My Order</a></li>\n                <li><a href=\"#\">Help</a></li> \n                <li><a [routerLink]=\"['./faq']\">Faq</a></li> \n\n                <li *ngFor=\"let cmspage3 of cmspages3\"><a [routerLink]=\"['./page',cmspage3['slug']]\">{{cmspage3[\"title\"]}}</a></li>\n            </ul>\n        </div>\n           \n        <div class=\"col m3 s12 foot-link\">\n          <div class=\"footer-heading\">\n            <h4>INFORMATION</h4>\n            <span class=\"sapreter\"></span> </div>\n            <ul class=\"footer-link\">\n                <li *ngFor=\"let cmspage1 of cmspages1\"><a [routerLink]=\"['./page',cmspage1['slug']]\">{{cmspage1[\"title\"]}}</a></li>\n            </ul>\n        </div>\n           \n        <div class=\"col m3 s12 foot-link\">\n          <div class=\"footer-heading\">\n            <h4>CONTACT US</h4>\n            <span class=\"sapreter\"></span> </div>\n          <div class=\"addrs\">\n            <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae interdum orci, at mattis erat.</p>-->\n            <ul>\n                <li><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i> <span [innerHTML]=\"settings['CONFIG_ADDRESS']\"></span></li>\n              <li><i class=\"fa fa-envelope\" aria-hidden=\"true\"></i> <span [innerHTML]=\"settings['CONFIG_SUPPORT_MAIL']\"></span></li>\n              <li><i class=\"fa fa-phone\" aria-hidden=\"true\"></i> <span [innerHTML]=\"settings['CONFIG_PHONE_NUMBER']\"></span></li>\n            </ul>\n          </div>\n        </div>\n           \n      </div>\n    </div>\n    <div class=\"footer-copyright\">\n      <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"copyright\">\n            <div class=\"col m6 s12 col-1\"> \n                <a [routerLink]=\"['/']\" [innerHTML]=\"settings['CONFIG_COPYRIGHT']\"></a> \n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</footer>"

/***/ },

/***/ 422:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(3);
	var facebookParams_1 = __webpack_require__(423);
	var twitterParams_1 = __webpack_require__(424);
	var googlePlusParams_1 = __webpack_require__(425);
	var pinterestParams_1 = __webpack_require__(426);
	var linkedinParams_1 = __webpack_require__(427);
	var CeiboShare = (function () {
	    function CeiboShare() {
	        this.sharers = {
	            facebook: {
	                shareUrl: 'https://www.facebook.com/sharer/sharer.php',
	            },
	            googleplus: {
	                shareUrl: 'https://plus.google.com/share',
	            },
	            linkedin: {
	                shareUrl: 'https://www.linkedin.com/shareArticle',
	            },
	            twitter: {
	                shareUrl: 'https://twitter.com/intent/tweet/',
	            },
	            email: {
	                //shareUrl: 'mailto:' + this.to,
	                /*params: {
	                    subject: this.subject,
	                    body: this.title + '\n' + this.url
	                },*/
	                isLink: true
	            },
	            whatsapp: {
	                shareUrl: 'whatsapp://send',
	                /*params: {
	                    text: this.title + ' ' + this.url
	                },*/
	                isLink: true
	            },
	            telegram: {
	                shareUrl: 'tg://msg_url',
	                /*params: {
	                    text: this.title + ' ' + this.url
	                },*/
	                isLink: true
	            },
	            viber: {
	                shareUrl: 'viber://forward',
	                /*params: {
	                    text: this.title + ' ' + this.url
	                },*/
	                isLink: true
	            },
	            line: {
	                //shareUrl: 'http://line.me/R/msg/text/?' + encodeURIComponent(this.title + ' ' + this.url),
	                isLink: true
	            },
	            pinterest: {
	                shareUrl: 'https://www.pinterest.com/pin/create/button/',
	            },
	            tumblr: {
	                shareUrl: 'http://tumblr.com/widgets/share/tool',
	            },
	            hackernews: {
	                shareUrl: 'https://news.ycombinator.com/submitlink',
	            },
	            reddit: {
	                shareUrl: 'https://www.reddit.com/submit',
	            },
	            vk: {
	                shareUrl: 'http://vk.com/share.php',
	            },
	            xing: {
	                shareUrl: 'https://www.xing.com/app/user',
	            },
	            buffer: {
	                shareUrl: 'https://buffer.com/add',
	            },
	            instapaper: {
	                shareUrl: 'http://www.instapaper.com/edit',
	            },
	            pocket: {
	                shareUrl: 'https://getpocket.com/save',
	            },
	            digg: {
	                shareUrl: 'http://www.digg.com/submit',
	            },
	            stumbleupon: {
	                shareUrl: 'http://www.stumbleupon.com/submit',
	            },
	            flipboard: {
	                shareUrl: 'https://share.flipboard.com/bookmarklet/popout',
	            },
	            /*weibo: {
	                shareUrl: 'http://service.weibo.com/share/share.php',
	                params: {
	                    url: this.url,
	                    title: this.title,
	                    pic: this.image,
	                    appkey: this.getValue('appkey'),
	                    ralateUid: this.getValue('ralateuid'),
	                    language: 'zh_cn'
	                }
	            },*/
	            renren: {
	                shareUrl: 'http://share.renren.com/share/buttonshare',
	            },
	            myspace: {
	                shareUrl: 'https://myspace.com/post',
	            },
	            blogger: {
	                shareUrl: 'https://www.blogger.com/blog-this.g',
	            },
	            baidu: {
	                shareUrl: 'http://cang.baidu.com/do/add',
	            },
	            douban: {
	                shareUrl: 'https://www.douban.com/share/service',
	            },
	            okru: {
	                shareUrl: 'https://connect.ok.ru/dk',
	            }
	        };
	    }
	    CeiboShare.prototype.urlSharer = function (sharer) {
	        var p = sharer.params || {}, keys = Object.keys(p), i, str = keys.length > 0 ? '?' : '';
	        for (i = 0; i < keys.length; i++) {
	            if (str !== '?') {
	                str += '&';
	            }
	            if (p[keys[i]]) {
	                str += keys[i] + '=' + encodeURIComponent(p[keys[i]]);
	            }
	        }
	        sharer.shareUrl += str;
	        if (!sharer.isLink) {
	            var popWidth = sharer.width || 600, popHeight = sharer.height || 480, left = window.innerWidth / 2 - popWidth / 2 + window.screenX, top = window.innerHeight / 2 - popHeight / 2 + window.screenY, popParams = 'scrollbars=no, width=' + popWidth + ', height=' + popHeight + ', top=' + top + ', left=' + left, newWindow = window.open(sharer.shareUrl, '', popParams);
	            if (window.focus) {
	                newWindow.focus();
	            }
	        }
	        else {
	            window.location.href = sharer.shareUrl;
	        }
	    };
	    CeiboShare.prototype.getSharer = function () {
	        var _sharer = {};
	        if (this.facebook) {
	            _sharer = this.sharers['facebook'];
	            _sharer.params = this.facebook;
	        }
	        if (this.googlePlus) {
	            _sharer = this.sharers['googleplus'];
	            _sharer.params = this.googlePlus;
	        }
	        if (this.twitter) {
	            _sharer = this.sharers['twitter'];
	            _sharer.params = this.twitter;
	        }
	        if (this.pinterest) {
	            _sharer = this.sharers['pinterest'];
	            _sharer.params = this.pinterest;
	        }
	        if (this.linkedIn) {
	            _sharer = this.sharers['linkedin'];
	            _sharer.params = this.linkedIn;
	        }
	        return _sharer;
	    };
	    CeiboShare.prototype.share = function () {
	        var s = this.getSharer();
	        // custom popups sizes
	        if (s) {
	            s.width = this.shareWidth;
	            s.height = this.shareHeight;
	        }
	        return s !== undefined ? this.urlSharer(s) : false;
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', facebookParams_1.FacebookParams)
	    ], CeiboShare.prototype, "facebook", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', twitterParams_1.TwitterParams)
	    ], CeiboShare.prototype, "twitter", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', googlePlusParams_1.GooglePlusParams)
	    ], CeiboShare.prototype, "googlePlus", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', pinterestParams_1.PinterestParams)
	    ], CeiboShare.prototype, "pinterest", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', linkedinParams_1.LinkedinParams)
	    ], CeiboShare.prototype, "linkedIn", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', String)
	    ], CeiboShare.prototype, "shareWidth", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', String)
	    ], CeiboShare.prototype, "shareHeight", void 0);
	    __decorate([
	        core_1.HostListener('click'), 
	        __metadata('design:type', Function), 
	        __metadata('design:paramtypes', []), 
	        __metadata('design:returntype', void 0)
	    ], CeiboShare.prototype, "share", null);
	    CeiboShare = __decorate([
	        core_1.Directive({
	            selector: '[ceiboShare]'
	        }), 
	        __metadata('design:paramtypes', [])
	    ], CeiboShare);
	    return CeiboShare;
	}());
	exports.CeiboShare = CeiboShare;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmcyLXNvY2lhbC1zaGFyZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm5nMi1zb2NpYWwtc2hhcmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLHFCQUErQyxlQUFlLENBQUMsQ0FBQTtBQUMvRCwrQkFBK0Isa0JBQy9CLENBQUMsQ0FEZ0Q7QUFDakQsOEJBQThCLGlCQUFpQixDQUFDLENBQUE7QUFDaEQsaUNBQWtDLG9CQUFvQixDQUFDLENBQUE7QUFDdkQsZ0NBQWdDLG1CQUFtQixDQUFDLENBQUE7QUFDcEQsK0JBQStCLGtCQUFrQixDQUFDLENBQUE7QUFLbEQ7SUEwTmtCO1FBaE5SLFlBQU8sR0FBRztZQUNBLFFBQVEsRUFBRTtnQkFDTixRQUFRLEVBQUUsNENBQTRDO2FBRXpEO1lBQ0QsVUFBVSxFQUFFO2dCQUNSLFFBQVEsRUFBRSwrQkFBK0I7YUFFNUM7WUFDRCxRQUFRLEVBQUU7Z0JBQ04sUUFBUSxFQUFFLHVDQUF1QzthQUtwRDtZQUNELE9BQU8sRUFBRTtnQkFDTCxRQUFRLEVBQUUsbUNBQW1DO2FBT2hEO1lBQ0QsS0FBSyxFQUFFO2dCQUNILGdDQUFnQztnQkFDaEM7OztvQkFHSTtnQkFDSixNQUFNLEVBQUUsSUFBSTthQUNmO1lBQ0QsUUFBUSxFQUFFO2dCQUNOLFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCOztvQkFFSTtnQkFDSixNQUFNLEVBQUUsSUFBSTthQUNmO1lBQ0QsUUFBUSxFQUFFO2dCQUNOLFFBQVEsRUFBRSxjQUFjO2dCQUN4Qjs7b0JBRUk7Z0JBQ0osTUFBTSxFQUFFLElBQUk7YUFDZjtZQUNELEtBQUssRUFBRTtnQkFDSCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQjs7b0JBRUk7Z0JBQ0osTUFBTSxFQUFFLElBQUk7YUFDZjtZQUNELElBQUksRUFBRTtnQkFDRiw0RkFBNEY7Z0JBQzVGLE1BQU0sRUFBRSxJQUFJO2FBQ2Y7WUFDRCxTQUFTLEVBQUU7Z0JBQ1AsUUFBUSxFQUFFLDhDQUE4QzthQU0zRDtZQUNELE1BQU0sRUFBRTtnQkFDSixRQUFRLEVBQUUsc0NBQXNDO2FBU25EO1lBQ0QsVUFBVSxFQUFFO2dCQUNSLFFBQVEsRUFBRSx5Q0FBeUM7YUFLdEQ7WUFDRCxNQUFNLEVBQUU7Z0JBQ0osUUFBUSxFQUFFLCtCQUErQjthQUU1QztZQUNELEVBQUUsRUFBRTtnQkFDQSxRQUFRLEVBQUUseUJBQXlCO2FBT3RDO1lBQ0QsSUFBSSxFQUFFO2dCQUNGLFFBQVEsRUFBRSwrQkFBK0I7YUFNNUM7WUFDRCxNQUFNLEVBQUU7Z0JBQ0osUUFBUSxFQUFFLHdCQUF3QjthQU9yQztZQUNELFVBQVUsRUFBRTtnQkFDUixRQUFRLEVBQUUsZ0NBQWdDO2FBTTdDO1lBQ0QsTUFBTSxFQUFFO2dCQUNKLFFBQVEsRUFBRSw0QkFBNEI7YUFJekM7WUFDRCxJQUFJLEVBQUU7Z0JBQ0YsUUFBUSxFQUFFLDRCQUE0QjthQUl6QztZQUNELFdBQVcsRUFBRTtnQkFDVCxRQUFRLEVBQUUsbUNBQW1DO2FBS2hEO1lBQ0QsU0FBUyxFQUFFO2dCQUNQLFFBQVEsRUFBRSxnREFBZ0Q7YUFPN0Q7WUFDRDs7Ozs7Ozs7OztnQkFVSTtZQUNKLE1BQU0sRUFBRTtnQkFDSixRQUFRLEVBQUUsMkNBQTJDO2FBSXhEO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFFBQVEsRUFBRSwwQkFBMEI7YUFNdkM7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsUUFBUSxFQUFFLHFDQUFxQzthQU1sRDtZQUNELEtBQUssRUFBRTtnQkFDSCxRQUFRLEVBQUUsOEJBQThCO2FBSzNDO1lBQ0QsTUFBTSxFQUFFO2dCQUNKLFFBQVEsRUFBRSxzQ0FBc0M7YUFNbkQ7WUFDRCxJQUFJLEVBQUU7Z0JBQ0YsUUFBUSxFQUFFLDBCQUEwQjthQU12QztTQUNKLENBQUE7SUFFZ0IsQ0FBQztJQUd4Qiw4QkFBUyxHQUFqQixVQUFrQixNQUFXO1FBQ25CLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksRUFBRSxFQUN2QixJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFDckIsQ0FBTSxFQUNOLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ3JDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUMvQixFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDZCxHQUFHLElBQUksR0FBRyxDQUFDO1lBQ2YsQ0FBQztZQUNELEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUQsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsUUFBUSxJQUFJLEdBQUcsQ0FBQztRQUV2QixFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pCLElBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxLQUFLLElBQUksR0FBRyxFQUM5QixTQUFTLEdBQUcsTUFBTSxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQ2hDLElBQUksR0FBRyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsR0FBRyxRQUFRLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxPQUFPLEVBQzVELEdBQUcsR0FBRyxNQUFNLENBQUMsV0FBVyxHQUFHLENBQUMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxPQUFPLEVBQzdELFNBQVMsR0FBRyx1QkFBdUIsR0FBRyxRQUFRLEdBQUcsV0FBVyxHQUFHLFNBQVMsR0FBRyxRQUFRLEdBQUcsR0FBRyxHQUFHLFNBQVMsR0FBRyxJQUFJLEVBQzVHLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBRTVELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNmLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN0QixDQUFDO1FBQ0wsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUMzQyxDQUFDO0lBQ0wsQ0FBQztJQUdELDhCQUFTLEdBQWpCO1FBQ0ksSUFBSSxPQUFPLEdBQVEsRUFBRSxDQUFDO1FBQ3RCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQSxDQUFDO1lBQ2QsT0FBTyxHQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUE7WUFDakMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFBO1FBQ2xDLENBQUM7UUFDRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUEsQ0FBQztZQUNoQixPQUFPLEdBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUNuQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUE7UUFDcEMsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQSxDQUFDO1lBQ2IsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbEMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ2xDLENBQUM7UUFFRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNoQixPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNwQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDcEMsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQSxDQUFDO1lBQ2QsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDbkMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ25DLENBQUM7UUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDO0lBRW5CLENBQUM7SUFFd0IsMEJBQUssR0FBTDtRQUdiLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtRQUN4QixzQkFBc0I7UUFDdEIsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNKLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUMxQixDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUE7UUFDL0IsQ0FBQztRQUNELE1BQU0sQ0FBQyxDQUFDLEtBQUssU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBRTdELENBQUM7SUFyU0M7UUFBQyxZQUFLLEVBQUU7O2dEQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7OytDQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O2tEQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O2lEQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O2dEQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O2tEQUFBO0lBQ1I7UUFBQyxZQUFLLEVBQUU7O21EQUFBO0lBb1JWO1FBQUMsbUJBQVksQ0FBQyxPQUFPLENBQUM7Ozs7MkNBQUE7SUE5UnhCO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxjQUFjO1NBQ3pCLENBQUM7O2tCQUFBO0lBMlNGLGlCQUFDO0FBQUQsQ0FBQyxBQTFTRCxJQTBTQztBQTFTWSxrQkFBVSxhQTBTdEIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRmFjZWJvb2tQYXJhbXMgfSBmcm9tICcuL2ZhY2Vib29rUGFyYW1zJ1xuaW1wb3J0IHsgVHdpdHRlclBhcmFtcyB9IGZyb20gJy4vdHdpdHRlclBhcmFtcyc7XG5pbXBvcnQgeyAgR29vZ2xlUGx1c1BhcmFtcyB9IGZyb20gJy4vZ29vZ2xlUGx1c1BhcmFtcyc7XG5pbXBvcnQgeyBQaW50ZXJlc3RQYXJhbXMgfSBmcm9tICcuL3BpbnRlcmVzdFBhcmFtcyc7XG5pbXBvcnQgeyBMaW5rZWRpblBhcmFtcyB9IGZyb20gJy4vbGlua2VkaW5QYXJhbXMnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbY2VpYm9TaGFyZV0nXG59KVxuZXhwb3J0IGNsYXNzIENlaWJvU2hhcmUge1xuICAgIEBJbnB1dCgpIGZhY2Vib29rIDogRmFjZWJvb2tQYXJhbXM7XG4gICAgQElucHV0KCkgdHdpdHRlciA6IFR3aXR0ZXJQYXJhbXM7XG4gICAgQElucHV0KCkgZ29vZ2xlUGx1cyA6IEdvb2dsZVBsdXNQYXJhbXM7XG4gICAgQElucHV0KCkgcGludGVyZXN0IDogUGludGVyZXN0UGFyYW1zO1xuICAgIEBJbnB1dCgpIGxpbmtlZEluIDogTGlua2VkaW5QYXJhbXM7XG4gICAgQElucHV0KCkgc2hhcmVXaWR0aDogc3RyaW5nO1xuICAgIEBJbnB1dCgpIHNoYXJlSGVpZ2h0OiBzdHJpbmc7XG5cblxuICBwcml2YXRlIHNoYXJlcnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIGZhY2Vib29rOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS9zaGFyZXIvc2hhcmVyLnBocCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAvL3BhcmFtczoge3U6IHRoaXMudXJsfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBnb29nbGVwbHVzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vcGx1cy5nb29nbGUuY29tL3NoYXJlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vcGFyYW1zOiB7dXJsOiB0aGlzLnVybH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgbGlua2VkaW46IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnaHR0cHM6Ly93d3cubGlua2VkaW4uY29tL3NoYXJlQXJ0aWNsZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAvKnBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogdGhpcy51cmwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWluaTogdHJ1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHR3aXR0ZXI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnaHR0cHM6Ly90d2l0dGVyLmNvbS9pbnRlbnQvdHdlZXQvJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50aXRsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc2h0YWdzOiB0aGlzLmhhc2h0YWdzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpYTogdGhpcy52aWFcbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBlbWFpbDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9zaGFyZVVybDogJ21haWx0bzonICsgdGhpcy50byxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3ViamVjdDogdGhpcy5zdWJqZWN0LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvZHk6IHRoaXMudGl0bGUgKyAnXFxuJyArIHRoaXMudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCovXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0xpbms6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgd2hhdHNhcHA6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnd2hhdHNhcHA6Ly9zZW5kJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50aXRsZSArICcgJyArIHRoaXMudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCovXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0xpbms6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgdGVsZWdyYW06IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAndGc6Ly9tc2dfdXJsJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50aXRsZSArICcgJyArIHRoaXMudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCovXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0xpbms6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgdmliZXI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAndmliZXI6Ly9mb3J3YXJkJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGV4dDogdGhpcy50aXRsZSArICcgJyArIHRoaXMudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICB9LCovXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0xpbms6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgbGluZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9zaGFyZVVybDogJ2h0dHA6Ly9saW5lLm1lL1IvbXNnL3RleHQvPycgKyBlbmNvZGVVUklDb21wb25lbnQodGhpcy50aXRsZSArICcgJyArIHRoaXMudXJsKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzTGluazogdHJ1ZVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBwaW50ZXJlc3Q6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnaHR0cHM6Ly93d3cucGludGVyZXN0LmNvbS9waW4vY3JlYXRlL2J1dHRvbi8nLFxuICAgICAgICAgICAgICAgICAgICAgICAgLypwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lZGlhOiB0aGlzLmltYWdlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiB0aGlzLmRlc2NyaXB0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgdHVtYmxyOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHA6Ly90dW1ibHIuY29tL3dpZGdldHMvc2hhcmUvdG9vbCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAvKnBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbm9uaWNhbFVybDogdGhpcy51cmwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudDogdGhpcy51cmwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zdHR5cGU6ICdsaW5rJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogdGhpcy50aXRsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXB0aW9uOiB0aGlzLmNhcHRpb24sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFnczogdGhpcy50YWdzXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgaGFja2VybmV3czoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2hhcmVVcmw6ICdodHRwczovL25ld3MueWNvbWJpbmF0b3IuY29tL3N1Ym1pdGxpbmsnLFxuICAgICAgICAgICAgICAgICAgICAgICAgLypwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1OiB0aGlzLnVybCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0OiB0aGlzLnRpdGxlXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcmVkZGl0OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vd3d3LnJlZGRpdC5jb20vc3VibWl0JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vcGFyYW1zOiB7J3VybCc6IHRoaXMudXJsfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICB2azoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2hhcmVVcmw6ICdodHRwOi8vdmsuY29tL3NoYXJlLnBocCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAvKnBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogdGhpcy51cmwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IHRoaXMudGl0bGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IHRoaXMuY2FwdGlvbixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbWFnZTogdGhpcy5pbWFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHhpbmc6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnaHR0cHM6Ly93d3cueGluZy5jb20vYXBwL3VzZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgLypwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnb3AnOiAnc2hhcmUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICd1cmwnOiB0aGlzLnVybCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAndGl0bGUnOiB0aGlzLnRpdGxlXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgYnVmZmVyOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vYnVmZmVyLmNvbS9hZGQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgLypwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0aGlzLnRpdGxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpYTogdGhpcy52aWEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGljdHVyZTogdGhpcy5waWN0dXJlXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgaW5zdGFwYXBlcjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2hhcmVVcmw6ICdodHRwOi8vd3d3Lmluc3RhcGFwZXIuY29tL2VkaXQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgLypwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0aGlzLnRpdGxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiB0aGlzLmRlc2NyaXB0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcG9ja2V0OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vZ2V0cG9ja2V0LmNvbS9zYXZlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiB0aGlzLnVybFxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGRpZ2c6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnaHR0cDovL3d3dy5kaWdnLmNvbS9zdWJtaXQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgLypwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgc3R1bWJsZXVwb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnaHR0cDovL3d3dy5zdHVtYmxldXBvbi5jb20vc3VibWl0JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiB0aGlzLnVybCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogdGhpcy50aXRsZVxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGZsaXBib2FyZDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2hhcmVVcmw6ICdodHRwczovL3NoYXJlLmZsaXBib2FyZC5jb20vYm9va21hcmtsZXQvcG9wb3V0JyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdjogMixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogdGhpcy50aXRsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHRoaXMudXJsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHQ6IERhdGUubm93KClcbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAvKndlaWJvOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHA6Ly9zZXJ2aWNlLndlaWJvLmNvbS9zaGFyZS9zaGFyZS5waHAnLFxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiB0aGlzLnVybCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogdGhpcy50aXRsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwaWM6IHRoaXMuaW1hZ2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBwa2V5OiB0aGlzLmdldFZhbHVlKCdhcHBrZXknKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByYWxhdGVVaWQ6IHRoaXMuZ2V0VmFsdWUoJ3JhbGF0ZXVpZCcpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhbmd1YWdlOiAnemhfY24nXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sKi9cbiAgICAgICAgICAgICAgICAgICAgcmVucmVuOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHA6Ly9zaGFyZS5yZW5yZW4uY29tL3NoYXJlL2J1dHRvbnNoYXJlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGluazogdGhpcy51cmxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBteXNwYWNlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vbXlzcGFjZS5jb20vcG9zdCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAvKnBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHU6IHRoaXMudXJsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHQ6IHRoaXMudGl0bGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYzogdGhpcy5kZXNjcmlwdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGJsb2dnZXI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNoYXJlVXJsOiAnaHR0cHM6Ly93d3cuYmxvZ2dlci5jb20vYmxvZy10aGlzLmcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgLypwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1OiB0aGlzLnVybCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuOiB0aGlzLnRpdGxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHQ6IHRoaXMuZGVzY3JpcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBiYWlkdToge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2hhcmVVcmw6ICdodHRwOi8vY2FuZy5iYWlkdS5jb20vZG8vYWRkJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qcGFyYW1zOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXQ6IHRoaXMudGl0bGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXU6IHRoaXMudXJsXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgZG91YmFuOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vd3d3LmRvdWJhbi5jb20vc2hhcmUvc2VydmljZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAvKnBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHRoaXMudGl0bGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaHJlZjogdGhpcy51cmwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2U6IHRoaXMuaW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBva3J1OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzaGFyZVVybDogJ2h0dHBzOi8vY29ubmVjdC5vay5ydS9kaycsXG4gICAgICAgICAgICAgICAgICAgICAgICAvKnBhcmFtczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdzdC5jbWQnOiAnV2lkZ2V0U2hhcmVQcmV2aWV3JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnc3Quc2hhcmVVcmwnOiB0aGlzLnVybCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAndGl0bGUnOiB0aGlzLnRpdGxlXG4gICAgICAgICAgICAgICAgICAgICAgICB9Ki9cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgY29uc3RydWN0b3IoKSB7fVxuXG5cbiAgcHJpdmF0ZSB1cmxTaGFyZXIoc2hhcmVyOiBhbnkpIHtcbiAgICAgICAgICAgIHZhciBwID0gc2hhcmVyLnBhcmFtcyB8fCB7fSxcbiAgICAgICAgICAgICAgICBrZXlzID0gT2JqZWN0LmtleXMocCksXG4gICAgICAgICAgICAgICAgaTogYW55LFxuICAgICAgICAgICAgICAgIHN0ciA9IGtleXMubGVuZ3RoID4gMCA/ICc/JyA6ICcnO1xuICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGtleXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBpZiAoc3RyICE9PSAnPycpIHtcbiAgICAgICAgICAgICAgICAgICAgc3RyICs9ICcmJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKHBba2V5c1tpXV0pIHtcbiAgICAgICAgICAgICAgICAgICAgc3RyICs9IGtleXNbaV0gKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQocFtrZXlzW2ldXSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2hhcmVyLnNoYXJlVXJsICs9IHN0cjtcblxuICAgICAgICAgICAgaWYgKCFzaGFyZXIuaXNMaW5rKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBvcFdpZHRoID0gc2hhcmVyLndpZHRoIHx8IDYwMCxcbiAgICAgICAgICAgICAgICAgICAgcG9wSGVpZ2h0ID0gc2hhcmVyLmhlaWdodCB8fCA0ODAsXG4gICAgICAgICAgICAgICAgICAgIGxlZnQgPSB3aW5kb3cuaW5uZXJXaWR0aCAvIDIgLSBwb3BXaWR0aCAvIDIgKyB3aW5kb3cuc2NyZWVuWCxcbiAgICAgICAgICAgICAgICAgICAgdG9wID0gd2luZG93LmlubmVySGVpZ2h0IC8gMiAtIHBvcEhlaWdodCAvIDIgKyB3aW5kb3cuc2NyZWVuWSxcbiAgICAgICAgICAgICAgICAgICAgcG9wUGFyYW1zID0gJ3Njcm9sbGJhcnM9bm8sIHdpZHRoPScgKyBwb3BXaWR0aCArICcsIGhlaWdodD0nICsgcG9wSGVpZ2h0ICsgJywgdG9wPScgKyB0b3AgKyAnLCBsZWZ0PScgKyBsZWZ0LFxuICAgICAgICAgICAgICAgICAgICBuZXdXaW5kb3cgPSB3aW5kb3cub3BlbihzaGFyZXIuc2hhcmVVcmwsICcnLCBwb3BQYXJhbXMpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHdpbmRvdy5mb2N1cykge1xuICAgICAgICAgICAgICAgICAgICBuZXdXaW5kb3cuZm9jdXMoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gc2hhcmVyLnNoYXJlVXJsO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cblxucHJpdmF0ZSBnZXRTaGFyZXIoKXtcbiAgICB2YXIgX3NoYXJlcjogYW55ID0ge307XG4gICAgaWYodGhpcy5mYWNlYm9vayl7XG4gICAgICAgIF9zaGFyZXI9IHRoaXMuc2hhcmVyc1snZmFjZWJvb2snXVxuICAgICAgICBfc2hhcmVyLnBhcmFtcyA9IHRoaXMuZmFjZWJvb2tcbiAgICB9XG4gICAgaWYodGhpcy5nb29nbGVQbHVzKXtcbiAgICAgICAgX3NoYXJlcj0gdGhpcy5zaGFyZXJzWydnb29nbGVwbHVzJ11cbiAgICAgICAgX3NoYXJlci5wYXJhbXMgPSB0aGlzLmdvb2dsZVBsdXNcbiAgICB9XG5cbiAgICBpZih0aGlzLnR3aXR0ZXIpe1xuICAgICAgICBfc2hhcmVyID0gdGhpcy5zaGFyZXJzWyd0d2l0dGVyJ107XG4gICAgICAgIF9zaGFyZXIucGFyYW1zID0gdGhpcy50d2l0dGVyO1xuICAgIH1cblxuICAgIGlmKHRoaXMucGludGVyZXN0KSB7XG4gICAgICAgIF9zaGFyZXIgPSB0aGlzLnNoYXJlcnNbJ3BpbnRlcmVzdCddO1xuICAgICAgICBfc2hhcmVyLnBhcmFtcyA9IHRoaXMucGludGVyZXN0O1xuICAgIH1cblxuICAgIGlmKHRoaXMubGlua2VkSW4pe1xuICAgICAgICBfc2hhcmVyID0gdGhpcy5zaGFyZXJzWydsaW5rZWRpbiddO1xuICAgICAgICBfc2hhcmVyLnBhcmFtcyA9IHRoaXMubGlua2VkSW47XG4gICAgfVxuXG4gICAgcmV0dXJuIF9zaGFyZXI7XG5cbn1cblxuICBASG9zdExpc3RlbmVyKCdjbGljaycpIHNoYXJlKCl7XG4gICAgICAgIFxuXG4gICAgICAgICAgICB2YXIgcyA9IHRoaXMuZ2V0U2hhcmVyKClcbiAgICAgICAgICAgIC8vIGN1c3RvbSBwb3B1cHMgc2l6ZXNcbiAgICAgICAgICAgIGlmIChzKSB7XG4gICAgICAgICAgICAgICAgcy53aWR0aCA9IHRoaXMuc2hhcmVXaWR0aDtcbiAgICAgICAgICAgICAgICBzLmhlaWdodCA9IHRoaXMuc2hhcmVIZWlnaHRcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBzICE9PSB1bmRlZmluZWQgPyB0aGlzLnVybFNoYXJlcihzKSA6IGZhbHNlO1xuXG4gIH1cblxuICAgXG5cbn1cbiJdfQ==

/***/ },

/***/ 423:
/***/ function(module, exports) {

	"use strict";
	var FacebookParams = (function () {
	    function FacebookParams() {
	    }
	    return FacebookParams;
	}());
	exports.FacebookParams = FacebookParams;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZWJvb2tQYXJhbXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmYWNlYm9va1BhcmFtcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFBQTtJQUVBLENBQUM7SUFBRCxxQkFBQztBQUFELENBQUMsQUFGRCxJQUVDO0FBRlksc0JBQWMsaUJBRTFCLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgRmFjZWJvb2tQYXJhbXMge1xuICAgIHU6IHN0cmluZ1xufSJdfQ==

/***/ },

/***/ 424:
/***/ function(module, exports) {

	"use strict";
	var TwitterParams = (function () {
	    function TwitterParams() {
	    }
	    return TwitterParams;
	}());
	exports.TwitterParams = TwitterParams;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHdpdHRlclBhcmFtcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInR3aXR0ZXJQYXJhbXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0lBQUE7SUFLQSxDQUFDO0lBQUQsb0JBQUM7QUFBRCxDQUFDLEFBTEQsSUFLQztBQUxZLHFCQUFhLGdCQUt6QixDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFR3aXR0ZXJQYXJhbXMge1xuICAgIHRleHQ6IHN0cmluZztcbiAgICB1cmw6IHN0cmluZztcbiAgICBoYXNodGFnczogc3RyaW5nO1xuICAgIHZpYTogc3RyaW5nO1xufSJdfQ==

/***/ },

/***/ 425:
/***/ function(module, exports) {

	"use strict";
	var GooglePlusParams = (function () {
	    function GooglePlusParams() {
	    }
	    return GooglePlusParams;
	}());
	exports.GooglePlusParams = GooglePlusParams;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlUGx1c1BhcmFtcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImdvb2dsZVBsdXNQYXJhbXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0lBQUE7SUFJQSxDQUFDO0lBQUQsdUJBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQztBQUpZLHdCQUFnQixtQkFJNUIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBHb29nbGVQbHVzUGFyYW1zIHtcbiAgICBcbiAgICB1cmw6IHN0cmluZ1xuXG59Il19

/***/ },

/***/ 426:
/***/ function(module, exports) {

	"use strict";
	var PinterestParams = (function () {
	    function PinterestParams() {
	    }
	    return PinterestParams;
	}());
	exports.PinterestParams = PinterestParams;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGludGVyZXN0UGFyYW1zLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGludGVyZXN0UGFyYW1zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtJQUFBO0lBT0EsQ0FBQztJQUFELHNCQUFDO0FBQUQsQ0FBQyxBQVBELElBT0M7QUFKSyx1QkFBZSxrQkFJcEIsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAvKipcbiAqIFBpbnRlcmVzdFBhcmFtc1xuICovXG5jbGFzcyBQaW50ZXJlc3RQYXJhbXMge1xuICAgIHVybDogc3RyaW5nO1xuICAgIG1lZGlhOiBzdHJpbmc7XG4gICAgZGVzY3JpcHRpb246IHN0cmluZztcbn0iXX0=

/***/ },

/***/ 427:
/***/ function(module, exports) {

	"use strict";
	var LinkedinParams = (function () {
	    function LinkedinParams() {
	    }
	    return LinkedinParams;
	}());
	exports.LinkedinParams = LinkedinParams;
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGlua2VkaW5QYXJhbXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsaW5rZWRpblBhcmFtcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7SUFBQTtJQUVBLENBQUM7SUFBRCxxQkFBQztBQUFELENBQUMsQUFGRCxJQUVDO0FBRlksc0JBQWMsaUJBRTFCLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgTGlua2VkaW5QYXJhbXMge1xuICAgIHVybDpzdHJpbmdcbn0iXX0=

/***/ },

/***/ 428:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var platform_browser_1 = __webpack_require__(21);
	var KeysPipe = (function () {
	    function KeysPipe() {
	    }
	    KeysPipe.prototype.transform = function (value, args) {
	        var keys = [];
	        for (var key in value) {
	            keys.push({ key: key, value: value[key] });
	        }
	        return keys;
	    };
	    return KeysPipe;
	}());
	KeysPipe = __decorate([
	    core_1.Pipe({ name: 'keys' }),
	    __metadata("design:paramtypes", [])
	], KeysPipe);
	exports.KeysPipe = KeysPipe;
	var ValuesPipe = (function () {
	    function ValuesPipe() {
	    }
	    ValuesPipe.prototype.transform = function (value, args) {
	        var values = [];
	        for (var key in value) {
	            values.push(value[key]);
	        }
	        return values;
	    };
	    return ValuesPipe;
	}());
	ValuesPipe = __decorate([
	    core_1.Pipe({ name: 'values' }),
	    __metadata("design:paramtypes", [])
	], ValuesPipe);
	exports.ValuesPipe = ValuesPipe;
	var FlattenPipe = (function () {
	    function FlattenPipe() {
	    }
	    FlattenPipe.prototype.transform = function (value, args) {
	        return [].concat.apply([], value);
	    };
	    return FlattenPipe;
	}());
	FlattenPipe = __decorate([
	    core_1.Pipe({ name: 'flatten' }),
	    __metadata("design:paramtypes", [])
	], FlattenPipe);
	exports.FlattenPipe = FlattenPipe;
	var CapitalizePipe = (function () {
	    function CapitalizePipe() {
	    }
	    CapitalizePipe.prototype.transform = function (value) {
	        if (value) {
	            return value.charAt(0).toUpperCase() + value.slice(1);
	        }
	        return value;
	    };
	    return CapitalizePipe;
	}());
	CapitalizePipe = __decorate([
	    core_1.Pipe({ name: 'capitalize' }),
	    __metadata("design:paramtypes", [])
	], CapitalizePipe);
	exports.CapitalizePipe = CapitalizePipe;
	var UcupperPipe = (function () {
	    function UcupperPipe() {
	    }
	    UcupperPipe.prototype.transform = function (value) {
	        if (value) {
	            return value.toUpperCase();
	        }
	        return value;
	    };
	    return UcupperPipe;
	}());
	UcupperPipe = __decorate([
	    core_1.Pipe({ name: 'ucupper' }),
	    __metadata("design:paramtypes", [])
	], UcupperPipe);
	exports.UcupperPipe = UcupperPipe;
	var SanitizeHtmlPipe = (function () {
	    function SanitizeHtmlPipe(_sanitizer) {
	        this._sanitizer = _sanitizer;
	    }
	    SanitizeHtmlPipe.prototype.transform = function (v) {
	        return this._sanitizer.bypassSecurityTrustHtml(v);
	    };
	    return SanitizeHtmlPipe;
	}());
	SanitizeHtmlPipe = __decorate([
	    core_1.Pipe({
	        name: 'sanitizeHtml'
	    }),
	    __metadata("design:paramtypes", [platform_browser_1.DomSanitizer])
	], SanitizeHtmlPipe);
	exports.SanitizeHtmlPipe = SanitizeHtmlPipe;
	var SafeUrlPipe = (function () {
	    function SafeUrlPipe(domSanitizer) {
	        this.domSanitizer = domSanitizer;
	    }
	    SafeUrlPipe.prototype.transform = function (url) {
	        return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
	    };
	    return SafeUrlPipe;
	}());
	SafeUrlPipe = __decorate([
	    core_1.Pipe({
	        name: 'safeUrl'
	    }),
	    __metadata("design:paramtypes", [platform_browser_1.DomSanitizer])
	], SafeUrlPipe);
	exports.SafeUrlPipe = SafeUrlPipe;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(26)))

/***/ },

/***/ 429:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__decorate, __param, __metadata) {"use strict";
	var core_1 = __webpack_require__(3);
	var forms_1 = __webpack_require__(27);
	var EqualValidator = EqualValidator_1 = (function () {
	    function EqualValidator(validateEqual, reverse) {
	        this.validateEqual = validateEqual;
	        this.reverse = reverse;
	    }
	    Object.defineProperty(EqualValidator.prototype, "isReverse", {
	        get: function () {
	            if (!this.reverse)
	                return false;
	            return this.reverse === 'true' ? true : false;
	        },
	        enumerable: true,
	        configurable: true
	    });
	    EqualValidator.prototype.validate = function (c) {
	        var v = c.value;
	        var e = c.root.get(this.validateEqual);
	        if (e && v !== e.value && !this.isReverse) {
	            return {
	                validateEqual: false
	            };
	        }
	        if (e && v === e.value && this.isReverse) {
	            delete e.errors['validateEqual'];
	            if (!Object.keys(e.errors).length)
	                e.setErrors(null);
	        }
	        if (e && v !== e.value && this.isReverse) {
	            e.setErrors({
	                validateEqual: false
	            });
	        }
	        return null;
	    };
	    return EqualValidator;
	}());
	EqualValidator = EqualValidator_1 = __decorate([
	    core_1.Directive({
	        selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
	        providers: [
	            { provide: forms_1.NG_VALIDATORS, useExisting: core_1.forwardRef(function () { return EqualValidator_1; }), multi: true }
	        ]
	    }),
	    __param(0, core_1.Attribute('validateEqual')),
	    __param(1, core_1.Attribute('reverse')),
	    __metadata("design:paramtypes", [String, String])
	], EqualValidator);
	exports.EqualValidator = EqualValidator;
	var EqualValidator_1;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(25), __webpack_require__(430), __webpack_require__(26)))

/***/ },

/***/ 430:
/***/ function(module, exports) {

	function __param (paramIndex, decorator) {
	    return function (target, key) { decorator(target, key, paramIndex); }
	};
	
	if (typeof module !== 'undefined' && module.exports) {
	    exports = module.exports = __param;
	}
	
	exports.__param = __param;

/***/ },

/***/ 431:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(__extends) {"use strict";
	var http_1 = __webpack_require__(31);
	var authData = JSON.parse(localStorage.getItem('loginUser'));
	var MyOptions = (function (_super) {
	    __extends(MyOptions, _super);
	    function MyOptions() {
	        return _super.call(this, {
	            method: http_1.RequestMethod.Get,
	            headers: new http_1.Headers({
	                'Content-Type': 'application/json',
	                'user-id': (authData != undefined) ? authData['id'] : 'null',
	                'user-token': (authData != undefined) ? authData['token'] : 'null',
	                'X-Requested-With': 'XMLHttpRequest'
	            })
	        }) || this;
	    }
	    return MyOptions;
	}(http_1.RequestOptions));
	exports.MyOptions = MyOptions;
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(432)))

/***/ },

/***/ 432:
/***/ function(module, exports) {

	function __extends (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	}
	
	if (typeof module !== 'undefined' && module.exports) {
	    exports = module.exports = __extends;
	}
	
	exports.__extends = __extends;

/***/ }

});
//# sourceMappingURL=app.js.map