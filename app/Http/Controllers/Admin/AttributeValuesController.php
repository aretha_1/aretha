<?php
namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AttributeValue;
use App\Attribute;
use App\Helpers\BasicFunction;
use Validator;

class AttributeValuesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($attribute_id) {
        $attribute_values_list = AttributeValue::where('attribute_id','=',$attribute_id)->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        
          $attributes = Attribute::find($attribute_id);
        $pageTitle = $attributes->name."'s ".trans('admin.ATTRIBUTE_VALUES');
        $title = $attributes->name."'s ".trans('admin.ATTRIBUTE_VALUES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $pages[$attributes->name."'s ".trans('admin.ATTRIBUTES')] = 'admin.attributes.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ATTRIBUTE_VALUES'));
        setCurrentPage('admin.attribute_values');

        return view('admin.attribute_values.index', compact('attribute_values_list', 'pageTitle', 'title', 'breadcrumb', 'attribute_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($attribute_id) {


        $attributes = Attribute::find($attribute_id);
        $pageTitle = trans('admin.ADD_ATTRIBUTE_VALUES');
        $title = trans('admin.ADD_ATTRIBUTE_VALUES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
          $pages[trans('admin.ATTRIBUTES')] = 'admin.attributes.index';
        $pages[$attributes->name."'s ".trans('admin.ATTRIBUTE_VALUES')] = array('admin.attribute_values.index', array('id' => $attribute_id)); 


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_ATTRIBUTE_VALUES'));

        return view('admin.attribute_values.create', compact('pageTitle', 'title', 'breadcrumb','attribute_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$attribute_id) {
        $attribute_valuesObj = new AttributeValue();


        $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'order_key' => 'required',
                    'value' => 'required',
             
        ]);


        if ($validator->fails()) {
            return redirect()->action('Admin\AttributeValuesController@create',$attribute_id)
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $input['attribute_id'] = $attribute_id;

        $attribute_values = $attribute_valuesObj->create($input);
              $getCurrentPage['id'] = $attribute_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.attribute_values');
        return redirect()->action('Admin\AttributeValuesController@index',$getCurrentPage)->with('alert-sucess', trans('admin.ATTRIBUTE_VALUES_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$attribute_id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $attribute_values = AttributeValue::find($id);
        if (empty($attribute_values)) {
            return $this->InvalidUrl();
        }

        $attributes = Attribute::find($attribute_id);

        $pageTitle =$attributes->name."'s ". trans('admin.ATTRIBUTE_VALUES');
        $title = $attributes->name."'s ".   trans('admin.ATTRIBUTE_VALUES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[$attributes->name."'s ".trans('admin.ATTRIBUTE_VALUES')] = array('admin.attribute_values.index', array('id' => $attribute_id));


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_ATTRIBUTE_VALUES'));

        return view('admin.attribute_values.edit', compact('attribute_values', 'pageTitle', 'title', 'breadcrumb','attribute_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$attribute_id) {


        $validator = validator::make($request->all(), [
                   'name' => 'required|max:255',
                    'order_key' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->action('Admin\AttributeValuesController@edit',array('id' => $id, 'attribute_id' => $attribute_id))
                            ->withErrors($validator)
                            ->withInput();
        }



        $attribute_values = AttributeValue::findOrFail($id);
        $input = $request->all();
        $attribute_values->fill($input)->save();
        $getCurrentPage['id'] = $attribute_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.attribute_values');
        return redirect()->action('Admin\AttributeValuesController@index',$getCurrentPage)->with('alert-sucess', trans('admin.ATTRIBUTE_VALUES_UPDATE_SUCCESSFULLY')); 
    }

    /**
     * Function To chnage Status of attribute_values pages
     *
     * @param  int  $id id of attribute_values pages
     * @param  int  $status 1/0 (current status of attribute_values page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $attribute_values = AttributeValue::where('id', '=', $id)->first();
        $attribute_values->status = $new_status;
        $attribute_values->save();
        return redirect()->action('Admin\AttributeValuesController@index', getCurrentPage('admin.attribute_values'))->with('alert-sucess', trans('admin.ATTRIBUTE_VALUES_CHANGE_STATUS_SUCCESSFULLY'));
    }

}