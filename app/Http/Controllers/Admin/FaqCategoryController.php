<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FaqCategory;
use App\Helpers\BasicFunction;
use Validator;

class FaqCategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $faq_categorylist = FaqCategory::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        

        $pageTitle = trans('admin.FAQ_CATEGORY');
        $title = trans('admin.FAQ_CATEGORY');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.FAQ_CATEGORY'));
        setCurrentPage('admin.faq_category');

        return view('admin.faq_category.index', compact('faq_categorylist', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.FAQ_CATEGORY');
        $title = trans('admin.FAQ_CATEGORY');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.FAQ_CATEGORY')] = 'admin.faq_category.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_FAQ_CATEGORY'));

        return view('admin.faq_category.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $faq_categoryObj = new FaqCategory();


        $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'order_key' => 'required',
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\FaqCategoryController@create')
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($faq_categoryObj, $request->name);

        $faq_category = $faq_categoryObj->create($input);
        return redirect()->action('Admin\FaqCategoryController@index', getCurrentPage('admin.faq_category'))->with('alert-sucess', trans('admin.FAQ_CATEGORY_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $faq_category = FaqCategory::find($id);
        if (empty($faq_category)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_FAQ_CATEGORY');
        $title = trans('admin.EDIT_FAQ_CATEGORY');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.FAQ_CATEGORY')] = 'admin.faq_category.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_FAQ_CATEGORY'));

        return view('admin.faq_category.edit', compact('faq_category', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $validator = validator::make($request->all(), [
                   'name' => 'required|max:255',
                    'order_key' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\FaqCategoryController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }



        $faq_category = FaqCategory::findOrFail($id);
        $input = $request->all();
        $faq_category->fill($input)->save();
        return redirect()->action('Admin\FaqCategoryController@index', getCurrentPage('admin.faq_category'))->with('alert-sucess', trans('admin.FAQ_CATEGORY_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of faq_category pages
     *
     * @param  int  $id id of faq_category pages
     * @param  int  $status 1/0 (current status of faq_category page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {
       

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $faq_category = FaqCategory::where('id', '=', $id)->first();
        $faq_category->status = $new_status;
        $faq_category->save();
        return redirect()->action('Admin\FaqCategoryController@index', getCurrentPage('admin.faq_category'))->with('alert-sucess', trans('admin.FAQ_CATEGORY_CHANGE_STATUS_SUCCESSFULLY'));
    }

}
