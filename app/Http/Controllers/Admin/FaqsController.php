<?php
namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Faq;
use App\FaqCategory;
use App\Helpers\BasicFunction;
use Validator;

class FaqsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category_id) {
       $faqs_list = Faq::where('category_id','=',$category_id)->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        
          $faq_category = FaqCategory::find($category_id);
        $pageTitle = $faq_category->name."'s ".trans('admin.FAQS');
        $title = $faq_category->name."'s ".trans('admin.FAQS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $pages[$faq_category->name."'s ".trans('admin.FAQ_CATEGORY')] = 'admin.faq_category.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.FAQS'));
        setCurrentPage('admin.faqs');

        return view('admin.faqs.index', compact('faqs_list', 'pageTitle', 'title', 'breadcrumb', 'category_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category_id) {


        $faq_category = FaqCategory::find($category_id);
        $pageTitle = trans('admin.ADD_FAQS');
        $title = trans('admin.ADD_FAQS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
          $pages[trans('admin.FAQ_CATEGORY')] = 'admin.faq_category.index';
        $pages[$faq_category->name."'s ".trans('admin.FAQS')] = array('admin.faqs.index', array('id' => $category_id)); 


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_FAQS'));

        return view('admin.faqs.create', compact('pageTitle', 'title', 'breadcrumb','category_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$category_id) {
        $faqsObj = new Faq();


        $validator = validator::make($request->all(), [
                    'question' => 'required',
                    'answer' => 'required',
                    'order_key' => 'required',
             
        ]);


        if ($validator->fails()) {
            return redirect()->action('Admin\FaqsController@create',$category_id)
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $input['category_id'] = $category_id;

        $faqs = $faqsObj->create($input);
              $getCurrentPage['id'] = $category_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.faqs');
        return redirect()->action('Admin\FaqsController@index',$getCurrentPage)->with('alert-sucess', trans('admin.FAQS_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$category_id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $faqs = Faq::find($id);
        if (empty($faqs)) {
            return $this->InvalidUrl();
        }

        $faq_category = FaqCategory::find($category_id);

        $pageTitle =$faq_category->name."'s ". trans('admin.FAQS');
        $title = $faq_category->name."'s ".   trans('admin.FAQS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[$faq_category->name."'s ".trans('admin.FAQS')] = array('admin.faqs.index', array('id' => $category_id));


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_FAQS'));

        return view('admin.faqs.edit', compact('faqs', 'pageTitle', 'title', 'breadcrumb','category_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$category_id) {


        $validator = validator::make($request->all(), [
                               'question' => 'required',
                    'answer' => 'required',
                    'order_key' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->action('Admin\FaqsController@edit',array('id' => $id, 'category_id' => $category_id))
                            ->withErrors($validator)
                            ->withInput();
        }



        $faqs = Faq::findOrFail($id);
        $input = $request->all();
        $faqs->fill($input)->save();
        $getCurrentPage['id'] = $category_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.faqs');
        return redirect()->action('Admin\FaqsController@index',$getCurrentPage)->with('alert-sucess', trans('admin.FAQS_UPDATE_SUCCESSFULLY')); 
    }

    /**
     * Function To chnage Status of faqs pages
     *
     * @param  int  $id id of faqs pages
     * @param  int  $status 1/0 (current status of faqs page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status,$category_id) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $faqs = Faq::where('id', '=', $id)->first();
        $faqs->status = $new_status;
        $faqs->save();
           $getCurrentPage['id'] = $category_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.faqs');
        return redirect()->action('Admin\FaqsController@index',$getCurrentPage)->with('alert-sucess', trans('admin.FAQS_CHANGE_STATUS_SUCCESSFULLY'));
    }

}