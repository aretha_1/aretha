<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Coupon;
use App\CouponCategory;
use App\CouponUser;
use App\Helpers\BasicFunction;
use App\CouponTag;
use Validator;
use App\Tag;

class CouponController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $couponslist = Coupon::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        

        $pageTitle = trans('admin.COUPONS');
        $title = trans('admin.COUPONS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.COUPONS'));
        setCurrentPage('admin.coupons');

        return view('admin.coupons.index', compact('couponslist', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.ADD_COUPONS');
        $title = trans('admin.ADD_COUPONS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.COUPONS')] = 'admin.coupons.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_COUPONS'));

        return view('admin.coupons.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $couponsObj = new Coupon();






      $validatorArry  = array(
                    'title' => 'required|max:255',
                    'redeem_count' => 'required|numeric',
                    'coupons_code' => 'required|alpha_num|unique:coupons|max:255',
                    'start_date' => 'required|before:end_date',
                    'end_date' => 'required',
                    'description' => 'required',
                    'discount_type' => 'required',
                    
             
        );



 $input = $request->all();
 if(!empty($input['discount_type'])) {
            if($input['discount_type']=='fixed'){

            $validatorArry['amount'] = 'required|numeric';

        }else{

        $validatorArry['percentage'] = 'required|numeric';
        $validatorArry['max_discount'] = 'required|numeric';

        }
    }

     if(!empty($input['discount_on'])) {
            if($input['discount_on']=='product'){

            $validatorArry['max_product_quantity'] = 'required';
            if(empty($input['tags']) && empty($input['category_id'])){
                $validatorArry['tags'] = 'required';
                $validatorArry['category_id'] = 'required';

            }


        }else{

              $validatorArry['min_total_value'] = 'required|numeric';

        }
    }

        $validator = validator::make($request->all(),$validatorArry );
        if ($validator->fails()) {
           
            return redirect()->action('Admin\CouponController@create')
                            ->withErrors($validator)
                            ->withInput();
        }


        
        $input['slug'] = BasicFunction::getUniqueSlug($couponsObj, $request->title);
         $input['start_date'] = makeTimeDatePicker($input['start_date']);
         $input['end_date'] = makeTimeDatePicker($input['end_date'],"23:59:59");
           if($input['discount_type']=='fixed'){

                $input['percentage'] = '';
           }else{

             $input['amount'] = '';
           }
        $category_ids =array();
        if(isset($input['category_id'])){
                $category_ids = $input['category_id'] ;
                unset($input['category_id']);
        }
        $users =array();
        if(isset($input['users'])){
            $users = $input['users'] ;
            unset($input['users']);
        }
        $tags =array();
        if(isset($input['tags'])){
              $tags = $input['tags'] ;
              unset($input['tags']);
        }
                 
        $coupons = $couponsObj->create($input);

        foreach ($tags as $key => $value) {
            # code...
            $data = array();
            $data['coupon_id'] =$coupons->id; 
            $data['tag_id'] =$value; 
            $CouponTagsObj = new CouponTag();
            $CouponTagsObj->create($data);
       }

        foreach ($category_ids as $key => $value) {
            $data = array();
            $data['coupon_id'] =$coupons->id; 
            $data['category_id'] =$value; 
            $CouponCategoryObj = new CouponCategory();
            $CouponCategoryObj->create($data);
            # code...
        }

        foreach ($users as $key => $value) {
            # code...
            $data = array();
            $data['coupon_id'] =$coupons->id; 
            $data['user_id'] =$value; 
            $CouponUserObj = new CouponUser();
            $CouponUserObj->create($data);
        }

        return redirect()->action('Admin\CouponController@index', getCurrentPage('admin.coupons'))->with('alert-sucess', trans('admin.COUPONS_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $coupons = Coupon::find($id);

        $coupons->start_date = makeDatePickerDate($coupons->start_date);
        $coupons->end_date = makeDatePickerDate($coupons->end_date);
        $coupons->tags = CouponTag::where('coupon_id','=',$coupons->id)->lists('tag_id', 'tag_id')->toArray();
        $coupons->category_id = CouponCategory::where('coupon_id','=',$coupons->id)->lists('category_id', 'category_id')->toArray();
        $coupons->users = CouponUser::where('coupon_id','=',$coupons->id)->lists('user_id', 'user_id')->toArray();

        
        if (empty($coupons)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_COUPONS');
        $title = trans('admin.EDIT_COUPONS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.COUPONS')] = 'admin.coupons.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_COUPONS'));

        return view('admin.coupons.edit', compact('coupons', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


     
      $validatorArry  = array(
                    'title' => 'required|max:255',
                    'redeem_count' => 'required|numeric',
                    'coupons_code' => 'required|alpha_num|unique:coupons,coupons_code,' . $id,
                    'start_date' => 'required|before:end_date',
                    'end_date' => 'required',
                    'description' => 'required',
                    'discount_type' => 'required',
                    
             
        );



 $input = $request->all();
 if(!empty($input['discount_type'])) {
            if($input['discount_type']=='fixed'){

            $validatorArry['amount'] = 'required|numeric';

        }else{

        $validatorArry['percentage'] = 'required|numeric';
        $validatorArry['max_discount'] = 'required|numeric';

        }
    }

     if(!empty($input['discount_on'])) {
            if($input['discount_on']=='product'){

            $validatorArry['max_product_quantity'] = 'required';
            if(empty($input['tags']) && empty($input['category_id'])){
                $validatorArry['tags'] = 'required';
                $validatorArry['category_id'] = 'required';

            }


        }else{

              $validatorArry['min_total_value'] = 'required|numeric';

        }
    }   
        $validator = validator::make($request->all(),$validatorArry );

        if ($validator->fails()) {
            return redirect()->action('Admin\CouponController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }



        $coupons = Coupon::findOrFail($id);
        $input = $request->all();
        $input['start_date'] = makeTimeDatePicker($input['start_date']);
         $input['end_date'] = makeTimeDatePicker($input['end_date'],"23:59:59");
           if($input['discount_type']=='fixed'){

                $input['percentage'] = '';
           }else{

             $input['amount'] = '';
           }
           $category_ids =array();
           if(isset($input['category_id'])){
                $category_ids = $input['category_id'] ;
        }
          $users =array();
           if(isset($input['users'])){
              $users = $input['users'] ;
        }
        $tags =array();
           if(isset($input['tags'])){
              $tags = $input['tags'] ;
      
        }
        unset($input['tags']);
        unset($input['category_id']);
        unset($input['users']);

        $coupons->fill($input)->save();

        $CouponCategoryObj = new CouponCategory();
        $couen_category =$CouponCategoryObj::where('coupon_id','=',$id)->lists('id', 'id')->toArray();
        if(!empty($couen_category)){
            $CouponCategoryObj::destroy($couen_category);
        }
        if(!empty($category_ids)){
        foreach ($category_ids as $key => $value) {
            $data = array();
            $data['coupon_id'] =$coupons->id; 
            $data['category_id'] =$value; 
            
            $CouponCategoryObj->create($data);
            # code...
        }
    }

    $CouponUserObj = new CouponUser();
    $couen_users =$CouponUserObj::where('coupon_id','=',$id)->lists('id', 'id')->toArray();
    if(!empty($couen_users)){
    $CouponUserObj::destroy($couen_users);
    }
    if(!empty($users)){
        foreach ($users as $key => $value) {
            # code...
            $data = array();
            $data['coupon_id'] =$coupons->id; 
            $data['user_id'] =$value; 
          
            $CouponUserObj->create($data);
        }
    }

        $CouponTagObj = new CouponTag();
        $couen_tags =$CouponTagObj::where('coupon_id','=',$id)->lists('id', 'id')->toArray();
        if(!empty($couen_tags)){
        $CouponTagObj::destroy($couen_tags);
        }
        if(!empty($tags)){
         foreach ($tags as $key => $value) {
            # code...
            $data = array();
            $data['coupon_id'] =$coupons->id; 
             $data['tag_id'] =$value;
          
            $CouponTagObj->create($data);
        }
    }
        return redirect()->action('Admin\CouponController@index', getCurrentPage('admin.coupons'))->with('alert-sucess', trans('admin.COUPONS_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of coupons pages
     *
     * @param  int  $id id of coupons pages
     * @param  int  $status 1/0 (current status of coupons page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {
       

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $coupons = Coupon::where('id', '=', $id)->first();
        $coupons->status = $new_status;
        $coupons->save();
        return redirect()->action('Admin\CouponController@index', getCurrentPage('admin.coupons'))->with('alert-sucess', trans('admin.COUPONS_CHANGE_STATUS_SUCCESSFULLY'));
    }

}
