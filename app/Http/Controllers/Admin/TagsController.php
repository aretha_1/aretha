<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Helpers\BasicFunction;
use Validator;

class TagsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $tagslist = Tag::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        ;

        $pageTitle = trans('admin.TAGS_PAGES');
        $title = trans('admin.TAGS_PAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.TAGS_PAGES'));
        setCurrentPage('admin.tags');

        return view('admin.tags.index', compact('tagslist', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.TAGS_PAGES');
        $title = trans('admin.TAGS_PAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.TAGS_PAGES')] = 'admin.tags.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_TAGS_PAGES'));

        return view('admin.tags.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $tagsObj = new Tag();


        $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
                    
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\TagsController@create')
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($tagsObj, $request->name);

        $tags = $tagsObj->create($input);
        return redirect()->action('Admin\TagsController@index', getCurrentPage('admin.tags'))->with('alert-sucess', trans('admin.TAGSPAGES_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $tags = Tag::find($id);
        if (empty($tags)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.TAGS_PAGES');
        $title = trans('admin.TAGS_PAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.TAGS_PAGES')] = 'admin.tags.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_TAGS_PAGES'));

        return view('admin.tags.edit', compact('tags', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
                  
                   
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\TagsController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }


        $tags = Tag::findOrFail($id);
        $input = $request->all();
        $tags->fill($input)->save();
        return redirect()->action('Admin\TagsController@index', getCurrentPage('admin.tags'))->with('alert-sucess', trans('admin.TAGSPAGES_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of tags pages
     *
     * @param  int  $id id of tags pages
     * @param  int  $status 1/0 (current status of tags page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $tags = Tag::where('id', '=', $id)->first();
        $tags->status = $new_status;
        $tags->save();
        return redirect()->action('Admin\TagsController@index', getCurrentPage('admin.tags'))->with('alert-sucess', trans('admin.TAGSPAGES_CHANGE_STATUS_SUCCESSFULLY'));
    }

}
