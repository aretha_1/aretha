<?php
namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tax;
use App\Helpers\BasicFunction;
use App\CategoryAttribute;
use Validator;
use Config;
use Input;
use view;

class TaxsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index() {
        $taxs_list = Tax::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));

      //  echo "<pre>";
      //  print_r($taxslist); die;

        $pageTitle = trans('admin.TAXS');
        $title = trans('admin.TAXS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.TAXS'));
        setCurrentPage('admin.taxs');

        return view('admin.taxs.index', compact('taxs_list', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.TAXS');
        $title = trans('admin.TAXS');
       
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.TAXS')] = 'admin.taxs.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_TAXS'));
 

        return view('admin.taxs.create', compact('pageTitle', 'title', 'breadcrumb'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) {
        $taxsObj = new Tax();

        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'name' => 'required|max:255',
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\TaxsController@create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();
       // pr($input); die;
        $taxs_list = $taxsObj->create($input);
        return redirect()->action('Admin\TaxsController@index', getCurrentPage('admin.taxs'))->with('alert-sucess', trans('admin.TAX_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $taxs_list = Tax::find($id);
        if (empty($taxs_list)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_TAXS');
        $title = trans('admin.EDIT_TAXS');
                /* * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.TAXS')] = 'admin.taxs.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_TAXS'));

        return view('admin.taxs.edit', compact('taxs_list', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function update(Request $request, $id) {

        $validator = validator::make($request->all(), [
                    
                    'name' => 'required|max:255',
                    'title' => 'required|max:255',             
               
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\TaxsController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $input = $request->all();
        
        $taxs_list = Tax::findOrFail($id);
         //echo "<pre>";
        //print_r($taxs_list); die;
       
       
        $taxs_list->fill($input)->save();
        return redirect()->action('Admin\TaxsController@index', getCurrentPage('admin.taxs'))->with('alert-sucess', trans('admin.TAXS_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of tags pages
     *
     * @param  int  $id id of tags pages
     * @param  int  $status 1/0 (current status of tags page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $taxs = Tax::where('id', '=', $id)->first();
        $taxs->status = $new_status;
        $taxs->save();
        return redirect()->action('Admin\TaxsController@index', getCurrentPage('admin.taxs'))->with('alert-sucess', trans('admin.TAXS_CHANGE_STATUS_SUCCESSFULLY'));
    }

   

}