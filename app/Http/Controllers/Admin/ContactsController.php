<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use URL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contact;
use Illuminate\Support\Facades\Auth;

class ContactsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $ContactObj = new Contact();
        $contact = $ContactObj->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        $pageTitle = trans('admin.CONTACT_US');
        $title = trans('admin.CONTACT_US');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CONTACT_US'));

        return view('admin.contacts.index', compact('contact', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function view($id) {
        $contact = Contact::find($id);
        $pageTitle = trans('admin.CONTACT_US');
        $title = trans('admin.CONTACT_US');
        /*         * breadcrumb* */

        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CONTACT_US')] = 'admin.contacts.index';
        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.VIEW_CONTACT_US'));

        return view('admin.contacts.view', compact('contact', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $grades = Contact::find($id)->delete();
        return redirect()->action('Admin\ContactsController@index')->with('alert-sucess', trans('admin.CONTACTS_DELETED_SUCCESSFULLY'));
    }

}
