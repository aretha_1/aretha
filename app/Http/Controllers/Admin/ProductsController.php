<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Helpers\BasicFunction;
use App\CategoryAttribute;
use App\Attribute;
use App\AttributeValue;
use App\ProductImage;
use Validator;
use Config;
use App\ProductAttributeValue;
use App\ProductTag;
use App\Stock;
use view;
use Input;
use URL;
use DB;
//use Requests;
class ProductsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $productslist = Product::with('category')->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        

        $pageTitle = trans('admin.PRODUCTS');
        $title = trans('admin.PRODUCTS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.PRODUCTS'));
        setCurrentPage('admin.products');

        return view('admin.products.index', compact('productslist', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.PRODUCTS');
        $title = trans('admin.PRODUCTS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.PRODUCTS')] = 'admin.products.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_PRODUCTS'));
 

        return view('admin.products.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $productsObj = new Product();

        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'price' => 'required|numeric',
                    'description' => 'required',
                    'category_id' => 'required',
                    'short_description' => 'required|max:255',
                    'meta_title' => 'required',
                    'meta_keywords' => 'required',
                    'meta_description' => 'required',
                    'size_chart' => 'required',
             
        ]);

        if ($validator->fails()) {
            return redirect()->action('Admin\ProductsController@create')
                            ->withErrors($validator)
                            ->withInput();
        }

        $input = $request->all();

        $input['slug'] = BasicFunction::getUniqueSlug($productsObj, $request->title);

       $attributes = array();
        $input['attributes'] = '';
        if(isset($input['attribute_value'])){
            $attributes = $input['attribute_value'];
             $input['attributes'] = implode(',',array_keys($attributes));
              unset($input['attribute_value']);

        }
      
        
       
          //$tags = $ ;
        $tags = array();
          if(isset($input['tags'])){
            $tags = $input['tags'];
                    unset($input['tags']);

        }  
        $stock['size'] = array();
            
        $products = $productsObj->create($input);

        $ProductAttributeValueObj = New ProductAttributeValue();

           foreach ($attributes as $key => $value) {
            foreach ($value as  $attribute_value) {
                # code...
            $data = array();
            # code... $data    =   array();
               $data['product_id']    =   $products->id;
               $data['attribute_value_id']    =   $attribute_value;
               $data['attribute_id']    =   $key;

               if($key== Config::get('global.size_id')){

                $stock['size'][] = $attribute_value;

               }
               $ProductAttributeValueObj->create($data);
           }
        }
             $ProducttageObj = New ProductTag();
              foreach ($tags as $key => $value) {
               $data = array();
               $data['product_id']    =   $products->id;
               $data['tag_id']    =   $value;
               $ProducttageObj->create($data);

              }
      
            foreach ($stock['size'] as $key => $value) {
                $data= array();
                $data['product_id'] = $products->id;
                $data['size_id'] = $value;
                $data['quantity'] = 0;
                Stock::create($data);
             }



        return redirect()->action('Admin\ProductsController@index', getCurrentPage('admin.products'))->with('alert-sucess', trans('admin.PRODUCTS_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $products = Product::with('attributesValue')->find($id);
        $attributes_value = $products->attributesValue;
        $data =  array();
        foreach ($attributes_value as $key => $value) {
            $data[$value->attribute_id][]=$value->attribute_value_id;
            # code...
        }
            $products->attributes_value =$data;
              $products->tags = ProductTag::where('product_id','=',$id)->lists('tag_id', 'tag_id')->toArray();
        if (empty($products)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_PRODUCTS');
        $title = trans('admin.EDIT_PRODUCTS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.PRODUCTS')] = 'admin.products.index';

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_PRODUCTS'));

        return view('admin.products.edit', compact('products', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $validator = validator::make($request->all(), [
                    'title' => 'required|max:255',
                    'price' => 'required|numeric',
                    'description' => 'required',
                    'category_id' => 'required',
                    'short_description' => 'required|max:255',
                    'meta_title' => 'required',
                    'meta_keywords' => 'required',
                    'meta_description' => 'required',
                    'size_chart' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\ProductsController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }

        $products = Product::findOrFail($id);
        $input = $request->all();
        
        $attributes = array();
        $input['attributes'] = '';
        if(isset($input['attribute_value'])){
            $attributes = $input['attribute_value'];
             $input['attributes'] = implode(',',array_keys($attributes));
              unset($input['attribute_value']);

        }
      
        
       
          //$tags = $ ;
        $tags = array();
          if(isset($input['tags'])){
            $tags = $input['tags'];
                    unset($input['tags']);

        }      
        
   
       $products->fill($input)->save();
        $stock['size'] = array();

        $ProductAttributeValueObj = New ProductAttributeValue();
        $product_attribute =$ProductAttributeValueObj::where('product_id','=',$id)->lists('id', 'id')->toArray();
        $ProductAttributeValueObj::destroy($product_attribute);
        
         foreach ($attributes as $key => $value) {
            foreach ($value as  $attribute_value) {
            
            # code... $data    =   array();
               $data['product_id']    =   $id;
               $data['attribute_value_id']   =  $attribute_value;
               $data['attribute_id']    =   $key;
               $ProductAttributeValueObj->create($data);
              if($key== Config::get('global.size_id')){

                     $stock['size'][] = $attribute_value;
                }

           }
        }

        foreach($stock['size'] as $key => $value) {
            $data= array();
            $data['product_id'] = $id;
            $data['size_id'] = $value;
            $data['quantity'] = 0;


             $sockData =   Stock::where('size_id', $value)->where('product_id',$id)->first();
             
            if(empty($sockData)){
                Stock::create($data);
            }
            
        }

        if(!empty($stock['size'])){
               Stock::where('product_id',$id)->whereNotIn('size_id', $stock['size'])->delete(); 
        }else{
               Stock::where('product_id',$id)->delete();
        }
        



        $ProducttageObj = New ProductTag();
               $product_tags =$ProducttageObj::where('product_id','=',$id)->lists('id', 'id')->toArray();
        $ProducttageObj::destroy($product_tags);
              foreach ($tags as $key => $value) {
               $data = array();
               $data['product_id']    =   $id;
               $data['tag_id']    =   $value;
               $resu = $ProducttageObj->create($data);
              
              }

        return redirect()->action('Admin\ProductsController@index', getCurrentPage('admin.products'))->with('alert-sucess', trans('admin.PRODUCTS_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of products pages
     *
     * @param  int  $id id of products pages
     * @param  int  $status 1/0 (current status of products page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {
       
        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $products = Product::where('id', '=', $id)->first();
        $products->status = $new_status;
        $products->save();
        return redirect()->action('Admin\ProductsController@index', getCurrentPage('admin.products'))->with('alert-sucess', trans('admin.PRODUCTS_CHANGE_STATUS_SUCCESSFULLY'));
    }




    public function getAttributeByCategory(Request $request){

            $input = $request->all();
            $category_id = $input['category_id'];
            $data_array =  BasicFunction::getAttributeByCategory($category_id);
          return   view('admin.products.get_attribute', $data_array)->render();
          

          

    }

// Function for product Images

    public function productsImage($id){


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $products = Product::find($id);
        
        if (empty($products)) {
            return $this->InvalidUrl();
        }
    $products_images = ProductImage::where('product_id', '=', $id)->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        
    setCurrentPage('admin.products_image');


        
        $pageTitle = ucfirst($products->title)."'s ".trans('admin.IMAGES');
        $title =   ucfirst($products->title)."'s ".trans('admin.IMAGES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.PRODUCTS')] = 'admin.products.index';


        $breadcrumb = array('pages' => $pages, 'active' =>   ucfirst($products->title)."'s ".trans('admin.IMAGES'));

         return view('admin.products.products_image', compact('products', 'pageTitle', 'title', 'breadcrumb','products_images'));
    } 



public function updalodProductsImage(Request $request,$id){




 $validator = validator::make($request->all(), [
                    
                    'files' => 'required|image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),
             
        ]);

 $input = $request->all();
 $productsData = Product::find($id);
 
    $image = Input::file('files');
        $return_data = array();
      if ($validator->fails()) {
                     $error = '';
                    
                    
                    $return_data['name']        =   $image->getClientOriginalName();
                    $return_data['size']        =   $image->getSize();
                    $return_data['type']        =   $image->getClientOriginalExtension();           
                    $return_data['url']         =   '';             
                    $return_data['status']      =   'error';
                    $return_data['error']       =   $validator->errors()->all();;
                    
                    $files['files'][]           =   $return_data;
                    echo json_encode($files);
                    die;


      }else{
        if($image->isValid() ){

                            $imageData                      =   array();    
                            $imageData['product_id']           =   $id;                    
                            $imageData['is_main']           =   0;                    
                            $imageData['is_main']           =   0;                    
                            $image_size =  $image->getSize();

                             $imageData['name']  =   BasicFunction::uploadImage($image, PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH,$productsData->slug.'_','','','',THUMB);
                            
                                                                                 
                            $ProductImageObj = New ProductImage();
                            $products = $ProductImageObj->create($imageData);
                                                
                            $gallery_id                 =   $products->id;                       
                        
                            $return_data['name']        =  $image->getClientOriginalName();;
                            $return_data['size']        =   $image_size;
                            $return_data['type']        =    $image->getClientOriginalExtension();              
                            $return_data['url']         =   PRODUCT_IMAGES_URL.$imageData['name'];
                            
                            $return_data['thumbnailUrl']=   BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH ,PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH, $imageData['name'],array('width' => 76,'height' => 80));
                            $return_data['deleteUrl']   =  URL::route('admin.products.delete_products_image', ['id' => $gallery_id,'product_id'=>$id]);
                             $return_data['deleteType']  =   'DELETE';
                            
                            $files['files'][]           =   $return_data;
                            echo json_encode($files);
                            die;
                    }   
                }
                  
      }

 
      

function deleteProductsImage($id,$product_id,Request $request){

        
if(!empty($product_id) && !empty($id))
        {   
               $ProductImageData = ProductImage::find($id);

                 $image_name =   $ProductImageData->name;
               
               

                 $ProductImageData->delete();

                @unlink(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . $image_name);
                @unlink(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH.T130 . $image_name);
                @unlink(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH.T540 . $image_name);


                   if($request->ajax()){

  
                    $return_data['status']  =   true;
                
                     echo json_encode($return_data);

                }else{

                      $getCurrentPage['id'] = $product_id;
                      $getCurrentPage = $getCurrentPage + getCurrentPage('admin.products_image');
                      return redirect()->action('Admin\ProductsController@productsImage',$getCurrentPage)->with('alert-sucess', trans('admin.PRODUCT_IMAGES_DELETED_SUCCESSFULLY'));
                }


        }else{

             return $this->InvalidUrl();
        }

 
}
    /**
     * Function To chnage Status of products pages
     *
     * @param  int  $id id of products pages
     * @param  int  $status 1/0 (current status of products page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function productImageStatusChange($id, $status,$product_id) {
       

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $products_img = ProductImage::where('id', '=', $id)->first();
        $products_img->status = $new_status;
        $products_img->save();
        $getCurrentPage['id'] = $product_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.products_image');
        return redirect()->action('Admin\ProductsController@productsImage', $getCurrentPage)->with('alert-sucess', trans('admin.PRODUCTS_IMAGE_CHANGE_STATUS_SUCCESSFULLY'));
    }

    public function setMainImage($id,$product_id){

        if(!empty($product_id) && !empty($id))
        {   
            ProductImage::where('product_id', '=', $product_id)->update(['is_main' => 0]);

            $products_img = ProductImage::where('id', '=', $id)->first();
            $products_img->is_main = 1;
            $products_img->save();
                 $getCurrentPage['id'] = $product_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.products_image');
        return redirect()->action('Admin\ProductsController@productsImage', $getCurrentPage)->with('alert-sucess', trans('admin.IMAGE_SET_MAIN_SUCCESSFULLY'));

        }else{
            return $this->InvalidUrl();

        }

    }

    function ManageStock($id,$from=''){


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $products = Product::find($id);
        
        if (empty($products)) {
            return $this->InvalidUrl();
        }

        $attributedata = ProductAttributeValue::with('attribute_value')->where('product_id', '=', $products->id)->where('attribute_id', Config::get('global.size_id'))->get();
                $attribute = array();
        foreach ($attributedata as $key => $value) {
            
            $attribute[] = $value['attribute_value'];
        }
        if(empty($attribute)){
            return redirect()->action('Admin\ProductsController@index', getCurrentPage('admin.products'))->with('alert-error', trans('admin.SIZE_NOTE_AVAILABLE'));

        }
        $stock['size'] =  Stock::where('product_id',$products->id)->lists('quantity','size_id')->toArray();
        
        $pageTitle = ucfirst($products->title)."'s ".trans('admin.STOCk');
        $title =   ucfirst($products->title)."'s ".trans('admin.STOCk');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
         if($from =='' || $from=='index'){

            
            $pages[trans('admin.PRODUCTS')] = 'admin.products.index';
         }else{
            $pages[trans('admin.OUTOFFSTOCK_PRODUCTS')] = 'admin.products.out_off_stock';    
         }
        
        $breadcrumb = array('pages' => $pages, 'active' =>   ucfirst($products->title)."'s ".trans('admin.STOCk'));
        return view('admin.products.products_stock', compact('products', 'pageTitle', 'title', 'breadcrumb','attribute','stock','from'));


    }
    function saveStock($id,$from='',Request $request){
        if ($id == '') {
            return $this->InvalidUrl();
        }
        $products = Product::find($id);
        
        if (empty($products)) {
            return $this->InvalidUrl();
        }

         $stock = $request->all();
         
         foreach ($stock['size'] as $key => $value) {
            $data= array();
            $data['product_id'] = $id;
            $data['size_id'] = $key;
            $data['quantity'] = $value;
       


              $sockData =   Stock::where('size_id', $key)->where('product_id',$id)->first();
              if(!empty($sockData)){
                $sockData->fill($data)->save();
              }else{

                      Stock::create($data);
              }


            
         }
        
         if($from==''){

            $from = 'index';
         }
        return redirect()->action('Admin\ProductsController@'.$from, getCurrentPage('admin.products'))->with('alert-sucess', trans('admin.PRODUCTS_STOCK_ADD_SUCCESSFULLY'));

    }

    function outOffStock(){

        
      
      $stock =  Stock::with('product')->where('quantity',0)->sortable(['updated_at' => 'asc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
       $pageTitle = trans('admin.OUTOFFSTOCK_PRODUCTS');
        $title = trans('admin.OUTOFFSTOCK_PRODUCTS');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.OUTOFFSTOCK_PRODUCTS'));
 
    $attributedataValue = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('value', 'id')->toArray();
        return view('admin.products.out_off_stock', compact('pageTitle', 'title', 'breadcrumb','stock','attributedataValue'));



    }

}
