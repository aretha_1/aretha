<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Attribute;
use App\Helpers\BasicFunction;
use Validator;

class AttributeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $attributeslist = Attribute::sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        

        $pageTitle = trans('admin.ATTRIBUTES');
        $title = trans('admin.ATTRIBUTES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ATTRIBUTES'));
        setCurrentPage('admin.attributes');

        return view('admin.attributes.index', compact('attributeslist', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $pageTitle = trans('admin.ATTRIBUTES');
        $title = trans('admin.ATTRIBUTES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ATTRIBUTES')] = 'admin.attributes.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_ATTRIBUTES'));

        return view('admin.attributes.create', compact('pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $attributesObj = new Attribute();


        $validator = validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'order_key' => 'required',
             
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\AttributeController@create')
                            ->withErrors($validator)
                            ->withInput();
        }


        $input = $request->all();
        $input['slug'] = BasicFunction::getUniqueSlug($attributesObj, $request->name);

        $attributes = $attributesObj->create($input);
        return redirect()->action('Admin\AttributeController@index', getCurrentPage('admin.attributes'))->with('alert-sucess', trans('admin.ATTRIBUTES_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $attributes = Attribute::find($id);
        if (empty($attributes)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.EDIT_ATTRIBUTES');
        $title = trans('admin.EDIT_ATTRIBUTES');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.ATTRIBUTES')] = 'admin.attributes.index';


        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_ATTRIBUTES'));

        return view('admin.attributes.edit', compact('attributes', 'pageTitle', 'title', 'breadcrumb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $validator = validator::make($request->all(), [
                   'name' => 'required|max:255',
                    'order_key' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->action('Admin\AttributeController@edit',$id)
                            ->withErrors($validator)
                            ->withInput();
        }



        $attributes = Attribute::findOrFail($id);
        $input = $request->all();
        $attributes->fill($input)->save();
        return redirect()->action('Admin\AttributeController@index', getCurrentPage('admin.attributes'))->with('alert-sucess', trans('admin.ATTRIBUTES_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of attributes pages
     *
     * @param  int  $id id of attributes pages
     * @param  int  $status 1/0 (current status of attributes page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status) {
       

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $attributes = Attribute::where('id', '=', $id)->first();
        $attributes->status = $new_status;
        $attributes->save();
        return redirect()->action('Admin\AttributeController@index', getCurrentPage('admin.attributes'))->with('alert-sucess', trans('admin.ATTRIBUTES_CHANGE_STATUS_SUCCESSFULLY'));
    }

}
