<?php

namespace App\Http\Controllers\Admin;

use Hash;
use DB;
use Config;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\CategoryAttribute;
use App\Attribute;
use App\Helpers\BasicFunction;
use Validator;
use Input;

class CategoryController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null) {

        if ($id == null) {
            $id = 0;
        }

        $categorylist = Category::with('children')->where('parent_id', '=', $id)->sortable(['created_at' => 'desc'])->paginate(Configure('CONFIG_PAGE_LIMIT'));
        

        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        if ($id == 0) {
            $breadcrumb = array('pages' => $pages, 'active' => trans('admin.CATEGORY'));

            $pageTitle = trans('admin.CATEGORY');
            $title = trans('admin.CATEGORY');
        } else {

            $pages[trans('admin.CATEGORY')] = 'admin.category.index';
            $category = Category::find($id);

            if (empty($category)) {
                return $this->InvalidUrl();
            }

            $breadcrumb = array('pages' => $pages, 'active' => ucfirst($category->name));

            $pageTitle = ucfirst($category->name) . "'s " . trans('admin.SUB_CATEGORY');
            $title = ucfirst($category->name) . "'s " . trans('admin.SUB_CATEGORY');
        }
        setCurrentPage('admin.category');


        return view('admin.category.index', compact('categorylist', 'pageTitle', 'title', 'breadcrumb', 'id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($parent_id = null) {
        $pageTitle = trans('admin.ADD_CATEGORY');
        $title = trans('admin.ADD_CATEGORY');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CATEGORY')] = 'admin.category.index';
        if ($parent_id != 0) {
            $category = Category::find($parent_id);
            $pages[ucfirst($category->name)] = array('admin.category.index', array('id' => $parent_id));




            if (empty($category)) {
                return $this->InvalidUrl();
            }
        }

        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.ADD_CATEGORY'));

        $attribute_list = Attribute::where('status','=',1)->orderBy('order_key')->lists('name', 'id')->toArray();;    


        return view('admin.category.create', compact('pageTitle', 'title', 'breadcrumb', 'parent_id','attribute_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $parent_id = null) {
        $categoryObj = new Category();
        //echo "<pre>";
        //print_r($categoryObj); die;
        $validationRule = array(
            'name' => 'required|max:255',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'image' => 'required|image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),

           
        );

        $input = $request->all();
         //echo "<pre>";
         //print_r($input); die;
        if ($input['category_type'] == Config::get('global.category_type.sub_category')) {
            $validationRule['parent_id'] = 'required';
            $validationRule['attributes'] = 'required';
                   
        }else{
            $validationRule['category_for'] = 'required';
              

        }
            
        $validator = validator::make($request->all(), $validationRule);
        if ($validator->fails()) {
            
            return redirect()->action('Admin\CategoryController@create', $parent_id)
                            ->withErrors($validator)
                            ->withInput();
        } 

            $title = $request->name;

            if ($request->hasFile('image')) {
            $name = BasicFunction::uploadImage(Input::file('image'), CATEGORY_IMAGES_UPLOAD_DIRECTROY_PATH, 'category');
            $input['image'] = $name;
        }
           if ($input['category_type'] == Config::get('global.category_type.sub_category')) {
           		if($parent_id==null){
           			
           			 $pere_category = $categoryObj->find($input['parent_id']);
           		}else{

           			$pere_category = $categoryObj->find($parent_id);
           		}
               
//                if($pere_category->category_for!= Config::get('global.category_for.accessories')){
//                    $title =     $request->name.' for '. $pere_category->category_for;
//                }

           }else{
               if($input['category_for']!= Config::get('global.category_for.accessories')){
                    $title =     $request->name;
               }
           }
        $input['slug'] = BasicFunction::getUniqueSlug($categoryObj, $title);


        if ($request->category_type == Config::get('global.category_type.main_category')) {
            $input['parent_id'] = 0;
            // $input['slug'] =  $input['slug']
             $attributes   = array();
        }else{

            $input['category_for'] = '';
             $attributes     =   $input['attributes'];
        }

        unset($input['category_type']);
        unset($input['attributes']);

        $category = $categoryObj->create($input);
       
        if(!empty($attributes)){
        $CategoryAttributeObj = New CategoryAttribute();
        foreach ($attributes as $key => $value) {
           $data    =   array();
           $data['category_id']    =   $category->id;

           $data['attribute_id']    =   $value;
           $CategoryAttributeObj->create($data);
        }
    }
        $getCurrentPage['id'] = $parent_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.category');

        return redirect()->action('Admin\CategoryController@index', $getCurrentPage)->with('alert-sucess', trans('admin.CATEGORY_ADD_SUCCESSFULLY'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $parent_id = null) {


        if ($id == '') {
            return $this->InvalidUrl();
        }
        $category = Category::find($id);

        $category_attribute = CategoryAttribute::where('category_id','=',$id)->lists('attribute_id', 'attribute_id')->toArray();
        //echo  "<pre>";
       // print_r($category); die;
        $category->attributes= $category_attribute;
   
      
        if (empty($category)) {
            return $this->InvalidUrl();
        }

        $pageTitle = trans('admin.CATEGORY');
        $title = trans('admin.CATEGORY');
        /*         * breadcrumb* */
        $pages["<i class='fa fa-dashboard'></i>" . trans('admin.DASHBOARD')] = 'dashboard';
        $pages[trans('admin.CATEGORY')] = 'admin.category.index';
        if ($parent_id != 0) {
            $parent_category = Category::find($parent_id);
            $pages[ucfirst($parent_category->name)] = array('admin.category.index', array('id' => $parent_id));

            if (empty($parent_category)) {
                return $this->InvalidUrl();
            }
        }

         $attribute_list = Attribute::where('status','=',1)->orderBy('order_key')->lists('name', 'id')->toArray();    



        $breadcrumb = array('pages' => $pages, 'active' => trans('admin.EDIT_CATEGORY'));

        return view('admin.category.edit', compact('category', 'pageTitle', 'title', 'breadcrumb', 'parent_id','attribute_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $parent_id = null) {

        $validationRule = array(
            'name' => 'required|max:255',
            'attributes' => 'required',
            'meta_title' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required',
            'image' => 'image|mimes:'.Config::get('global.image_mime_type').'|max:'.Config::get('global.file_max_size'),

        );

        $input = $request->all();
      // echo  "<pre>"; print_r($input); die;
       
        if ($input['category_type'] == Config::get('global.category_type.sub_category')) {
            $validationRule['parent_id'] = 'required';
        }else{

             $validationRule['category_for'] = 'required';
        }
      //  pr($input);
       //  echo "<pre>"; 
        //print_r($validationRule); die;
        $validator = validator::make($request->all(), $validationRule);
       
        if ($validator->fails()) {
            //dd($validator->errors());
            return redirect()->action('Admin\CategoryController@edit', array('id' => $id, 'parent_id' => $parent_id))
                            ->withErrors($validator)
                            ->withInput();

        }
        $category = Category::findOrFail($id);
        if ($request->hasFile('image')) {
        

          $name =   BasicFunction::uploadImage(Input::file('image'), CATEGORY_IMAGES_UPLOAD_DIRECTROY_PATH, 'banner_', true, $category->image);
        

            $input['image'] = $name;
        } else {

            $input['image'] = $category->image;
        }

  
        if ($request->category_type == Config::get('global.category_type.main_category')) {
            $input['parent_id'] = 0;
        }else{

            $input['category_for'] = '';
        }
        $attributes     =   $input['attributes'];
        unset($input['category_type']);
        unset($input['attributes']);
        
        $category->fill($input)->save();


        $CategoryAttributeObj = New CategoryAttribute();
        $category_attribute =$CategoryAttributeObj::where('category_id','=',$id)->lists('id', 'id')->toArray();
        $CategoryAttributeObj::destroy($category_attribute);


    // delete old data

        foreach ($attributes as $key => $value) {
           $data    =   array();
           $data['category_id']    =   $category->id;
           $data['attribute_id']    =   $value;
           $CategoryAttributeObj->create($data);
        }

        $getCurrentPage['id'] = $parent_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.category');

        return redirect()->action('Admin\CategoryController@index', $getCurrentPage)->with('alert-sucess', trans('admin.CATEGORY_UPDATE_SUCCESSFULLY'));
    }

    /**
     * Function To chnage Status of cms pages
     *
     * @param  int  $id id of cms pages
     * @param  int  $status 1/0 (current status of cms page i.e active or inactive)
     * @return \Illuminate\Http\Response
     */
    public function status_change($id, $status, $parent_id = null) {

        if (empty($id)) {
            return $this->InvalidUrl();
        }
        if ($status == 1) {

            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $category = Category::where('id', '=', $id)->first();
        $category->status = $new_status;
        $category->save();
        $getCurrentPage['id'] = $parent_id;
        $getCurrentPage = $getCurrentPage + getCurrentPage('admin.category');

        return redirect()->action('Admin\CategoryController@index', $getCurrentPage)->with('alert-sucess', trans('admin.CATEGORY_CHANGE_STATUS_SUCCESSFULLY'));
    }

}
