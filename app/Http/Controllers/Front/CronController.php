<?php

namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use App\OrderDelivery;
use Config;
use App\Helpers\GlobalHelper;
use App\Helpers\EmailHelper;
use App\TempEmailSend;
use Omnipay;
use PDF;
class CronController extends Controller {

  function OrderTrack(){


  }
  function sendMailFromTemp(){
  			$result = TempEmailSend::orderBy('created_at', 'asc')->limit(20)->get();

  			if(!empty($result)){

  				foreach ($result as $key => $value) {

					EmailHelper::sendMailFromTemp($value->email_to,$value->email_from,$value->subject,$value->message,$value->email_type,$value->attachments);
          
					 TempEmailSend::find($value->id)->delete();
  				}

  			}
  			echo "Mail send successfully";
  			die;


  			//\App\AddressBook::find($id)->delete();
  }

  function orderStatus(){
      $currntTime = getCurrentTime();
      $order_deliveries =   OrderDelivery::where('StatusType','=','UD')->where('last_track_time','<=',$currntTime)->limit(20)->get();


      foreach ($order_deliveries as $key => $order_delivery) {
               $waybill  =  $order_delivery->waybill;
              $token = Configure('CONFIG_DELHIVERY_API'); 

              if(Configure('CONFIG_DELHIVERY_TEST_MODE')){
                $get_url = TEST_PACKAGES_URL."/?token=" . $token . "&waybill=".$waybill;
              }else{
                $get_url = PROD_PACKAGES_URL."/?token=" . $token . "&waybill=".$waybill;

              }
               
                
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $get_url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Token " . $token));
              $result = curl_exec($ch);
              curl_close($ch);
              $data_result = json_decode($result, true);

              $track_responce = array();
              if(isset($data_result['ShipmentData'][0]['Shipment'])){

                $track_responce = $data_result['ShipmentData'][0]['Shipment'];  
              }
              
              
               $order_id = $order_delivery->order_id;
              if(!empty($track_responce)){

                  $input['track_responce'] = serialize($track_responce);
                  $input['status'] = $track_responce['Status']['Status'];
                  $status        = $track_responce['Status']['Status'];
                  $input['StatusType'] = $track_responce['Status']['StatusType'];
                  $input['last_track_time'] = getCurrentTime();
                  $order_delivery->fill($input)->save();  
                 
                  if($status=='Delivered'){
                     $Orders = Order::with('user')->where('id', '=', $order_id)->first();
                     $orderdata = $Orders;
                      $Orders->order_status =  Config::get('global.order_status.delivered');
                      if($Orders->payment_by=='cod'){
                          $Orders->payment_status = Config::get('global.payment_status.success');
                      }
                      $Orders->delivery_date =getCurrentTime();
                      $Orders->save();

                      if(Configure('CONFIG_ENABLE_SMS')){

                         EmailHelper::sendOrderSms($orderdata->user->phone,$orderdata->user->id,$orderdata->order_id,6); 
                      } 
                      $this->sendDeliverMail($order_id);
                  }


              }
      }
      
      return 1;
  }
 function sendDeliverMail($id){

         $order = \App\Order::with('user','order_detail')->where('id', $id)->first();
           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
            $order->order_detail = $order_details;
            
            $subject= 'Order Shipped - Your order #'.$order->order_id.' has been delivered';
            $email_type= 'Order Delivered';

            EmailHelper::sendOrderMail($order->user->email, '',  $subject, 'delivered_order', $order->toArray(), $email_type);
         
        return true;
    }

  function checkPaymentStatus(){
    set_time_limit(0);


    $order = new Order();
     $currntTime = getCurrentTime();
      $order = $order->where('payment_status','=',Config::get('global.payment_status.pending'));
      
      $order = $order->where('created_at','<',date(MYSQL_DATE_FORMATE,$currntTime-1800));

         $order = $order->where(function($query)  {
                                
                              $query = $query->orWhere('payment_by', 'PayUmoney')->orWhere('payment_by', 'payu')->orWhere('payment_by', 'paytm');
                       
                                

                                return $query;
                            });
         $order = $order->limit(20)->get();
       
         foreach ($order as $key => $value) {
         // pr($value);
              $data = array();
              $errors = 1;
           
              
                  if($value->payment_by == 'paytm'){
                        $requestParamList = array("MID" => Configure('CONFIG_MID') , "ORDERID" => $value->order_id);

                    
                       $result = getTxnStatus($requestParamList);
                         
                       $data['transaction_id'] = $result['TXNID'];
                       $data['payment_response'] = serialize($result);
                       if ($result['STATUS'] == 'TXN_SUCCESS') {
                        $data['payment_mode'] = $result['GATEWAYNAME'];
                        $data['payment_status'] = 'success';
                        $data['order_status'] = 'success';
                         $errors = 0;
                    } else if ($result['STATUS'] == 'TXN_FAILURE') {
                        $data['reject_reason'] = 'Payment failed';
                        $data['payment_status'] = 'failure';
                        $data['order_status'] = 'failed';
                         $errors = 0;
                    }else{
                            $respcode = array('141','227','810','8102','8103');
                            if(in_array($result['RESPCODE'], $respcode) ){


                            $errors = 0;
                              $data['reject_reason'] = 'Payment failed';
                              $data['payment_status'] = 'failure';
                              $data['order_status'] = 'failed';
                            }

                    }
                    


                  }else{
                    


                    $string = Configure('CONFIG_MERCHANT_ID').'|verify_payment|'.$value->transaction_id.'|'.Configure('CONFIG_SALT');
    
                    $hash =  hash('sha512', $string);
                    
                     $r = array('key' => Configure('CONFIG_MERCHANT_ID') , 'hash' =>$hash , 'var1' =>$value->transaction_id, 'command' =>'verify_payment');   

                    $wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
                    $qs= http_build_query($r);  
                    $result = $this->curlCall($qs,$wsUrl);
                    if($result['status']==1){
                        $transaction_details = $result['transaction_details'][$value->transaction_id]; 
                          $data['payment_response'] =serialize($transaction_details);


                          $data['mihpayid'] = $transaction_details['mihpayid'];
                          $data['payment_mode'] = $transaction_details['mode'];
                          
                          if ($transaction_details['status'] == 'success') {
                              $data['payment_status'] = 'success';
                              $data['order_status'] = 'success';
                               $errors = 0;
                          } else if ($transaction_details['status'] == 'failure') {

                              $data['payment_status'] = 'failure';
                              $data['order_status'] = 'failed';
                              $data['reject_reason'] ='Payment failed';
                               $errors = 0;
                          }


                    }else{
                        $data['payment_status'] = 'failure';
                        $data['order_status'] = 'failed';
                        $data['reject_reason'] ='Payment failed';
                        $data['payment_response'] =serialize($result);
                         $errors = 0;



                    }

                

                  }
                  
                   $order = \App\Order::where('order_id', $value->order_id)->first();

                     if (!empty($data) && $errors == 0) {

                $order->fill($data)->save();
                if ($data['order_status'] == 'success') {

                     $this->SendOrderMail($order->id);
                    $carts = \App\OrderDetail::where('order_id', $order->id)->lists('cart_id', 'cart_id')->toArray();


                    \App\Cart::destroy($carts);




                }

                if ($data['payment_status'] == 'failure' && $order->wallet_amount > 0) {
                    $wallet = array();
                    $wallet['user_id'] = $order->user_id;
                    $wallet['flag'] = Config::get('global.wallet_flag.cr');
                    $wallet['cash'] = $order->wallet_amount;
                    $wallet['description'] = "Payment failure,Refund from Order #" . $order->order_id;
                    $wallet['order_id'] = $order->id;
                    \App\Wallet::create($wallet);
                }
                  if ($data['payment_status'] == 'failure') {
                          $carts = \App\OrderDetail::where('order_id', $order->id)->get();
                          foreach ($carts as $key => $Pvalue) {
                                 $sockData =   \App\Stock::where('size_id', $Pvalue->product_size)->where('product_id',$Pvalue->product_id)->increment('quantity', $Pvalue->quantity);
                          }
                         
                    }


                    $ordData['order_id'] = $order->id;
                  $ordData['status'] = $data['order_status'];
                   \App\OrderStatus::create($ordData);


    
            }
                    
                             
            
         }

     return '1';
      
    
  }

function sendInvoice(){

    set_time_limit(0);
    $order = new Order();
    
    $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();

      $order = $order-> with('user','order_detail')->where('order_status','=',Config::get('global.order_status.delivered'));
      
      $order = $order->where('is_send_invoice','=',0);

         $orders = $order->limit(20)->get();
        
         foreach ($orders as $key => $order) {
               $id = $order->id;
             $order_details= array();
        foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
           $order->order_detail = $order_details;

            if(empty($order->invoice_id)){
               
                
                
                

                $OrderObj = Order::where('id', '=', $id)->first();
                $OrderObj->invoice_id = INVOICE_START+$order->id;
                $order->invoice_id = INVOICE_START+$order->id;
                $OrderObj->save();

               
            }
                
                    $html =  view('admin.orders.invoice_pdf', compact('id','order'))->render();
                  
                  $html =  mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
                    
                   $file_name = 'invoice-'.$order->order_id;
                  
                   $pdf = PDF::loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false)->save(INVOICE_UPLOAD_DIRECTROY_PATH.$file_name.'.pdf');
                
              
                    $email_template = \App\EmailTemplate::where('slug', '=', 'invoice')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;
            $user = $order->user;
            $to = $user->email;
           
            
            $body = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
                
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $order->order_id,
           
                
          
                    ), $body);    
   $subject = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
                
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $order->order_id,
           
                
          
                    ), $subject);    

    
    

             EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type,INVOICE_UPLOAD_DIRECTROY_PATH.$file_name.'.pdf');

            
                $OrderObj = Order::where('id', '=', $id)->first();
                $OrderObj->is_send_invoice = 1;
                $OrderObj->save();

         }
         return 1;


}

function curlCall ($qs, $wsUrl)
{
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $wsUrl);
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        $o = curl_exec($c);
        $result = json_decode($o,true);
       return $result; 
}
  
           function SendOrderMail($order_id){
          $order = \App\Order::with('user','order_detail')->where('id', $order_id)->first();
           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
            $order->order_detail = $order_details;

            $subject= 'Thank for your order, your Order ID is '.$order->order_id;
            $email_type= 'Order placed to user';

            EmailHelper::sendOrderMail($order->user->email, '',  $subject, 'create_order', $order->toArray(), $email_type);




            $subject= 'New order received #'.$order->order_id;
            $email_type= 'Order placed to admin';
             EmailHelper::sendOrderMail(Configure('CONFIG_STAFF_MAIL'), '',  $subject, 'create_order_admin', $order->toArray(), $email_type);






            if(Configure('CONFIG_ENABLE_SMS')){

              EmailHelper::sendOrderSms($order->user->phone,$order->user->id,$order->order_id,1); 
            }
            
        return true;
    }
     
  
   
}
