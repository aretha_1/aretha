<?php

namespace App\Http\Controllers\Api;

use Hash;
use DB;
use Config;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Cms;
use App\Helpers\BasicFunction;
use Validator;
use Input;
use App\Category;
use App\SliderImage;
use App\User;
use App\Helpers\EmailHelper;
use App\ProductAttributeValue;
use App\ProfileWishlist;
use URL;
use Image;
use Debugbar;
use Omnipay;

class ApiController extends Controller {

    public function __construct(Request $request) {

        Debugbar::info($request->ajax());

        if (!$request->ajax()) {
            //  $response = array("error" => 'API access not granted');
            //  echo json_encode($response);
            // die;
        }
    }

    /* Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getAllsettings() {
        $settings = Config::get('settings');
       $settings = array_except($settings, ['CONFIG_MAIL_DRIVER','CONFIG_MAIL_USERNAME','CONFIG_MAIL_PASSWORD','CONFIG_SMS_USER_NAME','CONFIG_SMS_PASSWORD','CONFIG_SENDER_ID','CONFIG_MID','CONFIG_WEBSITE','CONFIG_MERCHANT_KEY','CONFIG_CHANNEL_ID','CONFIG_INDUSTRY_TYPE_ID','CONFIG_MERCHANT_ID','CONFIG_SALT','CONFIG_DELHIVERY_CLIENT','CONFIG_PICKUP_ADDRESS','CONFIG_PICKUP_CITY','CONFIG_PICKUP_PHONE','CONFIG_PICKUP_PINCODE','CONFIG_PICKUP_STATE','CONFIG_DELHIVERY_API','CONFIG_DELHIVERY_PICKUP']);

        return response()->json($settings);
    }

    /* Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getAllCms() {
        $cmslist = Cms::where('status', '=', 1)->where('position', '!=', '')->select('title', 'slug', 'position')->get()->toArray();

        $newCmsArray['data'] = array();
        foreach ($cmslist as $key => $value) {
            $newCmsArray['data'][$value['position']][] = $value;
        }
        return response()->json($newCmsArray);
    }

    /* Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getSlider() {
        $data = SliderImage::where('status', '=', 1)->select('title', 'image', 'description')->get()->toArray();
        foreach ($data as $value) {
            //$link = str_replace(WEBSITE_URL, "", $value['link']);
            $value['link'] = WEBSITE_URL;
            //$value['image'] = showImageUrlSLIDER_IMAGES_URL.$value['image'];
            $value['image'] = BasicFunction::showImageUrl(SLIDER_IMAGES_UPLOAD_DIRECTROY_PATH, SLIDER_IMAGES_ONTHEFLY_IMAGE_PATH, $value['image'], array('width' => '1368', 'height' => '550', 'zc' => 2, 'type' => 'banner'), true);
            $result['data'][] = $value;
        }
        return response()->json($result, 201);
    }

    public function getCmsDetail($slug) {

        if ($slug) {
            $cmslist = Cms::where('slug', '=', $slug)->get()->toArray();
            $newCmsArray['data'] = $cmslist;
            $newCmsArray['status'] = 'success';
            $status = 201;
        } else {
            $newCmsArray['data'] = array();
            $newCmsArray['status'] = 'error';
            $status = 500;
        }
        return response()->json($newCmsArray, $status);
    }

    public function getAllCategoryForMenu() {


        $categorylist = Category::with('children')->where('parent_id', '=', 0)->where('status', '=', 1)->orderBy('id', 'asc')->get()->toArray();
        $result = array();
        $i = 0;
        $j = 0;
        foreach ($categorylist as $key => $value) {
            $result['data'][Config::get('global.category_for_index.' . $value['category_for'])]['parent'][] = $value;
            $result['data'][Config::get('global.category_for_index.' . $value['category_for'])]['name'] = ucfirst($value['category_for']);
        }
        $result['settings'] = Config::get('settings');
        return response()->json($result);
    }

    public function getfaq() {


        $result['data'] = \App\FaqCategory::with('faqs')->where('status', '=', 1)->orderBy('order_key', 'asc')->get()->toArray();
        return response()->json($result);
    }

    function login(Request $request) {



        $input = $request->all();


        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required',
                        ]
        );

        if ($validator->errors()->all()) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['errors'] = $validator->errors();
            $data['message'] = "Please Enter Valid Input";
        } else {

            $email = $input['email'];
            $password = $input['password'];
            if (Auth::attempt(['email' => $email, 'password' => $password, 'role_id' => 2])) {
              
                $login_data = \App\User::where('email', '=', $email)->select('id', 'first_name', 'last_name', 'email', 'phone', 'gender','email_verify','status')->first()->toArray();
               if($login_data['email_verify']==1 && $login_data['status']==1){

                    $id = $login_data['id'];
                    $login_data['id'] = encode_value($login_data['id']);
                    //$token = encode_value(generateStrongPassword(15, '', 'lud'));
                    $token = encode_value($id);
                    \App\User::where('id', '=', $id)->update(['token' => $token]);

                    $login_data['token'] = $token;
                    $data['status_code'] = 1;
                    $data['status_text'] = 'Success';
                    $data['message'] = trans('admin.LOGIN_SUCCESSFULLY', ['site_tite' => Configure('CONFIG_SITE_TITLE'), 'Name' => $login_data['first_name']]);
                    $data['user_data'] = $login_data;

             }else{
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                
                if($login_data['email_verify']==0){

                    $data['message'] = 'Your email is not verify.Please verify your email.';
                }else{
                    if($login_data['status']==0){

                        $data['message'] = 'You are blocked by admin.Please contact to site admin.';
                    }
                }

             }



            } else {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'Incorrect email or password';
            }
        }


        return response()->json($data);
    }

    function singUp(Request $request) {

        $request_data = $request->all();
        $input = $request_data['data'];
        $input['phone'] = validPhoneNumber($input['phone']);
        $validator = validator::make($input, [
                    'first_name' => 'required|max:255',
                    'last_name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'phone' => 'required|phone|unique:users',
                    'password' => 'required',
                    'gender' => 'required',
                    'confirm_password' => 'required|same:password',
        ]); 

        if ($validator->errors()->all()) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['errors'] = $validator->errors();
            $data['message'] = "Please Enter Valid Input";
        } else {

            $activation_key = encode_value($input['email']);
            $input['password'] = Hash::make($input['password']);
            $input['role_id'] = Config::get('global.role_id.user');
            $input['activation_key'] = $activation_key;
            $input['status'] = 1;

            $user = \App\User::create($input);
            if (Configure('CONFIG_ENABLE_SMS')) {
                EmailHelper::sendSms($input['phone'], $user->id);
            }
            $email_template = \App\EmailTemplate::where('slug', '=', 'registration')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;

            $to = $user->email;
            $email_varified_link = URL::route('admin.activation', ['activation_key' => $activation_key]);
            $login_link = WEBSITE_URL . 'login';
            $body = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{EMAIL_ADDRESS}',
                '{LOGIN_LINK}',
                '{EMAIL_CONFIRMATION_LINK}'
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $user->email,
                $login_link,
                $email_varified_link,
                    ), $body);
            $subject = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{EMAIL_ADDRESS}',
                '{LOGIN_LINK}',
                '{EMAIL_CONFIRMATION_LINK}'
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $user->email,
                $login_link,
                $email_varified_link,
                    ), $subject);




            EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);
            $data['status_code'] = 1;
            $data['phone_verify'] = (int) Configure('CONFIG_ENABLE_SMS');
            $data['status_text'] = 'Success';
            $data['mobile_number'] = $input['phone'];
            $data['user_id'] = encode_value($user->id);
            $data['message'] = 'You have  successfully Registered.';
            $data['user_token'] = encode_value($user->id);
        }
        return response()->json($data);
    }

    function contactUs(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];
        
        $validator = validator::make($input, [
                    'name' => 'required|max:255',
                    'subject' => 'required|max:255',
                    'email' => 'required|email|max:255',
                    'phone_number' => 'required|phone',
                    'message' => 'required',
        ]);

        if ($validator->errors()->all()) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['errors'] = $validator->errors();
            $data['message'] = "Please Enter Valid Input";
        } else {

            $input['phone_number'] = validPhoneNumber($input['phone_number']);
            $conact = \App\Contact::create($input);






            $email_template = \App\EmailTemplate::where('slug', '=', 'registration')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;

            $to = Config::get('settings.CONFIG_STAFF_MAIL');

            $body = str_replace(array(
                '{NAME}',
                '{EMAIL}',
                '{SUBJECT}',
                '{MESSAGE}',
                '{PHONE_NUMBER}'
                    ), array(
                ucfirst($input['name']),
                $input['email'],
                $input['subject'],
                $input['message'],
                $input['phone_number'],
                    ), $body);
            $subject = str_replace(array(
                '{NAME}',
                '{EMAIL}',
                '{SUBJECT}',
                '{MESSAGE}',
                '{PHONE_NUMBER}'
                    ), array(
                ucfirst($input['name']),
                $input['email'],
                $input['subject'],
                $input['message'],
                $input['phone_number'],
                    ), $subject);





            EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);
            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['message'] = 'You have  successfully Registered.';
        }
        return response()->json($data);
    }

    public function sendPasswordLink(Request $request) {



        $inputData = $request->all();
        $input = $inputData['data'];
        // $input = $inputData;


        $validator = validator::make($input, [
                    'email_add' => 'required|email|max:255',
        ]);
        if ($validator->fails()) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['errors'] = $validator->errors();
            $data['message'] = "Please Enter Valid Input";
        }







        $email = $input['email_add'];
        $user = \App\User::where('email', '=', $email)->where('role_id', '=', 2)->first();


        if (empty($user->id)) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['errors'] = array("User Not exist.Please try with another email.");
            $data['message'] = "User Not exist.Please try with another email.";
        } else {
            $email_token = md5(uniqid(rand(), true));
            $email_token_exp = getCurrentTime() + (3600 * 24);

            $is_update = \App\User::where('id', $user->id)
                    ->update(['email_token' => $email_token, 'email_token_exp' => $email_token_exp]);


            
            $email_template = \App\EmailTemplate::where('slug', '=', 'forgot-password')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;
            $to = $user->email;
            $reset_link = WEBSITE_URL . 'reset-password/' . $email_token;
            $login_link = WEBSITE_URL . 'login';
            $body = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{EMAIL}',
                '{LOGIN_LINK}',
                '{PASSWORD_RESET_URL}'
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                ucfirst($user->email),
                $login_link,
                $reset_link,
                    ), $body);
            $subject = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{EMAIL}',
                '{LOGIN_LINK}',
                '{PASSWORD_RESET_URL}'
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                ucfirst($user->email),
                $login_link,
                $reset_link,
                    ), $subject);


            EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);

            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['message'] = 'Reset password  link successfully send to your mail box';
        }

        return response()->json($data);
    }

    function checkSocailLogin($token) {

        $result = \App\User::where('token', '=', $token)->select('id', 'first_name', 'last_name', 'email', 'phone', 'gender')->get()->toArray();

        if (!empty($result)) {
            $login_data = $result[0];
            $id = $login_data['id'];
            $login_data['id'] = encode_value($login_data['id']);
            //$token = encode_value(generateStrongPassword(15, '', 'lud'));
            $token = encode_value($id);

            \App\User::where('id', '=', $id)->update(['token' => $token]);

            $login_data['token'] = $token;
            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['message'] = trans('admin.LOGIN_SUCCESSFULLY', ['site_tite' => Configure('CONFIG_SITE_TITLE'), 'Name' => $login_data['first_name']]);
            $data['user_data'] = $login_data;
        } else {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Authentication failed.Please try again';
        }
        return response()->json($data);
    }

    function checkResetPassword($email_token) {
        if ($email_token) {


            $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 2)->first();
            if (empty($user->id)) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'Reset password link has been expired.';
            } else {

                /*                 * ** Check for email token expired ***** */
                $current_time_for_token = getCurrentTime();


                $updates_timestamp_for_token = $user->email_token_exp;


                if ($current_time_for_token > $updates_timestamp_for_token) {

                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'Reset password link has been expired.';
                } else {

                    $data['status_code'] = 1;
                    $data['status_text'] = 'Success';
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Reset password link has been expired.';
        }

        return response()->json($data);
    }

    function resetPasswordUpdate(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];

        $email_token = $input['token'];

        $validator = validator::make($input, [
                    'password' => 'required',
                    'confirm_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['errors'] = $validator->errors();
            $data['message'] = "Please enter valid input data";
        } else {
            $user = User::where('email_token', '=', $email_token)->where('role_id', '=', 2)->first();
            $password = Hash::make($input['password']);
            $is_update = User::where('id', $user->id)->update(['email_token' => '', 'password' => $password]);
            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['message'] = 'Account password changed successfully.';
        }
        return response()->json($data);
    }

    function getProductBycategory($slug, Request $request) {
        $inputData = $request->all();


        $input = $inputData['data'];


        if ($slug) {
            $category = \App\Category::where('slug', '=', $slug)->select('id', 'name', 'slug', 'image')->first();

            if (empty($category->id)) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'Category Not found.';
            } else {


                if (isset($inputData['sort_by']) && $inputData['sort_by'] != '') {
                    $sort_by = $inputData['sort_by'];
                } else {

                    $sort_by = 'popular';
                }


                $sort_field = Config::get('global.product_sort_field.' . $sort_by);
                $orderBy = Config::get('global.product_sort_order.' . $sort_by);
                $intersect = array();
                if (!empty($input)) {


                    $attribute = \App\Attribute::lists('id', 'slug')->toArray();
                    $attribute_value = \App\AttributeValue::lists('id', 'slug')->toArray();
                    $i = 0;
                    foreach ($input as $key => $value) {
                        $attrObject = new ProductAttributeValue();
                        $attrObject = $attrObject->where('attribute_id', $attribute[$value['attribute']]);
                        $orThose = array();
                        $attrData = explode(',', $value['data']);
                        $orThose['where_condition'] = array('attribute_id', $attribute[$value['attribute']]);
                        foreach ($attrData as $attr_slug) {
                            $orThose['data'][] = array('attribute_value_id', $attribute_value[$attr_slug]);
                        }
                        if (!empty($orThose)) {
                            $attrObject = $attrObject->where(function($query) use ($orThose) {
                                foreach ($orThose['data'] as $key => $value) {
                                    $query = $query->orWhere($value[0], $value[1]);
                                }

                                return $query;
                            });
                        }
                        if ($i == 0) {
                            $intersect = $attrObject->distinct('product_id')->lists('product_id')->toArray();
                            ;
                        } else {
                            $intersect = array_intersect($intersect, $attrObject->distinct('product_id')->lists('product_id')->toArray());
                        }
                        $i++;
                    }
                }




                if (!empty($input) && empty($intersect)) {
                    $productslist = array();
                } else {

                    $productslistObj = \App\Product::with("getImage")->where('category_id', '=', $category->id)->where('status', '=', 1);

                    if (!empty($input) && !empty($intersect)) {

                        $productslistObj = $productslistObj->whereIn('id', $intersect);
                    }



                    $productslist = $productslistObj->orderBy($sort_field, $orderBy)->paginate(Configure('CONFIG_FRONT_PAGE_LIMIT'))->toArray();
                }

                if ($productslist) {
                    $products = $productslist;

                    foreach ($products['data'] as $key => $result) {

                        //$products['data'][$key]['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH, $result['get_image']['name'], array('width' => '214', 'height' => '272', 'zc' => 2), false);

                        $products['data'][$key]['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $result['get_image']['name'], array('width' => T540_WIDTH, 'height' => '650', 'zc' => 0, 'cc' => 'fff'), THUMB);
                        $products['data'][$key]['price'] = display_price($result['price']);
                        $products['data'][$key]['description'] = str_limit($result['description'], 100, '...');
                        $products['data'][$key]['id'] = encode_value($result['id']);

                        unset($products['data'][$key]['get_image']);
                    }


                    $data['products'] = $products;
                } else {


                    $data['products'] = array('total' => 0, 'per_page' => Configure('CONFIG_FRONT_PAGE_LIMIT'), 'current_page' => 0, 'last_page' => 1, 'from' => 0, 'to' => 1, 'data' => array());
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Requested url is invalid.';
        }

        return response()->json($data);
    }

    function getCategoryAttribute($slug) {


        if ($slug) {
            $category = \App\Category::where('slug', '=', $slug)->select('id', 'name', 'slug', 'image')->first();

            if (empty($category->id)) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'Category Not found.';
            } else {
                $category->image = BasicFunction::showImageUrl(CATEGORY_IMAGES_UPLOAD_DIRECTROY_PATH, CATEGORY_IMAGES_ONTHEFLY_IMAGE_PATH, $category->image, array('height' => '412', 'width' => '1356', 'zc' => 2, 'type' => 'banner'), true);
                $data['category'] = $category->toArray();

                $category_attribute = \App\CategoryAttribute::where('category_id', '=', $category->id)->lists('attribute_id', 'attribute_id')->toArray();

                $attribute = \App\Attribute::with("AttributeValue")->whereIn('id', $category_attribute)->where('status', '=', 1)->select('id', 'name', 'slug')->orderBy('order_key', 'asc')->get();
                if (!$attribute->isEmpty()) {
                    $data['filter'] = $attribute->toArray();
                } else {
                    $data['filter'] = array();
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Requested url is invalid.';
        }

        return response()->json($data);
    }

    function getProductDetail(Request $request) {
        $input = $request->all();

        $slug = $input['slug'];

        if ($slug) {
            $products = \App\Product::with('getImages','category')->where('slug', '=', $slug)->get()->toArray();
            if ($products) {

                $product = $products[0];

                $product['in_wishlist'] = 0;

                if ($input['userId'] != '') {

                    $input['userId'] = decode_value($input['userId']);

                    $productWishlisted = ProfileWishlist::where('user_id', '=', $input['userId'])->lists('product_id')->toArray();

                    if (in_array($product['id'], $productWishlisted)) {

                        $product['in_wishlist'] = 1;
                    }
                }

                \App\Product::find($product['id'])->increment('popular');
                $attributedata = \App\ProductAttributeValue::with('attribute_value')->where('product_id', '=', $product['id'])->where('attribute_id', Config::get('global.size_id'))->get()->toArray();
                $attribute = array();
                foreach ($attributedata as $key => $value) {
                    unset($value['attribute_value']['id']);
                    $attribute[] = $value['attribute_value'];
                }
                $limit = Configure('CONFIG_RELATED_PRODUCT_LIMIT');
                $images = array();
                $thumbnail = array();
                if (!empty($product['get_images'])) {
                    foreach ($product['get_images'] as $key => $image) {
                        $thumbnail[] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T130, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T130, $image['name'], array('width' => T130_WIDTH, 'height' => '112', 'zc' => 0, 'cc' => 'fff'), THUMB);
                        $images[$key]['orignail_image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH, $image['name'], array('width' => '540', 'zc' => 0, 'cc' => 'fff'), true);
                        $images[$key]['name'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $image['name'], array('width' => T540_WIDTH, 'height' => '650', 'zc' => 0, 'cc' => 'fff'), THUMB);
                    }
                    $data['no_images'] = "";
                } else {

                    $data['no_images'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH, '', array('width' => T540_WIDTH, 'height' => '650','zc' => 0, 'cc' => 'fff'), false);
                }


                $data['images'] = $images;
                $data['size'] = $attribute;
                $data['thumbnail'] = $thumbnail;
                $data['category'] = $product['category'];

                                unset($product['get_images']);
                unset($product['category']);
                $product['price'] = display_price($product['price']);
                $product_id = $product['id'];
                $product['id'] = encode_value($product['id']);
                $data['product'] = $product;
                $data['attribute'] = $attribute;
                $related_product = \App\Product::with("getImage")->where('category_id', '=', $product['category_id'])->where('id', '!=', $product_id)->where('status', '=', 1)->orderBy('popular', 'desc')->select('id', 'slug', 'title')->limit($limit)->get()->toArray();
                if (empty($related_product)) {
                    $parent_category = Category::find($product['category_id']);
                    $related_product = \App\Product::with("getImage")->where('category_id', '=', $parent_category->parent_id)->where('id', '!=', $product_id)->where('status', '=', 1)->orderBy('popular', 'desc')->select('id', 'slug', 'title')->limit($limit)->get()->toArray();

                    if (empty($related_product)) {
                        $related_product = \App\Product::with("getImage")->where('id', '!=', $product_id)->where('status', '=', 1)->orderBy('popular', 'desc')->select('id', 'slug', 'title')->limit($limit)->get()->toArray();
                    }
                }
                $related_product_data = array();

                if (!empty($related_product)) {

                    foreach ($related_product as $key => $result) {

                        $related_product_data[$key]['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $result['get_image']['name'], array('width' => '350', 'zc' => 2,'height' => '450'), THUMB);
                        $related_product_data[$key]['slug'] = $result['slug'];
                        $related_product_data[$key]['title'] = $result['title'];
                    }
                }



                $data['related_product'] = $related_product_data;
                $data['status_text'] = 'Success';
                $data['message'] = '';
            } else {

                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'Requested url is invalid.';
            }
            // / dd($products);
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Requested url is invalid.';
        }


        return response()->json($data);
    }

    function getUserDetail($id = '', $token = '') {
        if (!empty($token) && !empty($id)) {
            $id = decode_value($id);

            $user = User::where('token', '=', $token)->where('id', '=', $id)->where('role_id', '=', 2)->select('id', 'first_name', 'last_name', 'email', 'phone', 'gender', 'phone_verify')->first();
            if (empty($user->id)) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'User Token not match';
            } else {
                $user_data = $user->toArray();
                $user_data['id'] = encode_value($user_data['id']);
                $data['status_code'] = 1;
                $data['status_text'] = 'Success';
                $data['enble_sms'] = Configure('CONFIG_ENABLE_SMS');
                $data['user_data'] = $user_data;
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }  

     function getUserDetails($id = '', $token = '') {
        if (!empty($token) && !empty($id)) {
            $id = decode_value($id);

            $user = User::where('token', '=', $token)->where('id', '=', $id)->where('role_id', '=', 2)->select('id', 'first_name', 'last_name', 'email', 'phone', 'gender', 'phone_verify','password','facebook_id','google_id')->first();
            if (empty($user->id)) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'User Token not match';
            } else {
                $is_socail = 0;
                if(($user->facebook_id !='' || $user->google_id !='' ) && $user->password==''){
                        $is_socail = 1;

                }

                
                
                $data['status_code'] = 1;
                $data['is_socail'] = $is_socail;                
                $data['status_text'] = 'Success';                
                
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    /* Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getAllBlock() {
        $block = \App\Block::with("category")->select('description', 'title', 'title_2', 'title_3', 'position', 'image', 'category_id')->get()->toArray();
        $newData = array();
        foreach ($block as $key => $value) {
            $value['slug'] = $value['category']['slug'];
            $value['image'] = BasicFunction::showImageUrl(BLOCKS_IMAGES_UPLOAD_DIRECTROY_PATH, BLOCKS_IMAGES_ONTHEFLY_IMAGE_PATH, $value['image'], array('height' => '712', 'width' => '285', 'zc' => 2, 'type' => 'banner'), true);
            ;
            unset($value['category']);
            $newData[$value['position']] = $value;
        }

        return response()->json($newData);
    }

    function getUserAddressBook($id = '', $token = '') {
        if (!empty($token) && !empty($id)) {
            $id = decode_value($id);

            $address_book = \App\AddressBook::where('user_id', '=', $id)->select('id', 'first_name', 'last_name', 'address_1', 'address_2', 'pin_code', 'city', 'state', 'mobile', 'is_default')->orderBy('is_default', 'desc')->get()->toArray();
            foreach ($address_book as $key => $value) {
                $address_book[$key]['id'] = encode_value($value['id']);
            }

            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['data'] = $address_book;
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Reset password link has been expired.';
        }

        return response()->json($data);
    }

    function saveUserAddress(Request $request) {
        $inputData = $request->all();
        $input = $inputData['data'];
        $token = $input['token'];
        $id = $input['user_id'];
        $input['mobile'] = validPhoneNumber($input['mobile']);
        if (!empty($token) && !empty($id)) {

            $validator = validator::make($input, [
                        'first_name' => 'required|max:255',
                        'last_name' => 'required|max:255',
                        'address_1' => 'required|max:255',
                        'address_2' => 'required|max:255',
                        'pin_code' => 'required|max:6',
                        'city' => 'required|max:255',
                        'state' => 'required|max:255',
                        'mobile' => 'required|phone',
            ]);

            if ($validator->errors()->all()) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['error'] = $validator->errors();
                $data['message'] = "Please enter valid input data";
            } else {
                $id = decode_value($id);
                $user = User::findOrFail($id);
                if (empty($user->id)) {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'User not found';
                } else {
                    $dataValues = array();
                    $dataValues['first_name'] = $input['first_name'];
                    $dataValues['last_name'] = $input['last_name'];
                    $dataValues['address_1'] = $input['address_1'];
                    $dataValues['address_2'] = $input['address_2'];
                    $dataValues['pin_code'] = $input['pin_code'];
                    $dataValues['city'] = $input['city'];
                    $dataValues['state'] = $input['state'];
                    $dataValues['mobile'] = $input['mobile'];
                    $dataValues['user_id'] = $id;
                    $address_book = \App\AddressBook::create($dataValues);


                    $address_books = \App\AddressBook::where('id', '=', $address_book->id)->select('id', 'first_name', 'last_name', 'address_1', 'address_2', 'pin_code', 'city', 'state', 'mobile', 'is_default')->orderBy('is_default', 'desc')->first()->toArray();

                    $address_books['id'] = encode_value($address_books['id']);


                    $data['status_code'] = 1;
                    $data['status_text'] = 'Success';
                    $data['data'] = $address_books;
                    $data['message'] = "Address added successfully.";
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function setAddressDefault(Request $request) {
        $inputData = $request->all();
        $input = $inputData['data'];
        $address_id = $input['address_id'];
        $user_id = $input['user_id'];

        if (!empty($address_id) && !empty($user_id)) {


            $id = decode_value($address_id);
            $user_id = decode_value($user_id);

            \App\AddressBook::where('user_id', '=', $user_id)->update(['is_default' => 0]);

            $address_books = \App\AddressBook::where('id', '=', $id)->first();
            $address_books->is_default = 1;
            $address_books->save();

            $address_books = \App\AddressBook::where('user_id', '=', $user_id)->select('id', 'first_name', 'last_name', 'address_1', 'address_2', 'pin_code', 'city', 'state', 'mobile', 'is_default')->orderBy('is_default', 'desc')->get()->toArray();
            foreach ($address_books as $key => $value) {
                $address_books[$key]['id'] = encode_value($value['id']);
            }

            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['data'] = $address_books;
            $data['message'] = 'Address Set default successfully.';
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function deleteAddress($id, $user_id) {
        if (!empty($id) && !empty($user_id)) {
            $id = decode_value($id);
            $user_id = decode_value($user_id);

            $address_books = \App\AddressBook::where('id', '=', $id)->where('user_id', '=', $user_id)->first();
            if ($address_books->id) {


                $addreas = \App\AddressBook::find($id)->delete();

                $address_book = \App\AddressBook::where('user_id', '=', $user_id)->select('id', 'first_name', 'last_name', 'address_1', 'address_2', 'pin_code', 'city', 'state', 'mobile', 'user_id', 'is_default')->orderBy('is_default', 'desc')->get()->toArray();
                foreach ($address_book as $key => $value) {
                    $address_book[$key]['id'] = encode_value($value['id']);
                }

                $data['status_code'] = 1;
                $data['status_text'] = 'Success';
                $data['data'] = $address_book;
                $data['message'] = 'Address deleted successfully';
            } else {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'Address deleted error';
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Requested Url is invalid';
        }

        return response()->json($data);
    }

    function updateUserAddress(Request $request) {
        $inputData = $request->all();
        $input = $inputData['data'];
        $token = $input['token'];
        $id = $input['id'];
        $user_id = $input['user_id'];

        if (!empty($token) && !empty($id)) {

            $validator = validator::make($input, [
                        'edit_first_name' => 'required|max:255',
                        'edit_last_name' => 'required|max:255',
                        'edit_address_1' => 'required|max:255',
                        'edit_address_2' => 'required|max:255',
                        'edit_pin_code' => 'required|max:6',
                        'edit_city' => 'required|max:255',
                        'edit_state' => 'required|max:255',
                        'edit_mobile' => 'required|phone',
            ]);

            if ($validator->errors()->all()) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['errors'] = $validator->errors();
                $data['message'] = "Please Enter Valid Input";
            } else {
                $user_id = decode_value($user_id);

                $user = User::find($user_id);
                if (empty($user->id)) {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'User not found';
                } else {
                    $id = decode_value($id);
                    $dataValues = array();
                    $dataValues['first_name'] = $input['edit_first_name'];
                    $dataValues['last_name'] = $input['edit_last_name'];
                    $dataValues['address_1'] = $input['edit_address_1'];
                    $dataValues['address_2'] = $input['edit_address_2'];
                    $dataValues['pin_code'] = $input['edit_pin_code'];
                    $dataValues['city'] = $input['edit_city'];
                    $dataValues['state'] = $input['edit_state'];
                    $dataValues['mobile'] = $input['edit_mobile'];
                    $addressObj = \App\AddressBook::findOrFail($id);
                    if ($addressObj->user_id = $user_id) {

                        $addressObj->fill($dataValues)->save();
                        $address_book = \App\AddressBook::where('user_id', '=', $user_id)->select('id', 'first_name', 'last_name', 'address_1', 'address_2', 'pin_code', 'city', 'state', 'mobile', 'user_id', 'is_default')->orderBy('is_default', 'desc')->get()->toArray();
                        foreach ($address_book as $key => $value) {
                            $address_book[$key]['id'] = encode_value($value['id']);
                        }

                        $data['status_code'] = 1;
                        $data['status_text'] = 'Success';
                        $data['data'] = $address_book;
                        $data['message'] = 'Address updated successfully';
                    } else {
                        $data['status_code'] = 0;
                        $data['status_text'] = 'Failed';
                        $data['message'] = 'Address not matched to the user';
                    }
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function changePassword(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];

        $token = $input['token'];
        $id = $input['user_id'];
        $id = decode_value($id);
        $user = \App\User::where('id', '=', $id)->first();
        $validationArray = array();
        $validationArray['password'] ='required';
        $validationArray['confirm_password'] ='required|same:password';

           if(($user->facebook_id !='' || $user->google_id !='' ) && $user->password==''){
                        $is_socail = 1;

                }else{
                        $validationArray['old_password'] ='required|OldPasswordCheck:' . $user->password;
                }


                

        $validator = validator::make($input, $validationArray);

        if ($validator->fails()) {
            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['error'] = $validator->errors();
            $data['message'] = "Please enter valid input data";
        } else {

            $password = Hash::make($input['password']);
            $is_update = \App\User::where('id', $id)->update(['password' => $password]);
            $data['status_code'] = 1;
            $data['status_text'] = 'Success';
            $data['message'] = 'Password updated successfully.';
        }
        return response()->json($data);
    }

    function updateProfile(Request $request) {
        $inputData = $request->all();


        $input = $inputData['data'];

        $token = $input['token'];
        $id =$input['id'];
         $id = decode_value($id);
        if (!empty($token) && !empty($id)) {
              $user_data = User::where('id', '=', $id)->where('role_id', '=', 2)->select('id', 'first_name', 'last_name', 'email', 'phone', 'gender','phone_verify')->first();

              
              $validationArray = array();
              
               $validationArray = array('first_name' => 'required|max:255',
                        'last_name' => 'required|max:255',
                        'gender' => 'required');
                if($user_data->phone =='' &&  $input['phone']!=''){
                    $input['phone'] = validPhoneNumber($input['phone']);
                    $validationArray['phone'] = 'required|phone|unique:users';
                    
                }
               
              $validator = validator::make($input,$validationArray);
              

            if ($validator->errors()->all()) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['errors'] = $validator->errors();
                $data['message'] = "Please Enter Valid Input";
            } else {
                $new_number = 0;
               
                $user = User::findOrFail($id);
                if (empty($user->id)) {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'User not found';
                } else {
                    $dataValues = array();
                    $dataValues['first_name'] = $input['first_name'];
                    $dataValues['last_name'] = $input['last_name'];
                    $dataValues['gender'] = $input['gender'];
                    if($user_data->phone=='' &&  $input['phone']!=''){
                        $dataValues['phone'] = $input['phone'];    
                        $new_number = 1;
                    }
                    $user->fill($dataValues)->save();
                    $user_data = User::where('id', '=', $id)->where('role_id', '=', 2)->select('id', 'first_name', 'last_name', 'email', 'phone', 'gender','phone_verify','token')->first();
                    if($new_number==1){
                        if (Configure('CONFIG_ENABLE_SMS')) {
                            EmailHelper::sendSms($input['phone'], $user_data->id);
                        }else{
                            $new_number=0;
                        }
                        
                    }
                    $user_data = $user_data->toArray();
                    $user_data['id'] = encode_value($user_data['id']);
                    $data['status_code'] = 1;
                    $data['new_number'] = $new_number;
                    $data['status_text'] = 'Success';
                    $data['message'] = 'User profile update successfully.';
                    $data['user_data'] = $user_data;
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function VarifyNumber(Request $request) {


        $inputData = $request->all();
        $input = $inputData['data'];

        $id = $input['user'];

        if (!empty($id)) {

            $validator = validator::make($input, [
                        'otp' => 'required',
            ]);

            if ($validator->errors()->all()) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['errors'] = $validator->errors();
                $data['message'] = "Please Enter Valid Input";
            } else {
                $id = decode_value($id);
                $user = User::find($id);
                if (empty($user->id)) {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'User not found';
                } else {
                    if ($user->phone_verify == 0) {

                        $last_otp = \App\userOtp::where("user_id", $id)->orderBy('created_at', 'desc')->first();

                        if (!empty($last_otp->id) && ($last_otp->otp == $input['otp'])) {
                            if (time() < $last_otp->expire) {
                                $last_otp->expire = '';
                                $last_otp->save();


                                $user->phone_verify = 1;
                                $user->save();
                                $data['status_code'] = 1;
                                $data['status_text'] = 'Success';
                                $data['message'] = 'Mobile Number Verified successfully';
                            } else {

                                $data['status_code'] = 0;
                                $data['status_text'] = 'Failed';
                                $data['message'] = 'OTP password expired.Please click on resend OTP link';
                            }
                        } else {
                            $data['status_code'] = 0;
                            $data['status_text'] = 'Failed';
                            $data['message'] = 'OTP password not matched';
                        }
                    } else {

                        $data['status_code'] = 0;
                        $data['status_text'] = 'Failed';
                        $data['message'] = 'You already Verified you number';
                    }
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function UpdatePhoneNumber(Request $request) {


        $inputData = $request->all();
        $input = $inputData['data'];

        $id = $input['user'];
        $input['phone'] = $input['phone_number'];
        unset($input['phone_number']);
        if (!empty($id)) {
            $input['phone'] = validPhoneNumber($input['phone']);
            $validator = validator::make($input, [
                        'phone' => 'required|phone|unique:users'
            ]);

            if ($validator->errors()->all()) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['error'] = $validator->errors();
                $data['message'] = "Please Enter Valid Input";
            } else {
                $id = decode_value($id);
                $user = User::find($id);

                if (empty($user->id)) {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'User not found';
                } else {
                    if ($user->phone == '') {

                        $user->phone = $input['phone'];
                        $user->save();
                        if (Configure('CONFIG_ENABLE_SMS')) {
                            EmailHelper::sendSms($input['phone'], $user->id);
                        }
                        $data['status_code'] = 1;
                        $data['status_text'] = 'Success';
                        $data['message'] = 'Mobile Number updated successfully';
                        $data['phone_verify'] = (int) Configure('CONFIG_ENABLE_SMS');
                        $data['mobile_number'] = $input['phone'];
                    } else {


                        $data['status_code'] = 0;
                        $data['status_text'] = 'Failed';
                        $data['message'] = 'You already update you mobile number';
                    }
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function resendOtpPassword($user_id) {

        $id = $user_id;
        if (!empty($id)) {
            $id = decode_value($id);
            $user = User::find($id);

            if (empty($user->id)) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'User not found';
            } else {

                EmailHelper::sendSms($user->phone, $user->id);
                $data['status_code'] = 1;
                $data['status_text'] = 'Success';
                $data['message'] = 'OTP Send successfully.';
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function socialSignup(Request $request, $type) {

        $inputData = $request->all();
        $input = $inputData['data'];

        $check_user_data = \App\User::where('email', '=', $input['email'])->select('id', 'first_name', 'last_name', 'email', 'phone', 'gender')->first();


        $new_token = encode_value(generateStrongPassword(15));

        if (empty($check_user_data)) {

            $data = array();
            if ($type == 'facebook') {

                $data['facebook_id'] = $input['id'];
            } else {
                $data['google_id'] = $input['id'];
            }
            $data['email'] = (isset($input['email'])) ? $input['email'] : '';
            $data['first_name'] = (isset($input['name'])) ? $input['name'] : '';
            $data['role_id'] = Config::get('global.role_id.user');
            $data['gender'] = (isset($user_data->user['gender'])) ? $user_data->user['gender'] : '';

            $data['token'] = $new_token;
            $data['status'] = 1;
            $data['email_verify'] = 1;
            $user = \App\User::create($data);
            $login_data = \App\User::where('id', '=', $user->id)->select('id', 'facebook_id', 'google_id', 'first_name', 'last_name', 'email', 'phone', 'gender', 'phone_verify')->first()->toArray();
        } else {
            $login_data = \App\User::where('id', '=', $check_user_data["id"])->select('id', 'facebook_id', 'google_id', 'first_name', 'last_name', 'email', 'phone', 'gender', 'phone_verify')->first()->toArray();
        }


        $id = $login_data['id'];
        $login_data['id'] = encode_value($login_data['id']);
        //$token = encode_value(generateStrongPassword(15, '', 'lud'));
        $token = encode_value($id);
        $updateArray = array();
        $updateArray['token'] = $token;
        if ($type == 'facebook' && $login_data['facebook_id'] == '') {

            $updateArray['facebook_id'] = $input['id'];
            $login_data['facebook_id'] = $input['id'];
        }

        if ($type == 'google' && $login_data['google_id'] == '') {
            $updateArray['google_id'] = $input['id'];
            $login_data['google_id'] = $input['id'];
        }


        \App\User::where('id', '=', $id)->update($updateArray);
        $login_data['token'] = $token;
        $result = array();
        $result['status_code'] = 1;
        $result['status_text'] = 'Success';
        $result['message'] = trans('admin.LOGIN_SUCCESSFULLY', ['site_tite' => Configure('CONFIG_SITE_TITLE'), 'Name' => $login_data['first_name']]);
        ;
        $result['user_data'] = $login_data;

        return response()->json($result);
    }

    function addToCart(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];
        $product_id = decode_value($input['product_id']);
        $user_id = decode_value($input['user_id']);
        $size = $input['product_size'];

        $attributedata = \App\AttributeValue::where('slug', $size)->first();

        $size_id = $attributedata->id;

        $sockData =   \App\Stock::where('size_id', $size_id)->where('product_id',$product_id)->first();

        if(!empty($sockData) && $sockData->quantity >0){
            $check_data = \App\Cart::where('product_id', '=', $product_id)->where('user_id', '=', $user_id)->where('size', '=', $size)->first();

            if (empty($check_data)) {


                $dataSave['product_id'] = $product_id;
                $dataSave['user_id'] = $user_id;

                $dataSave['size'] = $size;
                $cart = \App\Cart::create($dataSave);
                $result['status_code'] = 1;
                $result['status_text'] = 'Success';
                $result['message'] = 'Item added successfully.';
            } else {
                $result['status_code'] = 0;
                $result['status_text'] = 'Failed';
                $result['message'] = 'Product already in bag.';
            }

        }else{

            $result['status_code'] = 2;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Product out off stock';
        }


        return response()->json($result);
    }

    function getCartProduct($user_id) {

        $user_id = decode_value($user_id);
        $all_data = \App\Cart::where('user_id', '=', $user_id)->get()->toArray();
        $data = array();
        foreach ($all_data as $key => $value) {

            $data[$key]['product_id'] = encode_value($value['product_id']);
            $data[$key]['user_id'] = encode_value($value['user_id']);
            $data[$key]['product_size'] = $value['size'];
        }

        $result['data'] = $data;
        return response()->json($result);
    }

    function getCartProductItem($user_id, $coupan_code = '') {

        $user_id = decode_value($user_id);
        $result = $this->getCartData($user_id, $coupan_code);
        $result['status_code'] = 1;
        $result['status_text'] = 'Success';

        return response()->json($result);
    }

    function getCartData($user_id, $coupan_code = '') {


        $all_data = \App\Cart::where('user_id', '=', $user_id)->get()->toArray();
        $data = array();
        $total_price = 0;
        $shipping_charge = 0;
        $i = 0;
        $couponCodeResult = array();
        $couponCodeApply = 0;
        $discount_on = '';
        $product_ids = array();
        if ($coupan_code) {
            $couponCodeResult = $this->checkCouponCode(encode_value($user_id), $coupan_code, 1);
            if (!empty($couponCodeResult) && $couponCodeResult['status_code'] == 1) {
                $couponCodeApply = 1;

                $discount_on = $couponCodeResult['discount_on'];
                if ($discount_on == 'product') {

                    $product_ids = $couponCodeResult['product_ids'];
                }
            }
        }

        $product_max_price = 0;
        $total_product = 0;
         $attributedataValue = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('id', 'slug')->toArray();
         $stock_error =0;
        foreach ($all_data as $key => $value) {

            $data['products'][$i]['id'] = encode_value($value['id']);
            $data['products'][$i]['product_id'] = encode_value($value['product_id']);
            $data['products'][$i]['user_id'] = encode_value($value['user_id']);
            $data['products'][$i]['product_size'] = $value['size'];
            $data['products'][$i]['quantity'] = $value['quantity'];
       
            $sockData =   \App\Stock::where('size_id',$attributedataValue[$value['size']])->where('product_id',$value['product_id'])->first();
            $inStock  = 0;
            $stock  = 0;
             if(!empty($sockData)){
                        if($sockData->quantity >0){

                                $inStock = 1;
                                $stock = $sockData->quantity;

                                if($sockData->quantity < $value['quantity']){
                                    $stock_error=1;  

                                }

                        }else{
                                    
                            $inStock = 0;
                            $stock = 0;  
                            $stock_error=1;                          
                        }

             }else{

                $inStock = 0;
                $stock = 0;
                $stock_error = 1;

             }






             $data['products'][$i]['in_stock'] = $inStock;
             $data['products'][$i]['stock'] = $stock;
            $data['products'][$i]['product'] = \App\Product::with("getImage")->where('id', '=', $value['product_id'])->select('id', 'slug', 'title', 'price', 'shipping_charge', 'short_description')->first()->toArray();
            $data['products'][$i]['product']['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $data['products'][$i]['product']['get_image']['name'], array('width' => '540', 'zc' => 2), THUMB);

            $total_price = $total_price + ($value['quantity'] * $data['products'][$i]['product']['price']);
            $productPrice = $value['quantity'] * $data['products'][$i]['product']['price'];
            $total_product = $total_product + $value['quantity'];


            if ($couponCodeApply == 1 && $discount_on == 'product') {
                $couponDetail = $couponCodeResult['coupons'];
                if (in_array($value['product_id'], $product_ids) && $couponDetail->max_product_quantity >= $value['quantity']) {

                    $productPrice = $value['quantity'] * $data['products'][$i]['product']['price'];

                    if ($product_max_price < $productPrice) {

                        $product_max_price = $productPrice;
                    }
                }
            }

          //  $shipping_charge = $shipping_charge + ($value['quantity'] * $data['products'][$i]['product']['shipping_charge']);
            unset($data['products'][$i]['product']['get_image']);
            $data['products'][$i]['product']['price'] = display_price($data['products'][$i]['product']['price']);
            $data['products'][$i]['product']['short_description'] = str_limit($data['products'][$i]['product']['short_description'], 100, '');

            $attributedata = \App\ProductAttributeValue::with('attribute_value')->where('product_id', '=', $value['product_id'])->where('attribute_id', Config::get('global.size_id'))->get()->toArray();
            $attribute = array();
            foreach ($attributedata as $attrkey => $attrvalue) {
                unset($attrvalue['attribute_value']['id']);
                $attribute[] = $attrvalue['attribute_value'];
            }
            $data['products'][$i]['product']['size'] = $attribute;

            $i++;
        }
                     

        $enable_cashback = Configure('CONFIG_ENABLE_CASHBACK');

        if ($enable_cashback) {

            $enable_cashback = 1;
        } else {
            $enable_cashback = 0;
        }

        $result['data'] = $data;
        $result['data']['total_price'] = display_price($total_price);
        $result['data']['enable_cashback'] = $enable_cashback;
        if ($enable_cashback) {

            $result['data']['coupon_code'] = Configure('CONFIG_COUPON_CODE');
            $result['data']['coupon_description'] = Configure('CONFIG_COUPON_DESCRIPTION');
            $min_bill_amount = Configure('CONFIG_MIN_BILL_AMOUNT');
            $max_cashback_amount = Configure('CONFIG_MAX_CASHBACK_AMOUNT');
            $cash_back_amount = 0;
            if ($total_price >= $min_bill_amount) {
                $cashback_type = Configure('CONFIG_CASHBACK_TYPE');
                if ($cashback_type == 'percentage') {
                    $percentage = Configure('CONFIG_PERCENTAGE');
                    $ckamount = ($total_price * $percentage) / 100;
                    if ($ckamount > $max_cashback_amount) {
                        $cash_back_amount = $max_cashback_amount;
                    } else {

                        $cash_back_amount = $ckamount;
                    }
                } else {
                    $amount = Configure('CONFIG_AMOUNT');
                    $cash_back_amount = $amount;
                }
            }
            if ($cash_back_amount == 0) {


                $result['data']['enable_cashback'] = 0;
            }
            $cash_back_amount = cashback_round($cash_back_amount);
            $result['data']['cash_back_amount'] = display_price($cash_back_amount);
        } else {
            $result['data']['coupon_code'] = '';
            $result['data']['coupon_description'] = '';
            $result['data']['cash_back_amount'] = 0;
        }
        $payble_amount = $total_price;
        $total_tax = 0;
        $taxArray = \App\Tax::where('status', 1)->get()->toArray();
        if (!empty($taxArray)) {
            $result['data']['enable_tax'] = 1;
            $k = 0;
            foreach ($taxArray as $taxKey => $taxValue) {
                $result['data']['tax'][$k]['name'] = ucfirst($taxValue['title']);
                $rate = $taxValue['tax_rate'];
                $tax = round_value($total_price * $rate / 100);
                $result['data']['tax'][$k]['amount'] = display_price($tax);
                $total_tax = $total_tax + $tax;

                $k++;
            }
        } else {
            $result['data']['enable_tax'] = 0;
        }
        $discountAmount = 0;
        if ($couponCodeApply == 1) {

            $couponDetail = $couponCodeResult['coupons'];

            if ($discount_on == 'total_price') {
                $product_max_price = $total_price;
            }

            if ($couponDetail->discount_type == 'percentage') {

                $discountAmount = cashback_round(($product_max_price * $couponDetail->percentage) / 100);
                if ($discountAmount > $couponDetail->max_discount) {

                    $discountAmount = $couponDetail->max_discount;
                }
            } else {

                $discountAmount = $couponDetail->amount;
            }




            if ($discountAmount > 0) {
                $result['data']['enable_cashback'] = 0;
                $result['data']['cash_back_amount'] = 0;
                $result['data']['enable_discount'] = 1;
                $result['data']['discount'] = display_price($discountAmount);
                $result['data']['coupon_code'] = $coupan_code;
            } else {
                $result['data']['enable_discount'] = 0;
                $result['data']['discount'] = 0;
            }



            $result['data']['coupon_code_error'] = '';
        } else {
            $result['data']['enable_discount'] = 0;
            $result['data']['discount'] = 0;
            if (!empty($couponCodeResult)) {

                $result['data']['coupon_code_error'] = $couponCodeResult['message'];
            }
        }



        if($total_price<Configure('CONFIG_MIN_TOTAL_AMOUNT_FOR_SHIPPING')){

           $shipping_charge = Configure('CONFIG_SHIPPING_CHARGES');
        }





        $result['data']['total_tax'] = display_price($total_tax);
        $result['data']['expected_delivery_date'] = date(EXP_DATE_FORMATE, expected_delivery_date());
        if ($shipping_charge == 0) {
            $result['data']['enable_shipping_charge'] = 0;
        } else {

            $result['data']['enable_shipping_charge'] = 1;
        }
        $result['data']['shipping_charge'] = display_price($shipping_charge);

        $result['data']['payble_amount'] = display_price($total_price + $total_tax + $shipping_charge - $discountAmount);
        $result['data']['stock_error'] =$stock_error;
        if (Configure('CONFIG_ENABLE_COD')) {
            $cod_money = 0;
            if($total_price<Configure('CONFIG_MIN_TOTAL_AMOUNT_FOR_COD')){

               $cod_money = Configure('CONFIG_COD_CHARGE');
            }

            
            if($cod_money>0){

                  $result['data']['is_cod_money'] = 1; 
            }

            $result['data']['cod_money'] = display_price($cod_money);
 
            $result['data']['cod_payble_amount'] = display_price($total_price + $total_tax + $shipping_charge - $discountAmount + $cod_money);
        }
        
        return $result;
    }

    function updateCart(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];
        $id = decode_value($input['cart_id']);
        $product_id = decode_value($input['product_id']);
        $user_id = decode_value($input['user_id']);
        $size = $input['product_size'];
        $quantity = $input['quantity'];
        $flag = $input['flag'];
        $coupan_code = $input['coupan_code'];



        $check_data = \App\Cart::where('id', '=', $id)->where('product_id', '=', $product_id)->where('user_id', '=', $user_id)->first();



        if (!empty($check_data)) {

            if ($flag == 'size') {
                $isInCart = \App\Cart::where('product_id', '=', $product_id)->where('user_id', '=', $user_id)->where('size', '=', $size)->first();
                if (empty($isInCart)) {


                    $cart = \App\Cart::where('id', '=', $id)->update(['size' => $size]);

                    $result = $this->getCartData($user_id, $coupan_code);
                    $result['status_code'] = 1;
                    $result['status_text'] = 'Success';
                    $result['message'] = 'Size updated successfully.';
                } else {

                    $result = $this->getCartData($user_id, $coupan_code);
                    $result['status_code'] = 0;
                    $result['status_text'] = 'Failed';
                    $result['message'] = 'Product already in bag.';
                }
            } else {

                $cart = \App\Cart::where('id', '=', $id)->update(['quantity' => $quantity]);

                $result = $this->getCartData($user_id, $coupan_code);
                $result['status_code'] = 1;
                $result['status_text'] = 'Success';
                $result['message'] = 'Quantity updated successfully.';
            }
        } else {
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Product not matched for updatation.';
        }




        return response()->json($result);
    }

    function removeCart(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];
        $id = decode_value($input['cart_id']);

        $user_id = decode_value($input['user_id']);
        $coupan_code = $input['coupan_code'];




        $check_data = \App\Cart::where('id', '=', $id)->where('user_id', '=', $user_id)->first();



        if (!empty($check_data)) {
            $cart = \App\Cart::find($id)->delete();
            $result = $this->getCartData($user_id, $coupan_code);
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
            $result['message'] = 'Product successfully remove from cart.';
        } else {
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Product not matched for remove from cart.';
        }




        return response()->json($result);
    }

    public function addToWishlist(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];
        $product_id = decode_value($input['product_id']);
        $user_id = decode_value($input['user_id']);

        $validator = Validator::make($input, [
                    'user_id' => 'required',
                    'product_id' => 'required',
        ]);

        if ($validator->errors()->all()) {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = $validator->errors()->first();
        } else {

            $user = User::find($user_id);

            if (empty($user->id)) {

                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['message'] = 'User not found';
            } else {
                $wishlistData = ProfileWishlist::where('product_id', '=', $product_id)->where('user_id', '=', $user_id)->first();
                //dd($wishlistData);
                if (!empty($wishlistData)) {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'Already have product in wishlist';
                } else {

                    $dataSave['product_id'] = $product_id;
                    $dataSave['user_id'] = $user_id;
                    ProfileWishlist::create($dataSave);
                    $data = $this->getWishlistData($user_id);
                    $data['status_code'] = 1;
                    $data['status_text'] = 'Success';
                    $data['message'] = 'Product successfully added to wishlist';
                }
            }
        }


        return response()->json($data);
    }

    function getWishlistProduct($user_id) {
        $all_data = ProfileWishlist::where('user_id', '=', $user_id)->get()->toArray();
        $data = array();

        foreach ($all_data as $key => $value) {

            $data[] = encode_value($value['product_id']);
        }
        return $data;
    }

    function removeWishlist(Request $request) {

        $inputData = $request->all();
        $input = $inputData['data'];

        $id = decode_value($input['product_id']);

        $user_id = decode_value($input['user_id']);

        $check_data = ProfileWishlist::where('product_id', '=', $id)->where('user_id', '=', $user_id)->first();

        if (!empty($check_data)) {

            ProfileWishlist::where('product_id', '=', $id)->where('user_id', '=', $user_id)->delete();
            $result = $this->getWishlistData($user_id);
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
            $result['message'] = 'Product successfully remove from wishlist.';
        } else {
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Product not matched for remove from wishlist.';
        }

        return response()->json($result);
    }

    function getWishlistProfile($user_id) {

        $user_id = decode_value($user_id);
        $result = $this->getWishlistData($user_id);
        $result['status_code'] = 1;
        $result['status_text'] = 'Success';
        return response()->json($result);
    }

    function getWishlistData($user_id) {
        $all_data = ProfileWishlist::where('user_id', '=', $user_id)->get()->toArray();
        $data = array();

        foreach ($all_data as $key => $value) {

            $data[$key]['product_id'] = encode_value($value['product_id']);
            $data[$key]['user_id'] = encode_value($value['user_id']);
        }

        $result['data'] = $data;
        $result['dataArray'] = $this->getWishlistProduct($user_id);
        return $result;
    }

    function getProductWishlist(Request $request) {

        $input = $request->all();
        if (!empty($input)) {
            $input['uId'] = decode_value($input['uId']);

            $uId = $input['uId'];

            $intersect = array();

            $intersect = ProfileWishlist::where('user_id', $uId)->distinct('product_id')->lists('product_id')->toArray();
            if (!empty($intersect)) {

                $productslist = \App\Product::with("getImage")->where('status', '=', 1)->whereIn('id', $intersect)->orderBy('created_at', 'desc')->paginate(Configure('CONFIG_FRONT_PAGE_LIMIT'))->toArray();


                if ($productslist) {

                    $products = $productslist;

                    foreach ($products['data'] as $key => $result) {

                        $products['data'][$key]['id'] = encode_value($result['id']);
                        $products['data'][$key]['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $result['get_image']['name'], array('width' => T540_WIDTH, 'height' => '650', 'zc' => 0, 'cc' => 'fff'), THUMB);
                        // $products[$key]['image'] =  BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH, $result['get_image']['name'], array('width' => '214', 'height' => '272', 'zc' => 2), false);
                        $products['data'][$key]['price'] = display_price($result['price']);
                        $attributedata = \App\ProductAttributeValue::with('attribute_value')->where('product_id', '=', $result['id'])->where('attribute_id', Config::get('global.size_id'))->get()->toArray();
                        $attribute = array();
                        foreach ($attributedata as $attrkey => $attrvalue) {
                            unset($attrvalue['attribute_value']['id']);
                            $attribute[] = $attrvalue['attribute_value'];
                        }
                        $products['data'][$key]['size'] = $attribute;
                        unset($products['data'][$key]['get_image']);
                    }
                } else {

                    $products = array('total' => 0, 'per_page' => Configure('CONFIG_FRONT_PAGE_LIMIT'), 'current_page' => 0, 'last_page' => 1, 'from' => 0, 'to' => 1, 'data' => array());
                }



               
            } else {

                    $products = array('total' => 0, 'per_page' => Configure('CONFIG_FRONT_PAGE_LIMIT'), 'current_page' => 0, 'last_page' => 1, 'from' => 0, 'to' => 1, 'data' => array());
                }

            
            
            $data = $products;
                $data['status_code'] = 1;
                $data['status_text'] = 'Success';
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'No record found';
        }

        return response()->json($data);
    }

    function getPincodeDetail($pincode) {



        $check_data = \App\Postal::where('pin', '=', $pincode)->first();



        if (!empty($check_data)) {
            $data['state'] = $check_data->state;
            $data['city'] = $check_data->district;
            $result['data'] = $data;
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
            $result['message'] = 'Success';
        } else {
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'errors';
        }




        return response()->json($result);
    }

    function checkCouponCode($user_id, $coupon_code, $response_flag = 0) {

        $user_id = decode_value($user_id);
        $coupons = \App\Coupon::where('coupons_code', $coupon_code)->where("status", 1)->first();

        $current_time = getCurrentTime();

        if (!empty($coupons)) {
            if ($coupons->start_date <= $current_time && $coupons->end_date >= $current_time) {

                $coupons_users = \App\CouponUser::where('coupon_id', '=', $coupons->id)->lists('user_id', 'user_id')->toArray();
                $flag = 0;

                if (!empty($coupons_users)) {
                    if (!in_array($user_id, $coupons_users)) {
                        $flag = 1;
                        $result['status_code'] = 0;
                        $result['status_text'] = 'Failed';
                        $result['message'] = 'Please enter a valid Promotional or Voucher Code';
                    }
                }


                if ($flag == 0) {
                    if ($coupons->discount_on == 'product') {
                        $data = $this->getCoupenAppliedProducts($coupons, $user_id);
                        $ApplyCoupenProducts = $data['ApplyCoupenProducts'];


                        if (empty($ApplyCoupenProducts)) {

                            if ($data['status'] == 2) {


                                $result['message'] = 'Please select maximum ' . $coupons->max_product_quantity . ' quantity';
                            } else {

                                $result['message'] = 'Please enter a valid promotional or voucher Code';
                            }
                            $result['status_code'] = 0;
                            $result['status_text'] = 'Failed';
                        } else {
                            if ($response_flag) {

                                $result['discount_on'] = $coupons->discount_on;
                                $result['product_ids'] = $ApplyCoupenProducts;
                            }


                            $result['status_code'] = 1;
                            $result['status_text'] = 'Success';
                            $result['message'] = 'Congratulations coupon code apply successful';
                        }
                    } else {

                        $all_data = \App\Cart::where('user_id', '=', $user_id)->get()->toArray();
                        $total_price = 0;

                        foreach ($all_data as $key => $value) {

                            $product = \App\Product::where('id', '=', $value['product_id'])->select('id', 'price')->first()->toArray();
                            $total_price = $total_price + ($value['quantity'] * $product['price']);
                        }

                        if ($total_price <= $coupons->min_total_value) {
                            $result['message'] = 'You need to maximum ' . display_price($coupons->min_total_value) . ' order price';
                            $result['status_code'] = 0;
                            $result['status_text'] = 'Failed';
                        } else {
                            if ($response_flag) {
                                $result['discount_on'] = $coupons->discount_on;
                            }
                            $result['status_code'] = 1;
                            $result['status_text'] = 'Success';
                            $result['message'] = 'congratulations coupon code apply successful';
                        }
                    }
                }
            } else {
                $result['status_code'] = 0;
                $result['status_text'] = 'Failed';
                $result['message'] = 'Coupon Code has been expired';
            }
        } else {
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Please enter a valid Promotional or Voucher Code';
        }

        if ($response_flag) {

            $result['coupons'] = $coupons;
            return $result;
        } else {
            return response()->json($result);
        }
    }

    function getCoupenAppliedProducts($coupons, $user_id) {
        $data = array();
        $ApplyCoupenProducts = array();
        $categoryids = \App\CouponCategory::where('coupon_id', '=', $coupons->id)->lists('category_id', 'category_id')->toArray();
        $tags = \App\CouponTag::where('coupon_id', '=', $coupons->id)->lists('tag_id', 'tag_id')->toArray();
        $productIds = \App\Cart::where('user_id', '=', $user_id)->where("quantity", '<=', $coupons->max_product_quantity)->distinct('product_id')->lists('product_id', 'product_id')->toArray();

        if (!empty($productIds)) {
            $productHaveCategory = \App\Product::whereIn('id', $productIds)->whereIn('category_id', $categoryids)->lists('id', 'id')->toArray();

            $productHaveCategory = \App\Product::whereIn('id', $productIds)->whereIn('category_id', $categoryids)->lists('id', 'id')->toArray();
            $productHaveTags = \App\ProductTag::whereIn('product_id', $productIds)->whereIn('tag_id', $tags)->lists('product_id', 'product_id')->toArray();

            if (!empty($productHaveCategory) && !empty($productHaveTags)) {
                $ApplyCoupenProducts = array_merge($productHaveCategory, $productHaveTags);
            } else {
                if (!empty($productHaveCategory) && empty($productHaveTags)) {

                    $ApplyCoupenProducts = $productHaveCategory;
                }

                if (!empty($productHaveTags) && empty($productHaveCategory)) {

                    $ApplyCoupenProducts = $productHaveTags;
                }
            }
            $data['ApplyCoupenProducts'] = array_unique($ApplyCoupenProducts);
            $data['status'] = 1;
        } else {
            $productIds = \App\Cart::where('user_id', '=', $user_id)->lists('product_id', 'product_id')->toArray();
            if (!empty($productIds)) {
                $data['ApplyCoupenProducts'] = $ApplyCoupenProducts;
                $data['status'] = 2;
            } else {

                $data['ApplyCoupenProducts'] = $ApplyCoupenProducts;
                $data['status'] = 0;
            }
        }


        return $data;
    }

    function payment(Request $request) {
        $inputData = $request->all();
        $input = $inputData['data'];
        $result = array();
        $payment_type = $input['type'];
        $order_id = BasicFunction::generateOrderId();
        $paytm = array();
        $payu = array();
        $PayUmoney = array();
        $order = array();
        $order['order_id'] = $order_id;
        $user_id = decode_value($input['user_id']);

        $order['user_id'] = $user_id;


        $order['total_price'] = pricetoint($input['total_price']);

        $user = \App\User::where('id', '=', $order['user_id'])->select('id', 'first_name', 'last_name', 'email', 'phone')->first()->toArray();
        $address = $input['address'];
        $order['address_id'] = decode_value($address['id']);
        $order['first_name'] = $address['first_name'];
        $order['last_name'] = $address['last_name'];
        $order['address_1'] = $address['address_1'];
        $order['address_2'] = $address['address_2'];
        $order['pin_code'] = $address['pin_code'];
        $order['state'] = $address['state'];
        $order['city'] = $address['city'];
        $order['mobile'] = $address['mobile'];
        $order['expected_delivery_date'] = expected_delivery_date();

        if ($input['enable_discount'] == 1) {
            $order['discount'] = pricetoint($input['discount']);
            $order['is_discount'] = 1;

            $order['coupon_code'] = $input['coupon_code'];
        } else {

            $order['discount'] = 0;
            $order['is_discount'] = 0;
        }


        if ($input['enable_shipping_charge'] == 1) {
            $order['shipping_charge'] = pricetoint($input['shipping_charge']);
        } else {

            $order['shipping_charge'] = 0;
        }
        if ($input['enable_tax'] == 1) {
            $order['total_tax'] = pricetoint($input['total_tax']);
            $tax = array();
            foreach ($input['tax'] as $key => $value) {
                $tax[] = array('name' => $value['name'], 'amount' => pricetoint($value['amount']));
            }
            $order['tax_description'] = serialize($tax);
        } else {

            $order['total_tax'] = 0;
            $order['tax_description'] = '';
        }
        $order['order_status'] = 'pending';
        $order['payment_status'] = 'pending';
        $from_wallate = 0;

        if (isset($input['wallet']) && !empty($input['wallet'])) {
            $walletData = $input['wallet'];
            $wallet_bal = $this->getWallateBalance($user_id);
            if ($wallet_bal == pricetoint($walletData['wallet'])) {
                if ($walletData['after_wallet_pay'] == 0) {
                    $payment_type = 'wallet';

                    $order['wallet_amount'] = pricetoint($input['payble_amount']);
                    $from_wallate = pricetoint($input['payble_amount']);
                } else {

                    $order['wallet_amount'] = $wallet_bal;
                    $from_wallate = $wallet_bal;
                }
            } else {

                $result['status_code'] = 0;
                $result['status_text'] = 'Failed';
                $result['message'] = 'Wallet  balace error.Please try again and check wallet information ';
                return response()->json($result);
            }
        }

        if ($payment_type == 'COD') {
            $order['payment_by'] = 'cod';
            $order['is_prepaid'] = 0;
            $order['is_cod'] = 1;
            $order['cod_money'] = pricetoint($input['cod_money']);
            $order['payble_amount'] = pricetoint($input['cod_payble_amount']);
            $order['cash_back_amount'] = 0;
            $order['is_cash_back'] = 0;
            if ($input['enable_cashback'] == 1) {
                $order['coupon_code'] = '';
            }
            $order['order_status'] = 'success';
        } else {
            $order['coupon_code'] = $input['coupon_code'];
            $order['is_prepaid'] = 1;
            $order['is_cod'] = 0;
            $order['payble_amount'] = pricetoint($input['payble_amount']);
            $cash_back_amount =  pricetoint($input['cash_back_amount']);
            if ($input['enable_cashback'] == 1 && $input['enable_discount'] == 0 && $payment_type != 'wallet' && $cash_back_amount>0) {

                $order['cash_back_amount'] = $cash_back_amount;
                $order['is_cash_back'] = 1;
            } else {

                $order['cash_back_amount'] = 0;
                $order['is_cash_back'] = 0;
            }
        }




        if ($payment_type == 'PAYTM') {
            $order['payment_by'] = 'paytm';
            $gateway = Omnipay::gateway('Paytm');

            $gateway->setTestMode(true);

            $params = [
                'MID' => Configure('CONFIG_MID'),
                'OrderId' => $order_id,
                'CustomerId' => $user['id'],
                'IndustryType' => Configure('CONFIG_INDUSTRY_TYPE_ID'),
                'ChannelId' => Configure('CONFIG_CHANNEL_ID'),
                'TransactionAmount' => $order['payble_amount'] - $from_wallate,
                'Website' => Configure('CONFIG_WEBSITE'),
                'MerchantKey' => Configure('CONFIG_MERCHANT_KEY'),
                'testMode' => Configure('CONFIG_PAYTM_TEST_MODE'),
                'returnUrl' => url('api/payment-success/' . $order_id . '/paytm')
            ];
            $response = $gateway->purchase($params)->send();
            $redirect_url = $response->getRedirectUrl();
            $data = $response->getRedirectData();
            $returnUrl = url('api/payment-success/' . $order_id . '/paytm');

            $data["CALLBACK_URL"] = $returnUrl;
            unset($data['CHECKSUMHASH']);
            $checkSum = getChecksumFromArray($data, Configure('CONFIG_MERCHANT_KEY'));
            $data['CHECKSUMHASH'] = $checkSum;
            foreach ($data as $key => $value) {
                $paytm['data'][] = array('name' => $key, 'value' => $value);
            }
            $paytm['redirect_url'] = $redirect_url;
        }
        if ($payment_type == 'PAYU') {
            $gateway = Omnipay::gateway('PayUBiz');
            $gateway->setKey(Configure('CONFIG_MERCHANT_ID'));
            $gateway->setSalt(Configure('CONFIG_SALT'));
            $gateway->setTestMode(Configure('CONFIG_PAYU_TEST_MODE'));
            $transaction_id = BasicFunction::generateTransactionId();
            $params = [
                'name' => ucwords($user['first_name'] . ' ' . $user['last_name']),
                'firstName' => ucfirst($user['first_name']),
                'lastName' => ucfirst($user['last_name']),
                'email' => $user['email'],
                'amount' => numberFormat($order['payble_amount'] - $from_wallate),
                'product' => Configure('CONFIG_SITE_TITLE') . ' product',
                'transactionId' => $transaction_id,
                'CancelUrl' => url('api/payment-success/' . $order_id . '/payu'),
                'failureUrl' => url('api/payment-success/' . $order_id . '/payu'),
                'returnUrl' => url('api/payment-success/' . $order_id . '/payu')
            ];
            $response = $gateway->purchase($params)->send();

            $data = $response->getRedirectData();
            $data['phone'] = $user['phone'];
            $redirect_url = $response->getRedirectUrl();

            foreach ($data as $key => $value) {
                $payu['data'][] = array('name' => $key, 'value' => $value);
            }
            $payu['redirect_url'] = $redirect_url;
            $order['payment_by'] = 'payu';
            $order['transaction_id'] = $transaction_id;
        }

        if ($payment_type == 'PayUmoney') {
            $gateway = Omnipay::gateway('PayUBiz');
            $gateway->setKey(Configure('CONFIG_MERCHANT_ID'));
            $gateway->setSalt(Configure('CONFIG_SALT'));
            $gateway->setTestMode(Configure('CONFIG_PAYU_TEST_MODE'));
            $transaction_id = BasicFunction::generateTransactionId();
            $params = [
                'name' => ucwords($user['first_name'] . ' ' . $user['last_name']),
                'firstName' => ucfirst($user['first_name']),
                'lastName' => ucfirst($user['last_name']),
                'email' => $user['email'],
                'amount' => numberFormat($order['payble_amount'] - $from_wallate),
                'product' => Configure('CONFIG_SITE_TITLE') . ' product',
                'transactionId' => $transaction_id,
                'CancelUrl' => url('api/payment-success/' . $order_id . '/PayUmoney'),
                'failureUrl' => url('api/payment-success/' . $order_id . '/PayUmoney'),
                'returnUrl' => url('api/payment-success/' . $order_id . '/PayUmoney')
            ];
            $response = $gateway->purchase($params)->send();

            $data = $response->getRedirectData();
            $data['phone'] = $user['phone'];
            $data['pg'] = 'WALLET';
            $data['bankcode'] = 'PAYUW';
            $redirect_url = $response->getRedirectUrl();

            foreach ($data as $key => $value) {
                $PayUmoney['data'][] = array('name' => $key, 'value' => $value);
            }
            $PayUmoney['redirect_url'] = $redirect_url;
            $order['payment_by'] = 'PayUmoney';
            $order['transaction_id'] = $transaction_id;


            
        }
        if ($payment_type == 'wallet') {
            $order['order_status'] = 'success';
            $order['payment_by'] = 'wallet';
            $order['payment_status'] = 'success';
        }



        $orderData = $order;
        $order = \App\Order::create($order);
        $orderId = $order->id;
        if ($from_wallate > 0) {

            $walletArray = array();
            $wallet['user_id'] = $user_id;
            $wallet['flag'] = Config::get('global.wallet_flag.dr');
            $wallet['cash'] = $from_wallate;
            $wallet['description'] = "Payment for Order #" . $order_id;
            $wallet['order_id'] = $orderId;
            \App\Wallet::create($wallet);
        }



        $products = $input['products'];

        $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('id', 'slug')->toArray();

        $orderProducts =  array(); 
        $k=1;
        foreach ($products as $key => $product) {
            $order_detail = array();
            $order_detail['order_id'] = $orderId;
            $order_detail['product_id'] = decode_value($product['product_id']);
            $order_detail['cart_id'] = decode_value($product['id']);
            $carts[] = decode_value($product['id']);
            $order_detail['quantity'] = $product['quantity'];
            $order_detail['product_size'] = $attributedata[$product['product_size']];
            $order_detail['price'] = pricetoint($product['product']['price']);
            $order_detail['shipping_charge'] = pricetoint($product['product']['shipping_charge']);

            $OrderDetails = \App\OrderDetail::create($order_detail);

            $orderProducts[$k]['product_id'] = decode_value($product['product_id']);
            $orderProducts[$k]['size'] = $attributedata[$product['product_size']];
            $orderProducts[$k]['quantity'] = $product['quantity'];
            $k++;

        }
        if(!empty($orderProducts)){
              foreach ($orderProducts as $Pkey => $Pvalue) {
                    $sockData =   \App\Stock::where('size_id', $Pvalue['size'])->where('product_id',$Pvalue['product_id'])->decrement('quantity', $Pvalue['quantity']);

                }

        }

        if ($orderData['order_status'] == 'success' && !empty($carts)) {

            \App\Cart::destroy($carts);
                $this->SendOrderMail($orderId);
                $ordData['order_id'] = $orderId;
	  		    $ordData['status'] = 'success';
	            \App\OrderStatus::create($ordData);
              
                
                
         }
        $result['paytm'] = $paytm;
        $result['payu'] = $payu;
        $result['PayUmoney'] = $PayUmoney;
        $result['payment_type'] = $payment_type;
        $result['status_code'] = 1;
        $result['order_id'] = $order_id;
        $result['status_code'] = 1;
        $result['status_text'] = 'Success';
        $result['message'] = 'Order successfully placed';
        return response()->json($result);
    }

    function paymentsuccess($order_id, $type, Request $request) {

        $input = $request->all();

        

        $order = \App\Order::where('order_id', $order_id)->first();


        $data = array();
        $errors = 1;
        if (!empty($order)) {
            if ($type == 'payu' || $type == 'PayUmoney') {

                if ($input['key'] == Configure('CONFIG_MERCHANT_ID')) {

                    $errors = 0;
                    $data['mihpayid'] = $input['mihpayid'];
                    $data['transaction_id'] = $input['txnid'];
                    $data['payment_mode'] = $input['mode'];
                    $data['payment_response'] = serialize($input);
                    if ($input['status'] == 'success') {
                        $data['payment_status'] = 'success';
                        $data['order_status'] = 'success';
                    } else if ($input['status'] == 'failure') {

                        $data['payment_status'] = 'failure';
                        $data['order_status'] = 'failed';
                        $data['reject_reason'] ='Payment failed';
                    }
                }
            }
            if ($type == 'paytm') {

                if ($input['MID'] == Configure('CONFIG_MID')) {
                    $errors = 0;

                    $data['transaction_id'] = $input['TXNID'];

                    $data['payment_response'] = serialize($input);
                    if ($input['STATUS'] == 'TXN_SUCCESS') {
                        $data['payment_mode'] = $input['GATEWAYNAME'];
                        $data['payment_status'] = 'success';
                        $data['order_status'] = 'success';
                    } else if ($input['STATUS'] == 'TXN_FAILURE') {
                        $data['reject_reason'] = 'Payment failed';
                        $data['payment_status'] = 'failure';
                        $data['order_status'] = 'failed';
                    }
                }
            }







            if (!empty($data) && $errors == 0) {

                $order->fill($data)->save();
                if ($data['order_status'] == 'success') {

                     $this->SendOrderMail($order->id);
                    $carts = \App\OrderDetail::where('order_id', $order->id)->lists('cart_id', 'cart_id')->toArray();


                    \App\Cart::destroy($carts);




                }

                if ($data['payment_status'] == 'failure' && $order->wallet_amount > 0) {
                    $wallet = array();
                    $wallet['user_id'] = $order->user_id;
                    $wallet['flag'] = Config::get('global.wallet_flag.cr');
                    $wallet['cash'] = $order->wallet_amount ;
                    $wallet['description'] = "Payment failure,Refund from Order #" . $order->order_id;
                    $wallet['order_id'] = $order->id;
                    \App\Wallet::create($wallet);
                }
                  if ($data['payment_status'] == 'failure') {
                          $carts = \App\OrderDetail::where('order_id', $order->id)->get();
                          foreach ($carts as $key => $Pvalue) {
                                 $sockData =   \App\Stock::where('size_id', $Pvalue->product_size)->where('product_id',$Pvalue->product_id)->increment('quantity', $Pvalue->quantity);
                          }
                         
                    }


                  	$ordData['order_id'] = $order->id;
	  		   		 $ordData['status'] = $data['order_status'];
	            	\App\OrderStatus::create($ordData);


                return redirect('myaccount/order-detail/' . $order_id.'?status='.$data['order_status']);
            } else {

                return redirect('/');
            }
        } else {

            return redirect('/');
        }
    }

    function getOrder($user_id) {
        if ($user_id) {

            $user_id = decode_value($user_id);
            $orderData = \App\Order::with('order_detail')->where('user_id', $user_id)->orderBy('created_at', 'desc')->paginate(Configure('CONFIG_FRONT_PAGE_LIMIT'))->toArray();

            $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();

        
            $order = array();
            if (!empty($orderData)) {
                foreach ($orderData['data'] as $key => $value) {
                	$order_id = $value['id'];
                    $value['id'] = encode_value($value['id']);
                    $value['tax_description'] = unserialize($value['tax_description']);
                    unset($value['payment_response']);
                    $value['cash_back_amount'] = display_price($value['cash_back_amount']);
                    $value['discount'] = display_price($value['discount']);
                    $value['total_price'] = display_price($value['total_price']);
                    $value['cod_money'] = display_price($value['cod_money']);
                    if ($value['total_tax'] > 0) {
                        $value['enable_tax'] = 1;
                    } else {
                        $value['enable_tax'] = 0;
                    }
                    if ($value['is_cod']== 0) {
                        $value['is_cod'] = 0;
                    } else {
                        $value['is_cod'] = 1;
                    }
                    $value['total_tax'] = display_price($value['total_tax']);

                    $value['shipping_charge'] = display_price($value['shipping_charge']);
                    
                    if ($value['shipping_charge'] > 0) {
                        $value['enable_shipping_charge'] = 1;
                    } else {
                        $value['enable_shipping_charge'] = 0;
                    }

                   $value['status']['ordered'] = date_val($value['created_at'],STATUS_DATE_FORMATE);
              
                    $value['payble_amount'] = display_price($value['payble_amount']);
                    $value['payment_by'] = strtoupper($value['payment_by']);
                    $value['created_at'] = date_val($value['created_at'], DATE_FORMATE);
                    $value['expected_delivery_date'] = date(EXP_DATE_FORMATE,$value['expected_delivery_date']);
                    if($value['delivery_date']){
                            $value['delivery_date'] = date(EXP_DATE_FORMATE,$value['delivery_date']);
                     }  
                    unset($value['user_id']);
                    foreach ($value['order_detail'] as $orderKey => $order_detail) {
                        unset($order_detail['id']);
                        unset($order_detail['order_id']);
                        unset($order_detail['cart_id']);
                        $product_id = $order_detail['product_id'];
                        unset($order_detail['product_id']);

                        $order_detail['price'] = display_price($order_detail['price']);
                        $order_detail['product_size'] = $attributedata[$order_detail['product_size']];


                        $order_detail['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first()->toArray();
                        unset($order_detail['product']['id']);
                        $order_detail['product']['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $order_detail['product']['get_image']['name'], array('width' => '84', 'zc' => 2), false);
                        unset($order_detail['product']['get_image']);
                        $value['order_detail'][$orderKey] = $order_detail;
                    }

              

                    foreach ( Config::get('global.order_status') as $Skey => $Svalue) {
                    	$value['status'][$Svalue] = '';
                    }

                 	$OrderStatus  = \App\OrderStatus::where('order_id',$order_id)->get()->toArray(); 

                 	foreach ($OrderStatus as $Odkey => $Odvalue) {

                 		 $value['status'][$Odvalue['status']] = date_val($Odvalue['created_at'],STATUS_DATE_FORMATE);
                 	}
                 	

                    $order[$key] = $value;
                }
               
                $orderData['data'] = $order;
            } else {

                $orderData = array('total' => 0, 'per_page' => Configure('CONFIG_FRONT_PAGE_LIMIT'), 'current_page' => 0, 'last_page' => 1, 'from' => 0, 'to' => 1, 'data' => array());
            }
    


            $result = $orderData;
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
        } else {

            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
        }


        return response()->json($result);
    }

    function orderCancel($orderId, $user_id, Request $request) {

        $inputdata = $request->all();
        $input = $inputdata['data'];
        $cancel_reason = $input['cancel_reason'];
        $user_id = decode_value($user_id);
        $order = \App\Order::with('user')->where('order_id', $orderId)->where('user_id', $user_id)->first();

        if (!empty($order)) {
            if ($order->order_status == Config::get('global.order_status.success')) {

                $input['order_status'] = Config::get('global.order_status.cancel');
                $orderdata = $order;
                $order = $order->fill($input)->save();

                $order = $orderdata;
                if ($order->payment_by != Config::get('global.payment_by.cod') && $order->payment_status == Config::get('global.payment_status.success')) {

                    $wallet = array();
                    $wallet['user_id'] = $user_id;
                    $wallet['flag'] = Config::get('global.wallet_flag.cr');
                    $wallet['cash'] = $order->payble_amount;

                    $wallet['description'] = "Order Cancel, Refund from Order #" . $order->order_id;
                    $wallet['order_id'] = $order->id;
                    \App\Wallet::create($wallet);
						$input['is_refund'] = 1;
                		$order = $order->fill($input)->save();

                	$result['message'] = 'Order canceled successfully.Amount refunded in fabivo wallet';
                }else{
                	 $result['message'] = 'Order canceled successfully.';
                }	

				$ordData['order_id'] = $orderdata->id;
	  		    $ordData['status'] =  Config::get('global.order_status.cancel');
	            \App\OrderStatus::create($ordData);
	            

                   $carts = \App\OrderDetail::where('order_id', $orderdata->id)->get();
                          foreach ($carts as $key => $Pvalue) {
                                 $sockData =   \App\Stock::where('size_id', $Pvalue->product_size)->where('product_id',$Pvalue->product_id)->increment('quantity', $Pvalue->quantity);
                          }



             if(Configure('CONFIG_ENABLE_SMS')){

                EmailHelper::sendOrderSms($orderdata->user->phone,$orderdata->user->id,$orderdata->order_id,3); 
            }    

            
	        $email_template = \App\EmailTemplate::where('slug', '=', 'cancel-order-to-user')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;
            $user = $orderdata->user;
            $to = $user->email;
           
           
            $body = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $orderdata->order_id,
          
                    ), $body);

                $subject = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $orderdata->order_id,
          
                    ), $subject);


           	 EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);


           	         $email_template = \App\EmailTemplate::where('slug', '=', 'cancel-order-to-admin')->first();
            $email_type = $email_template->email_type;
            $subject = $email_template->subject;
            $body = $email_template->body;
            $user = $orderdata->user;
            $to = Configure('CONFIG_STAFF_MAIL');
           
           
            $body = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
                '{REASON}',
           
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $orderdata->order_id,
                $cancel_reason
          
                    ), $body);

                $subject = str_replace(array(
                '{FIRST_NAME}',
                '{LAST_NAME}',
                '{ORDER_ID}',
           		'{REASON}',
                    ), array(
                ucfirst($user->first_name),
                ucfirst($user->last_name),
                $orderdata->order_id,
                $cancel_reason
          
                    ), $subject);


           	 EmailHelper::sendMail($to, '', '', $subject, 'default', $body, $email_type);


            

           }
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
        } else {
        	 $result['message'] = 'Order not found.';
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
        }



        return response()->json($result);
    }

    function walletBalance($user_id, Request $request) {
        $inputdata = $request->all();
        $input = $inputdata['data'];
        $user_id = decode_value($user_id);

        $token = $input['token'];

        $user = \App\User::where('id', '=', $user_id)->where('token', '=', $token)->select('id', 'first_name', 'last_name', 'email', 'phone')->first();
        if ($user) {

            $wallet = $this->getWallateBalance($user_id);
            $payble_amount = pricetoint($input['payble_amount']);
            $after_wallet_pay = 0;
            $remaining_balance = 0;
            if ($payble_amount <= $wallet) {

                $after_wallet_pay = 0;
                $remaining_balance = $wallet - $payble_amount;
            } else {

                $after_wallet_pay = $payble_amount - $wallet;
            }


            if ($wallet > 0) {

                $result['enable_wallet'] = 1;
            } else {
                $result['enable_wallet'] = 0;
            }

            $result['wallet'] = display_price($wallet);
            $result['after_wallet_pay_rs'] = display_price($after_wallet_pay);
            $result['after_wallet_pay'] = $after_wallet_pay;
            $result['remaining_balance'] = $remaining_balance;
            $result['remaining_balance_rs'] = display_price($remaining_balance);
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
        } else {

            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
        }


        return response()->json($result);
    }


    function getWallateBalance($user_id) {

        $wallet = DB::table('wallet')
                ->select(DB::raw('SUM(cash) as cash'), 'flag')
                ->where('user_id', '=', $user_id)
                ->groupBy('flag')
                ->get();
        $data = array();


        foreach ($wallet as $key => $value) {
            $data[$value->flag] = $value->cash;
        }

        $cr = 0;
        $dr = 0;
        if (isset($data['cr'])) {

            $cr = $data['cr'];
        }
        if (isset($data['dr'])) {

            $dr = $data['dr'];
        }

        return number_format($cr - $dr, 2, '.', '');
    }

    function orderDdetail($user_id, $order_id) {
        if ($user_id) {

            $user_id = decode_value($user_id);
            $orderData = \App\Order::with('order_detail')->where('user_id', $user_id)->where('order_id', $order_id)->first()->toArray();

            $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();


            $order = array();
            if (!empty($orderData)) {
                $oId =$orderData['id'];
                $orderData['id'] = encode_value($orderData['id']);
                $orderData['tax_description'] = unserialize($orderData['tax_description']);
                unset($orderData['payment_response']);
                if($orderData['cash_back_amount']==0){
                    $orderData['is_cash_back'] = 0;

                }
                $orderData['cash_back_amount'] = display_price($orderData['cash_back_amount']);

                

                $orderData['discount'] = display_price($orderData['discount']);
                $orderData['total_price'] = display_price($orderData['total_price']);
                if ($orderData['total_tax'] > 0) {
                    $orderData['enable_tax'] = 1;
                } else {
                    $orderData['enable_tax'] = 0;
                }
                   if ($orderData['is_cod']== 0) {
                        $orderData['is_cod'] = 0;
                    } else {
                        $orderData['is_cod'] = 1;
                    }
                $orderData['total_tax'] = display_price($orderData['total_tax']);

                $orderData['shipping_charge'] = display_price($orderData['shipping_charge']);
                if ($orderData['shipping_charge'] > 0) {
                    $orderData['enable_shipping_charge'] = 1;
                } else {
                    $orderData['enable_shipping_charge'] = 0;
                }

                $orderData['status']['ordered'] = date_val($orderData['created_at'],STATUS_DATE_FORMATE);
                $orderData['payble_amount'] = display_price($orderData['payble_amount']);
                $orderData['created_at'] = date_val($orderData['created_at'], DATE_FORMATE);
                    $orderData['expected_delivery_date'] = date(EXP_DATE_FORMATE,$orderData['expected_delivery_date']);
                    if($orderData['delivery_date']){
                            $orderData['delivery_date'] = date(EXP_DATE_FORMATE,$orderData['delivery_date']);
                     } 
                unset($orderData['user_id']);
                foreach ($orderData['order_detail'] as $orderKey => $order_detail) {
                    unset($order_detail['id']);
                    unset($order_detail['order_id']);
                    unset($order_detail['cart_id']);
                    $product_id = $order_detail['product_id'];
                    unset($order_detail['product_id']);

                    $order_detail['price'] = display_price($order_detail['price']);
                    $order_detail['product_size'] = $attributedata[$order_detail['product_size']];


                    $order_detail['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first()->toArray();
                    unset($order_detail['product']['id']);
                    $order_detail['product']['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $order_detail['product']['get_image']['name'], array('width' => '84', 'zc' => 2), false);
                    unset($order_detail['product']['get_image']);
                    $orderData['order_detail'][$orderKey] = $order_detail;
                }


                 $order_delivery  = \App\OrderDelivery::where('order_id', $oId)->first();
                 $order_delivery_data = array();
                 if(!empty($order_delivery)){
                    $order_delivery_data['waybill'] = $order_delivery->waybill;
                    $order_delivery_data['status'] = $order_delivery->status;
               

                     if(Configure('CONFIG_DELHIVERY_TEST_MODE')){

                          $get_url =          TEST_DELHIVERY_TRACK_URL.$order_delivery->waybill;    
                    }else{
                          $get_url =          PROD_DELHIVERY_TRACK_URL.$order_delivery->waybill;;    
             
                    }
                    $order_delivery_data['link'] = $get_url;
                      $scan = array();
                    if($order_delivery->track_responce!=''){
                        $track_responce =unserialize($order_delivery->track_responce);
                      

                        foreach ($track_responce['Scans'] as $key => $value) {
                                $scan[$key]=$value['ScanDetail'];
                                $scan[$key]['StatusDateTime'] = date_val($scan[$key]['StatusDateTime'],COMMENT_DATE_FORMATE );
                                $scan[$key]['ScanType'] =Config::get('global.StatusType.'.$scan[$key]['ScanType']);
                        }

                    }
                       $order_delivery_data['scan'] = $scan;
                  
                    
                 }

                 $orderData['delivery'] = $order_delivery_data;
                 if(!empty($order_delivery_data)){

                    $orderData['waybill'] = $order_delivery_data['waybill'];
                 }else{
                     $orderData['waybill'] = '';
                 }
            }
          	
                    foreach ( Config::get('global.order_status') as $Skey => $Svalue) {
                    	$orderData['status'][$Svalue] = '';
                    }

                 	$OrderStatus  = \App\OrderStatus::where('order_id',$oId)->get()->toArray(); 

                 	foreach ($OrderStatus as $Odkey => $Odvalue) {

                 		 $orderData['status'][$Odvalue['status']] = date_val($Odvalue['created_at'],STATUS_DATE_FORMATE);
                 	}
                 	
             
            $result['data'] = $orderData;
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
        } else {

            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
        }


        return response()->json($result);
    }

    function getWalletData($user_id) {

        if ($user_id) {

            $user_id = decode_value($user_id);

            $walletData = \App\Wallet::where('user_id', $user_id)->orderBy('created_at', 'desc')->paginate(Configure('CONFIG_FRONT_PAGE_LIMIT'))->toArray();

            if (!empty($walletData)) {
                foreach ($walletData['data'] as $key => $value) {
                    $value['cash'] = display_price($value['cash']);
                    $value['created_at'] = date_val($value['created_at'], WALLEt_DATE_FORMATE);
                    $walletData['data'][$key] = $value;
                }
            } else {

                $walletData = array('total' => 0, 'per_page' => Configure('CONFIG_FRONT_PAGE_LIMIT'), 'current_page' => 0, 'last_page' => 1, 'from' => 0, 'to' => 1, 'data' => array());
            }




            $result = $walletData;
            $result['status_code'] = 1;
            $result['wallet_amount'] = display_price($this->getWallateBalance($user_id));
            $result['status_text'] = 'Success';
        } else {

            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
        }


        return response()->json($result);
    }
    function checkAddressNumber($user_id) {
       
        $user_id = decode_value($user_id);

        

        $user = \App\User::where('id', '=', $user_id)->select('id', 'first_name', 'last_name', 'email', 'phone','phone_verify')->first();
        if ($user) {
            $send_otp = 0;
           
            if($user['phone']!=''){
            	          $mobile = $user['phone'];
           			 $result['phone_verify']=0;
       
               if (Configure('CONFIG_ENABLE_SMS')) {
                    EmailHelper::sendSms($mobile, $user_id,1);
                    $send_otp = 1;
                }else{
                    $send_otp = 0;
                }

            
            
            $result['status_code'] = 1;
            $result['send_otp'] = $send_otp;
            $result['mobile'] = $user['phone'];
            $result['status_text'] = 'Success';

            }else{
           	$result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Please update mobile number for COD';

            }
  
        } else {

            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            
        }


        return response()->json($result);
    }
    function resendOtpCodPhoneNumber($user_id) {
        
        
        $user_id = decode_value($user_id);

        

        $user = \App\User::where('id', '=', $user_id)->select('id', 'first_name', 'last_name', 'email', 'phone','phone_verify')->first();
        if ($user) {
                      
            
            $mobile = $user['phone'];
            EmailHelper::sendSms($mobile, $user_id,1);
              
                
                
        
            
            
            $result['status_code'] = 1;
            $result['status_text'] = 'Success';
        } else {

            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            
        }


        return response()->json($result);
    }
    function codVarifyNumber($user_id,Request $request) {


        $inputData = $request->all();
        $input = $inputData['data'];
        $id = $user_id;

        if (!empty($id)) {

            $validator = validator::make($input, [
                        'otp' => 'required',
            ]);

            if ($validator->errors()->all()) {
                $data['status_code'] = 0;
                $data['status_text'] = 'Failed';
                $data['errors'] = $validator->errors();
                $data['message'] = "Please Enter Valid Input";
            } else {
                $id = decode_value($id);
                $user = User::find($id);
                if (empty($user->id)) {
                    $data['status_code'] = 0;
                    $data['status_text'] = 'Failed';
                    $data['message'] = 'User not found';
                } else {
                    

                        $last_otp = \App\userOtp::where("user_id", $id)->where("mobile", $user->phone)->orderBy('created_at', 'desc')->first();

                        if (!empty($last_otp->id) && ($last_otp->otp == $input['otp'])) {
                            if (time() < $last_otp->expire) {
                                $last_otp->expire = '';
                                $last_otp->save();

                              
                                $data['status_code'] = 1;
                                $data['status_text'] = 'Success';
                                $data['message'] = 'Mobile Number Verified successfully';
                            } else {

                                $data['status_code'] = 0;
                                $data['status_text'] = 'Failed';
                                $data['message'] = 'OTP password expired.Please click on resend otp link';
                            }
                        } else {
                            $data['status_code'] = 0;
                            $data['status_text'] = 'Failed';
                            $data['message'] = 'OTP password not matched';
                        }
                    
                }
            }
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'User Token has been expired.';
        }

        return response()->json($data);
    }

    function SendOrderMail($order_id){
          $order = \App\Order::with('user','order_detail')->where('id', $order_id)->first();
           $attributedata = \App\AttributeValue::where('attribute_id', Config::get('global.size_id'))->lists('name', 'id')->toArray();
                   $order_details= array();
            foreach ($order->order_detail  as $orderKey => $order_detail) {
                       
                        $product_id = $order_detail['product_id'];
                     
                        $order_details[$orderKey] = $order_detail;
                        $order_details[$orderKey]['product_size'] = $attributedata[$order_detail['product_size']];
                        $order_details[$orderKey]['product'] = \App\Product::with("getImage")->where('id', '=', $product_id)->select('id', 'slug', 'title')->first();
                       
                        
            }
                  
            $order->order_detail = $order_details;
            $subject= 'Thank for your order, your Order ID is '.$order->order_id;
            $email_type= 'Order placed to user';

            EmailHelper::sendOrderMail($order->user->email, '',  $subject, 'create_order', $order->toArray(), $email_type);




            $subject= 'New order received #'.$order->order_id;
            $email_type= 'Order placed to admin';
             EmailHelper::sendOrderMail(Configure('CONFIG_STAFF_MAIL'), '',  $subject, 'create_order_admin', $order->toArray(), $email_type);






            if(Configure('CONFIG_ENABLE_SMS')){

            	EmailHelper::sendOrderSms($order->user->phone,$order->user->id,$order->order_id,1);	
            }
            
        return true;
    }


     function getPincodeAvalability($pincode) {



        $check_data = \App\Postal::where('pin', '=', $pincode)->first();



        if (!empty($check_data)) {
            
            
            $result['expected_delivery_date'] = 'Expect delivery by '.date(EXP_DATE_FORMATE, expected_delivery_date());

            $result['status_code'] = 1;

            $result['status_text'] = 'Success';
            $result['cash'] = strtolower($check_data->cash);

            if(strtolower($check_data->cash)=='y'){
                $result['message'] = 'Cash on delivery available';
            }else{

                $result['message'] = 'Cash on delivery not available on this pincode';
            }
            
        } else {
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Shipping to this pincode not available';
        }




        return response()->json($result);
    }
    function newslatter( Request $request){


           $inputData = $request->all();
           $input = $inputData['data'];
           $email = $input['email_news'];
           
           $check_data = \App\NewsletterSubscriber::where('email', '=', $email)->first();
           
           if (empty($check_data)) {
            $data['email'] = $email;
            $result = \App\NewsletterSubscriber::create($data);

            $result['status_code'] = 1;

            $result['status_text'] = 'Success';
             $result['message'] = 'Newsletter successfully subscribe.';
           }
           else {
            $result['status_code'] = 0;
            $result['status_text'] = 'Failed';
            $result['message'] = 'Already subscribe Newsletter.';
            }

           

         return response()->json($result);
    }

    
    function getSearchProduct($slug, Request $request) {
        $inputData = $request->all();


        $input = $inputData['data'];


        if ($slug) {
          //  $slug = '';

                if (isset($inputData['sort_by']) && $inputData['sort_by'] != '') {
                    $sort_by = $inputData['sort_by'];
                } else {

                    $sort_by = 'popular';
                }


                $sort_field = Config::get('global.product_sort_field.' . $sort_by);
                $orderBy = Config::get('global.product_sort_order.' . $sort_by);
                $intersect = array();
                if (!empty($input)) {


                    $attribute = \App\Attribute::lists('id', 'slug')->toArray();
                    $attribute_value = \App\AttributeValue::lists('id', 'slug')->toArray();
                    $i = 0;
                    $categories = '';
                    
                    foreach ($input['search'] as $key => $value) {

                


                      

                                $attrObject = new ProductAttributeValue();
                                $attrObject = $attrObject->where('attribute_id', $attribute[$value['attribute']]);
                                $orThose = array();
                                $attrData = explode(',', $value['data']);
                                $orThose['where_condition'] = array('attribute_id', $attribute[$value['attribute']]);
                                foreach ($attrData as $attr_slug) {
                                    $orThose['data'][] = array('attribute_value_id', $attribute_value[$attr_slug]);
                                }
                                if (!empty($orThose)) {
                                    $attrObject = $attrObject->where(function($query) use ($orThose) {
                                        foreach ($orThose['data'] as $key => $value) {
                                            $query = $query->orWhere($value[0], $value[1]);
                                        }

                                        return $query;
                                    });
                                }
                                if ($i == 0) {
                                    $intersect = $attrObject->distinct('product_id')->lists('product_id')->toArray();
                                    ;
                                } else {
                                    $intersect = array_intersect($intersect, $attrObject->distinct('product_id')->lists('product_id')->toArray());
                                }
                                $i++;

                          
                    }
                }

                  if (isset($input['category']) && !empty($input['category'])) {
                            
                              $category =   \App\Category::whereIn('slug', $input['category'])->lists('id','id')->toArray();
                             $CatProduct = \App\Product::whereIn('category_id', $category)->lists('id','id')->toArray();
                             
                             if(!empty($intersect) && !empty($CatProduct)) {
                              $intersect =    array_intersect($intersect,$CatProduct);

                             }else{
                                $intersect = $CatProduct;

                             }
                             
                    } 
                        


                if (!empty($input['search']) && empty($intersect)) {
                    $productslist = array();
                } else {

                    $productslistObj = \App\Product::with("getImage")->where('status', '=', 1)->where('title','like',"%".$slug."%");

                      
       
                       


                    if (!empty($input) && !empty($intersect)) {

                        $productslistObj = $productslistObj->whereIn('id', $intersect);
                    }



                    $productslist = $productslistObj->orderBy($sort_field, $orderBy)->paginate(Configure('CONFIG_FRONT_PAGE_LIMIT'))->toArray();
                }

                if ($productslist) {
                    $products = $productslist;

                    foreach ($products['data'] as $key => $result) {

                        //$products['data'][$key]['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH, $result['get_image']['name'], array('width' => '214', 'height' => '272', 'zc' => 2), false);

                        $products['data'][$key]['image'] = BasicFunction::showImageUrl(PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH . T540, PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH . T540, $result['get_image']['name'], array('width' => T540_WIDTH, 'height' => '650', 'zc' => 0, 'cc' => 'fff'), THUMB);
                        $products['data'][$key]['price'] = display_price($result['price']);
                        $products['data'][$key]['description'] = str_limit($result['description'], 100, '...');
                        $products['data'][$key]['id'] = encode_value($result['id']);

                        unset($products['data'][$key]['get_image']);
                    }


                    $data['products'] = $products;
                } else {


                    $data['products'] = array('total' => 0, 'per_page' => Configure('CONFIG_FRONT_PAGE_LIMIT'), 'current_page' => 0, 'last_page' => 1, 'from' => 0, 'to' => 1, 'data' => array());
                }
            
        } else {

            $data['status_code'] = 0;
            $data['status_text'] = 'Failed';
            $data['message'] = 'Requested url is invalid.';
        }

        return response()->json($data);
    }

    function getAllAttribute($slug) {


      
            
        $category_ids = \App\Product::with("getImage")->where('status', '=', 1)->where('title','like',"%".$slug."%")->lists('category_id')->toArray();

        if(!empty($category_ids)){
        $category= Category::whereIn('id', $category_ids)->where('status', '=', 1)->select('id','name','slug','parent_id')->orderBy('name', 'asc')->get()->toArray();
        $data['category'] = $category;
     


        $category_attribute = \App\CategoryAttribute::whereIn('category_id', $category_ids)->lists('attribute_id', 'attribute_id')->toArray();

                $attribute = \App\Attribute::with("AttributeValue")->whereIn('id', $category_attribute)->where('status', '=', 1)->select('id', 'name', 'slug')->orderBy('order_key', 'asc')->get();

        $attribute = \App\Attribute::with("AttributeValue")->where('status', '=', 1)->select('id', 'name', 'slug')->orderBy('order_key', 'asc')->get();
            if (!$attribute->isEmpty()) {
                $data['filter'] = $attribute->toArray();
            } else {
                $data['filter'] = array();
            }
        }else{

            $data['filter'] = array();
            $data['category'] = array();
        }
               


        
        return response()->json($data);
    }


}
