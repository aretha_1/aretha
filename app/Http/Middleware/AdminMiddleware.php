<?php
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Session;
use Visitor;


class AdminMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
   public function handle($request, Closure $next)
    {
     /* dd(Session::has('url.intended'));
       echo $uri = $request->path();
       
       die;*/

          $current_path =   $request->path();

         if($current_path=='admin/logout'){
            Session::put('url.intended', 'admin/dashboard');

         }else{
             Session::put('url.intended', $request->path());
         }
    
    
     
    if (!Auth::guard('admin')->check()) {
        if($current_path=='admin'){
                return redirect()->route('admin.login');
        }else{


            return redirect()->route('admin.login')->with('alert-error', trans('admin.NOT_LOGIN'));
        }
    }elseif (Auth::guard('admin')->user()->role_id != 1){
       
        return redirect()->route('admin.login')->with('alert-error', trans('admin.NOT_AUTHORITY'));
    }
  $response = $next($request);

     return $response->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
 ->header('Pragma','no-cache')
 ->header('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
 
    }

}
