<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::get('/', function () {

    return view('welcome');
});
Route::group(['namespace' => 'Api', 'prefix' => 'api'], function() {
    Route::get('get-all-cms', ['uses' => 'ApiController@getAllCms']);
    Route::get('get-all-settings ', ['uses' => 'ApiController@getAllsettings']);
    Route::get('get-cms-page-detail/{slug}', ['uses' => 'ApiController@getCmsDetail']);
    Route::get('get-all-category-for-menu', ['uses' => 'ApiController@getAllCategoryForMenu']);
    Route::get('get-slider', ['uses' => 'ApiController@getSlider']);
    Route::get('get-faq', ['uses' => 'ApiController@getfaq']);
    Route::post('login', ['uses' => 'ApiController@login']);
    Route::post('sign-up', ['uses' => 'ApiController@singUp']);
    Route::post('contact-us', ['uses' => 'ApiController@contactUs']);
    Route::post('forgot-password', ['uses' => 'ApiController@sendPasswordLink']);
    Route::get('check-socail-login/{token}', ['uses' => 'ApiController@checkSocailLogin']);
    Route::get('check-reset-password/{email_token}', ['uses' => 'ApiController@checkResetPassword']);
    Route::post('save-reset-password', ['uses' => 'ApiController@resetPasswordUpdate']);
    Route::post('get-product-by-category/{slug}', ['uses' => 'ApiController@getProductBycategory']);
    Route::post('get-search-product/{slug}', ['uses' => 'ApiController@getSearchProduct']);

    Route::get('get-category-attribute/{slug}', ['uses' => 'ApiController@getCategoryAttribute']);
    Route::get('get-all-attribute/{slug}', ['uses' => 'ApiController@getAllAttribute']);
   
   
//    Route::get('get-product-detail/{slug}', ['uses' => 'ApiController@getProductDetail']);
    Route::post('get-product-detail', ['uses' => 'ApiController@getProductDetail']);
    Route::get('get-user-detail/{id}/{token}', ['uses' => 'ApiController@getUserDetail']);
    Route::get('get-user-details/{id}/{token}', ['uses' => 'ApiController@getUserDetails']);

    Route::get('get-all-block', ['uses' => 'ApiController@getAllBlock']);
    Route::get('get-user-address/{id}/{token}', ['uses' => 'ApiController@getUserAddressBook']);

    
Route::post('update-profile', ['uses' => 'ApiController@updateProfile']);
Route::get('get-user-address/{id}/{token}', ['uses' => 'ApiController@getUserAddressBook']);
Route::post('save-address', ['uses' => 'ApiController@saveUserAddress']);
Route::post('set-address-default', ['uses' => 'ApiController@setAddressDefault']);
Route::get('delete-address/{id}/{user_id}', ['uses' => 'ApiController@deleteAddress']);
    Route::post('mobile-verify', ['uses' => 'ApiController@VarifyNumber']);
    Route::post('mobile-number-update', ['uses' => 'ApiController@UpdatePhoneNumber']);
    Route::get('resend-otp-password/{id}', ['uses' => 'ApiController@resendOtpPassword']);
    Route::post('social-signup/{type}', ['uses' => 'ApiController@socialSignup']);
    Route::post('update-address', ['uses' => 'ApiController@updateUserAddress']);
    Route::post('change-password', ['uses' => 'ApiController@changePassword']);
    Route::post('add-cart', ['uses' => 'ApiController@addToCart']);
    Route::get('get-cart/{user_id}', ['uses' => 'ApiController@getCartProduct']);
    Route::get('get-cart-item/{user_id}/{coupan_code?}', ['uses' => 'ApiController@getCartProductItem']);
    Route::post('update-cart', ['uses' => 'ApiController@updateCart']);
    Route::post('remove-cart', ['uses' => 'ApiController@removeCart']);
    Route::post('add-wishlist', ['uses' => 'ApiController@addToWishlist']);
    Route::get('get-wishlist/{user_id}', ['uses' => 'ApiController@getWishlistProfile']);
    Route::post('get-product-wishlist', ['uses' => 'ApiController@getProductWishlist']);
    Route::post('remove-wishlist', ['uses' => 'ApiController@removeWishlist']);
     Route::get('get-pincode-detail/{pin_code}', ['uses' => 'ApiController@getPincodeDetail']);
     Route::get('get-pincode-avalability/{pin_code}', ['uses' => 'ApiController@getPincodeAvalability']);
     Route::get('check-coupon-code/{user_id}/{coupon_code}', ['uses' => 'ApiController@checkCouponCode']);
     Route::post('payment/', ['uses' => 'ApiController@payment']);
     Route::post('payment-success/{order_id}/{payment_mode}', ['uses' => 'ApiController@paymentsuccess']);
     Route::get('get-order/{user_id}', ['uses' => 'ApiController@getOrder']);
     Route::post('order-cancel/{order_id}/{user_id}', ['uses' => 'ApiController@orderCancel']);
     Route::post('wallet-balance/{user_id}', ['uses' => 'ApiController@walletBalance']);
     Route::get('get-order-detail/{user_id}/{order_id}', ['uses' => 'ApiController@orderDdetail']);
     Route::get('get-wallet/{user_id}', ['uses' => 'ApiController@getWalletData']);
     Route::get('check-address-number/{user_id}', ['uses' => 'ApiController@checkAddressNumber']);
     Route::get('resend-otp-cod-mobile-number/{user_id}', ['uses' => 'ApiController@resendOtpCodPhoneNumber']);
     Route::post('cod-mobile-verify/{user_id}', ['uses' => 'ApiController@codVarifyNumber']);
     Route::post('newslatter', ['uses' => 'ApiController@newslatter']);

});

Route::group(['namespace' => 'Front'], function() {

    Route::get('facebook-login', ['uses' => 'PagesController@facebbok_redirect']);
    Route::get('facebook-callback', ['uses' => 'PagesController@facebook_callback']);
    Route::get('email-varified/{activation_key}', ['as' => 'admin.activation', 'uses' => 'PagesController@userActivation']);
    Route::get('send-mail-from-temp', ['uses' => 'CronController@sendMailFromTemp']);
    Route::get('chack-order-status', ['uses' => 'CronController@orderStatus']);
    Route::get('check-payment-status', ['uses' => 'CronController@checkPaymentStatus']);
    Route::get('send-invoice', ['uses' => 'CronController@sendInvoice']);
});


Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {

    Route::group(['middleware' => 'IsNotAuthenticated'], function() {

        Route::get('login', function() {
            return View::make('home');
        });
        Route::get('login', ['as' => 'admin.login', 'uses' => 'AuthController@getLogin']);
        Route::post('login', ['as' => 'admin.login', 'uses' => 'AuthController@postLogin']);
        Route::get('forgot-password', ['as' => 'admin.forgot_password', 'uses' => 'UsersController@forgotPassword']);

        Route::post('forgot-password', ['as' => 'admin.forgot_password', 'uses' => 'UsersController@sendPasswordLink']);
        Route::get('reset-password/{email_token}', ['as' => 'admin.reset_password', 'uses' => 'UsersController@resetPassword']);
        Route::post('reset-password/{email_token}', ['as' => 'admin.reset_password', 'uses' => 'UsersController@resetPasswordUpdate']);
    });

    Route::group(['middleware' => 'auth.admin'], function() {
        Route::resource('users', 'UsersController');
        Route::get('user-status-change/{id}/{status}', ['as' => 'admin.users.status_change', 'uses' => 'UsersController@status_change']);
        Route::get('user-email_verify/{id}/{email_verify}', ['as' => 'admin.users.email_verify', 'uses' => 'UsersController@email_verify']);
        Route::get('send-credentials/{id}', ['as' => 'admin.users.send_credentials', 'uses' => 'UsersController@sendCredentials']);
        Route::get('view-users/{id}', ['as' => 'admin.users.view', 'uses' => 'UsersController@view']);


        Route::resource('cms', 'CmsController');
        Route::resource('emailtemplates', 'EmailtemplatesController');
        Route::get('cms-status-change/{id}/{status}', ['as' => 'admin.cms.status_change', 'uses' => 'CmsController@status_change']);
        Route::get('profile', ['as' => 'admin.profile', 'uses' => 'UsersController@profile']);
        Route::post('profile', ['as' => 'admin.updateProfile', 'uses' => 'UsersController@updateProfile']);
        Route::get('change-password', ['as' => 'admin.ChangePassword', 'uses' => 'UsersController@ChangePassword']);
        Route::post('change-password', ['as' => 'admin.UpdateChangePassword', 'uses' => 'UsersController@UpdateChangePassword']);
        Route::resource('settings', 'SettingsController');
        Route::resource('dashboard', 'PagesController');
        Route::resource('emaillogs', 'EmailLogsController');
        Route::get('view-emaillogs/{id}', ['as' => 'admin.view_emaillogs', 'uses' => 'EmailLogsController@view']);

        // Route::get('/', 'PagesController@index');
        Route::get('logout', ['as' => 'admin.logout', 'uses' => 'AuthController@getLogout']);
        Route::get('/', ['as' => 'dashboard', 'uses' => 'PagesController@index']);
        Route::get('/newlatters', ['as' => 'newlatters', 'uses' => 'PagesController@newlatters']);
        Route::get('/newlatter-delete/{id}', ['as' => 'delete_newlatter', 'uses' => 'PagesController@delete_newlatter']);

        Route::get('upload-ckeditor-images', ['as' => 'upload_ckeditor_images', 'uses' => 'PagesController@uploadCkeditorImages']);

        ////////////////////***********Tags Controller************//////////////////////////////////////////////

        Route::resource('tags', 'TagsController');
        Route::get('tags-status-change/{id}/{status}', ['as' => 'admin.tags.status_change', 'uses' => 'TagsController@status_change']);

        // Category routing 
        /* Route::resource('category', 'CategoryController', [
          'except' => ['index', 'show','create','edit','update']
          ]); */
        Route::get('category/{id?}', ['as' => 'admin.category.index', 'uses' => 'CategoryController@index']);
        Route::get('edit-category/{id}/{parent_id?}', ['as' => 'admin.category.edit', 'uses' => 'CategoryController@edit']);
        Route::post('edit-category/{id}/{parent_id?}', ['as' => 'admin.category.update', 'uses' => 'CategoryController@update']);
        Route::get('add-category/{id?}', ['as' => 'admin.category.create', 'uses' => 'CategoryController@create']);
        Route::post('add-category/{id?}', ['as' => 'admin.category.store', 'uses' => 'CategoryController@store']);
        Route::get('category-status-change/{id}/{status}/{parent_id?}', ['as' => 'admin.category.status_change', 'uses' => 'CategoryController@status_change']);

        /* Admin menu route */
        Route::resource('adminmenus', 'AdminMenuController');
        Route::get('add-adminmenu/{id?}', ['as' => 'admin.adminmenus.create', 'uses' => 'AdminMenuController@create']);
        Route::post('add-adminmenu/{id?}', ['as' => 'admin.adminmenus.store', 'uses' => 'AdminMenuController@store']);
        Route::get('adminmenu-status-change/{id}/{status}/{parent_id?}', ['as' => 'admin.adminmenus.status_change', 'uses' => 'AdminMenuController@status_change']);
        Route::get('child-adminmenu/{id?}', ['as' => 'admin.adminmenus.child_menu', 'uses' => 'AdminMenuController@childMenu']);
        Route::get('edit-adminmenu/{id}/{parent_id?}', ['as' => 'admin.adminmenus.edit', 'uses' => 'AdminMenuController@edit']);
        Route::post('edit-adminmenu/{id}/{parent_id?}', ['as' => 'admin.adminmenus.update', 'uses' => 'AdminMenuController@update']);


        /* fornt menu route */


        Route::resource('attributes', 'AttributeController');
        Route::get('attribute-status-change/{id}/{status}', ['as' => 'admin.attributes.status_change', 'uses' => 'AttributeController@status_change']);

        Route::resource('site_images', 'SiteImagesController');

        /* Blocks mangemant   */
        Route::resource('blocks', 'BlocksController');

        /* Taxs mangemant   */
        Route::resource('taxs', 'TaxsController');
        Route::get('taxs-status-change/{id}/{status}', ['as' => 'admin.taxs.status_change', 'uses' => 'TaxsController@status_change']);



        Route::get('attribute-value/{id}', ['as' => 'admin.attribute_values.index', 'uses' => 'AttributeValuesController@index']);
        Route::get('add-attribute-value/{id}', ['as' => 'admin.attribute_values.create', 'uses' => 'AttributeValuesController@create']);
        Route::post('add-attribute-value/{id}', ['as' => 'admin.attribute_values.store', 'uses' => 'AttributeValuesController@store']);
        Route::get('attribute-value-status-change/{id}/{status}/{attribute_id}', ['as' => 'admin.attribute_values.status_change', 'uses' => 'AttributeValuesController@status_change']);
        Route::post('edit-attribute-value/{id}/{attribute_id}', ['as' => 'admin.attribute_values.update', 'uses' => 'AttributeValuesController@update']);
        Route::get('edit-attribute-value/{id}/{attribute_id}', ['as' => 'admin.attribute_values.edit', 'uses' => 'AttributeValuesController@edit']);



        Route::resource('products', 'ProductsController');

        Route::get('products-status-change/{id}/{status}', ['as' => 'admin.products.status_change', 'uses' => 'ProductsController@status_change']);
        Route::get('products-image-status-change/{id}/{status}/{product_id}', ['as' => 'admin.products.product_image_status_change', 'uses' => 'ProductsController@productImageStatusChange']);
        Route::get('products-image/{id}', ['as' => 'admin.products.products_image', 'uses' => 'ProductsController@productsImage']);
        Route::get('manage-stock/{id}/{from?}', ['as' => 'admin.products.manage_stock', 'uses' => 'ProductsController@ManageStock']);
        Route::post('manage-stock/{id}/{from?}', ['as' => 'admin.products.save_stock', 'uses' => 'ProductsController@saveStock']);
        Route::get('out-off-stock', ['as' => 'admin.products.out_off_stock', 'uses' => 'ProductsController@outOffStock']);
        Route::post('upload-product-images/{id}', ['as' => 'admin.products.updalod_products_image', 'uses' => 'ProductsController@updalodProductsImage']);
        Route::match(['get', 'delete'], 'delete-product-images/{id}/{product_id}', ['as' => 'admin.products.delete_products_image', 'uses' => 'ProductsController@deleteProductsImage']);
        Route::get('set-main-images/{id}/{product_id}', ['as' => 'admin.products.set_main_image', 'uses' => 'ProductsController@setMainImage']);

        Route::post('get-attribute-by-category', ['as' => 'admin.products.get_attribute_by_category', 'uses' => 'ProductsController@getAttributeByCategory']);


        // Coupen mangemant

        Route::resource('coupons', 'CouponController');
        Route::get('coupon-status-change/{id}/{status}', ['as' => 'admin.coupons.status_change', 'uses' => 'CouponController@status_change']);

        Route::resource('slider_images', 'SliderImagesController');
        Route::get('slider_images/{id}/{status}', ['as' => 'admin.slider_images.status_change', 'uses' => 'SliderImagesController@status_change']);

        /* COntact US */

        Route::resource('contacts', 'ContactsController');
        Route::get('view-contacts/{id}', ['as' => 'admin.view-contacts', 'uses' => 'ContactsController@view']);

        /* Faq management */
        Route::resource('faq_category', 'FaqCategoryController');
        Route::get('faq-category-status-change/{id}/{status}', ['as' => 'admin.faq_category.status_change', 'uses' => 'FaqCategoryController@status_change']);


        Route::get('faqs/{id}', ['as' => 'admin.faqs.index', 'uses' => 'FaqsController@index']);
        Route::get('add-faq/{id}', ['as' => 'admin.faqs.create', 'uses' => 'FaqsController@create']);
        Route::post('add-faq/{id}', ['as' => 'admin.faqs.store', 'uses' => 'FaqsController@store']);
        Route::get('faq-status-change/{id}/{status}/{category_id}', ['as' => 'admin.faqs.status_change', 'uses' => 'FaqsController@status_change']);
        Route::post('edit-faq/{id}/{category_id}', ['as' => 'admin.faqs.update', 'uses' => 'FaqsController@update']);
        Route::get('edit-faq/{id}/{category_id}', ['as' => 'admin.faqs.edit', 'uses' => 'FaqsController@edit']);

        /* Address Books mangemant   */
         Route::get('address-books/{id}', ['as'=>'admin.address_books.index','uses' => 'AddressBooksController@index']);
         Route::get('add-address-book/{id}', ['as'=>'admin.address_books.create','uses' => 'AddressBooksController@create']);
         Route::post('add-address-book/{id}', ['as'=>'admin.address_books.store','uses' => 'AddressBooksController@store']);
         Route::get('address-book-status-change/{id}/{status}/{user_id}', ['as'=>'admin.address_books.status_change','uses' => 'AddressBooksController@status_change']);    
         Route::post('edit-address-book/{id}/{user_id}', ['as'=>'admin.address_books.update','uses' => 'AddressBooksController@update']);
         Route::get('edit-address-book/{id}/{user_id}', ['as'=>'admin.address_books.edit','uses' => 'AddressBooksController@edit']);


         //Route::resource('orders', 'OrderController');
         Route::get('orders/{status?}', ['as'=>'admin.orders.index','uses' => 'OrderController@index']);
      
         Route::get('orders-detail/{id}/{status}', ['as'=>'admin.orders.show','uses' => 'OrderController@show']);
         Route::get('order-shipped/{id}/{status}', ['as' => 'admin.orders.shipped', 'uses' => 'OrderController@shipped']);
         Route::get('order-delivered/{id}/{status}', ['as' => 'admin.orders.delivered', 'uses' => 'OrderController@delivered']);
         Route::get('order-inprocess-by-postal/{id}/{status}', ['as' => 'admin.orders.inprocess_by_postal', 'uses' => 'OrderController@shippedByPostal']);
         Route::post('order-shipped-by-postal/{id}/{status}', ['as' => 'admin.orders.shipped_by_postal', 'uses' => 'OrderController@saveShippedByPostal']);
         Route::get('order-reject/{id}/{status}', ['as' => 'admin.orders.reject', 'uses' => 'OrderController@reject']);
         Route::get('order-process/{id}/{status}', ['as' => 'admin.orders.process', 'uses' => 'OrderController@process']);
         Route::get('order-package-slip-print/{id}', ['as' => 'admin.orders.package_slip_print', 'uses' => 'OrderController@packageSlipPrint']);
         Route::get('order-package-slip-download/{id}', ['as' => 'admin.orders.package_slip_download', 'uses' => 'OrderController@packageSlipDownload']); 
         Route::post('download-manifest', ['as' => 'admin.orders.download_manifest', 'uses' => 'OrderController@downloadManifest']);
         Route::get('track-orders/{status?}', ['as'=>'admin.orders.track_orders','uses' => 'OrderController@orderTrack']);
         Route::get('track/{id}', ['as'=>'admin.orders.track','uses' => 'OrderController@Track']);
         Route::get('invoice/{id}/{status?}', ['as'=>'admin.orders.invoice','uses' => 'OrderController@invoice']);
         Route::get('invoice-print/{id}', ['as'=>'admin.orders.invoice_print','uses' => 'OrderController@invoicePrint']);
         Route::get('invoice-send/{id}/{status?}', ['as'=>'admin.orders.invoice_send','uses' => 'OrderController@invoiceSend']);


    });
});


$middleware = array_merge(\Config::get('lfm.middlewares'), ['\Unisharp\Laravelfilemanager\middleware\MultiUser']);
$prefix = \Config::get('lfm.prefix', 'laravel-filemanager');
$as = 'unisharp.lfm.';
$namespace = '\Unisharp\Laravelfilemanager\controllers';

// make sure authenticated
Route::group(compact('middleware', 'prefix', 'as', 'namespace'), function () {

    // Show LFM
    Route::get('laravel-filemanager', [
        'uses' => 'LfmController@show',
        'as' => 'show'
    ]);

    // upload
    Route::any('laravel-filemanager/upload', [
        'uses' => 'UploadController@upload',
        'as' => 'upload'
    ]);

    // list images & files
    Route::get('laravel-filemanager/jsonitems', [
        'uses' => 'ItemsController@getItems',
        'as' => 'getItems'
    ]);

    // folders
    Route::get('laravel-filemanager/newfolder', [
        'uses' => 'FolderController@getAddfolder',
        'as' => 'getAddfolder'
    ]);
    Route::get('laravel-filemanager/deletefolder', [
        'uses' => 'FolderController@getDeletefolder',
        'as' => 'getDeletefolder'
    ]);
    Route::get('laravel-filemanager/folders', [
        'uses' => 'FolderController@getFolders',
        'as' => 'getFolders'
    ]);

    // crop
    Route::get('laravel-filemanager/crop', [
        'uses' => 'CropController@getCrop',
        'as' => 'getCrop'
    ]);
    Route::get('laravel-filemanager/cropimage', [
        'uses' => 'CropController@getCropimage',
        'as' => 'getCropimage'
    ]);

    // rename
    Route::get('laravel-filemanager/rename', [
        'uses' => 'RenameController@getRename',
        'as' => 'getRename'
    ]);

    // scale/resize
    Route::get('laravel-filemanager/resize', [
        'uses' => 'ResizeController@getResize',
        'as' => 'getResize'
    ]);
    Route::get('laravel-filemanager/doresize', [
        'uses' => 'ResizeController@performResize',
        'as' => 'performResize'
    ]);

    // download
    Route::get('laravel-filemanager/download', [
        'uses' => 'DownloadController@getDownload',
        'as' => 'getDownload'
    ]);

    // delete
    Route::get('laravel-filemanager/delete', [
        'uses' => 'DeleteController@getDelete',
        'as' => 'getDelete'
    ]);

    Route::get('laravel-filemanager/demo', function () {
        return view('laravel-filemanager::demo');
    });
});

            $namespace = '\Barryvdh\Debugbar\Controllers';
            $prefix  =\Config::get('debugbar.route_prefix', '_debugbar');
       
Route::group(compact('namespace','prefix'), function () {

             Route::get('open', [
                'uses' => 'OpenHandlerController@handle',
                'as' => 'debugbar.openhandler',
            ]);

             Route::get('clockwork/{id}', [
                'uses' => 'OpenHandlerController@clockwork',
                'as' => 'debugbar.clockwork',
            ]);

             Route::get('assets/stylesheets', [
                'uses' => 'AssetController@css',
                'as' => 'debugbar.assets.css',
            ]);

             Route::get('assets/javascript', [
                'uses' => 'AssetController@js',
                'as' => 'debugbar.assets.js',
            ]);
});


Route::get('{all}/{id?}/{id1?}', function () {
    return view('welcome');
});

require base_path() . '/global_constants.php';
