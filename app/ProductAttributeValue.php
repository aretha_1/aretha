<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class ProductAttributeValue extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_attribute_values';

    /**
     * The product attribute values that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];
     /*
      * get child  category  data
      *
      * */
    public function attribute_value()
    {
        return $this->belongsTo('App\AttributeValue', 'attribute_value_id')->where('status','=', 1)->orderBy('order_key', 'asc')->select('id','name','value','slug');;
    }
    /*
      * get child  category  data
      *
      * */
    public function attribute()
    {
        return $this->belongsTo('App\Attribute', 'attribute_id')->where('status','=', 1)->orderBy('order_key', 'asc')->select('id','name','slug');;;
    }
     
  

}
