<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class AddressBook extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'address_books';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];
     
     public function addressbook()
    {   
         $instance = $this->hasMany('App\User', 'id');
         $instance->getQuery()->where('status','=', 1)->orderBy('order_key', 'asc');
         return $instance;
    }
  
}
