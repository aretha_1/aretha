<?php

namespace App\Helpers;

use Html;
use File;
use Image;

class BasicFunction {

    /**
     * @description to upload images on website
     * @param 	   $image (image name to be uploading), 
     * @param 	   $uploadpath  Path for uplaoding file, 
     * @param 	   $image_prefix  prefix for image name, 
     * @param 	   $edit  if true than unlink previous uploaded image of the edit section, 
     * @param 	   $old_image  old image name we need to unlink, 
     * @return 	   image name
     */
     public static function uploadImage($image = array(), $uploadpath = '', $image_prefix = '', $edit = false, $old_image = '',$subfolder=false,$thubnail=false) {
        $image_name = '';


        if (!$image->isValid() || empty($uploadpath)) {
            return $image_name;
        }
       $edit_updaload_path  =   $uploadpath;

       if($subfolder){

            $year   =    date('Y');
            
            $uploadpath.=  $year.'/';
            if(!File::exists($uploadpath)) {
                    File::makeDirectory($uploadpath,  0777, true, true);
                    
            }
            $month   =    date('m');
            $uploadpath.=  $month.'/';
            if(!File::exists($uploadpath)) {
                    File::makeDirectory($uploadpath, 0777, true, true);
                    
            }

            $day   =    date('d');
            $uploadpath.=  $day.'/';
            if(!File::exists($uploadpath)) {
                    File::makeDirectory($uploadpath, 0777, true, true);
                   
            }
      }

       if ($image->isValid()) {
            $image_prefix = $image_prefix  . date('d_m_Y'). '_' . rand(0, 999999999);


            $ext = $image->getClientOriginalExtension();

            $image_name = $image_prefix . '.' . $ext;
   
            $image->move($uploadpath, $image_name);


            if($thubnail){

                              $thumb_folder_name = T130;
                       

                               if (!File::exists($uploadpath . $thumb_folder_name)) {
                                    File::makeDirectory($uploadpath . $thumb_folder_name, 0777, true, true);
                                }

                              $thumb_img = Image::make($uploadpath . $image_name);
                               $thumb_img->resize(T130_WIDTH, null, function ($constraint) {
                                  $constraint->aspectRatio();
                                }) ->save($uploadpath . $thumb_folder_name . $image_name);


                                $thumb_folder_name = T540;
                       

                               if (!File::exists($uploadpath . $thumb_folder_name)) {
                                    File::makeDirectory($uploadpath . $thumb_folder_name, 0777, true, true);
                                }

                              $thumb_img = Image::make($uploadpath . $image_name);
                            //   $thumb_img->resize(400)

                               // resize the image to a width of 300 and constrain aspect ratio (auto height)
                          $thumb_img->resize(T540_WIDTH, null, function ($constraint) {
                              $constraint->aspectRatio();
                          }) ->save($uploadpath . $thumb_folder_name . $image_name);
          }






            //$image_name =   $year.'/'.$month.'/'.$day.'/'.$image_name;

            if ($edit) {
                @unlink($edit_updaload_path . $old_image);
            }
        } else {
            if ($edit) {
                $image_name = $old_image;
            }
        }
      
        return $image_name;
    }

     function currentTime() {

        return time();
    }

    /**
     * @description to show images on website
     * @param      $root_path , 
     * @param      $http_path, 
     * @param      $image_name  image name, 
     * @param      $attribute all attributes of image like(height,width, class),    
     * @return     image url
     * */
     public static function showImage($root_path = '', $http_path = '', $image_name = '', $attribute = array()) {
        $alt =  Configure('CONFIG_SITE_TITLE');
        $title = Configure('CONFIG_SITE_TITLE');
        $height = '';
        $width = '';
        $class = '';
        $link_url = '';
        $type = '';
        $zc = '2';
        $ct = '0';
        if (isset($attribute['alt']) && $attribute['alt'] != '') {
            $alt = $attribute['alt'];
        }
        if (isset($attribute['title']) && $attribute['title'] != '') {
            $title = $attribute['title'];
        }
        if (isset($attribute['height']) && $attribute['height'] != '') {
            $height = $attribute['height'] . 'px';
        }

        if (isset($attribute['width']) && $attribute['width'] != '') {
            $width = $attribute['width'] . 'px';
        }
        if (isset($attribute['class']) && $attribute['class'] != '') {

            $class = $attribute['class'];
        }

        if (isset($attribute['url']) && $attribute['url'] != '') {

            $link_url = $attribute['url'];
        }

        // override Default zoom/crop setting of img.php file . 

        if (isset($attribute['zc']) && $attribute['zc'] != '') {

            $zc = $attribute['zc'];
        }

        if (isset($attribute['ct']) && $attribute['ct'] != '') {

            $ct = $attribute['ct'];
        }

        if (isset($attribute['type']) && $attribute['type'] != '') {

            $type = $attribute['type'];
        }

        if (file_exists($root_path . $image_name) && $image_name != '') {
            $url = WEBSITE_IMG_FILE_URL . '?image=' . $http_path . $image_name . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&ct=' . $ct;



            return Html::image($url, $alt, $attributes = array('class' => $class, 'title' => $title));
            //return $this->Html->image($url, array('alt' => $alt, 'class' => $class, 'url' => $link_url, "title" => $title));
        } else {

            if($type=='banner'){
                $url = WEBSITE_IMG_FILE_URL . '?image=' . 'img/banner_no.png' . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&ct=' . $ct;
            }else{

                $url = WEBSITE_IMG_FILE_URL . '?image=' . 'img/fabivo_no_image.png' . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&ct=' . $ct;

            }

            return Html::image($url, $alt, $attributes = array('class' => $class, 'title' => $title));
        }
    }   

  /**
     * @description to show images on website
     * @param 	   $root_path , 
     * @param 	   $http_path, 
     * @param 	   $image_name image name, 
     * @param 	   $attribute all attributes of image like(height,width, class), 		
     * @return 	   image url
     * */
     public static function showImageUrl($root_path = '', $http_path = '', $image_name = '', $attribute = array(),$imageUrl=false) {
        $alt =  Configure('CONFIG_SITE_TITLE');
        $title = Configure('CONFIG_SITE_TITLE');
        $height = '';
        $width = '';
        $class = '';
        $link_url = '';
        $type = '';
        $zc = '2';
        $ct = '0';
        if (isset($attribute['alt']) && $attribute['alt'] != '') {
            $alt = $attribute['alt'];
        }
        if (isset($attribute['title']) && $attribute['title'] != '') {
            $title = $attribute['title'];
        }
        if (isset($attribute['height']) && $attribute['height'] != '') {
            $height = $attribute['height'] . 'px';
        }

        if (isset($attribute['width']) && $attribute['width'] != '') {
            $width = $attribute['width'] . 'px';
        }
        if (isset($attribute['class']) && $attribute['class'] != '') {

            $class = $attribute['class'];
        }

        if (isset($attribute['url']) && $attribute['url'] != '') {

            $link_url = $attribute['url'];
        }

        // override Default zoom/crop setting of img.php file . 

        if (isset($attribute['zc']) && $attribute['zc'] != '') {

            $zc = $attribute['zc'];
        }
        $zc =2;
      
        if (isset($attribute['ct']) && $attribute['ct'] != '') {
            $ct = $attribute['ct'];
        }else{

            $ct = 'fff';
        }

        if (isset($attribute['type']) && $attribute['type'] != '') {

            $type = $attribute['type'];
        }

        if (file_exists($root_path . $image_name) && $image_name != '') {

          if($imageUrl){
                      $url = WEBSITE_PUBLIC_URL . $http_path . $image_name ;
          }else{


            $url = WEBSITE_IMG_FILE_URL . '?image=' . $http_path . $image_name . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&cc=' . $ct.'&ct=1';
        }


            return $url;
            //return $this->Html->image($url, array('alt' => $alt, 'class' => $class, 'url' => $link_url, "title" => $title));
        } else {

            if($type=='banner'){
                $noimage  ='img/banner_no.png';
               
            }else{

                $noimage =  'img/fabivo_no_image.png';

            }



                 if($imageUrl){
                        $url = WEBSITE_PUBLIC_URL . $noimage ;
                    }else{


                      $url = WEBSITE_IMG_FILE_URL . '?image=' .$noimage . '&height=' . $height . '&width=' . $width . '&zc=' . $zc . '&cc=' . $ct;
                }
               // $url = WEBSITE_PUBLIC_URL . $http_path . $image_name ;

            return $url;
        }
    }

    /**
     * Generate a unique slug.
     * If it already exists, a number suffix will be appended.
     * It probably works only with MySQL.
     *
     * @link http://chrishayes.ca/blog/code/laravel-4-generating-unique-slugs-elegantly
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @param string $value
     * @return string
     */
     public static function getUniqueSlug($model, $value) {
        $slug = \Illuminate\Support\Str::slug(trim($value));
        $slugCount = count($model->whereRaw("slug REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());

        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }


    public static function getParentCategory(){
       
      $categorylist =     \App\Category::where('parent_id','=',0)->orderBy('name')->lists('name', 'id')->toArray();;
        return $categorylist;

    }    
    public static function getCategoryList(){
       
      $categorylist =     \App\Category::where('parent_id','=',0)->orderBy('name')->lists('name', 'id')->toArray();
      $new_list_array = array();
      foreach ($categorylist as $key => $value) {
        $new_list_array[$key] =  ucfirst($value);
        $subcategory_list =     \App\Category::where('parent_id','=',$key)->orderBy('name')->lists('name', 'id')->toArray();
        foreach ($subcategory_list as $sub_key => $sub_value) {
                 $new_list_array[$sub_key]   = '&nbsp;&nbsp;&nbsp;'. ucfirst($sub_value);
        }
      }
      return $new_list_array;

    }  


    public static function getAllChildTags(){
       
      $tagslist =     \App\Tag::where('status','=',1)->orderBy('name')->lists('name', 'id')->toArray();
      $new_list_array = array();
      foreach ($tagslist as $key => $value) {
        $subtags_list =     \App\Tag::where('status','=',$key)->orderBy('name')->lists('name', 'id')->toArray();

        $new_list_array[ucfirst($value)] = array_map('ucfirst', $subtags_list);
        
      }
      return $new_list_array;

    }   

   public static function getAllChildCategory(){
       
      $categorylist =     \App\Category::where('parent_id','=',0)->orderBy('name')->lists('name', 'id')->toArray();
      $new_list_array = array();
      
      foreach ($categorylist as $key => $value) {
        $subcategory_list =     \App\Category::where('parent_id','=',$key)->orderBy('name')->lists('name', 'id')->toArray();

        $new_list_array[ucfirst($value)] = array_map('ucfirst', $subcategory_list);
        
      }
      return $new_list_array;

    }  

    public static function getAllList(){
       
      $mainMneuList =     \App\AdminMenu::where('parent_id','=',0)->where('status','=',1)->orderBy('menu_order')->get()->toArray();
     
      $new_list_array = array();
      foreach ($mainMneuList as $key => $value) {
        $new_list_array[$key] =  $value;
        $new_list_array[$key]['child_list'] =     \App\AdminMenu::where('parent_id','=',$value['id'])->where('status','=',1)->orderBy('menu_order')->get()->toArray();
        
      }

      return $new_list_array;

    } 

     public static function getAllTagsList(){
       
      $tags_list =     \App\Tag::where('status','=',1)->orderBy('name')->get()->toArray();
           
      $list   =  array();
      foreach ($tags_list as $key => $value) {
        $list[$value['id']] = ucwords($value['name']);
      }
 
      return $list;

    }

    public static function getAllUserList(){
       
      $user_list =     \App\User::where('status','=',1)->where('role_id','=',2)->orderBy('first_name')->get()->toArray();
      $list   =  array();
      foreach ($user_list as $key => $value) {
        $list[$value['id']] = ucwords($value['first_name'].' '. $value['last_name']);
      }
      
      return $list;

    }



    public static function getAttributeByCategory($category_id){




                $category_attribute = \App\CategoryAttribute::where('category_id','=',$category_id)->lists('attribute_id', 'attribute_id')->toArray();
            
                $attribute =\App\ Attribute::find($category_attribute)->toArray();
              
                $data_array     = array();        
                foreach($attribute as $key=>$value){

                    $data_array['data'][$key] = $value;
                    $attribute_values = \App\AttributeValue::where('attribute_id','=',$value['id'])->lists('name', 'id')->toArray();
                    $data_array['data'][$key]['AttributeValue'] = $attribute_values;

                }
                return $data_array;
    }



     public static function getLastQuery() {
        if (App::environment('local')) {

            // The environment is local
            DB::enableQueryLog();
            return dd(DB::getQueryLog());
        } else {

            return false;
        }
    }

 public static function generateOrderId() {
    $number =  'ORD'. generateStrongPassword(17, '', 'ud'); // better than rand()

    // call the same function if the barcode exists already
    if (static::OrderIdExists($number)) {
        return static::generateOrderId();
    }

    // otherwise, it's valid and can be used
    return $number;
}

public static function OrderIdExists($number) {
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    return \App\Order::whereOrderId($number)->exists();
}   


 public static function generateTransactionId() {
    $number =  generateStrongPassword(20, '', 'ud');// better than rand()

    // call the same function if the barcode exists already
    if (static::TransactionIdExists($number)) {
        return static::generateTransactionId();
    }

    // otherwise, it's valid and can be used
    return $number;
}

public static function TransactionIdExists($number) {
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    return \App\Order::whereTransactionId($number)->exists();
}

}

?>
