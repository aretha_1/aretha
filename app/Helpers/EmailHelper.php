<?php
namespace App\Helpers;
use App\Helpers\BasicFunction;
use App\Helpers\GlobalHelper;
use Mail;
use Config;
use view;
use App\EmailLog;
use App\TempEmailSend;
use App\userOtp;

class EmailHelper {

    public static function sendMail($to, $from, $replyTo, $subject, $layout = 'default', $body = '', $email_type = '', $attachments = "", $sendAs = 'html', $bcc = array('demosite4u5@gmail.com')) {
        $data = array();

        $data['body'] = $body;
        $data['pathToFile'] = WEBSITE_IMG_URL . 'logoemail.png';
        $temp_email_send = new TempEmailSend();
        $temp_email_send->email_to = $to;
        $temp_email_send->email_from = $from;
        $temp_email_send->email_type = $email_type;
        $temp_email_send->subject = $subject;
        $temp_email_send->attachments = $attachments;
        $temp_email_send->message = view('emails.' . $layout, $data)->render();
        $temp_email_send->save();
    }


    public static function sendMailFromTemp($to,$from,$subject,$message,$email_type,$attachments=''){

        

         $message = str_replace(array(
            '{EMAIL_SIGNATURE}',
            '{SITE_TITLE}',
            '{SUPPORT_EMAIL_ADDRESS}','{SUPPORT_CONTACT_NUMBER}'
                ), array(
            nl2br(Configure('CONFIG_EMAIL_SIGNATURE')),
            Configure('CONFIG_SITE_TITLE'),
            Configure('CONFIG_SUPPORT_MAIL'), Configure('CONFIG_PHONE_NUMBER')

                ), $message);        


        $subject = str_replace(array(
            '{EMAIL_SIGNATURE}',
            '{SITE_TITLE}',
            '{SUPPORT_EMAIL_ADDRESS}','{SUPPORT_CONTACT_NUMBER}'
                ), array(
            nl2br(Configure('CONFIG_EMAIL_SIGNATURE')),
            Configure('CONFIG_SITE_TITLE'),
            Configure('CONFIG_SUPPORT_MAIL'), Configure('CONFIG_PHONE_NUMBER')

                ), $subject); 
            
         

        
          $data['body'] = $message;
         $data['pathToFile'] = WEBSITE_IMG_URL . 'logoemail.png';
        $fromName = Configure('CONFIG_FROM_NAME');
       
            $from = Configure('CONFIG_FROMEMAIL');
        

        
            $replyTo = Configure('CONFIG_REPLY_TO_EMAIL');
        
            $bcc = array();
           // $to = 'vijaysoni1990@gmail.com';

        $datas = Mail::send('emails.layout', $data, function ($message) use ($from, $fromName, $to, $replyTo, $subject, $bcc, $attachments) {

                    $message->from($from, $fromName);

                    $message->to($to, $name = null);
                   
                    if (!empty($bcc)) {
                        $message->bcc($bcc, $name = null);
                    }
                    $message->replyTo($replyTo, $name = null);
                    $message->subject($subject);
                    // $message->priority($level);
                    if ($attachments != '') {
                        $message->attach($attachments);
                    }


                    // Get the underlying SwiftMailer message instance...
                    $message->getSwiftMessage();
                });

        $email_log = new EmailLog();
        $email_log->email_to = $to;
        $email_log->email_from = $from;
        $email_log->email_type = $email_type;
        $email_log->subject = $subject;
        $email_log->message = view('emails.layout', $data)->render();
        $email_log->save();

    }

public static function sendOrderMail($to,$from, $subject, $layout = 'default', $data = array(), $email_type = '',$attachments='') {
        

    
        $data['pathToFile'] = WEBSITE_IMG_URL . 'logoemail.png';
        $temp_email_send = new TempEmailSend();
        $temp_email_send->email_to = $to;
        $temp_email_send->email_from = $from;
        $temp_email_send->email_type = $email_type;
        $temp_email_send->subject = $subject;
        $temp_email_send->attachments = $attachments;
        $temp_email_send->message = view('emails.' . $layout, $data)->render();
        $temp_email_send->save();
    }

    public static function sendSms($mobile,$user_id,$cod=0){


        $sms_api = SMS_API;

        $enable_sms = Configure('CONFIG_ENABLE_SMS');
        $sms_user_name = Configure('CONFIG_SMS_USER_NAME');
        $sms_password = Configure('CONFIG_SMS_PASSWORD');
        $smsSender = Configure('CONFIG_SENDER_ID');
        $user = $sms_user_name.":".$sms_password;

        $otp = generateStrongPassword(6,'','d');
        $result  ="";
        //$smsBody = "Your verification code is ".$otp." Kindly enter the code & verify your mobile on zypog.com";
        if($cod==0){
            $smsBody = "Your verification code is ".$otp." . Kindly enter the code & verify your mobile on fabivo.com";
        }else{
            $smsBody = "Your Verification code is ".$otp." . Kindly enter the code to confirm your Cash on Delivery order";

        }
        //echo "<br>";
        //echo "Your verification code is 523587 . Kindly enter the code & verify your mobile on fabivo.com";

        //die;
        if($enable_sms){
            $ch = curl_init();
            $receipientno=$mobile;
            $senderID=$smsSender;
            $msgtxt=urlencode($smsBody);
            curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
            $buffer = curl_exec($ch);
            if(empty ($buffer))
            { 
                $result = "buffer is empty ";
            }
            else
            { 
                $result =  serialize($buffer); 
            }
            curl_close($ch);
           
        }
       
        $user_otp = new userOtp();
        $user_otp->user_id  = $user_id;
        $user_otp->mobile   = $mobile;
        $user_otp->message  = $smsBody;
        $user_otp->otp      = $otp;
        $user_otp->result   = $result;
        $user_otp->expire   = time() + 15*60;
        $user_otp->save();
    }   
   public static function sendOrderSms($mobile,$user_id,$order_id,$type=0,$order=array()){
        

        $sms_api = SMS_API;

        $enable_sms = Configure('CONFIG_ENABLE_SMS');
        $sms_user_name = Configure('CONFIG_SMS_USER_NAME');
        $sms_password = Configure('CONFIG_SMS_PASSWORD');
        $smsSender = Configure('CONFIG_SENDER_ID');
        $user = $sms_user_name.":".$sms_password;

        
        $result  ="";
        if($type==1){
            $smsBody = "Thank you for placing order ".$order_id." . Once the order is processed, the shipment and courier details will be sent on SMS & email";
        }   
        if($type==2){

            if($order->shipped_by==Config::get('global.shipped_by.delhivery')){
                $smsBody = "Your order from fabivo.com has been shipped via ".$order->shipped_by."  AWB ".$order->awb_number." ";
            }else{

                $smsBody = "Your order from fabivo.com has been shipped via ".$order->post_by."  AWB ".$order->awb_number." ";
            }
        }
       
        if($type==3){


                    $smsBody =  "You have cancelled your order ".$order_id." from fabivo.com";

        } 
        
        if($type==5){


                    $smsBody =  $order->cash_back_amount." cashback credited to your FABIVO wallet against order id ".$order_id." . Happy Shopping!";

        }   
        if($type==6){


                    $smsBody =  "Your Order id ".$order_id." from Fabivo.com has been delivered. Thank you for Shopping with us.";

        }

        if($enable_sms){
            $ch = curl_init();
            $receipientno=$mobile;
            $senderID=$smsSender;
            $msgtxt=urlencode($smsBody);
            curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
            $buffer = curl_exec($ch);
            if(empty ($buffer))
            { 
                $result = "buffer is empty ";
            }
            else
            { 
                $result =  serialize($buffer); 
            }
            curl_close($ch);
           
        }
       
        $user_otp = new userOtp();
        $user_otp->user_id  = $user_id;
        $user_otp->mobile   = $mobile;
        $user_otp->message  = $smsBody;
        $user_otp->otp      = '';
        $user_otp->result   = $result;
        $user_otp->expire   = '';
        $user_otp->save();
    }   








}

?>