<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Category extends Model
{
      use  Sortable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $guarded = ['id'];
     
     
     /*
      * get parent category  data
      *
      * */
     public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }
/*
      * get child  category  data
      *
      * */
    public function children()
    {
        $instance =  $this->hasMany('App\Category', 'parent_id');
        $instance->getQuery()->where('status','=', 1);
        return $instance;
    }  

    public function children_list()
    {
        $instance =  $this->hasMany('App\Category', 'parent_id');
        $instance->getQuery()->where('status','=', 1)->select('id', 'name', 'slug','parent_id');
        return $instance;
    }
}
