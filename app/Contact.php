<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Contact extends Model {
       public $timestamps = true;
    use  Sortable;
    protected $table = 'contact_enquiries';
    protected $guarded = ['id'];
      

}
