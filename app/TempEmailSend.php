<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TempEmailSend extends Model {
    protected $table = 'temp_email_sends';
    protected $guarded = ['id'];
}
