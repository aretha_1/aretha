<?php

######## DEFINE FOLDER & PATH CONSTANTS ##########
if (!defined('DS')) {
    define("DS", '/');
}

if (!defined('MAIN_FOLDER')) {
    define("MAIN_FOLDER",  basename(__DIR__).'/');
}
if (!defined('PUBLIC_FOLDER')) {
    define("PUBLIC_FOLDER",  'public/');
}
if (!defined('ADMIN_FOLDER')) {
    define('ADMIN_FOLDER', 'admin/');
}
if (!defined('SITE_ROOT_PATH')) {
    define('SITE_ROOT_PATH', base_path());
}

if (!defined('SITE_WEBROOT_PATH')) {
    define('SITE_WEBROOT_PATH', public_path().'/');
}

if (!defined('SITE_URL')) {
    define("SITE_URL",url('/'));
}
if (!defined('ADMIN_URL')) {
    define("ADMIN_URL",SITE_URL.'/'.ADMIN_FOLDER);
}
if (!defined('WEBSITE_URL')) {
    define("WEBSITE_URL",url('/').'/');
}
if (strpos(WEBSITE_URL,'public') == false) {
   if (!defined('WEBSITE_PUBLIC_URL')) {
     define("WEBSITE_PUBLIC_URL",WEBSITE_URL.'public/');
  }
} else {
  if (!defined('WEBSITE_PUBLIC_URL')) {
     define("WEBSITE_PUBLIC_URL",WEBSITE_URL. '/');
  }
}




if (!defined('WEBSITE_ADMIN_URL')) {
    define("WEBSITE_ADMIN_URL", WEBSITE_URL . ADMIN_FOLDER);
}
if (!defined('WEBSITE_IMG_URL')) {
    define("WEBSITE_IMG_URL", WEBSITE_PUBLIC_URL . 'img' . '/');
}
if (!defined('WEBSITE_ADMIN_IMG_URL')) {
    define("WEBSITE_ADMIN_IMG_URL", WEBSITE_URL . ADMIN_FOLDER . 'img' . '/');
}




if (!defined('WEBSITE_IMG_FILE_URL')) {
    define("WEBSITE_IMG_FILE_URL", WEBSITE_PUBLIC_URL . 'image.php');
}


    ######## DEFINE FOLDER & PATH CONSTANTS ##########
    ######## COMMON CONSTANTS FOR DATE & TIME FORMAT ############

if (!defined('CONCATE_STRING')) {
    define("CONCATE_STRING", '@#@');
}
if (!defined('ENCRYPT_SLAT_STRING')) {
    define("ENCRYPT_SLAT_STRING", '6354$#@!*|KARJKP');
}

if (!defined('DATE_SEPERATOR')) {
    define("DATE_SEPERATOR", '/');
}
if (!defined('SQL_DATE_SEPERATOR')) {
    define("SQL_DATE_SEPERATOR", '-');
}
if (!defined('TIME_SEPERATOR')) {
    define("TIME_SEPERATOR", ':');
}
if (!defined('DATE_FORMATE')) {
    define('DATE_FORMATE', 'd' . DATE_SEPERATOR . 'm' . DATE_SEPERATOR . 'Y');
}



if (!defined('DATE_TIME_FORMATE_DATE_PICKER')) {
    define('DATE_TIME_FORMATE_DATE_PICKER', 'm' . DATE_SEPERATOR . 'd' . DATE_SEPERATOR . 'Y' . ' H' . TIME_SEPERATOR . 'i');
}

if (!defined('TIME_FORMATE')) {
    define('TIME_FORMATE', 'h' . TIME_SEPERATOR . 'i' . " A");
}

if (!defined('DATE_FORMATE_DATE_PICKER')) {
    define('DATE_FORMATE_DATE_PICKER', 'm' . DATE_SEPERATOR . 'd' . DATE_SEPERATOR . 'Y');
}
if (!defined('DATE_TIME_FORMATE')) {
    define('DATE_TIME_FORMATE', 'm' . DATE_SEPERATOR . 'd' . DATE_SEPERATOR . 'Y' . ', h' . TIME_SEPERATOR . 'i' . " A");
}
if (!defined('DATE_FORMATE_JS')) {
    define('DATE_FORMATE_JS', 'mm' . DATE_SEPERATOR . 'dd' . DATE_SEPERATOR . 'yyyy');
}

if (!defined('DATE_TIME_FORMATE_JS')) {
    define('DATE_TIME_FORMATE_JS', 'mm' . DATE_SEPERATOR . 'dd' . DATE_SEPERATOR . 'yyyy' . ' hh' . TIME_SEPERATOR . 'ii');
}

if (!defined('MESSAGE_DATE_FORMATE')) {
    define('MESSAGE_DATE_FORMATE', 'M. d, Y');
}
if (!defined('MESSAGE_TIME_FORMATE')) {
    define('MESSAGE_TIME_FORMATE', 'h:i a');
}
if (!defined('MESSAGE_DATE_TIME_FORMATE')) {
    define('MESSAGE_DATE_TIME_FORMATE', 'M. d, Y h:i a');
}

if (!defined('COMMENT_DATE_FORMATE')) {
    define('COMMENT_DATE_FORMATE', 'F j, Y h:i a');
}

if (!defined('EXP_DATE_FORMATE')) {
    define('EXP_DATE_FORMATE', 'F j, Y');
}
if (!defined('STATUS_DATE_FORMATE')) {
    define('STATUS_DATE_FORMATE', 'j F Y');
}
if (!defined('WALLEt_DATE_FORMATE')) {
    define('WALLEt_DATE_FORMATE', 'd-m-Y h:i a');
}
if (!defined('MYSQL_DATE_FORMATE')) {
    define('MYSQL_DATE_FORMATE', 'Y-m-d H:i:s');
}



if (!defined('FACEBOOK_CALLBACK_URL')) {
    define('FACEBOOK_CALLBACK_URL', WEBSITE_URL.'facebook-callback');
}


######## COMMON CONSTANTS FOR DATE & TIME FORMAT ############

######## CONSTANTS FOR IMAGES START HERE ############

if (!defined('STUFF_ROOT_PATH')) {
    define('STUFF_ROOT_PATH', SITE_WEBROOT_PATH . 'stuff');
}
if (!defined('STUFF_FOLDER_URL')) {
    define('STUFF_FOLDER_URL', WEBSITE_PUBLIC_URL . 'stuff/');
}
if (!defined('ONTHEFLY_IMAGE_ROOT_PATH')) {
    define('ONTHEFLY_IMAGE_ROOT_PATH', 'stuff/');
}

/* SYSTEM IMAGES */
if (!defined('SYSTEM_IMAGE_URL')) {
    define('SYSTEM_IMAGE_URL', STUFF_FOLDER_URL . 'system_images/');
}

if (!defined('SYSTEM_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('SYSTEM_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'system_images' . DS);
}
if (!defined('SYSTEM_IMAGE_ONTHEFLY_IMAGE_PATH')) {
    define('SYSTEM_IMAGE_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "system_images/");
}

/* Profile Image */
if (!defined('PROFILE_IMAGES_URL')) {
    define('PROFILE_IMAGES_URL', STUFF_FOLDER_URL . 'profile_image/');
}

if (!defined('PROFILE_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('PROFILE_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'profile_image' . DS);
}
if (!defined('PROFILE_IMAGES_ONTHEFLY_IMAGE_PATH')) {
    define('PROFILE_IMAGES_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "profile_image/");
}

/* Site Image */
if (!defined('SITE_IMAGES_URL')) {
    define('SITE_IMAGES_URL', STUFF_FOLDER_URL . 'site_images/');
}

if (!defined('SITE_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('SITE_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'site_images' . DS);
}
if (!defined('SITE_IMAGES_ONTHEFLY_IMAGE_PATH')) {
    define('SITE_IMAGES_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "site_images/");
}
/* SLider Image */
if (!defined('SLIDER_IMAGES_URL')) {
    define('SLIDER_IMAGES_URL', STUFF_FOLDER_URL . 'slider_images/');
}

if (!defined('SLIDER_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('SLIDER_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'slider_images' . DS);
}
if (!defined('SLIDER_IMAGES_ONTHEFLY_IMAGE_PATH')) {
    define('SLIDER_IMAGES_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "slider_images/");
}
/* Admin Menu Image */
if (!defined('MENU_IMAGES_URL')) {
    define('MENU_IMAGES_URL', STUFF_FOLDER_URL . 'admin_menu/');
}

if (!defined('MENU_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('MENU_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'admin_menu' . DS);
}
if (!defined('MENU_IMAGES_ONTHEFLY_IMAGE_PATH')) {
    define('MENU_IMAGES_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "admin_menu/");
}


/* Admin Menu Image */
if (!defined('PRODUCT_IMAGES_URL')) {
    define('PRODUCT_IMAGES_URL', STUFF_FOLDER_URL . 'products/');
}

if (!defined('PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('PRODUCT_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'products' . DS);
}
if (!defined('PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH')) {
    define('PRODUCT_IMAGES_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "products/");
}
if (!defined('THUMB')) {
    define('THUMB', 0);
}

if (!defined('T130_WIDTH')) {
    define('T130_WIDTH','90');
}
if (!defined('T540_WIDTH')) {
    define('T540_WIDTH','540');
}

if (!defined('T130')) {
    //define('T130', 'thumb130/');
    define('T130', '');
}


if (!defined('T540')) {
    //define('T540', 'thumb540/');
    define('T540', '');
}

/* Admin Menu Image */
if (!defined('CATEGORY_IMAGES_URL')) {
    define('CATEGORY_IMAGES_URL', STUFF_FOLDER_URL . 'category/');
}

if (!defined('CATEGORY_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('CATEGORY_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'category' . DS);
}
if (!defined('CATEGORY_IMAGES_ONTHEFLY_IMAGE_PATH')) {
    define('CATEGORY_IMAGES_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "category/");
}

if (!defined('SLIP_UPLOAD_DIRECTROY_PATH')) {
    define('SLIP_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'slip' . DS);
}
if (!defined('INVOICE_UPLOAD_DIRECTROY_PATH')) {
    define('INVOICE_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'invoice' . DS);
}

/* Admin Blocks Image */
if (!defined('BLOCKS_IMAGES_URL')) {
    define('BLOCKS_IMAGES_URL', STUFF_FOLDER_URL . 'blocks/');
}

if (!defined('BLOCKS_IMAGES_UPLOAD_DIRECTROY_PATH')) {
    define('BLOCKS_IMAGES_UPLOAD_DIRECTROY_PATH', STUFF_ROOT_PATH . DS . 'blocks' . DS);
}
if (!defined('BLOCKS_IMAGES_ONTHEFLY_IMAGE_PATH')) {
    define('BLOCKS_IMAGES_ONTHEFLY_IMAGE_PATH', ONTHEFLY_IMAGE_ROOT_PATH . "blocks/");
}

if (!defined('SMS_API')) {
    define('SMS_API', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose');
}

if (!defined('TEST_DELHIVERY_TRACK_URL')) {
    define('TEST_DELHIVERY_TRACK_URL', 'https://test.delhivery.com/p/');
}
if (!defined('PROD_DELHIVERY_TRACK_URL')) {
    define('PROD_DELHIVERY_TRACK_URL', 'https://track.delhivery.com/p/');
}


if (!defined('TEST_FETCH_WAYBILL_URL')) {
    define('TEST_FETCH_WAYBILL_URL', 'https://test.delhivery.com/waybill/api/bulk/json/');
}
if (!defined('PROD_FETCH_WAYBILL_URL')) {
    define('PROD_FETCH_WAYBILL_URL', 'https://track.delhivery.com/waybill/api/bulk/json/');
}

if (!defined('TEST_CREATE_PACKAGE_URL')) {
    define('TEST_CREATE_PACKAGE_URL', 'https://test.delhivery.com/api/cmu/create.json');
}
if (!defined('PROD_CREATE_PACKAGE_URL')) {
    define('PROD_CREATE_PACKAGE_URL', 'https://track.delhivery.com/api/cmu/create.json');
}
if (!defined('TEST_PACKING_SLIP_URL')) {
    define('TEST_PACKING_SLIP_URL', 'https://test.delhivery.com/api/p/packing_slip');
}
if (!defined('PROD_PACKING_SLIP_URL')) {
    define('PROD_PACKING_SLIP_URL', 'https://track.delhivery.com/api/p/packing_slip');
}
if (!defined('TEST_PACKAGES_URL')) {
    define('TEST_PACKAGES_URL', 'https://test.delhivery.com/api/v1/packages/json');
}
if (!defined('PROD_PACKAGES_URL')) {
    define('PROD_PACKAGES_URL', 'https://track.delhivery.com/api/packages/');
}


if (!defined('TEST_PAYTM_STATUS_QUERY_URL')) {
    define('TEST_PAYTM_STATUS_QUERY_URL', 'https://pguat.paytm.com/oltp/HANDLER_INTERNAL/TXNSTATUS');
}
if (!defined('PROD_PAYTM_STATUS_QUERY_URL')) {
    define('PROD_PAYTM_STATUS_QUERY_URL', 'https://secure.paytm.in/oltp/HANDLER_INTERNAL/TXNSTATUS');
}
if (!defined('INVOICE_START')) {
    define('INVOICE_START', 10000);
}
