<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpseclib\\' => array($vendorDir . '/phpseclib/phpseclib/phpseclib'),
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/reflection-docblock/src', $vendorDir . '/phpdocumentor/type-resolver/src'),
    'XdgBaseDir\\' => array($vendorDir . '/dnoegel/php-xdg-base-dir/src'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Unisharp\\Laravelfilemanager\\' => array($vendorDir . '/unisharp/laravel-filemanager/src'),
    'Symfony\\Polyfill\\Util\\' => array($vendorDir . '/symfony/polyfill-util'),
    'Symfony\\Polyfill\\Php56\\' => array($vendorDir . '/symfony/polyfill-php56'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'SuperClosure\\' => array($vendorDir . '/jeremeamia/SuperClosure/src'),
    'Scopdrag\\LaravelGoogleChart\\' => array($vendorDir . '/scopdrag/laravel-google-chart/src'),
    'PulkitJalan\\GeoIP\\' => array($vendorDir . '/pulkitjalan/geoip/src'),
    'Psy\\' => array($vendorDir . '/psy/psysh/src/Psy'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'PhpParser\\' => array($vendorDir . '/nikic/php-parser/lib/PhpParser'),
    'Omnipay\\Paytm\\' => array($vendorDir . '/sumityadav/omnipay-paytm/src'),
    'Omnipay\\PayUBiz\\' => array($vendorDir . '/jaysson/omnipay-payubiz/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Mayoz\\Omnipay\\' => array($vendorDir . '/mayoz/laravel-omnipay/src'),
    'MaxMind\\Db\\' => array($vendorDir . '/maxmind-db/reader/src/MaxMind/Db'),
    'MaxMind\\' => array($vendorDir . '/maxmind/web-service-common/src'),
    'League\\Flysystem\\' => array($vendorDir . '/league/flysystem/src'),
    'Kyslik\\ColumnSortable\\' => array($vendorDir . '/kyslik/column-sortable/src/ColumnSortable'),
    'Jenssegers\\Agent\\' => array($vendorDir . '/jenssegers/agent/src'),
    'Intervention\\Image\\' => array($vendorDir . '/intervention/image/src/Intervention/Image'),
    'Illuminate\\Html\\' => array($vendorDir . '/illuminate/html'),
    'Illuminate\\' => array($vendorDir . '/laravel/framework/src/Illuminate'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'GeoIp2\\' => array($vendorDir . '/geoip2/geoip2/src'),
    'FontLib\\' => array($vendorDir . '/phenx/php-font-lib/src/FontLib'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Dompdf\\' => array($vendorDir . '/dompdf/dompdf/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'DebugBar\\' => array($vendorDir . '/maximebf/debugbar/src/DebugBar'),
    'Cron\\' => array($vendorDir . '/mtdowling/cron-expression/src/Cron'),
    'Composer\\CaBundle\\' => array($vendorDir . '/composer/ca-bundle/src'),
    'ClassPreloader\\' => array($vendorDir . '/classpreloader/classpreloader/src'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
    'Barryvdh\\DomPDF\\' => array($vendorDir . '/barryvdh/laravel-dompdf/src'),
    'Barryvdh\\Debugbar\\' => array($vendorDir . '/barryvdh/laravel-debugbar/src'),
    'App\\' => array($baseDir . '/app'),
);
