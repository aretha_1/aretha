<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="initial-scale=1.0" />
      <meta name="format-detection" content="telephone=no" />
      <title></title>
      <style type="text/css">
         body {
         width: 100%;
         margin: 0;
         padding: 0;
         -webkit-font-smoothing: antialiased;
         }
         @media  only screen and (max-width: 600px) {
         table[class="table-row"] {
         float: none !important;
         width: 98% !important;
         padding-left: 20px !important;
         padding-right: 20px !important;
         }
         table[class="table-row-fixed"] {
         float: none !important;
         width: 98% !important;
         }
         table[class="table-col"], table[class="table-col-border"] {
         float: none !important;
         width: 100% !important;
         padding-left: 0 !important;
         padding-right: 0 !important;
         table-layout: fixed;
         }
         td[class="table-col-td"] {
         width: 100% !important;
         }
         table[class="table-col-border"] + table[class="table-col-border"] {
         padding-top: 12px;
         margin-top: 12px;
         border-top: 1px solid #E8E8E8;
         }
         table[class="table-col"] + table[class="table-col"] {
         margin-top: 15px;
         }
         td[class="table-row-td"] {
         padding-left: 0 !important;
         padding-right: 0 !important;
         }
         table[class="navbar-row"] , td[class="navbar-row-td"] {
         width: 100% !important;
         }
         img {
         max-width: 100% !important;
         display: inline !important;
         }
         img[class="pull-right"] {
         float: right;
         margin-left: 11px;
         max-width: 125px !important;
         padding-bottom: 0 !important;
         }
         img[class="pull-left"] {
         float: left;
         margin-right: 11px;
         max-width: 125px !important;
         padding-bottom: 0 !important;
         }
         table[class="table-space"], table[class="header-row"] {
         float: none !important;
         width: 98% !important;
         }
         td[class="header-row-td"] {
         width: 100% !important;
         }
         }
         @media  only screen and (max-width: 480px) {
         table[class="table-row"] {
         padding-left: 16px !important;
         padding-right: 16px !important;
         }
         }
         @media  only screen and (max-width: 320px) {
         table[class="table-row"] {
         padding-left: 12px !important;
         padding-right: 12px !important;
         }
         }
         @media  only screen and (max-width: 608px) {
         td[class="table-td-wrap"] {
         width: 100% !important;
         }
         }
      </style>
   </head>
   <body style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;" bgcolor="#E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
      <table width="100%" height="100%" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
         <tr>
            <td width="100%" align="center" valign="top" bgcolor="#E4E6E9" style="background-color:#E4E6E9; min-height: 200px;">
               <table>
                  <tr>
                     <td class="table-td-wrap" align="center" width="80%">
                    
                        <table class="table-row" style="table-layout: auto; padding: 20px; width: 600px; background-color: #48b5dc;" bgcolor="#FFFFFF" width="600" cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr height="55px" style="padding-right: 16px; padding-bottom: 12px; font-family:Conv_Raleway-Regular, Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color:#8e8e8e;font-size: 13px; font-weight: normal;">
                                 <td class="table-row-td" style="height: 55px; padding-right: 16px; font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; vertical-align: middle;" valign="middle" align="left">
                                    <a href="{{ WEBSITE_URL }}" target="_blank" style="color: #428bca; text-decoration: none; padding: 0px; font-size: 18px; line-height: 20px; height: 50px; background-color: transparent;">
                                    
                                       <img src="{{ $pathToFile }}" alt="Fabivo"> 
                                     </a>
                                </td>
                                   
                                 
                                 <td class="table-row-td" style="height: 55px; font-size: 20px; color: #fff; font-family: Roboto, sans-serif; line-height: 19px; color: #fff; font-size: 20px; font-weight: normal; text-align: right; vertical-align: middle;" align="right" valign="middle">
                                     <a href="{{ WEBSITE_URL }}page/about-us" target="_blank" style="color: #fff; text-decoration: none; font-size: 16px;   text-transform: uppercase; background-color: transparent;">
                                    About Us
                                    </a>
                                     |
                                    <a href="{{ WEBSITE_URL }}contact-us" target="_blank" style="color: #fff; text-decoration: none; font-size: 16px;    text-transform: uppercase;background-color: transparent;">
                                    Contact
                                    </a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                       
                       
           
                        {!! $body !!}
                       <table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left"> </td>
                              </tr>
                           </tbody>
                        </table>
                       <table class="table-space" height="24" style="height: 24px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td class="table-space-td" valign="middle" height="24" style="height: 24px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="center">
                                     
                                    <table bgcolor="#E8E8E8" height="0" width="100%" cellspacing="0" cellpadding="0" border="0">
                                       <tbody>
                                          <tr>
                                             <td bgcolor="#E8E8E8" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left"> </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <table class="table-row" width="600" bgcolor="#FFFFFF" style="padding-bottom: 20px;  table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td class="table-row-td" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
                                    <table class="table-col" align="left" width="273" style="padding-right: 18px; table-layout: fixed;" cellspacing="0" cellpadding="0" border="0">
                                       <tbody>
                                          <tr>
                                             <td class="table-col-td" width="255" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
                                                <table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
                                                   <tbody>
                                                      <tr>
                                                         <td class="header-row-td" width="255" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; font-weight: normal; line-height: 19px; color: #00aeef; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left">Our Social Channels</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <div style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 36px; color: #444444; font-size: 13px;">
                                                    <a href="{{ Configure('CONFIG_TWITTER_LINK') }}" target="_blank" style="color: #ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid #6fb3e0; padding: 4px 9px; font-size: 14px; line-height: 19px; background-color: #6fb3e0;">Twitter</a>
                                                   <a href="{{ Configure('CONFIG_FACEBOOK_LINK') }}" target="_blank" style="color: #6688a6; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border-width: 1px 1px 2px; border-style: solid; border-color: #8aafce; padding: 6px 12px; font-size: 14px; line-height: 20px; background-color: #ffffff;">Facebook</a>
                                                   <a href="{{ Configure('CONFIG_GOOGLE_PLUS_LINK') }}" target="_blank" style="color: #b7837a; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border-width: 1px 1px 2px; border-style: solid; border-color: #d7a59d; padding: 6px 12px; font-size: 14px; line-height: 20px; background-color: #ffffff;">Google+</a>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table class="table-col" align="left" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
                                       <tbody>
                                          <tr>
                                             <td class="table-col-td" width="255" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="right">
                                                <table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
                                                   <tbody>
                                                      <tr>
                                                         <td class="header-row-td" width="255" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; font-weight: normal; line-height: 19px; color: #00aeef; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="right">Our Contact Info</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                Phone: {{ Configure('CONFIG_PHONE_NUMBER') }}
                                                <br>
                                                Email:<a style="color:#48b5dc;" href="mailto:{{ Configure('CONFIG_FROMEMAIL') }}" target="_blank">{{ Configure('CONFIG_FROMEMAIL') }}</a>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>

                  
                        <table class="table-row" width="600" bgcolor="#48b5dc" style="table-layout: fixed; background-color: #48b5dc;" cellspacing="0" cellpadding="0" border="0">   
                           <tbody>
                              <tr>
                                 <td class="table-row-td" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
                                    <table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;">
                                       <tbody>
                                          <tr>
                                             <td class="table-col-td" width="528" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #444444; font-size: 18px; font-weight: normal;" valign="top" align="left">
                                                <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 528px; background-color: #ffffff;" width="528" bgcolor="#48b5dc" cellspacing="0" cellpadding="0" border="0">
                                                   <tbody>
                                                      <tr>
                                                         <td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 528px; " width="528" bgcolor="#48b5dc" align="left"> </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <div style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #fff; font-size: 17px; text-align: center;">Copyright &copy; Fabivo. All Rights Reserved. </div>
                                                <table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 528px; " width="528"  cellspacing="0" cellpadding="0" border="0">
                                                   <tbody>
                                                      <tr>
                                                         <td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 528px; " width="528"  align="left"> </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <div style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; line-height: 19px; color: #bbbbbb; font-size: 17px; text-align: center;">
                                                        <a href="{{ WEBSITE_URL }}page/terms-and-conditions" target="_blank" style="color: #fff; text-decoration: none;">Terms and Conditions |</a>
                                                    
                                                   <a href="{{ WEBSITE_URL }}page/privacy-policy" target="_blank" style="color: #fff; text-decoration: none; ">Privacy Policy</a>
                                                    
                                                   
                                                </div>
                                                <table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 528px; " width="528" cellspacing="0" cellpadding="0" border="0">
                                                   <tbody>
                                                      <tr>
                                                         <td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 528px; " width="528"  align="left"> </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <table class="table-space" height="8" style="height: 8px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
                           <tbody>
                              <tr>
                                 <td class="table-space-td" valign="middle" height="8" style="height: 8px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left"> </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>
