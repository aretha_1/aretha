<head>
  <meta charset="UTF-8" name="csrf-token" content="{{ csrf_token() }}">
 <title>{{
     isset($title) ? config('settings.CONFIG_SITE_TITLE')." :: ".$title : config('settings.CONFIG_SITE_TITLE') }}
  </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.4 -->
  {!! Html::style( asset('css/bootstrap.min.css')) !!}
  {!! Html::style( asset('css/modern-business.css')) !!}
  {!! Html::style( asset('font-awesome/css/font-awesome.min.css')) !!}
  {!! Html::style( asset('css/custom.css')) !!}


 


 {!! Html::script( asset('js/jquery.js')) !!}
 {!! Html::script( asset('js/bootstrap.min.js')) !!}
   <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
</head>

