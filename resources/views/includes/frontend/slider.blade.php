<!--slider-->
<div id="home-page-slider" class="helium-slider loading">
      <ul class="slide-nav">
  </ul>
      <div class="slide-window">
    <ul class="slide-holder">
          <li>
        <div class="slide"><img src="{{asset("images/banner-1.jpg")}}"></div>
        <div class="pane caption">Immergiti in un'esperienza completamente nuova <span class="cap-description">Guardarsi intorno <br>
          non è mai stato così facile</span> <a href="#" class="btn">Start Exploring</a> <a href="#" class="btn-down"></a> </div>
      </li>
          <li>
        <div class="slide"><img src="{{asset("images/banner-2.jpg")}}"></div>
        <div class="pane caption">Dove vuoi. Quando vuoi. Quello che vuoi<span class="cap-description">A portata di click</span> <a href="#" class="btn">Map search</a> <a href="#" class="btn-down"></a> </div>
      </li>
          <li>
        <div class="slide"><img src="{{asset("images/banner-3.jpg")}}"></div>
        <div class="pane caption">I negozi così, non li avevi mai visti<span class="cap-description">E'semplice: Guarda Cerca Vai</span> <a href="#" class="btn">Shop Listing</a> <a href="#" class="btn-down"></a> </div>
      </li>
        </ul>
  </div>
    </div>
<!---End Slider--> 