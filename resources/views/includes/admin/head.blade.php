<head>
  <meta charset="UTF-8" name="csrf-token" content="{{ csrf_token() }}">
  <title>{{
     isset($title) ? config('settings.CONFIG_SITE_TITLE')." :: ".$title : config('settings.CONFIG_SITE_TITLE') }}
  </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
     <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
  <!-- Bootstrap 3.3.4 -->
  {!! Html::style( asset('admin/bootstrap/css/bootstrap.min.css')) !!}
  {!! Html::style( asset('css/font-awesome.css')) !!}
  {!! Html::style( asset('css/ionicons.css')) !!}
  <!-- FontAwesome 4.3.0 
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  -->
  <!-- Ionicons 2.0.0 
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  -->
  <!-- Theme style -->

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->



  <!-- iCheck -->
  {!! Html::style( asset('admin/plugins/iCheck/flat/blue.css')) !!}
  <!-- Morris chart -->


  <!-- Date Picker -->
  {!! Html::style( asset('admin/plugins/datepicker/datepicker3.css')) !!}
  <!-- Daterange picker -->
  {!! Html::style( asset('admin/plugins/daterangepicker/daterangepicker-bs3.css')) !!}
  <!-- bootstrap wysihtml5 - text editor -->
  
  
  <!-- Select2 -->
  {!! Html::style( asset('admin/plugins/select2/select2.min.css')) !!}
 

  <!-- Bootstrap time Picker -->
  {!! Html::style( asset('admin/plugins/timepicker/bootstrap-timepicker.min.css')) !!}
  {!! Html::style( asset('css/bootstrap-multiselect.css')) !!}
  <!-- iCheck for checkboxes and radio inputs -->
  {!! Html::style( asset('admin/plugins/iCheck/all.css')) !!}
    {!! Html::style( asset('admin/css/jquery.noty.css')) !!}
  {!! Html::style( asset('admin/css/noty_theme_default.css')) !!}
    {!! Html::style( asset('admin/dist/css/AdminLTE.min.css')) !!}
      {!! Html::style( asset('admin/dist/css/skins/_all-skins.min.css')) !!}
      {!! Html::style( asset('admin/css/custom.css')) !!}
 
  
<!-- jQuery 2.1.4 -->
{!! Html::script( asset('admin/plugins/jQuery/jQuery-2.1.4.min.js')) !!}
{!! Html::script( asset('js/jquery-ui.min.js')) !!}
<!-- jQuery UI 1.11.4 -->


<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script type="text/javascript">

  var IMAGE_URL   =   "{!! WEBSITE_ADMIN_IMG_URL !!}";
  var WEBSITE_ADMIN_URL   =   "{!! WEBSITE_ADMIN_URL !!}";
  var WEBSITE_URL   =   "{!! WEBSITE_URL !!}";
  var csrf_token          = "{{ csrf_token()}}";
  var date_format          = "{{ DATE_FORMATE_JS }}";
  var CONFIG_ALLOWED_IMAGE_TYPE          = "{{ config('global.image_mime_type') }}";
  var UPLOAD_FILE_SIZE_IMAGE          = {{ config('global.file_max_upload_size') }};
  var MAX_FILE_COUNT_UPLOAD          = {{ config('global.max_file_count_upload') }};

</script>
<!-- Bootstrap 3.3.2 JS -->
{!! Html::script(asset('admin/bootstrap/js/bootstrap.min.js')) !!}
{!! Html::script(asset('js/jquery.noty.js')) !!}
{!! Html::script(asset('js/jquery.blockUI.js')) !!}
<!-- Morris.js charts -->

{!! Html::script( asset('admin/plugins/daterangepicker/daterangepicker.js')) !!}
<!-- datepicker -->
{!! Html::script( asset('admin/plugins/datepicker/bootstrap-datepicker.js')) !!}
<!-- Bootstrap WYSIHTML5 -->
{!! Html::script( asset('ckeditor/ckeditor.js')) !!}
{!! Html::script( asset('ckeditor/config.js')) !!}
<!-- Slimscroll -->
{!! Html::script( asset('admin/plugins/slimScroll/jquery.slimscroll.min.js')) !!}

<!-- adminLTE App -->
{!! Html::script( asset('admin/dist/js/app.min.js')) !!}
<!-- Select2 -->
{!! Html::script( asset('admin/plugins/select2/select2.full.min.js')) !!}
<!-- date-range-picker -->

{!! Html::script( asset('admin/plugins/daterangepicker/daterangepicker.js')) !!}
<!-- bootstrap time picker -->
{!! Html::script( asset('admin/plugins/timepicker/bootstrap-timepicker.min.js')) !!}
<!-- bootstrap color picker -->
{!! Html::script( asset('admin/plugins/colorpicker/bootstrap-colorpicker.min.js')) !!}


<!-- iCheck 1.0.1 -->
{!! Html::script( asset('admin/plugins/iCheck/icheck.min.js')) !!}
{!! Html::script( asset('admin/js/bootbox.min.js')) !!}
{!! Html::script( asset('js/bootstrap-multiselect.js')) !!}
<!-- AdminLTE for demo purposes -->
{!! Html::script( asset('admin/dist/js/demo.js')) !!}
{!! Html::script( asset('admin/js/global.js')) !!}

<!-- Page script -->
<script>
    
    

    $(function () {

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})
      
       if($('#edit-editor-content').length>0){  
CKEDITOR.replace('edit-editor-content',{
  customConfig: '/public/ckeditor'
});
 }
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0'
        });
        
    });
</script>
<script type="text/javascript">
    
    
    
    
  $(function () {
    var token = $('meta[name="csrf-token"]').attr('content');
  



	


                 
    //Initialize Select2 Elements
    $(".select2").select2();
    //Date range picker

   
    



/*

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });
*/


  });
</script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
