<!DOCTYPE html>
<html lang="en">
       @include('includes.frontend.head')
       <body>

	    @include('includes.frontend.header')
	    @yield('content')
	    @include('includes.frontend.footer')
	    </body>
</html>