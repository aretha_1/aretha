<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                             {!!  Html::decode(Html::link(route('admin.blocks.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary disabled'])) !!} 

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">@sortablelink('position', trans('admin.POSITION'))</th>
                                    <th width="20%">@sortablelink('title', trans('admin.TITLE'))</th>                 
                                    <th width="20%">@sortablelink('category', trans('admin.CATEGORY'))</th>
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$blockslist->isEmpty())
                                @foreach ($blockslist as $block)
                                <tr>
                                    <td>{{ ucfirst($block->position) }}</td>
                                    <td>{{ ucfirst($block->title) }}</td>
                                     <td>{{ $block->category->name }}</td>
                                                                                       
                                    <td>{{ date_val($block->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($block->updated_at,DATE_FORMATE ) }}</td>
                                    <td align="center">
                                       
                                    {!!  Html::decode(Html::link(route('admin.blocks.edit', $block->id),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!} 


                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $blockslist->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
