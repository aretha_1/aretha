<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php
echo $pageTitle;
?>
       </h1>
        @include('includes.admin.breadcrumb')
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.blocks.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
            </div>
            {!! Form::open(['route'=>'admin.blocks.store','files'=>true]) !!}  

            <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                     <div class="row">

                        <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.POSITION'),null,['class'=>'required_label']) !!}
                                {!! Form::text('position',null,['class'=>'form-control','placeholder'=>trans('admin.POSITION')]) !!}
                                <div class="error">{{ $errors->first('position') }}</div>
                            </div><!-- /.form-group -->


                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.TITLE'),null,['class'=>'required_label']) !!}
                                {!! Form::text('title',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE')]) !!}
                                <div class="error">{{ $errors->first('title') }}</div>
                            </div><!-- /.form-group -->
                                   
                      </div><!-- /.row -->   
                      
                      <div class="row">

                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.TITLE2'),null,['class'=>'']) !!}
                                {!! Form::text('title_2',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE2')]) !!}
                                <div class="error">{{ $errors->first('title2') }}</div>
                            </div><!-- /.form-group -->
                            
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.TITLE3'),null,['class'=>'']) !!}
                                {!! Form::text('title_3',null,['class'=>'form-control','placeholder'=>trans('admin.TITLE3')]) !!}
                                <div class="error">{{ $errors->first('title3') }}</div>
                            </div><!-- /.form-group -->
                                   
                      </div><!-- /.row -->   

                      <div class="row">
                              <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.DESCRIPTION'),null,['class'=>'required_label']) !!}
                                {!! Form::text('description',null,['id'=>'description','class'=>'form-control','placeholder'=>trans('admin.DESCRIPTION')]) !!}
                              <div class="error">{{ $errors->first('description') }}</div>
                      </div><!-- /.form-group -->
                                
                      </div><!-- /.row -->
                                                                   
                         <div class="row">

                           <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.IMAGE'),null,['class'=>'required_label']) !!}
                                {!! Form::file('image') !!}
                                <div class="error">{{ $errors->first('image') }}</div>
                            </div><!-- /.form-group -->

                         
                        <div class="form-group col-md-6 ">
                            {!! Form::label(trans('admin.CATEGORY'),null,['class'=>'required_label']) !!}
                           <?php  

                            $category_list    =   array('' => trans('admin.PLEASE_SELECT')) + BasicFunction::getAllChildCategory();
                           
                           ?>
                           {!! Form::select('category_id', $category_list, null, ['class' => 'form-control select2','id'=>'category_id']) !!}
                            <div class="error">{{ $errors->first('category_id') }}</div>
                        </div><!-- /.form-group -->
                         </div><!-- /.row -->
                          

                         <div class="row">
                     
                      
                         </div><!-- /.row -->
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.box-body -->


            <div class="box-footer">

                <div class="pull-right">
                    {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!} 
                    {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                </div>
            </div>
            <!-- /.box-footer -->

            {!! Form::close() !!}

        </div><!-- /.box -->


    </section><!-- /.content -->
</div>
    
@stop
