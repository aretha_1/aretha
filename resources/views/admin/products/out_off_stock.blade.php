<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%">{{ trans('admin.TITLE')}}</th>
                                    <th width="20%">{{ trans('admin.SIZE')}}</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$stock->isEmpty())
                                @foreach ($stock as $stocks)
                                <tr>
                                    <td>{{ ucfirst($stocks->product->title) }}</td>
                                      <td>{{ ucfirst($attributedataValue[$stocks->size_id]) }}</td>
                                    <td>{{ date_val($stocks ->updated_at,DATE_FORMATE ) }}</td>
                              
                                    <td align="center">
                            {!!  Html::decode(Html::link(route('admin.products.manage_stock', ['id'=> $stocks->product_id,'from'=>'outOffStock']),"<i class='fa  fa-cart-plus'></i>",['class'=>'btn btn-info','data-toggle'=>'tooltip','title'=>trans('admin.MANAGE_STOCK')])) !!} 

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $stock->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
