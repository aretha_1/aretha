<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            
                        </div>
                    </div><!-- /.box-header -->
                       {!! Form::model($stock,['route'=>['admin.products.save_stock',$products->id,$from]]) !!} 
                    <div class="box-body">

                    
                       <div class="row">
                          @foreach ($attribute as $value)
                          <?php

                              $data = (isset($stock['size'][$value->id]))? $stock['size'][$value->id] :0;
                              
                           ?>

                              <div class="col-md-6 form-group ">
                                {!! Form::label($value->value,null,['class'=>'']) !!}
                                {!! Form::text("size[$value->id]",$data,['id'=>'','class'=>'form-control number','placeholder'=>trans('admin.QUANTITY')]) !!}
                                <div class="error">{{ $errors->first('quantity') }}</div>
                              </div><!-- /.form-group -->

              
                                
               
                          @endforeach
          </div><!-- /.row --> 
                          
                    </div><!-- /.box-body -->
                               <div class="box-footer">

                <div class="pull-right">
                    {!! Form::reset(trans('admin.RESET'),['class' => 'btn btn-default '])!!} 
                    {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                </div>
            </div>

            {!! Form::close() !!}

                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    
// Numeric only control handler
jQuery.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

$(".number").ForceNumericOnly();
</script>

@stop
<!-- /.content-wrapper -->
