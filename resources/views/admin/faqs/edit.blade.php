<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?php echo $pageTitle; ?>
        </h1>
        @include('includes.admin.breadcrumb')
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.faqs.index',['id'=>$category_id]),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
            </div>
            {!! Form::model($faqs,['method'=>'post','route'=>['admin.faqs.update',$faqs->id,$category_id]]) !!}

             <div class="box-body">

                <div class="row">

                    <div class="col-md-12">
                     <div class="row">
                            <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.QUESTION'),null,['class'=>'required_label']) !!}
                                {!! Form::text('question',null,['class'=>'form-control','placeholder'=>trans('admin.QUESTION')]) !!}
                                <div class="error">{{ $errors->first('question') }}</div>
                            </div><!-- /.form-group -->     
                           
                                 
                            
                        </div><!-- /.row -->
                <div class="row">
                         <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.ANSWER'),null,['class'=>'required_label']) !!}
                                {!! Form::textarea('answer',null,['class'=>'form-control ckeditor','placeholder'=>trans('admin.ANSWER')]) !!}
                                <div class="error">{{ $errors->first('answer') }}</div>
                            </div><!-- /.form-group -->
                        </div><!-- /.row -->
                        


                         <div class="row">
                       <div class="col-md-6 form-group ">
                                {!! Form::label(trans('admin.ORDER_KEY'),null,['class'=>'required_label']) !!}
                                {!! Form::text('order_key',null,['class'=>'form-control','placeholder'=>trans('admin.ORDER_KEY')]) !!}
                                <div class="error">{{ $errors->first('order_key') }}</div>
                                </div><!-- /.form-group -->
                        <div class="form-group col-md-6 ">
                            {!! Form::label(trans('admin.STATUS'),null,['class'=>'required_label']) !!}
                            <?php
                                    $status_list = Config::get('global.status_list');
                            ?>
                           {!! Form::select('status', $status_list, null, ['class' => 'form-control select2']) !!}
                        </div><!-- /.form-group -->
                           

                    


                        


                     </div><!-- /.row -->
                    </div><!-- /.col -->

                </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="pull-right">

                    {!!  Html::decode(Html::link(route('admin.faqs.index',['id'=>$category_id]),trans('admin.CANCEL'),['class'=>'btn btn-default'])) !!}
                    {!! Form::submit(trans('admin.SAVE'),['class' => 'btn btn-info '])!!}
                </div>
            </div>
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div><!-- /.box -->
    </section><!-- /.content -->
</div>
@stop