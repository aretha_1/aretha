<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                              <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                   
                         <li class="{{($status=='all')?'active':''}}"><a href="{{route('admin.orders.index',['status'=>
                         'all'])}}" >All</a></li> 
                          <li class="{{($status==Config::get('global.order_status.success'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.success')])}}" >{{ ucfirst(Config::get('global.order_status.success')) }}</a></li> 
                          <li class="{{($status==Config::get('global.order_status.inprocess'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.inprocess')])}}" >{{ ucfirst(Config::get('global.order_status.inprocess')) }}</a></li> 
                           <li class="{{($status==Config::get('global.order_status.shipped'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.shipped')])}}" >{{ ucfirst(Config::get('global.order_status.shipped')) }}</a></li>    
                          <li class="{{($status==Config::get('global.order_status.delivered'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.delivered')])}}" >{{ ucfirst(Config::get('global.order_status.delivered')) }}</a></li>
                                <li class="{{($status==Config::get('global.order_status.pending'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.pending')])}}" >{{ ucfirst(Config::get('global.order_status.pending')) }}</a></li>   
                        <li class="{{($status==Config::get('global.order_status.cancel'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.cancel')])}}" >{{ ucfirst(Config::get('global.order_status.cancel')) }}</a></li>   
                        <li class="{{($status==Config::get('global.order_status.failed'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.failed')])}}" >{{ ucfirst(Config::get('global.order_status.failed')) }}</a></li>  
                         <li class="{{($status==Config::get('global.order_status.reject'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.reject')])}}" >{{ ucfirst(Config::get('global.order_status.reject')) }}</a></li>




                    </ul>
                    </div>
                    <div class="row search-from">
                         {!!Form::model($search, ['route' => ['admin.orders.index', $status], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'get',  'files' => true  , 'id' => 'edit-settings']) !!}
                                <div class="col-md-12">
                                    <div class="col-md-4 form-group ">
                                      
                                         {!! Form::label(trans('admin.ORDER_ID'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                    
                                              {!! Form::text('order_id',Input::old('order_id'),['class'=>'form-control','placeholder'=>trans('admin.ORDER_ID')]) !!}
                                        </div>
                                    </div>
                                         <div class="col-md-4 form-group ">
                                           {!! Form::label(trans('admin.USER'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                           

                                            {!! Form::text('first_name',Input::old('first_name'),['class'=>'form-control','placeholder'=>trans('admin.FIRST_NAME')]) !!}
                                        </div>
                                    </div>      <div class="col-md-4 form-group ">
                                    {!! Form::label(trans('admin.PAYMENT_BY'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                             <?php
                                    $payment_by =   array('' => trans('admin.PLEASE_SELECT')) + array_map('ucfirst', Config::get('global.payment_by'));
                                    ?>
                                        {!! Form::select('payment_by', $payment_by, null, ['class' => 'form-control select2']) !!}
                                        </div>
                                    </div>

                                </div>
                                      <div class="col-md-12">
                                    <div class="col-md-4 form-group ">
                                      
                                         {!! Form::label(trans('admin.PAYMENT_STATUS'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                    
                                                             <?php
                                    $payment_status =   array('' => trans('admin.PLEASE_SELECT')) + array_map('ucfirst', Config::get('global.payment_status'));
                                    ?>
                                        {!! Form::select('payment_status', $payment_status, null, ['class' => 'form-control select2']) !!}
                                        </div>
                                    </div>
                                         <div class="col-md-4 form-group ">
                                           {!! Form::label(trans('admin.ORDER_FROM'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                           

                                            {!! Form::text('from',Input::old('from'),['id'=>'start_date','class'=>'form-control','placeholder'=>trans('admin.ORDER_FROM')]) !!}
                                        </div>
                                    </div>      <div class="col-md-4 form-group ">
                                    {!! Form::label(trans('admin.TO'),null,['class'=>'col-sm-4 control-label']) !!}
                                        <div class="col-sm-8">
                                              {!! Form::text('to',Input::old('to'),['id'=>'end_date','class'=>'form-control','placeholder'=>trans('admin.ORDER_TO')]) !!}
                                        </div>
                                        </div>
                                    </div>

                                </div>


                                            <!-- nav-tabs-custom -->
                    <div class="col-md-12">

                        {!! Html::link(route('admin.orders.index', $status), trans('admin.CANCEL'), ['id' => 'linkid','class' => 'btn btn-default pull-right']) !!}
                        

                        {!! Form::submit(trans('admin.SUBMIT'),['class' => 'btn btn-info pull-right'])!!}
                    </div>

            {!! Form::close() !!}
                            </div>
                              @if($status==Config::get('global.order_status.inprocess') )
                                     <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                   
                         
                          <li class="{{($shipped_by==Config::get('global.shipped_by.delhivery'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.inprocess'),'shipped_by'=>Config::get('global.shipped_by.delhivery')])}}" >{{ ucfirst(Config::get('global.shipped_by.delhivery')) }}</a></li> 
               
                          <li class="{{($shipped_by==Config::get('global.shipped_by.postal'))?'active':''}}"><a href="{{route('admin.orders.index',['status'=>Config::get('global.order_status.inprocess'),'shipped_by'=>Config::get('global.shipped_by.postal')])}}" >{{ ucfirst(Config::get('global.shipped_by.postal')) }}</a></li>
           




                    </ul>
                    </div>




                      @endif

                       @if($status==Config::get('global.order_status.inprocess') &&  $shipped_by==Config::get('global.shipped_by.delhivery'))

                      <div class="pull-right">
                             {!! Form::button("<i class='fa  fa-download'></i> ".trans('admin.DOWNLOAD_MANIFEST'),['class' => 'btn btn-info pull-right download_manifest'])!!}
                        </div>


                      @endif
                      {!!Form::model($search, ['route' => ['admin.orders.download_manifest'], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post',  'files' => true  , 'id' => 'download_manifest']) !!}
                        <table id="example2" class="table table-bordered table-striped example2">
                            <thead>
                                <tr>
                                 @if($status==Config::get('global.order_status.inprocess') &&  $shipped_by==Config::get('global.shipped_by.delhivery'))
                                    <th width="10%">  {!! Form::checkbox('selectAll',0,null, array('class' => 'minimal select_all '))!!}</th>
                                            @endif
                                    <th width="10%">{{trans('admin.ORDER_ID')}}</th>
                                    <th width="15%">{{trans('admin.USER')}}</th>
                                    <th width="10%">{{trans('admin.PAYMENT_BY')}}</th>
                                    <th width="10%">@sortablelink('payble_amount', trans('admin.PAYBLE_AMOUNT'))</th>
                                    @if($status=='all')
                                    <th width="10%">@sortablelink('order_status', trans('admin.ORDER_STATUS'))</th>
                                       @endif
                                        @if($status==Config::get('global.order_status.shipped'))
                                    <th width="10%">@sortablelink('shipped_by', trans('admin.SHIPPED_BY'))</th>
                                       @endif
                                    <th width="10%">@sortablelink('payment_status', trans('admin.PAYMENT_STATUS'))</th>

                                    <th width="7%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th  width="15%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$orders_list->isEmpty())
                                @foreach ($orders_list as $orders)
                                <tr>
                                    @if($status==Config::get('global.order_status.inprocess') &&  $shipped_by==Config::get('global.shipped_by.delhivery'))
                                    <td>  {!! Form::checkbox('order_ids[]', $orders->id,null, array('class' => 'minimal check_box_list'))!!}</td>
                                                   @endif
                                    <td> {{ $orders->order_id }} </td>
                                    <td> {{ ucwords($orders->user->first_name.' '.$orders->user->last_name) }} </td>
                                    <td> {{ ucfirst($orders->payment_by) }} </td>
                                    <td> {{ display_price($orders->payble_amount) }} </td>
                                     @if($status=='all')
                                      <td> {{ ucfirst($orders->order_status) }} </td>
                                      @endif   

                                      @if($status==Config::get('global.order_status.shipped'))
                                      <td> {{ ucfirst($orders->shipped_by) }} </td>
                                      @endif
                                    <td> {{ ucfirst($orders->payment_status) }} </td>
                                    <td> {{ date_val($orders->created_at,DATE_FORMATE ) }} </td>
                                    
                                    <td align="center">
                                        @if(($orders->order_status == Config::get('global.order_status.inprocess') || $orders->order_status == Config::get('global.order_status.shipped')) &&  $orders->shipped_by == Config::get('global.shipped_by.delhivery') )

                                        {!!  Html::decode(Html::link(route('admin.orders.package_slip_print',['id' => $orders->id]),"<i class='fa  fa-print'></i>",['target'=>'_blank', 'class'=>'btn btn-default ','data-toggle'=>'tooltip','title'=>trans('admin.PRINT_PACKAGE_SLIP')])) !!}
                                        {!!  Html::decode(Html::link(route('admin.orders.package_slip_download',['id' => $orders->id]),"<i class='fa  fa-file-pdf-o'></i>",['class'=>'btn btn-warning ','data-toggle'=>'tooltip','title'=>trans('admin.DOWNLOAD_PACKAGE_SLIP')])) !!}

                                           @endif
                                          @if($orders->order_status == Config::get('global.order_status.success'))
                                        {!!  Html::decode(Html::link(route('admin.orders.process',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.MANIFESTED'), "data-alert"=>trans('admin.MANIFESTED_ALERT')])) !!}
                               
                                        {!!  Html::decode(Html::link(route('admin.orders.reject',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger reject_link','data-toggle'=>'tooltip','title'=>trans('admin.REJECT'), "data-alert"=>trans('admin.REJECT_ALERT')])) !!}
                                        @endif

                                        {!!  Html::decode(Html::link(route('admin.orders.show',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-eye'></i>",['class'=>'btn btn-default ','data-toggle'=>'tooltip','title'=>trans('admin.VIEW')])) !!}


                                          @if($orders->order_status == Config::get('global.order_status.success'))
                                        {!!  Html::decode(Html::link(route('admin.orders.inprocess_by_postal',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-truck'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.PROCESS_BY_POSTAL'), "data-alert"=>trans('admin.SHIPPED_ALERT')])) !!}

                                               @endif

                                          @if($orders->order_status == Config::get('global.order_status.inprocess'))


                                          @if($orders->shipped_by == Config::get('global.shipped_by.delhivery'))
                                        {!!  Html::decode(Html::link(route('admin.orders.shipped',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-truck'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.SHIPPED'), "data-alert"=>trans('admin.SHIPPED_ALERT')])) !!}
                                         @endif  
                                           @if($orders->shipped_by == Config::get('global.shipped_by.postal'))

                                                   {!!  Html::decode(Html::link(route('admin.orders.shipped_by_postal',['id' => $orders->id,'status'=>$status]),"<i class='fa  fa-truck'></i>",['class'=>'btn btn-success shipped_by_postal','data-toggle'=>'tooltip','title'=>trans('admin.SHIPPED_POSTAL')])) !!}
                                              @endif 


                                               @endif          


                                           @if($orders->order_status == Config::get('global.order_status.shipped'))
                                                 {!!  Html::decode(Html::link(route('admin.orders.delivered',['id' => $orders->id,'status'=>$status]),"<i class='fa   fa-check-circle'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.DELIVERED'), "data-alert"=>trans('admin.DELIVERED_ALERT')])) !!}

                                           @endif
                                         @if($orders->order_status == Config::get('global.order_status.delivered'))
                                                 {!!  Html::decode(Html::link(route('admin.orders.invoice',['id' => $orders->id,'status'=>$status]),"<i class='fa   fa-files-o'></i>",['class'=>'btn btn-success ','data-toggle'=>'tooltip','title'=>trans('admin.INVOICE')])) !!}

                                           @endif
                                             

                                        
                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="7"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! Form::close() !!}
                        {!! $orders_list->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<div id="myModal" class="modal fade">
<div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Shipped vai Postal or Courier </h4>
              </div>
              <div class="modal-body">
                
                <div class="row">
 {!! Form::open(['id'=>'saveForm']) !!}  
                    <div class="col-md-12">
                     <div class="row">

                        <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.AWB_NUMBER'),null,['class'=>'required_label']) !!}
                                {!! Form::text('awb_number',null,['class'=>'form-control','placeholder'=>trans('admin.AWB_NUMBER')]) !!}
                                <div class="error awb_number"></div>
                            </div><!-- /.form-group -->

                        
                                   
                      </div><!-- /.row -->   
                                    <div class="row">                                
                             <div class="col-md-12 form-group ">
                                {!! Form::label(trans('admin.POST_BY'),null,['class'=>'required_label']) !!}
                                {!! Form::text('post_by',null,['class'=>'form-control','placeholder'=>trans('admin.POST_BY')]) !!}
                                <div class="error post_by"></div>
                            </div><!-- /.form-group -->
                                            
                         </div><!-- /.row -->                               
                         </div><!-- /.row -->   
                             {!! Form::close() !!}                            

                </div><!-- /.row -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary submitbtn">Submit</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
          var startDate = new Date();
        $("#start_date").datepicker({
            
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#end_date').datepicker('setStartDate', startDate);
        });
        ;


        $("#end_date").datepicker({
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#start_date').datepicker('setEndDate', FromEndDate);
        });
        
       $(document).on('click', '.shipped_by_postal', function(event){
 event.preventDefault()
    $this=  $(this);

        $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

            return false;
}); 
    
       $(document).on('click', '.download_manifest', function(event){ 
            if($('.check_box_list').filter(':checked').length ==0){

              bootbox.alert("Please select atleast one order");
            }else{
              $('#download_manifest').submit();

            }


        });
       $('.select_all').on('ifChecked', function(event){
       
            $('.check_box_list').iCheck('check');
             $('.check_box_list').iCheck('update');
         });  

         $('.select_all').on('ifUnchecked', function(event){
          $('.check_box_list').iCheck('uncheck');
           $('.check_box_list').iCheck('update');
         });


    



        $(document).on('click', '.submitbtn', function(event){
          event.preventDefault()
    
            $.ajax({
      url:$('.shipped_by_postal').attr('href'),
      method:'POST',
      data:$("#saveForm").serialize(),
      dataType:'json',
      success:function(data) {
        
        if(data.status_code==1){
           
          location.reload();

        }else{
          
            if( typeof data.errors.awb_number !='undefined' && data.errors.awb_number !=''){
                $('.awb_number').html(data.errors.awb_number['0']);
              
            }else{
                $('.awb_number').html('');

            }


            if( typeof data.errors.post_by !='undefined' && data.errors.post_by !=''){
                $('.post_by').html(data.errors.post_by['0']);
              
            }else{
                $('.post_by').html('');

            }
        }
      }
   });
    });

    });
    </script>
@stop
<!-- /.content-wrapper -->
