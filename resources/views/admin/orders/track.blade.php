<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

   <div class="col-md-12 col-xs-12">
          <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.orders.track_orders'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                </h3>
                 </div>
                 </div>
            <div class="col-md-3 col-xs-12">
               
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-title">
                                    SHIPMENT ADDRESS 
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                      <address>
                    <strong>
                            {{ucwords($order->first_name.' '.$order->last_name)}}</strong><br>
                            {{$order->address_1}}<br>
                            {{$order->address_2}}<br>
                            {{$order->city}} , {{$order->state}}  {{$order->pin_code}} <br>
                            Phone: {{$order->mobile}}
                    
                  </address>

                  
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
       
            </div><!-- /.col -->
              <div class="col-md-9 col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-title">
                            ORDER TRACK SUMMARY
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                             <table id="example2" class="table table-bordered table-striped example2">
                            <thead>
                                <tr>
                                    <th width="10%">{{trans('admin.STATUS_TYPE')}}</th>
                                    <th width="10%">{{trans('admin.STATUS')}}</th>
                                    <th width="20%">{{trans('admin.STATUS_DATE')}}</th>
                                    <th width="30%">{{trans('admin.REMARKS')}}</th>
                                    <th width="30%">{{trans('admin.LOCATION')}}</th>
                                    
                                    
                                </tr>


                            </thead>
                            <tbody>

                            <?php 
                                    $track_responce = unserialize($order_delivery->track_responce);
                                    $scans = $track_responce['Scans'];
                                    
                            ?>
                              @if(!empty($scans))
                                @foreach ($scans as $scan)
                                <tr>
                                <td> {{Config::get('global.StatusType.'.$scan['ScanDetail']['ScanType'])}}</td>
                                <td> {{$scan['ScanDetail']['Scan']}}</td> 
                                <td> {{ date_val($scan['ScanDetail']['StatusDateTime'],COMMENT_DATE_FORMATE )}}</td> 
                                <td> {{$scan['ScanDetail']['Instructions']}}</td> 
                                <td> {{$scan['ScanDetail']['ScannedLocation']}}</td> 
                                </tr>
                                    @endforeach
                                    @else
                                    <tr><td colspan="6"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif
                             </tbody>
                        </table>
                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
<!-- /.content-wrapper -->
