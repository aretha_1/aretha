<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                            
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                           
                  

                        <table id="example2" class="table table-bordered table-striped example2">
                            <thead>
                                <tr>
                                    <th width="15%">{{trans('admin.MANIFEST_ID')}}</th>
                                    <th width="15%">{{trans('admin.WAYBILL')}}</th>
                                    <th width="20%">{{trans('admin.ORDER_ID')}}</th>
                                    <th width="15%">{{trans('admin.PAYMENT_BY')}}</th>
                                    <th width="20%">@sortablelink('status', trans('admin.STATUS'))</th>
                                    <th width="15%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="10%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$orders_list->isEmpty())
                                @foreach ($orders_list as $orders)
                                <tr>
                                    <td> {{ $orders->manifest_id }} </td>
                                    <td> {{ $orders->waybill }} </td>
                                    <td> {{ $orders->order->order_id }} </td>
                                    
                                    <td> {{ ucfirst($orders->order->payment_by) }} </td>
                                    
                                    
                                      <td> {{ ucfirst($orders->status) }} </td>
                                       

                                    
                                    <td> {{ date_val($orders->updated_at,DATE_FORMATE ) }} </td>
                                    
                                    <td align="center">
                                      
                                        {!!  Html::decode(Html::link(route('admin.orders.track',['id' => $orders->id]),"<i class='fa  fa-eye'></i>",['class'=>'btn btn-default ','data-toggle'=>'tooltip','title'=>trans('admin.VIEW')])) !!}


                                       

                                        
                                    </td></tr>
                                    @endforeach
                                    @else

                                <tr><td colspan="7"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $orders_list->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    $(document).ready(function () {
          var startDate = new Date();
        $("#start_date").datepicker({
            
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#end_date').datepicker('setStartDate', startDate);
        });
        ;


        $("#end_date").datepicker({
            dateFormat: date_format,
            autoclose: true,
            todayHighlight: true
        }).attr('readonly', 'readonly').on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#start_date').datepicker('setEndDate', FromEndDate);
        });
        ;
    });
    </script>
@stop
<!-- /.content-wrapper -->
