<style type="text/css">

.table {
    margin-bottom: 20px;
    max-width: 100%;
    width: 100%;
    color: #333;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 12px;
    line-height: 1.42857;
}

.table th, .table td {
     border-top: none !important;
 }

 table{
    display: table;
    border-collapse: separate;
    border-spacing: 0px;
    
    
} 
  td,th,tr { 
padding: 0px;

}
</style>
                    <table width='100%' style="font-size: 12px;" class="table table-bordered">

                                        <tr>
            

                                    <td  style=' border:none;' colspan="2" align='left'> <strong>{{ strtoupper(Configure('CONFIG_SITE_TITLE'))}}  </strong></td>
                                    <td  style=' border:none;' colspan="3" align='left'>  <strong>DELHIVERY PICKUP LIST  </strong></td>
                                    <td   style=' border:none;' colspan="2" align='right'>  <strong>Print Date:- {{date('d-F-Y')}} | {{date('H:i')}}  Hrs</strong></td>
                                    
                            

                                </tr>

                            <tr >

                <th  align='left'> <strong> S.No.</strong></td>
                <th  align='left'> <strong> Manifest ID</strong></td>
                <th   align='left'> <strong> ORDER ID.</strong></td>
                <th   align='left'> <strong> ITEM NAME.</strong></td>
                <th  align='left'> <strong> QTY.</strong></td>
                <th   align='left'> <strong> CONSIGNEE PINCODE</strong></td>
                <th   align='left'> <strong> PHONE NO.</strong></td>
                <th   align='left'> <strong> AWB</strong></td>
                                
                                    
                            </tr>
                          
                            <?php  $i =1; 
                            $l=0;
                               
                                   # code...
                               
                            ?>
                            @foreach ($orders as $order)
                              <tr  >
                                <td  align='left'  valign='middel'>{{$i}}</td>
                                <td align='left'   valign='middel'>{{$order['order_delivery']['manifest_id']}}</td>
                                <td align='left'   valign='middel'>{{$order['order_id']}}</td>
                                <td align='left' style='padding-bottom: 10px;padding-left: 5px; text-transform: capitalize;' valign='middel'>
                                     
                                        <?php $quantity = 0; ?>
                                                   @foreach ($order['order_detail'] as $product) 
                                                  {{ucfirst($product['product']['title'])}}(Size = {{ $product['product_size']}})(Qty= {{$product['quantity']}})
                                                    <br>
                                                    <br>
                                                    <?php  $quantity =$quantity+ $product['quantity'];
                                                            $l++;
                                                     ?>
                                                     @endforeach   
                                     

                                </td>
                                <td  align='left' valign='middel'>{{$quantity}}</td>
                                <td  align='left' valign='middel'>{{$order['pin_code']}}</td>
                                <td  align='left' valign='middel'>{{$order['mobile']}}</td>
                                <td  valign='middel' align='left'>AWB # : {{$order['awb_number']}}</td>
                            </tr>
                        
                      
                                <?php  $i++;
                                $l++;
                                 ?>
                            @endforeach
                          
                            
                            <tr>
               
                                    <td style=' border:none;' colspan="2"  align='left'> 
                                      <strong>Marchant Signature: </strong> <br><br>
                                    <strong>Marchant SPOC Name: </strong> 
                                    </td>
                                    <td style=' border:none;' colspan="3"  align='left'>  
                                    <strong>Courier Signature:  </strong> <br><br>
                                    <strong>Courier SPOC Name:  </strong> 
                                        </td>
                                    <td  style=' border:none;' colspan="2"  align='right'>  <strong>Total Shipments: {{$i-1}}</strong></td>
                         

                    </tr>
                    </table>

