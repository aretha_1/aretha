  <!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$pageTitle}}
        <small>#{{$order->invoice_id}}</small>
      </h1>
       @include('includes.admin.breadcrumb')
    </section>



    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> {{ Configure('CONFIG_SITE_TITLE')}}
            <small class="pull-right">Date: {{date(DATE_FORMATE)}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>{{ Configure('CONFIG_SITE_TITLE')}}</strong><br>
           {{ Configure('CONFIG_ADDRESS')}}<br>
            Phone:  {{ Configure('CONFIG_PHONE_NUMBER')}}<br>
            Email:  {{ Configure('CONFIG_SUPPORT_MAIL')}}<br>
            
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>{{ucwords($order->first_name.' '.$order->last_name)}}</strong><br>
                    {{$order->address_1}}<br>
                    {{$order->address_2}}<br>
                    {{$order->city}} , {{$order->state}}  {{$order->pin_code}} <br>
                    Phone: {{$order->mobile}} <br>
                     Email: {{$order->user->email}} <br>
            
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #{{$order->invoice_id}}</b><br>
          <br>
          <b>Order ID:</b> {{$order->order_id}}<br>
       
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Product</th>
              <th>Size</th>
              <th>Qty</th>
              <th>Price</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
             @foreach ($order->order_detail as $product)
            <tr>
              <td>{{ucfirst($product->product->title)}}</td>
              <td>{{$product->product_size}}</td>
              <td>{{$product->quantity}}</td>
              <td> {{display_price($product->price)}}</td>
              <td> {{display_price($product->price*$product->quantity)}}</td>
              
              
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
        
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
        

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td{{display_price($order->total_price)}}</td>
              </tr>
                @if($order->shipping_charge > 0)
              <tr>
                <th>Shipping</th>
                <td>{{display_price($order->shipping_charge)}}</td>
              </tr>
                @endif  
                   @if($order->total_tax > 0)
                  <?php $tax_description = unserialize($order->tax_description); ?>
                   @foreach ($tax_description as $tax)
              <tr>
                <th>{{$tax['name']}}</th>
                <td>{{display_price($tax['amount'])}}</td>
              </tr>
              @endforeach
              @endif

               @if($order->is_cod)
              <tr>
                <th>Cod Charge</th>
                <td>{{display_price($order->cod_money)}}</td>
              </tr>
                 @endif   
               @if($order->is_discount > 0)
              <tr>
                <th>Discount</th>
                <td>{{display_price($order->discount)}}</td>
              </tr>
                 @endif  
                
              <tr>
                <th>Payble Amount</th>
                <td>{{display_price($order->payble_amount)}}</td>
              </tr>
                
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="{{route('admin.orders.invoice_print',['id' => $order->id])}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          
          <a href="{{route('admin.orders.invoice_send',['id' => $order->id,'status'=>$status])}}"  class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-send"></i> Send Invoice
          </a>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  @stop