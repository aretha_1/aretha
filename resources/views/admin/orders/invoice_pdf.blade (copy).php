<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  
       <style type="text/css">

@font-face {
  font-family: 'DejaVu Sans';
  font-style: normal;
  font-weight: normal;
  src: url({{WEBSITE_PUBLIC_URL}}fonts/DejaVuSans.ttf) format('truetype');
} 

        </style>
    </head>
    <body  style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; font-size:12px; color: #444444; min-height: 200px;margin-top: 5px;margin-bottom: 5px; " bgcolor="#fff" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="table-responsive">
        <table  class="table table-bordered" width="95%" >
            <tr >
                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;">
                        <tr>
                            <td width="50%" style="padding: 10px;" >

                                <img src="{{WEBSITE_IMG_URL.'logo1.png'}}" title="Fabivo">
                            </td>
                            <td  align='right' width="50%" >
                                Date: {{date(DATE_FORMATE)}}

                            </td>
                        <tr>

                    </table>
                </td>

            </tr>
                 <tr>
            <td>
                <table  width="100%" style="border-bottom: 1px solid #ddd;font-size: 15px;" >
                    <tr>
                        <td width="33%" style="padding: 5px;">
                                    From
          <address>
            <strong>{{ Configure('CONFIG_SITE_TITLE')}}</strong><br>
           {!! Configure('CONFIG_ADDRESS')!!}<br>
            Phone:  {{ Configure('CONFIG_PHONE_NUMBER')}}<br>
            Email:  {{ Configure('CONFIG_SUPPORT_MAIL')}}<br>
            
          </address>
                            
                        </td>
                        <td width="33%">
                        To
          <address>
            <strong>{{ucwords($order->first_name.' '.$order->last_name)}}</strong><br>
                    {{$order->address_1}}<br>
                    {{$order->address_2}}<br>
                    {{$order->city}} , {{$order->state}}  {{$order->pin_code}} <br>
                    Phone: {{$order->mobile}} <br>
                     Email: {{$order->user->email}} <br>
            
          </address>
                        </td>
                          <td width="33%">
                            <b>Invoice #{{$order->invoice_id}}</b><br>
          <br>
          <b>Order ID:</b> {{$order->order_id}}<br>
                        </td>
                    <tr>

                </table>
            </td>
            </tr>
     
            <tr>
                <td > 
                    <table  class="table table-striped" width="100%"  >
                        <tr>
                            <th>Product</th>
                            <th >Price</th>
                            <th >Quantity</th>
                            <th >Total</th>
                        </tr>
                       
                       <?php  for ($i=0; $i <20 ; $i++) { ?>
                         
                    

                        @foreach ($order->order_detail as $product)
                            <tr>
                                <td width='40%' ><strong>{{ucfirst($product->product->title)}}</strong></td>
                                <td width='20%'  >Rs {{number_format($product->price,2)}}</td>  
                                <td width='20%'  >{{$product->quantity}}</td>  
                                <td width='20%'  >Rs {{number_format($product->price*$product->quantity,2)}}</td>  
                            </tr>
                        @endforeach
                           <?php } ?>
                            <tr>
                              <td   >&nbsp;</td>  
                              <td   >&nbsp;</td>  
                                <td ><strong>Sub Total</strong></td>
               
                                <td  >Rs  {{number_format($order->total_price,2)}}</td>  
                            </tr>
                               @if($order->shipping_charge > 0)
                                    <tr>
                              <td   >&nbsp;</td>  
                              <td   >&nbsp;</td>  
                                <td ><strong>Shipping Charge</strong></td>
                       
                                <td  >Rs {{number_format($order->shipping_charge,2)}}</td>  
                            </tr>
                                @endif  


                                         @if($order->total_tax > 0)

                                         <?php $tax_description = unserialize($order->tax_description); 
                                          
                                        ?>
                                         @foreach ($tax_description as $tax)
                                                       <tr>
                                                            <td   >&nbsp;</td>  
                              <td   >&nbsp;</td>      
                                <td  ><strong>{{$tax['name']}}</strong></td>
                      
                                <td   >Rs {{number_format($tax['amount'],2)}}</td>  
                            </tr>

                                        @endforeach
                                          @endif
                                               

                                              @if($order->is_discount > 0)

                                                <tr>
                                                          <td   >&nbsp;</td>  
                              <td   >&nbsp;</td>           
                                <td  ><strong>Discount</strong></td>
        
                                <td  >Rs {{number_format($order->payble_amount,2)}}</td>  
                            </tr>

                                                @endif 
                                              @if($order->is_cod)

                                                <tr>
                                                     <td   >&nbsp;</td>  
                              <td   >&nbsp;</td>  
                                <td  ><strong>Cod Charge</strong></td>
                           
                                <td   >Rs {{number_format($order->cod_money,2)}}</td>  
                            </tr>

                                                @endif 
                                                   <tr>
                                                            <td   >&nbsp;</td>  
                              <td   >&nbsp;</td>  
                                <td  ><strong>Net Amount</strong></td>
                     
                                <td >Rs {{number_format($order->payble_amount,2)}}</td>  
                            </tr>


              
                        
                    </table>
                </td>
                
            </tr>



        </table>

        <!-- ./wrapper -->
    </body>
</html>
