<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
       <style type="text/css">

table{
    display: table;
    border-collapse: separate;
    border-spacing: 0px;
    
    
} 
  td,th,tr { 
padding: 0px;

}
@font-face {
  font-family: 'DejaVu Sans';
  font-style: normal;
  font-weight: normal;
  src: url({{WEBSITE_PUBLIC_URL}}fonts/DejaVuSans.ttf) format('truetype');
} 

        </style>
    </head>
    <body  style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; font-size:12px; color: #444444; min-height: 200px;margin-top: 5px;margin-bottom: 5px; " bgcolor="#fff" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
        <table width="95%" align="center">
            <tr >
                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;">
                        <tr>
                            <td width="50%" style="padding: 10px;" >

                                <img src="{{WEBSITE_IMG_URL.'logo1.png'}}" title="Fabivo">
                            </td>
                            <td  align='right' width="50%" style="border-left: 1px solid #ddd;padding: 10px;">
                                <img src="{{WEBSITE_IMG_URL.'delhivery_logo.png'}}" title="Fabivo">

                            </td>
                        <tr>

                    </table>
                </td>

            </tr>
                 <tr>
            <td>
                <table  width="100%" style="border-bottom: 1px solid #ddd;font-size: 18px;" >
                    <tr>
                        <td width="50%" style="padding: 5px;">

                            <img src="{{$packages_slip_data['barcode']}}" title="Fabivo">
                        </td>
                        <td width="50%">
                            <table width="100%" >
                                <tr>
                                    <td valign="top" align="right">{{$packages_slip_data['pin']}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 20px;" valign="bottom" align="right">{{$packages_slip_data['sort_code']}}</td>
                                </tr>
                            </table>
                        </td>
                    <tr>

                </table>
            </td>
            </tr> <tr>

                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;" >
                        <tr>
                            <td width="70%" style="padding-bottom: 5px;">
                                <h3>SHIPMENT ADDRESS:-</h3>
                                <address>
                                    <strong>
                                        {{ucwords($order->first_name.' '.$order->last_name)}}</strong><br>
                                    {{$order->address_1}},{{$order->address_2}},
                                    {{$order->city}} , {{$order->state}}   <br>
                                    Phone: +91-{{$order->mobile}}
                                    Pin Code: +91-{{$order->pin_code}}<br>
                                    {{$packages_slip_data['destination']}}

                                </address>
                            </td>
                            <td width="30%" style="border-left: 1px solid #ddd;padding-bottom: 5px;">
                                <table width="100%" >
                                    <tr>
                                        <td style="font-size: 20px;" valign="middel" align="center">{{($order->is_prepaid)? 'Pre Paid':'COD'}}</td>
                                    </tr>

                                </table>
                            </td>
                        <tr>

                    </table>
                </td>
            </tr>
            <tr>

                <td style="border-bottom: 1px solid #ddd; padding-bottom: 5px;" >
                    <h3>SELLER ADDRESS:-</h3>
                    <address> {{$packages_slip_data['sadd']}} </address>
                </td>
            </tr>
            <tr>
                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;" cellpadding="0" cellspacing='0'>
                        <tr>
                            <th>Product</th>
                            <th style="border-left:1px solid #ddd;">Price</th>
                            <th  style="border-left:1px solid #ddd;">Quantity</th>
                            <th  style="border-left:1px solid #ddd;">Total</th>
                        </tr>
                       

                        @foreach ($order->order_detail as $product)
                            <tr>
                                <td width='40%' valign="middel" style="padding: 5px;" cellpadding='0'><strong>{{ucfirst($product->product->title)}}</strong></td>
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;" align="center">Rs {{number_format($product->price,2)}}</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;" align="center">{{$product->quantity}}</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;" align="center">{{number_format($product->price*$product->quantity,2)}}</td>  
                            </tr>
                        @endforeach
                            <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Sub Total</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">Rs  {{number_format($order->total_price,2)}}</td>  
                            </tr>
                               @if($order->shipping_charge > 0)
                                    <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Shipping Charge</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">Rs {{number_format($order->shipping_charge,2)}}</td>  
                            </tr>
                                @endif  


                                         @if($order->total_tax > 0)

                                         <?php $tax_description = unserialize($order->tax_description); 
                                          
                                        ?>
                                         @foreach ($tax_description as $tax)
                                                       <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>{{$tax['name']}}</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">Rs {{number_format($tax['amount'],2)}}</td>  
                            </tr>

                                        @endforeach
                                          @endif
                                               

                                              @if($order->is_discount > 0)

                                                <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Discount</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">Rs {{number_format($order->payble_amount,2)}}</td>  
                            </tr>

                                                @endif 
                                              @if($order->is_cod)

                                                <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Cod Charge</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">Rs {{number_format($order->cod_money,2)}}</td>  
                            </tr>

                                                @endif 
                                                   <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Net Amount</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">Rs {{number_format($order->payble_amount,2)}}</td>  
                            </tr>


              
                        
                    </table>
                </td>
                
            </tr>
                    <tr >
                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;">
                        <tr>
                            <td width="50%" style="padding: 20px;" >

                                <img src="{{$packages_slip_data['oid_barcode']}}" title="Fabivo">
                            </td>
                            <td width="50%" style="border-left: 1px solid #ddd;padding: 20px;">
                             <h3>RETURN ADDRESS:-</h3>
                                <address>{{$packages_slip_data['radd']}}</address>
                              
                            </td>
                        <tr>

                    </table>
                </td>

            </tr>


        </table>

        <!-- ./wrapper -->
    </body>
</html>
