<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">

table{
    display: table;
    border-collapse: separate;
    border-spacing: 0px;
    
    
} 
  td,th,tr { 
padding: 0px;

}

        </style>

    </head>
    <body onload="window.print();" style="font-family: Conv_Raleway-SemiBold,Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;margin-top: 10px;margin-bottom: 20px; " bgcolor="#fff" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
        <table width="95%" align="center">
            <tr >
                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;">
                        <tr>
                            <td width="50%" style="padding: 20px;" >

                                <img src="{{WEBSITE_IMG_URL.'logo1.png'}}" title="Fabivo">
                            </td>
                            <td  align='right' width="50%" style="border-left: 1px solid #ddd;padding: 20px;">
                                <img src="{{WEBSITE_IMG_URL.'delhivery_logo.png'}}" title="Fabivo">

                            </td>
                        <tr>

                    </table>
                </td>

            </tr>
            <tr>
            <td>
                <table  width="100%" style="border-bottom: 1px solid #ddd;font-size: 30px;" >
                    <tr>
                        <td width="50%" style="padding: 10px;">

                            <img src="{{$packages_slip_data['barcode']}}" title="Fabivo">
                        </td>
                        <td width="50%">
                            <table width="100%" >
                                <tr>
                                    <td valign="top" align="right">{{$packages_slip_data['pin']}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 50px;" valign="bottom" align="right">{{$packages_slip_data['sort_code']}}</td>
                                </tr>
                            </table>
                        </td>
                    <tr>

                </table>
            </td>
            </tr>
            <tr>

                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;" >
                        <tr>
                            <td width="70%" style="padding-bottom: 20px;">
                                <h1>SHIPMENT ADDRESS:-</h1>
                                <address>
                                    <strong>
                                        {{ucwords($order->first_name.' '.$order->last_name)}}</strong><br>
                                    {{$order->address_1}},{{$order->address_2}}<br>
                                    {{$order->city}} , {{$order->state}}   <br>
                                    Phone: +91-{{$order->mobile}}<br>
                                    Pin Code: +91-{{$order->pin_code}}<br>
                                    {{$packages_slip_data['destination']}}

                                </address>
                            </td>
                            <td width="30%" style="border-left: 1px solid #ddd;padding-bottom: 20px;">
                                <table width="100%" >
                                    <tr>
                                        <td style="font-size: 50px;" valign="middel" align="center">{{($order->is_prepaid)? 'Pre Paid':'COD'}}</td>
                                    </tr>

                                </table>
                            </td>
                        <tr>

                    </table>
                </td>
            </tr>
            <tr>

                <td style="border-bottom: 1px solid #ddd; padding-bottom: 10px;" >
                    <h3>SELLER ADDRESS:-</h3>
                    <address> {{$packages_slip_data['sadd']}} </address>
                </td>
            </tr>
            <tr>
                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;" cellpadding="0" cellspacing='0'>
                        <thead>
                            <th>Product</th>
                            <th style="border-left:1px solid #ddd;">Price</th>
                            <th  style="border-left:1px solid #ddd;">Quantity</th>
                            <th  style="border-left:1px solid #ddd;">Total</th>
                        </thead>
                        <tbody style="font-size: 15px;border-left:1px solid #ddd;padding-top:0px;">

                        @foreach ($order->order_detail as $product)
                            <tr>
                                <td width='40%' valign="middel" style="padding: 5px;" cellpadding='0'><strong>{{ucfirst($product->product->title)}}</strong></td>
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;" align="center">{{display_price($product->price)}}</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;" align="center">{{$product->quantity}}</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;" align="center">{{display_price($product->price*$product->quantity)}}</td>  
                            </tr>
                        @endforeach
                            <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Sub Total</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">{{display_price($order->total_price)}}</td>  
                            </tr>
                               @if($order->shipping_charge > 0)
                                    <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Shipping Charge</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">{{display_price($order->shipping_charge)}}</td>  
                            </tr>
                                @endif  


                                         @if($order->total_tax > 0)

                                         <?php $tax_description = unserialize($order->tax_description); 
                                          
                                        ?>
                                         @foreach ($tax_description as $tax)
                                                       <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>{{$tax['name']}}</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">{{display_price($tax['amount'])}}</td>  
                            </tr>

                                        @endforeach
                                          @endif
                                                <tr>

                                              @if($order->is_discount > 0)

                                                <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Discount</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">{{display_price($order->payble_amount)}}</td>  
                            </tr>

                                                @endif 
                                              @if($order->is_cod)

                                                <tr>
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Cod Charge</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">{{display_price($order->cod_money)}}</td>  
                            </tr>

                                                @endif 
                                <td width='40%' valign="middel" style="padding: 5px;border-top: 1px solid #ddd;" cellpadding='0'><strong>Net Amount</strong></td>
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:0px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">&nbsp;</td>  
                                <td width='20%' valign="middel" style="border-left:1px solid #ddd;padding: 5px;border-top: 1px solid #ddd;" align="center">{{display_price($order->payble_amount)}}</td>  
                            </tr>


                        </tbody>
                        
                    </table>
                </td>
                
            </tr>
                    <tr >
                <td>
                    <table  width="100%" style="border-bottom: 1px solid #ddd;">
                        <tr>
                            <td width="50%" style="padding: 20px;" >

                                <img src="{{$packages_slip_data['oid_barcode']}}" title="Fabivo">
                            </td>
                            <td width="50%" style="border-left: 1px solid #ddd;padding: 20px;">
                             <h3>RETURN ADDRESS:-</h3>
                                <address>{{$packages_slip_data['radd']}}</address>

                            </td>
                        <tr>

                    </table>
                </td>

            </tr>

        </table>

        <!-- ./wrapper -->
    </body>
</html>
