<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                             {!!  Html::decode(Html::link(route('admin.address_books.create',['id'=>$id]),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!} 

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%">@sortablelink('first_name', trans('admin.FIRST_NAME'))</th>
                                    <th width="10%">@sortablelink('last_name', trans('admin.LAST_NAME'))</th>
                                    <th width="10%">@sortablelink('address_1', trans('admin.ADDRESS_1'))</th>                 
                                    <th width="10%">@sortablelink('pin_code', trans('admin.PIN_CODE'))</th>
                                    <th width="10%">@sortablelink('mobile', trans('admin.MOBILE'))</th>
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$address_list->isEmpty())
                                @foreach ($address_list as $address)
                                <tr>
                                    <td>  {{ ucfirst($address->first_name) }} </td>
                                    <td>  {{ ucfirst($address->last_name) }} </td>
                                    <td>  {{ $address->address_1 }}  </td>
                                    <td>  {{ $address->pin_code }}   </td>
                                     <td> {{ $address->mobile }}    </td>  


                                    <td>{{ date_val($address->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($address->updated_at,DATE_FORMATE ) }}</td>
                                      <td align="center">
                                        @if($address->status == 1)
                                        {!!  Html::decode(Html::link(route('admin.address_books.status_change',['id' => $address->id,'status'=>$address->status,'user_id'=>$id]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @else
                                        {!!  Html::decode(Html::link(route('admin.address_books.status_change',['id' => $address->id,'status'=>$address->status,'user_id'=>$id]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @endif

                                        {!!  Html::decode(Html::link(route('admin.address_books.edit',['id'=>$address->id,'user_id'=>$id]),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!} 
                          
                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>

                                @endif

                            </tbody>
                        </table>
                        {!! $address_list->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
