<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                        {!!  Html::decode(Html::link(route('admin.attributes.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn  btn-primary'])) !!}
                            {!!  Html::decode(Html::link(route('admin.attribute_values.create',['id'=>$attribute_id]),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%">@sortablelink('name', trans('admin.NAME'))</th>
                                    <th width="20%">@sortablelink('order_key', trans('admin.ORDER_KEY'))</th>
                                    
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="20%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$attribute_values_list->isEmpty())
                                @foreach ($attribute_values_list as $attribute_values)
                                <tr>
                                    <td>{{ ucfirst($attribute_values->name) }}</td>
                                    <td>{{ $attribute_values->order_key }}</td>
                                    
                                    <td>{{ date_val($attribute_values->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($attribute_values->updated_at,DATE_FORMATE ) }}</td>
                                    <td align="center">
                                        @if($attribute_values->status == 1)
                                        {!!  Html::decode(Html::link(route('admin.attribute_values.status_change',['id' => $attribute_values->id,'status'=>$attribute_values->status,'attribute_id'=>$attribute_id]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @else
                                        {!!  Html::decode(Html::link(route('admin.attribute_values.status_change',['id' => $attribute_values->id,'status'=>$attribute_values->status,'attribute_id'=>$attribute_id]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @endif
                                        {!!  Html::decode(Html::link(route('admin.attribute_values.edit',['id'=>$attribute_values->id,'attribute_id'=>$attribute_id]),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="5"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $attribute_values_list->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
