<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $pageTitle; ?>
        </h1>
        @include('includes.admin.breadcrumb')
    </section>
<?php
 //echo '<pre>';
 //print_r($users);
 //die;
 ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3 class="pull-right">  
                            {!!  Html::decode(Html::link(route('admin.users.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn btn-block btn-primary'])) !!}
                        </h3>
                    </div>                  

               <!-- About Me Box -->
               <div class="box box-primary">
                 <div class="box-header with-border">
                   <h3> <strong><i class="margin-r-5"></i> About User </strong></h3>
                     <hr>
                     <h4> <strong><i class="fa fa-user margin-r-5"></i> Name :-</strong></h4>
                  <p class="text-muted">
                  <h3>  {{ $users->first_name}} {{ $users->last_name}}</h4>
                  </p>                  
                  </div><!-- /.box-header -->               
                  <div class="box-body">
                  <h4> <strong><i class="fa fa-envelope margin-r-5"></i> Email :-</strong></h4>
                  <p class="text-muted">
                  <h4> {{ $users->email}}</h4>
                  </p>

                  <hr>

                  <h4> <strong><i class="fa fa-phone margin-r-5"></i> Phone :-</strong></h4>
                  <p class="text-muted">
                  <h4>   {{$users->phone}} </h4>
                  </p>

                  <hr>

                  <h4> <strong><i class="fa fa-intersex margin-r-5"></i> Gender :-</strong></h4>
                  <p class="text-muted">
                  <h4>   {{$users->gender}} 
                  <span class="mailbox-read-time pull-right">{{ date_val($users->created_at,DATE_FORMATE ) }}</span></h4>

                  </p>
          
                </div><!-- /.box-body -->
              </div><!-- /.col -->

              </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
