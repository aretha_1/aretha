  <!-- Content Wrapper. Contains page content -->
  @extends('layouts.default')

  @section('content')  

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pageTitle; ?>
      </h1>
    @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="pull-right">  
                    {!!  Html::decode(Html::link(route('admin.users.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!}
                </h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                 
                  <th width="13%">@sortablelink('title', trans('admin.NAME'))</th>
                  <th width="13%">@sortablelink('email', trans('admin.EMAIL'))</th>
                  <th width="9%">@sortablelink('phone', trans('admin.PHONE'))</th>
                  <th width="8%">@sortablelink('phone', trans('admin.GENDER'))</th>
                  <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                  <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                  <th  width="38%" align="center">{{trans('admin.ACTION')}}</th>
                </tr>
                </thead>
                <tbody>
                  @if(!$users->isEmpty())
                  @foreach ($users as $user)
                
               <tr>
                  
                  <td>{{ ucfirst($user->first_name.' '.$user->last_name)}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->phone}}</td>
                  <td>{{$user->gender}}</td>
                  <td>{{ date_val($user->created_at,DATE_FORMATE ) }}</td>
                  <td>{{ date_val($user->updated_at,DATE_FORMATE ) }}</td>
               
                  <td>
               
                        @if($user->status == 1)


                            {!!  Html::decode(Html::link(route('admin.users.status_change',['id' => $user->id,'status'=>$user->status]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}

                            @else
                            {!!  Html::decode(Html::link(route('admin.users.status_change',['id' => $user->id,'status'=>$user->status]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                            @endif
                             
                            {!!  Html::decode(Html::link(route('admin.users.edit', $user->id),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}
                            {!!  Html::decode(Html::link(route('admin.users.view', $user->id),"<i class='fa  fa-eye'></i>",['class'=>'btn btn-warning','data-toggle'=>'tooltip','title'=>trans('admin.VIEW_USERS')])) !!}
                            {!!  Html::decode(Html::link(route('admin.users.send_credentials', $user->id),"<i class='fa  fa-share'></i>",['class'=>'btn btn-info','data-toggle'=>'tooltip','title'=>trans('admin.SEND_CREDENTIALS')])) !!}

                             @if($user->email_verify == 0)

                            {!!  Html::decode(Html::link(route('admin.users.email_verify',['id' => $user->id,'email_verify'=>$user->email_verify]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success','data-toggle'=>'tooltip','title'=>trans('admin.EMAIL_VERIFY'), "data-alert"=>trans('admin.VERIFIED_EMAIL')])) !!}
                            @else
                            {!!  Html::decode(Html::link(route('admin.users.email_verify',['id' => $user->id,'email_verify'=>$user->email_verify]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success','data-toggle'=>'tooltip','title'=>trans('admin.EMAIL_VERIFIED'), "data-alert"=>trans('admin.UNVERIFY_EMAIL')])) !!}
                            @endif 
                          {!!  Html::decode(Html::link(route('admin.address_books.index',$user->id),"<i class='fa  fa-list'></i>",['class'=>'btn btn-warning','data-toggle'=>'tooltip','title'=>trans('admin.ADDRESSBOOKS')])) !!}

                 </td>
		         
                @endforeach
                 @else

                    <tr><td colspan="6"><div class="data_not_found"> Data Not Found </div></td></tr>

               @endif

                </tbody>
              </table>
               {!! $users->appends(Input::all('page'))->render() !!}
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  @stop
  <!-- /.content-wrapper -->
