<!-- Content Wrapper. Contains page content -->
@extends('layouts.default')

@section('content')  

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header with-border">
        <h1>
            {{$pageTitle}}
        </h1>
        @include('includes.admin.breadcrumb')
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-right">
                        <?php
                                if($id!=0){ ?>

                    {!! Html::decode( Html::link(route('admin.category.index'),"<i class='fa  fa-arrow-left'></i>".trans('admin.BACK'),['class'=>'btn  btn-primary'])) !!}
                                <?php 
                            }
                         ?>
 @if($id==0)
 <?php $id=''; ?>
                            {!!  Html::decode(Html::link(route('admin.category.create'),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!} @else

{!!  Html::decode(Html::link(route('admin.category.create',['id'=>$id]),"<i class='fa  fa-plus'></i>".trans('admin.ADD_NEW'),['class'=>'btn btn-primary'])) !!} 
                              @endif

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="40%">@sortablelink('name', trans('admin.NAME'))</th>
                                    @if($id==0)<th width="10%">{{trans('admin.SUBCATEGORY')}}</th>@endif
                                    
                                    <th width="10%">@sortablelink('created_at', trans('admin.CREATED_AT'))</th>
                                    <th width="10%">@sortablelink('updated_at', trans('admin.UPDATED_AT'))</th>
                                    <th  width="15%" align="center">{{trans('admin.ACTION')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!$categorylist->isEmpty())
                                @foreach ($categorylist as $category)
                                <tr>
                                    <td>{{ ucfirst($category->name) }}</td>
                                    @if($id==0) <td>{!!  Html::link(route('admin.category.index', ['id' =>$category->id]),$category->children->count(),['class'=>'']) !!} </td>@endif
                                    
                                    <td>{{ date_val($category->created_at,DATE_FORMATE ) }}</td>
                                    <td>{{ date_val($category->updated_at,DATE_FORMATE ) }}</td>
                                    <td align="center">
                                        @if($category->status == 1)
                                        {!!  Html::decode(Html::link(route('admin.category.status_change',['id' => $category->id,'status'=>$category->status,'parent_id'=>$id]),"<i class='fa  fa-remove'></i>",['class'=>'btn btn-danger confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.INACTIVE'), "data-alert"=>trans('admin.ACTIVE_ALERT')])) !!}
                                        @else
                                        {!!  Html::decode(Html::link(route('admin.category.status_change',['id' => $category->id,'status'=>$category->status,'parent_id'=>$id]),"<i class='fa  fa-check'></i>",['class'=>'btn btn-success confirm_link','data-toggle'=>'tooltip','title'=>trans('admin.ACTIVE'), "data-alert"=>trans('admin.INACTIVE_ALERT')])) !!}
                                        @endif
                                        {!!  Html::decode(Html::link(route('admin.category.edit',['id'=>$category->id,'parent_id'=>$id]),"<i class='fa  fa-edit'></i>",['class'=>'btn btn-primary','data-toggle'=>'tooltip','title'=>trans('admin.EDIT')])) !!}

                                    </td>
                                    @endforeach
                                    @else

                                <tr><td colspan="4"><div class="data_not_found"> Data Not Found </div></td></tr>


                                @endif

                            </tbody>
                        </table>
                        {!! $categorylist->appends(Input::all('page'))->render() !!}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
<!-- /.content-wrapper -->
