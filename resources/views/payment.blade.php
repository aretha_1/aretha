<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">

        <title>{{Configure('CONFIG_SITE_TITLE')}}</title>
        <meta name="title" content="{{Configure('CONFIG_META_TITLE')}}">
        <meta name="description" content="{{Configure('CONFIG_META_DESCRIPTION')}}">
        <meta name="keywords" content="{{Configure('CONFIG_META_KEYWORDS')}}">
        <base href="{{WEBSITE_URL}}" />
        <link rel="shortcut icon" href="{{ asset('img/favicon.png')}}">
        {!! Html::style('https://fonts.googleapis.com/icon?family=Material+Icons') !!}
        {!! Html::style('css/materialize.min.css') !!}
        {!! Html::style('css/font-awesome.min.css') !!}
        {!! Html::style('css/style.css') !!}
        {!! Html::style('css/custom.css') !!}
        {!! Html::style('css/style-responsive.css') !!}
        {!! Html::style('css/owl.carousel.css') !!}
        {!! Html::style('css/owl.theme.css') !!}
        {!! Html::style('css/slick.css') !!}


        <script src="//connect.facebook.net/en_US/sdk.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>

    </head>
    <body>
<section class="contact-us-section">
<div class="container"><div class="row"><div class="col m12 s12">
        <form id="payuForm" method="post" action=" {{$redirect_url}}" enctype="multipart/form-data" > 
            <input name="key" id="key"  value="{{$data['key']}}" type="text"> 
            <input name="hash" id="hash" value="{{$data['hash']}}" type="text"> 
            <input name="txnid" id="txnid" value="{{$data['txnid']}}" type="text"> 
            <input name="amount" id="amount" value="{{$data['amount']}}" type="text"> 
            <input name="firstname" id="firstname" value="Vijay" type="text"> 
            <input name="lastname" id="lastname" value="Swarnkar" type="text"> 
            <input name="email" id="email" value="vijay.soni@shubhashish.com" type="text">
            <input name="phone" value="9785168686" type="text">
            <input name="productinfo" value="{{$data['productinfo']}}" size="64" type="text"> 
            <input name="surl" value="{{$data['surl']}}" size="64" type="text"> 
            <input name="furl" value="{{$data['furl']}}" size="64" type="text">
         
   
           
            <button class="waves-effect fabivo-btn-2 btn">Submit</button>
        </form>
</div>
</div>
</div>
</section>
    </body>


</html>
