import { Component ,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthenticationService } from '../service/authentication.service';
import { CartService } from "../service/cart.service";
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery:any;

@Component({
  selector: 'login',
  template: require('./login.html')
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';
    success = '';
    authData: any = [];
    cartItem: any = [];

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private location: Location,
        private cartService: CartService
    ) {}
    
    ngOnInit() {
        blockUI();
        if(this.authenticationService.isLoggedIn()){
            this.location.back();
            unblockUI();
            toast('You are already logged in.', 'error');
        }
        else{
            unblockUI();
        }
    }

    login() {
        blockUI();
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.model = '';
                    this.error = '';
                    this.authData = JSON.parse(localStorage.getItem('loginUser'));
                    jQuery('#signInForm')[0].reset();
                    jQuery('#signInForm').find('label').removeClass('active');
                    this.getCartItem();
                    unblockUI();
                    this.loading = false;
                    this.router.navigate(['/myaccount', 'profile']);
                    toast(this.success, 'success');
                    setTimeout(() => {
                        jQuery('.dropdown-button').dropdown({
                            inDuration: 300,
                            outDuration: 225,
                            constrain_width: false, // Does not change width of dropdown to that of the activator
                            hover: true, // Activate on hover
                            gutter: 0, // Spacing from edge
                            belowOrigin: false, // Displays dropdown below the button
                            alignment: 'left' // Displays dropdown with edge aligned to the left of button
                        });
                        
                        jQuery("#nav-mobile .child-menu ul li a").click(function(){
                            jQuery('.button-collapse').sideNav('hide');
                        });
                    }, 2000);
                } else {
                    this.error = result['message'];
                    this.loading = false;
                    unblockUI();
                    toast(this.error, 'error');
                }
            });
    }
    
    getCartItem() {
        if(this.authData && this.authData.id){
            this.cartService.getCart(this.authData.id).subscribe(
                cartData => {
                      this.cartService.cart = cartData['data'];
                }, err => {
                    console.log(err);
                }
            );
        }
    }
}