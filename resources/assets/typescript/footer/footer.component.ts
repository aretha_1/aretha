import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PageService } from "../service/page.service";
import { AuthenticationService } from '../service/authentication.service';
import { Location } from '@angular/common';
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery: any;
@Component({
    selector: 'my-footer',
    template: require('./footer.html')
})

export class FooterComponent implements OnInit{
    cmspages1: any = [];
    cmspages2: any = [];
    cmspages3: any = [];
    settings: any = [];
    newslatter: any = {};
    constructor(
        private pageService: PageService,
             private location: Location,
        private authenticationService: AuthenticationService
    ){}
    
    ngOnInit() {
        this.pageService.getPages().subscribe(
            pages => {
                this.cmspages1 = pages['data'][1];
                this.cmspages2 = pages['data'][2];
                this.cmspages3 = pages['data'][3];
            }, err => {
                console.log(err);
            }
        );
        
        this.pageService.getSettings().subscribe(
            settings => {
                this.settings = settings;
            }, err => {
                console.log(err);
            }
        );
    }
    
    isLoggedIn() {
        return this.authenticationService.isLoggedIn();
    }
      isDisabled (path) {
        return this.location.path().indexOf(path) > -1;
    }

    newslattersaved(){


          blockUI();
        
        this.pageService.newslatterSubscription(this.newslatter)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                   
                    this.newslatter = {};
                    
                    
                    jQuery('#newsletter')[0].reset();
                    unblockUI();
                    
                    
                    toast(result['message'], 'success');
                } else {
                    
               
                    unblockUI();
                    toast(result['message'], 'error');
                }
            });
    }
}
