import { Component, OnInit, AfterViewInit } from '@angular/core';
import { UserService } from "../service/user.service";
import { AuthenticationService } from '../service/authentication.service';
import { CartService } from "../service/cart.service";
import { blockUI, unblockUI, toast } from '../global';
import { Router } from '@angular/router';
declare var jQuery:any;
const customEvent = document.createEvent('Event');  

@Component({
    selector: 'my-cart',
    template: require('./cart.html')
})

export class CartComponent implements OnInit, AfterViewInit {
    loading = false;
    error = '';
    success = '';
    authData: any = [];
    carts: any = [];
    cart_length: number;
    add_product: any = {};
    remove_product: any = {};
    coupan_code  = '';
    coupen_code_apply = 0;
    coupen_error = '';
    coupen_error_status = 0;
    coupen_massage = 0;
    add_wishlist: any = {};
     controlEnabled:boolean = true;
    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private cartService: CartService,
         private router: Router,
    ){}
    
    ngOnInit() {
   
        this.authData = JSON.parse(localStorage.getItem('loginUser'));
        if(this.authData && this.authData.id){
             this.allCartItem();
        }
 
            

        
    }
    
    ngAfterViewInit() {
   
      
    }
    allCartItem(){
        let coupan_code = '';
        if(this.coupen_code_apply){

            coupan_code = this.coupan_code;
        }

           this.cartService.getcartItem(this.authData.id,coupan_code).subscribe(
                cartsData => {
                    this.carts = cartsData['data'];
                    this.cart_length = cartsData['data']['products'].length;
                     this.materialSelectBox();
                      this.coupanCodeValidate(cartsData['data']);
                }, err => {
                    console.log(err);
                }
            );
            this.cartService.getCart(this.authData.id).subscribe(
                    cartData => {
                          this.cartService.cart = cartData['data'];

                    }, err => {
                        console.log(err);
                    }
            );


    }
    
    changeCart(cart_id, size, user_id, product_id, quantity, flag){
        blockUI();
        this.add_product['cart_id'] = cart_id;
        this.add_product['user_id'] = user_id;
        this.add_product['product_id'] = product_id;
        this.add_product['product_size'] = size;
        this.add_product['quantity'] = quantity;
        this.add_product['flag'] = flag;
        if(this.coupen_code_apply){

            this.add_product['coupan_code'] = this.coupan_code;
        }else{

            this.add_product['coupan_code'] = '';
        }
       
        this.cartService.updateCart(this.add_product)
        .subscribe(result => {
            if (result['status_code'] === 1) {
                this.carts = result['data'];
                  this.coupanCodeValidate(result['data']);
                this.cart_length = result['data']['products'].length;
                this.success = result['message'];
                this.materialSelectBox();
                setTimeout(() => {
                                unblockUI();
                           }, 500);
                toast(this.success, 'success');
               
            } else {
                this.carts = result['data'];
                  this.coupanCodeValidate(result['data']);
                this.cart_length = result['data']['products'].length;
                this.error = result['message'];
                this.materialSelectBox();
          setTimeout(() => {
                                unblockUI();
                           }, 500);
                toast(this.error, 'error');
            }
        });
    }
    
    removeCart(cart_id){
        blockUI();
        this.remove_product['cart_id'] = cart_id;
        this.remove_product['user_id'] = this.authData.id;
        if(this.coupen_code_apply){

            this.remove_product['coupan_code'] = this.coupan_code;
        }else{

            this.remove_product['coupan_code'] = '';
        }
        this.cartService.removeCart(this.remove_product)
        .subscribe(result => {
            if (result['status_code'] === 1) {
                this.cartService.getCart(this.authData.id).subscribe(
                    cartData => {
                          this.cartService.cart = cartData['data'];

                    }, err => {
                        console.log(err);
                    }
                );
                
                this.carts = result['data'];
                this.coupanCodeValidate(result['data']);
                if(result['data']['products'] && result['data']['products'].length > 0){
                    this.cart_length = result['data']['products'].length;



                }
                else{
                    this.cart_length = 0;
                }
                this.success = result['message'];
                this.materialSelectBox();
                         setTimeout(() => {
                                unblockUI();
                           }, 500);
                toast(this.success, 'success');
            } else {
                this.error = result['message'];
                unblockUI();
                toast(this.error, 'error');
            }
        });
    }
    materialSelectBox(){

            setTimeout(() => {
              
                         jQuery('select').material_select();
                           jQuery("select").on('change', function() {
                                  let id = jQuery(this).attr("id");
                                  customEvent.initEvent('change', true, true);
                                        jQuery("#"+id)[0].dispatchEvent(customEvent);
                                    
                                   });
                                   jQuery(".cart-product-info-bx").mCustomScrollbar('destroy');
                                    if(jQuery(window).width() > 641){
                                     jQuery(".cart-product-info-bx").mCustomScrollbar({
                                      autoDraggerLength: false,
                                      mouseWheel: {
                                          scrollAmount: 128
                                      }
                                    });
                                   }

                                       jQuery(window).resize(function() {
        if(window.innerWidth > 641) {
               jQuery(".cart-product-info-bx").mCustomScrollbar({
                                      autoDraggerLength: false,
                                      mouseWheel: {
                                          scrollAmount: 128
                                      }
                                    });
        } else {
             jQuery(".cart-product-info-bx").mCustomScrollbar('destroy');
        }
    });
                                                             
                                   
                              }, 500);

    }
    coupanCodeValidate(data){
               if(this.coupan_code!=''){
                       if(this.coupen_code_apply==1){
                           if(data['coupon_code_error']!=''){
                             this.coupen_error =data['coupon_code_error'];  
                             this.coupen_code_apply = 0;
                             this.coupen_error_status = 1; 
                           }
                            

                       }else{



                       }
                       
               }


    }

    checkCoupanCode(){
        if(this.coupan_code!=''){
                    blockUI();
                    this.coupen_error = '';
                    this.cartService.checkCode(this.authData.id,this.coupan_code)
                    .subscribe(result => {
                           if (result['status_code'] === 1) {

                                this.coupen_massage = result['message'];
                                this.coupen_code_apply = 1;
                                this.coupen_error_status = 0;
                                this.allCartItem();
                                     setTimeout(() => {
                                unblockUI();
                           }, 500);


                           }else{

                                this.coupen_error = result['message'];
                                this.coupen_code_apply = 0;
                                this.coupen_error_status = 1;
                                  this.allCartItem();
                                       setTimeout(() => {
                                unblockUI();
                           }, 500);


                           }
                        
               
                });


        }
        


    }  

     RemoveCoupanCode(){
        if(this.coupan_code!=''){
             blockUI();
                    this.coupen_error = '';
                   
                          

                                this.coupen_error = '';
                                this.coupen_code_apply = 0;
                                this.coupen_error_status = 0;
                                  this.allCartItem();
                                       setTimeout(() => {
                                unblockUI();
                           }, 500);

                           
                        
               
                

        }
        


    }
    checkout(){
      if(this.carts['stock_error'] == 1){
         toast('Some item out off stock', 'error');


      }else{
            this.cartService.orderData =        this.carts;
            this.router.navigate(['../checkout']);
      }
           
    }
    addToWishlist(product_id){
  if(this.authenticationService.isLoggedIn()){
            blockUI();
            this.loading = true;
            let userData = this.authenticationService.authData;
            this.add_wishlist['product_id'] = product_id;
            this.add_wishlist['user_id'] = userData['id'];
            this.add_wishlist['userToken'] = userData['token'];
            alert("jkdhsakd");
            console.log(this.add_wishlist);
            this.cartService.addWishlist(this.add_wishlist).subscribe(
                result => {
                    unblockUI();
                    this.loading = false;
                    if(result['status_code'] === 1) {
                        this.success = result['message'];
                        toast(this.success, 'success');
      
                    } else {
                        this.error = result['message'];
                        toast(this.error, 'error');
                    }
                }
            );
        }
        else{
           jQuery('#modal1').modal('open');
        }
    }
}
