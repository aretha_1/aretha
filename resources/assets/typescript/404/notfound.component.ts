import { Component } from '@angular/core';

@Component({
    selector: 'not-found',
    template: require('./404.html'),
})

export class NotFoundComponent {
    
}
