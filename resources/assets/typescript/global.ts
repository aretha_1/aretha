declare var Materialize: any;
declare var jQuery:any;
export function toast(massage,type) {
    let type_class='';
    if(type=='success'){

            type_class = "tost-success";
    }

    if(type=='error'){

                type_class = "tost-error";
        }

  Materialize.toast(massage, 5000, type_class);
}


export function blockUI() {
   jQuery(document).ready(function() {
       jQuery.blockUI({
        fadeIn: 0,
        fadeOut: 0,
        css: {
            padding: 0,
            margin: 0,
            left: '34%',
            textAlign: 'center',
            color: '#000',
            border: 'none',
            backgroundColor: 'none',
            cursor: 'wait',

        },
        overlayCSS: {
            backgroundColor: '#8E9093',
            opacity: .50
        },
        message: '<div class="preloader-wrapper small active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left">        <div class="circle"></div> </div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
        baseZ: 999999999
    });

});
    
}
export function unblockUI() {
  jQuery(document).ready(function() {
    jQuery.unblockUI();
    
    });
    
}
/* FOR MODEL BOX POPUP BLOCK */
export function modalBlockUI() {
 jQuery(document).ready(function() {
        jQuery('div.div-loader').block({ 
                message: '<div class="preloader-wrapper small active"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left">        <div class="circle"></div> </div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>', 
                css: { border: 'none'   ,backgroundColor: '#ccc'} ,
                overlayCSS: {
                    backgroundColor: '#ccc',
                    opacity: 0.99
                },
                baseZ: 999999999
            }); 
        });
}
/* FOR MODEL BOX POPUP BLOCK */

/* FOR MODEL BOX POPUP UNBLOCK */
export function modalUnBlockUI() {
     jQuery(document).ready(function() {
            jQuery('div.div-loader').unblock();
       });
    
}