import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { blockUI, unblockUI, toast } from '../global';
import { PagerService } from "../service/pager.service";
import { CartService } from "../service/cart.service";
import { PageService } from "../service/page.service";
import { Location } from '@angular/common';
declare var jQuery: any;
@Component({
    selector: 'order-detail',
    template: require('./orderdetail.html')
})

export class OrderDetailComponent {
      @Input() model;
    total: number;
    per_page: number;
    current_page: number;
    page: number = 1;
    last_page: number;
    trackdisable: number=0;
    pager: any = {};
    success: '';
    cancel_reason: '';
    error: '';
    orderDetail: any = [];
    status: any = [];
    authDatas: any = {};
        cancelmodel: any = {};
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private pagerService: PagerService,
        private pageService: PageService,
        private location: Location,
    ) { }

    ngOnInit() {
        blockUI();
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
        this.route.params.subscribe(params => {
              let order_id = params['order_id'];
                this.router.routerState.root.queryParams.subscribe(params => {
                    if(params['status']){
                    if (params['status']=='success') {

                        toast('Order successfully placed', 'success');    
                    }else{
                        toast('Your order is '+params['status'], 'error');


                    }
                    this.location.go('myaccount/order-detail/'+order_id);
                }

                });



          
            this.pageService.getOrderDetail(this.authDatas.id, order_id).subscribe(
                orderDetail => {
                    this.orderDetail = orderDetail['data'];
                    this.status = orderDetail['data']['status'];
                  
                        setTimeout(() => {
                           
                                 jQuery(document).ready(function(){
                                 jQuery('.tooltipped').tooltip({delay: 50});
                                   jQuery('#cancle_order').modal();
                            
                                       });
                                  setTimeout('resizeequalheight()', 50);
                        }, 1000);



                }, err => {
                    console.log(err);
                }
            );
        });
       setTimeout(() => {
             jQuery(document).ready(function() {
                 jQuery('.tabs').tabs();
               });

            }, 100);
        unblockUI();
    }
    cancelOrder(){
           blockUI();
               this.pageService.cancelOrder(this.authDatas.id, this.orderDetail.order_id,this.cancelmodel).subscribe(
                result => {
                    
              if (result['status_code'] === 1) {
                   this.orderDetail.order_status= 'cancel';
                   
                   this.orderDetail.cancel_reason = this.cancelmodel.cancel_reason;
                      jQuery('#cancle_order').modal('close');
                    this.cancelmodel.cancel_reason = '';

                    unblockUI();
               
                    toast(result['message'], 'success');
                } else {
                 
                    unblockUI();
                    toast(result['message'], 'error');
                }
                  


                }, err => {
                    console.log(err);
                }
            );
           
    }
}
