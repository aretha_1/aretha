import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PageService } from "../service/page.service";
import { Title } from '@angular/platform-browser';
declare var jQuery: any;
@Component({
    
    templateUrl: 'public/templates/page/page.html',
})

export class PageComponent implements OnInit{
    page: any = [];
    
    constructor(
        private pageService: PageService,
        private route: ActivatedRoute,
        private titleService: Title
    ){}
    
    ngOnInit() {
        this.route.params.subscribe(params => {
            let slug = params['slug'];
            this.pageService.getPageDetail(slug).subscribe(
                pageDetail => {
                     jQuery("html, body").animate({ scrollTop: 10 }, "slow");
                    this.page = pageDetail['data']['0'];
                    this.titleService.setTitle(this.page['title']);
                    jQuery('meta[name="title"]').attr('content', this.page['meta_title']);
                    jQuery('meta[name="description"]').attr('content', this.page['meta_description']);
                    jQuery('meta[name="keywords"]').attr('content', this.page['meta_keywords']);
                }, err => {
                    console.log(err);
                }
            );
        });
    }
}
