import { Component, AfterViewInit ,OnInit} from '@angular/core';
import { PageService } from "./service/page.service";
declare var jQuery:any;

@Component({
    selector: 'my-app',
    template: `
        <my-header></my-header>
        <router-outlet></router-outlet>
        <my-footer></my-footer>
    `
})

export class AppComponent implements OnInit, AfterViewInit {
    settings: any = [];
    
    constructor(
        private pageService: PageService
    ){}
    
    ngOnInit() {
        this.pageService.getSettings().subscribe(
            settings => {
                this.settings = settings;
            }, err => {
                console.log(err);
            }
        );
    }
    
    ngAfterViewInit() {
        jQuery(document).ready(function(){
            jQuery('.button-collapse').sideNav();
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            jQuery('.modal').modal();
            
            //jQuery('select').material_select();
           
            setTimeout(() => {
              
                jQuery('.dropdown-button').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrain_width: false, // Does not change width of dropdown to that of the activator
                    hover: true, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: false, // Displays dropdown below the button
                    alignment: 'left' // Displays dropdown with edge aligned to the left of button
                });  
            }, 2000);
            
            jQuery(".mobile_search").click(function(){
                jQuery("#search").toggle(200);
            });
            
            jQuery("button").click(function () {
                jQuery("input").toggle(200);
            });
            
            jQuery("#nav-mobile .child-menu ul li a").click(function(){
                jQuery('.button-collapse').sideNav('hide');
            });
        });
    }
}
