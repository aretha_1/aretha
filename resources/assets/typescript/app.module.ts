///<reference path="../../../typings/index.d.ts"/>

import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {RequestOptions, RequestMethod, Headers, HttpModule, JsonpModule } from '@angular/http';

import { AppComponent }   from './app.component';
import { AppRoutingModule }     from './app.routing.module';

import { AuthGuard } from './common/auth.guard';
import { AuthenticationService } from './service/authentication.service';
import { UserService } from './service/user.service';
import { ProductService } from './service/product.service';
import { PagerService } from "./service/pager.service";
import { CartService } from "./service/cart.service";

import { HeaderComponent }  from './header/header.component';
import { FooterComponent }  from './footer/footer.component';
import { HomeComponent }  from './home/home.component';

import { PageComponent } from './page/page.component';
import { FaqComponent } from './faq/faq.component';
import { LoginComponent } from './login/login.component';
import { ProductComponent } from './product/product.component';
import { SearchComponent } from './search/search.component';
import { ProductDetailComponent } from './productdetail/productdetail.component';
import { NotFoundComponent }  from './404/notfound.component';
import { ContactUsComponent }  from './contactus/contactus.component';
import { ResetPasswordComponent }  from './resetpassword/resetpassword.component';
import { MyAccountComponent }  from './myaccount/myaccount.component';
import { MyProfileComponent }  from './myprofile/myprofile.component';
import { MyAddressComponent }  from './myaddress/myaddress.component';
import { MyOrderComponent }  from './myorder/myorder.component';
import { MyWalletComponent }  from './mywallet/mywallet.component';
import { ChangePasswordComponent }  from './changepassword/changepassword.component';
import { WishlistComponent }  from './wishlist/wishlist.component';
import { CartComponent }  from './cart/cart.component';
import { CheckoutComponent }  from './checkout/checkout.component';
import { CheckoutAddressComponent }  from './checkoutaddress/checkoutaddress.component';
import { OrderSummaryComponent }  from './ordersummary/ordersummary.component';
import { OrderDetailComponent }  from './orderdetail/orderdetail.component';
import { PaymentComponent }  from './payment/payment.component';
import { TahnkYouComponent }  from './thank_you/tahnk_you.component';
import { CeiboShare } from 'ng2-social-share';
import { ValuesPipe, KeysPipe, FlattenPipe, CapitalizePipe,UcupperPipe,SanitizeHtmlPipe,SafeUrlPipe } from './pipe/pipe';
import { EqualValidator } from './equal-validator.directive';

import { MyOptions } from './myoption';
@NgModule({
    imports:      [ BrowserModule, AppRoutingModule, FormsModule, HttpModule ],
    declarations: [ 
        AppComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        PageComponent,
        ValuesPipe,
        KeysPipe,
        FlattenPipe,
        NotFoundComponent,
        FaqComponent,
        LoginComponent,
        ProductComponent,
        ContactUsComponent,
        ResetPasswordComponent,
        ProductDetailComponent,
        CapitalizePipe,
        UcupperPipe,
        MyAccountComponent,
        MyAddressComponent,
        MyProfileComponent,
        MyOrderComponent,
        MyWalletComponent,
        ChangePasswordComponent,
        CartComponent,
        WishlistComponent,
        CheckoutComponent,
        CheckoutAddressComponent,
        OrderSummaryComponent,
         PaymentComponent,
        CeiboShare,
        TahnkYouComponent,
        OrderDetailComponent,
        SearchComponent,
        SanitizeHtmlPipe,SafeUrlPipe,
        EqualValidator

    ],
    providers: [
        AuthGuard,
        AuthenticationService,
        UserService,
        ProductService,
        CartService,
        PagerService, { provide: RequestOptions, useClass: MyOptions} 
    ],    
    
    bootstrap:    [ AppComponent ]
})

export class AppModule { }
