import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Product } from '../model/product';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProductService {
    constructor(private http:Http) {}
    
    private productUrl = 'api/get-product-by-category/';
    getProduct(slug: string, page: number, sort_by, search: any): Observable<Product[]> {
        // ...using get request
        //  console.log(search);
        // let params = new URLSearchParams();
        let url = this.productUrl + slug + '?page=' + page;
        if(sort_by){
            
            url = url+"&sort_by="+sort_by;
        }
        
        return this.http.post(`${url}`, { data: search })
            // ...and calling .json() on the response to return data
            .map((res: Response) => res.json())
            //...errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }    
    private productSearchUrl = 'api/get-search-product/';
    getSearchProduct(slug: string, page: number, sort_by, search: any): Observable<Product[]> {
        // ...using get request
        //  console.log(search);
        // let params = new URLSearchParams();
        let url = this.productSearchUrl + slug + '?page=' + page;
        if(sort_by){
            
            url = url+"&sort_by="+sort_by;
        }
        
        return this.http.post(`${url}`, { data: search })
            // ...and calling .json() on the response to return data
            .map((res: Response) => res.json())
            //...errors if any
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    
    private categoryUrl = 'api/get-category-attribute/';
    getAttribute(slug: string): Observable<Product[]> {
        let url = this.categoryUrl + slug;
        return this.http.get(`${url}`)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }   


     private attributeUrl = 'api/get-all-attribute/';
    getAllAttribute(slug: string): Observable<Product[]> {
        let url = this.attributeUrl+slug;
        return this.http.get(`${url}`)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getProductDetail(slug: string, userId:any) : Observable<Product[]> {
       return this.http.post('api/get-product-detail',{slug:slug, userId:userId})
                       .map((res:Response) => res.json())
                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    
    getWishlistProduct(uId:any,page: number) : Observable<Product[]> {
       // ...using get request
       return this.http.post('api/get-product-wishlist?page=' + page,{uId:uId})
           // ...and calling .json() on the response to return data
           .map((res:Response) => res.json())
           //...errors if any
           .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
}
