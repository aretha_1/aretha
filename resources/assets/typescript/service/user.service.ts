import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { User } from '../model/user';

import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    getUsers(): Observable<User[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.authData });
        let options = new RequestOptions({ headers: headers });

        // get users from api
        return this.http.get('/api/users', options)
            .map((response: Response) => response.json());
    }
    
    private pageUrl = 'api/check-reset-password/';
    checkResetToken(token: string) : Observable<User[]> {
       // ...using get request
       return this.http.get(`${this.pageUrl}${token}/`)
                       // ...and calling .json() on the response to return data
                       .map((res:Response) => res.json())
                       //...errors if any
                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    
    submitResetPassword(modelData: any): Observable<boolean> {
        return this.http.post('api/save-reset-password', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
    
    private userGetUrl = 'api/get-user-detail/';
    getUserDetail(id: string, token: string) : Observable<User[]> {
       // ...using get request
       return this.http.get(this.userGetUrl + id + '/' + token)
                       // ...and calling .json() on the response to return data
                       .map((res:Response) => res.json())
                       //...errors if any
                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }   

     private usersGetUrl = 'api/get-user-details/';
    getUserDetails(id: string, token: string) : Observable<User[]> {
       // ...using get request
       return this.http.get(this.usersGetUrl + id + '/' + token)
                       // ...and calling .json() on the response to return data
                       .map((res:Response) => res.json())
                       //...errors if any
                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    
    profileSave(modelData: any): Observable<boolean> {
        return this.http.post('api/update-profile', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
    
    private addressGetUrl = 'api/get-user-address/';
    getUserAddress(id: string, token: string) : Observable<User[]> {
       // ...using get request
       return this.http.get(this.addressGetUrl + id + '/' + token)
                       // ...and calling .json() on the response to return data
                       .map((res:Response) => res.json())
                       //...errors if any
                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    
    addressSave(modelData: any): Observable<boolean> {
        return this.http.post('api/save-address', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
    
    setAddressDefault(modelData: any): Observable<boolean> {
        return this.http.post('api/set-address-default', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
    
    private addressUrl = 'api/delete-address/';
    deleteUserAddress(address_id: string, user_id: string) : Observable<User[]> {
       // ...using get request
       return this.http.get(this.addressUrl + address_id + '/' + user_id)
                       // ...and calling .json() on the response to return data
                       .map((res:Response) => res.json())
                       //...errors if any
                       .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    
    editAddressSave(modelData: any): Observable<boolean> {
        return this.http.post('api/update-address', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
    
    changePasswordSave(modelData: any): Observable<boolean> {
        return this.http.post('api/change-password', { data: modelData })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
       getPincodeDetail(pincode: number): Observable<boolean> {
        return this.http.get('api/get-pincode-detail/'+pincode)
            .map((response: Response) => {
                 return response.json();               
            });
    }
    getPincodeAvalability(pincode: string): Observable<boolean> {
        return this.http.get('api/get-pincode-avalability/'+pincode)
            .map((response: Response) => {
                 return response.json();               
            });
    }
}
