import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Product } from '../model/product';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery:any;
@Injectable()
export class CartService {
    cart: any = [];
    authDatas: any = [];
    wishlist: any = [];
    wishlistArray: any = [];
    orderData: any = [];
    add_wishlist: any = [];

    constructor(private http: Http) {
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
        if (this.authDatas) {
            this.getCart(this.authDatas.id).subscribe(
                cartData => {
                    this.cart = cartData['data'];
                }, err => {
                    console.log(err);
                }
            );
            this.getToWishlist(this.authDatas.id);
       
        }
    }

    private getCartUrl = 'api/get-cart/';
    getCart(id: string): Observable<Product[]> {
        return this.http.get(this.getCartUrl + id)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    addCart(add_cart: any): Observable<boolean> {

        return this.http.post('api/add-cart', { data: add_cart })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    this.cart.push(add_cart);
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    updateCart(edit_cart: any): Observable<boolean> {
        return this.http.post('api/update-cart', { data: edit_cart })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    removeCart(remove_product: any): Observable<boolean> {
        return this.http.post('api/remove-cart', { data: remove_product })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }
    checkCode(id: string, coupan_code: string): Observable<boolean> {
        return this.http.get('api/check-coupon-code/' + id + '/' + coupan_code)
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    private getCartItemUrl = 'api/get-cart-item/';
    getcartItem(id: string, coupan_code: string): Observable<any> {
        return this.http.get(this.getCartItemUrl + id + '/' + coupan_code)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    private getWishlistUrl = 'api/get-wishlist/';
    getWishlist(id: string): Observable<Product[]> {
        return this.http.get(this.getWishlistUrl + id)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    addWishlist(add_wishlist: any): Observable<boolean> {
        console.log(add_wishlist);
        return this.http.post('api/add-wishlist', { data: add_wishlist})
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    this.wishlist.push(add_wishlist);
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

    removeWishlist(remove_product: any): Observable<boolean> {
        return this.http.post('api/remove-wishlist', { data: remove_product })
            .map((response: Response) => {
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }


      addToWishlist(product_id){
            this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
            if(this.authDatas){
                  blockUI();
           
            let userData = this.authDatas;
            let add_wishlist:any = {}
            add_wishlist['product_id'] = product_id;
            add_wishlist['user_id'] = userData['id'];
            add_wishlist['userToken'] = userData['token'];
            
            this.addWishlist(add_wishlist).subscribe(
                result => {
                    unblockUI();
                    
                    if(result['status_code'] === 1) {
                        
                        toast(result['message'],'success');
                          this.wishlist = result['data'];
                          this.wishlistArray = result['dataArray'];
                        
                    } else {
                        
                        toast( result['message'], 'error');
                    }
                }
               ); 
            }
        else{
           jQuery('#modal1').modal('open');
        }
    }      

    removeToWishlist(product_id){
   
           
            let userData = this.authDatas;
            let remove_product:any = {}
            remove_product['product_id'] = product_id;
            remove_product['user_id'] = userData['id'];
      
            blockUI();
           this.removeWishlist(remove_product).subscribe(result => {

            if (result['status_code'] === 1) {
                   this.getToWishlist(userData['id']);
                  this.wishlist = result['data'];
                        this.wishlistArray = result['dataArray'];
                unblockUI();
                toast(result['message'], 'success');

            } else {

          
                unblockUI();
                toast(result['message'], 'error');
            }


        });
            
       
    }
    getToWishlist(user_id){

            this.getWishlist(user_id).subscribe(
                    wishData => {
                        this.wishlist = wishData['data'];
                        this.wishlistArray = wishData['dataArray'];
                    }, err => {
                        console.log(err);
                    }
                );

    }

    payment(data: any): Observable<boolean> {
       
        return this.http.post('api/payment', { data: data})
            .map((response: Response) => {

                
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }    

    checkwalletBalance(id: string,data: any): Observable<boolean> {
       
        return this.http.post('api/wallet-balance/'+id, { data: data})
            .map((response: Response) => {

                
                if (response.json().status_code == 1) {
                    return response.json();
                } else {
                    return response.json();
                }
            });
    }

}
