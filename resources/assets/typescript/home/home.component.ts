import { Component, OnInit } from '@angular/core';
import { PageService } from "../service/page.service";
import { blockUI, unblockUI, toast } from '../global';

declare var jQuery: any;

@Component({
    selector: 'my-home',
    template: require('./home.html'),

})

export class HomeComponent {
    sliders: any = [];
    blocks: any = [];
    block_1: any = [];
    block_2: any = [];
    block_3: any = [];
    block_4: any = [];
    block_5: any = [];
    block_6: any = [];
    block_7: any = [];
    block_8: any = [];
    newslatter: any = {};
    constructor(
        private pageService: PageService

    ) { }

    ngOnInit() {

//        this.pageService.getSliders().subscribe(
//            sliders => {
//                this.sliders = sliders['data'];
//                setTimeout(() => {
//                    jQuery(document).ready(function() {
//                        jQuery('.slider').slider({ full_width: true });
//                    });
//                }, 2000);
//            }, err => {
//                console.log(err);
//            }
//        );
        
        jQuery(document).ready(function(){
            jQuery('.slider').slider({full_width: true});
        });
        
        jQuery(window).load(function () {
            jQuery('.flexslider').flexslider({
                animation: "slide",
                start: function (slider) {
                    jQuery('body').removeClass('loading');
                }
            });
        });
        
        this.pageService.getBlocks().subscribe(
            block => {
                this.blocks = block;
                this.block_1 = this.blocks['position-1'];
                this.block_2 = this.blocks['position-2'];
                this.block_3 = this.blocks['position-3'];
                this.block_4 = this.blocks['position-4'];
                this.block_5 = this.blocks['position-5'];
                this.block_6 = this.blocks['position-6'];
                this.block_7 = this.blocks['position-7'];
                this.block_8 = this.blocks['position-8'];

            }, err => {
                console.log(err);
            }
        );

    }
        newslattersaved(){


          blockUI();
        
        this.pageService.newslatterSubscription(this.newslatter)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                   
                    this.newslatter = {};
                    
                    
                    jQuery('#newsletter')[0].reset();
                    unblockUI();
                    
                    
                    toast(result['message'], 'success');
                } else {
                    
               
                    unblockUI();
                    toast(result['message'], 'error');
                }
            });
    }
}
