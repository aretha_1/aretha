import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { DomSanitizer } from '@angular/platform-browser';
import { blockUI, unblockUI, toast } from '../global';
declare var jQuery: any;

@Component({
    selector: 'reset-password',
    template: require('./resetpassword.html')
})

export class ResetPasswordComponent {
    resetmodel: any = {};
    token: string = '';
    loading = false;
    error = '';
    success = '';

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private sanitizer: DomSanitizer
    ) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            let token = params['token'];
            this.token = token;
            this.userService.checkResetToken(token).subscribe(
                tokenResponse => {
                    console.log(tokenResponse);
                    if(tokenResponse['status_code'] === 1){
                        return true;
                    }
                    else{
                        this.router.navigate(['/']);
                        toast(tokenResponse['message'], 'error');
                    }
                }, err => {
                    console.log(err);
                }
            );
        });
    }

    reset_password() {
        blockUI();
        this.loading = true;
        this.resetmodel.token = this.token;
        this.userService.submitResetPassword(this.resetmodel)
            .subscribe(result => {
                if (result['status_code'] === 1) {
                    this.success = result['message'];
                    this.resetmodel = '';
                    this.error = '';
                    unblockUI();
                    this.router.navigate(['/login']);
                    toast(result['message'], 'success');

                } else {
                    this.error = result['errors'];
                    toast(result['message'], 'error');
                    this.loading = false;
                    unblockUI();
                }
            });
    }

}
