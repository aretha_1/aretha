import { NgModule }             from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { PageComponent } from './page/page.component';
import { FaqComponent } from './faq/faq.component';
import { LoginComponent } from './login/login.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './productdetail/productdetail.component';
import { NotFoundComponent } from './404/notfound.component';
import { ContactUsComponent }  from './contactus/contactus.component';

import { ResetPasswordComponent }  from './resetpassword/resetpassword.component';
import { MyAccountComponent }  from './myaccount/myaccount.component';
import { MyProfileComponent }  from './myprofile/myprofile.component';
import { MyAddressComponent }  from './myaddress/myaddress.component';
import { MyOrderComponent }  from './myorder/myorder.component';
import { MyWalletComponent }  from './mywallet/mywallet.component';
import { ChangePasswordComponent }  from './changepassword/changepassword.component';
import { WishlistComponent }  from './wishlist/wishlist.component';
import { CartComponent }  from './cart/cart.component';
import { CheckoutComponent }  from './checkout/checkout.component';
import { CheckoutAddressComponent }  from './checkoutaddress/checkoutaddress.component';
import { OrderSummaryComponent }  from './ordersummary/ordersummary.component';
import { OrderDetailComponent }  from './orderdetail/orderdetail.component';
import { PaymentComponent }  from './payment/payment.component';
import { TahnkYouComponent }  from './thank_you/tahnk_you.component';
import { AuthGuard } from './common/auth.guard';
import { SearchComponent } from './search/search.component';
import { PageService } from './service/page.service';

const appRoutes: Routes = [
    { path: '',  component: HomeComponent },
    { path: 'page/:slug', component: PageComponent },
    { path: 'login',  component: LoginComponent },
    { path: 'faq',  component: FaqComponent },
    { path: 'thanks-you',  component: TahnkYouComponent },
    { path: 'category/:slug',  component: ProductComponent },
    { path: 'product/:slug',  component: ProductDetailComponent },
    { path: 'contact-us',  component: ContactUsComponent },
    { path: 'search/:search_text',  component: SearchComponent },
    { path: 'wishlist',  component: WishlistComponent, canActivate: [AuthGuard] },
    { path: 'reset-password/:token',  component: ResetPasswordComponent },
    { path: 'cart',  component: CartComponent, canActivate: [AuthGuard] },
    { path: 'myaccount',  component: MyAccountComponent, canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'profile', pathMatch: 'full' },
            { path: 'profile', component: MyProfileComponent },
            { path: 'address', component: MyAddressComponent },
            { path: 'order', component: MyOrderComponent },
            { path: 'order-detail/:order_id', component: OrderDetailComponent },
            { path: 'wallet', component: MyWalletComponent },
            { path: 'change-password', component: ChangePasswordComponent },
        ]
    },
    { path: 'checkout',  component: CheckoutComponent, canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'address', pathMatch: 'full' },
            { path: 'address', component: CheckoutAddressComponent },
            { path: 'summary', component: OrderSummaryComponent },
            { path: 'payment', component: PaymentComponent },
        ]
    },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        ), HttpModule, JsonpModule
    ],
    providers: [
        PageService
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}

