export class Category {
    constructor(
        public parent_id: number, 
        public slug: string,
        public name:string,
        public category_for:string
    ){}
}
