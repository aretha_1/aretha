export class SocialUser {
    constructor(
        public id: string,
        public name: string,
        public first_name: string,
        public last_name: string,
        public email: string
    ){}
}
