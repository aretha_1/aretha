import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductService } from "../service/product.service";
import { PagerService } from "../service/pager.service";
import { AuthenticationService } from '../service/authentication.service';
import { CartService } from "../service/cart.service";
import { blockUI, unblockUI, toast } from '../global';
import { Location } from '@angular/common';
declare var jQuery: any;

@Component({
    selector: 'wish-list',
    template: require('./wishlist.html'),
})

export class WishlistComponent implements OnInit {

    authData: any = [];
    wish_products: any = [];
    add_to_cart: any = {};
    total: number;
    per_page: number;
    current_page: number;
    page: number = 1;
    last_page: number;
    pager: any = {};
    success: '';
    error: '';

    show_size: boolean = false;
    size_error: boolean = false;
    select_size: string = '';
    show_product: string = '';
    repeat_size: string = '';

    remove_product: any = {};

    constructor(
        private productService: ProductService,
        private route: ActivatedRoute,
        private router: Router,
        private pagerService: PagerService,
        private location: Location,
        private authenticationService: AuthenticationService,
        private cartService: CartService
    ) { }

    ngOnInit() {

        this.authData = JSON.parse(localStorage.getItem('loginUser'));

        if (this.authData && this.authData.id) {

            this.getList();

        }

    }

    getList() {

        blockUI();

        this.productService.getWishlistProduct(this.authData.id, this.page).subscribe(
            result => {
                this.show_product ='';
                this.wish_products = result['data'];
                this.total = result['total'];
                this.per_page = result['per_page'];;


                this.pager = this.pagerService.getPager(this.total, this.page, this.per_page);
                if(this.page>1){
                     this.changeUrl();
                }

            }, err => {

                console.log(err);
            }
        );

        unblockUI();
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service

        this.page = page;
        // get current page of items
        this.getList();
    }
      
    removeToWishlist(product_id){
   
           this.cartService.removeToWishlist(product_id);
           this.getList();
          
            
       
    }



    showSize(product_id) {
        this.show_product = product_id;
    }

    setSize(size,product_id) {
         blockUI();

                this.add_to_cart['user_id'] = this.authData.id;
                this.add_to_cart['product_id'] = product_id;
                this.add_to_cart['product_size'] = size;

                this.cartService.addCart(this.add_to_cart)
                    .subscribe(result => {
                        if (result['status_code'] === 1) {
                            this.repeat_size = this.select_size;
                            this.success = result['message'];
                            this.remove_product['product_id'] = product_id;
                            this.remove_product['user_id'] = this.authData.id;
                            this.cartService.removeWishlist(this.remove_product).subscribe(result => {
                                 this.cartService.getToWishlist(this.authData.id);
                                 this.getList();

                                   
                            });
                            unblockUI();
                            toast(this.success, 'success');

                        } else {
                            this.error = result['message'];
                            unblockUI();
                            toast(this.error, 'error');
                        }
                    });
        
        
        
    }


    changeUrl() {

        let url = 'wishlist?page=' + this.page;

        this.location.go(url);
    }
}
