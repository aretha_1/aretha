import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from "../service/user.service";
import { Location } from '@angular/common';
declare var jQuery: any;
@Component({
    selector: 'my-account',
    template: require('./myaccount.html')
})

export class MyAccountComponent implements OnInit{
    authDatas: any = [];
    url_path: string = '';
    
    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private location: Location
    ){}
    
    ngOnInit() {
       
        this.authDatas = JSON.parse(localStorage.getItem('loginUser'));
              setTimeout(() => {
                     jQuery(document).ready(function() {
                         jQuery('.tabs').tabs();
                       });
                       
                    }, 100);
    }
    isActive(path) {
        return this.location.path().indexOf(path) > -1;
    }

 
}
