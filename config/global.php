<?php 
return [
	'status_list' => array("1"=>"Active","0"=>"Deactive"),
	'role_id' => array("admin"=>1,"user"=>2),
	'file_max_size' => 2*1024,// in kb
	'file_max_upload_size' => 2*1024*1024,// in byite //2MB
	'upload_file_size' => '2MB',// in byite //2MB
	'max_file_count_upload' => 5,
	'image_mime_type' => 'jpeg,gif,jpg,png',// image  extention
	'html_entity_decode_option'=>array('remove' => false,'charset' => 'UTF-8','quotes' => ENT_QUOTES,'double' => true),
	'category_type' => array("main_category"=>0,"sub_category"=>1),
	'gender' => array("male"=>'male',"female"=>'female'),
	'menu_type' => array("dynamic"=>0,"cms_page"=>1),
	'discount_type' => array("fixed"=>'Fixed',"percentage"=>"Percentage"),
	'menu_position' => array("1"=>'Header',"2"=>'Footer'),
	'currency_code' =>'INR',
	'currency_sign' =>'&#8377;',
	'currency_icon' =>'fa-inr',
	'coupon_code_lenth' =>6,
	'coupon_code_set' =>'ud',//luds
	'category_for' =>array('main'=>'main'),
	'category_for_index' =>array('main'=>0),
	'cms_page_position' =>array('1'=>'Under Fabivo','2'=>'Under why buy from us','3'=>'My account'),
	'product_sort_field' =>array('popular'=>'popular','new'=>'created_at','price_high'=>'price','price_low'=>'price'),
	'product_sort_order' =>array('popular'=>'desc','new'=>'desc','price_high'=>'desc','price_low'=>'asc'),
	"mail_driver"=>array('smtp'=>'smtp','mail'=>'mail'),
	"size_id"=>2,
	"discount_on"=>array("product"=>'Product',"total_price"=>'Total Price'),
	"max_product_quantity"=>array('1'=>'1','2'=>'2',"3"=>'3',"4"=>'4','5'=>'5'),
	"order_status"=>array('pending'=>'pending','failed'=>'failed',"reject"=>'reject','shipped'=>'shipped','cancel'=>'cancel','delivered'=>'delivered','success'=>'success','inprocess'=>'inprocess'),
	"payment_status"=>array('pending'=>'pending','failure'=>'failure',"success"=>'success'),
	"payment_by"=>array('payu'=>'payu','cod'=>'cod',"paytm"=>'paytm','wallet'),
	"wallet_flag"=>array('cr'=>'cr','dr'=>'dr'),
	"shipped_by"=>array('delhivery'=>'delhivery','postal'=>'postal'),
	"StatusType"=>array('UD'=>'Undelivered','DL'=>'Delivered','CN'=>'Canceled'),
	
];
